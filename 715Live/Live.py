from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)



        
        
path='/home/admin/Dropbox/SERIS_Live_Data/[KH-003S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=8,microsecond=500000)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=500000)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgGsi00','AvgTroom','AvgHamb','AvgHroom','AvgHcab','AvgTmod','AvgTamb','AvgTcab','Act_Pwr-Tot_SL','Act_Pwr-Tot_CK','Act_E-Del_SL','Act_E-Del_CK']
while(1):
    try:
        if(flag==1):
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            cur2=currtime+datetime.timedelta(hours = -1)
            currtime2=cur2.replace(tzinfo=None)
            cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
            print('Now adding for',currtime2)
        else:
            curr=datetime.datetime.now(tz)
            cur2=curr+datetime.timedelta(hours = -1)
            currtime2=cur2.replace(tzinfo=None)
            cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
            currtime=curr.replace(tzinfo=None)
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            print('Now adding for',currtime)
        flag2=1
        j=0
        while(j<5 and flag2!=0):
            j=j+1
            files=ftp.nlst()
            for i in files:
                if(re.search(cur,i)):
                    req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                    try:
                        with urllib.request.urlopen(req) as response:
                            s = response.read()
                        df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                        a=df.loc[df[1] == 715] 
                        if(flag==1):
                            b=a[[2,3,4,5,6,7,8,9,10,24,71,39,86]]
                            print("B is",b)   
                            flag=0
                        else:
                            b=a[[2,3,4,5,6,7,8,9,10,24,71,39,86]]
                            print("B is",b)  
                            if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-003S] '+cur3[0:10]+'.txt')):
                                df= pd.read_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-003S] '+cur3[0:10]+'.txt',sep='\t')
                                df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                                df2.columns = cols2
                            std1=df2['Act_E-Del_SL'].std()
                            std2=df2['Act_E-Del_CK'].std()
                            if(std1>13000):
                                b.loc[:, 39] = 'NaN' 
                            if(std2>13000):
                                b.loc[:, 86] = 'NaN'  
                        #Taking care of Power
                        b.loc[(b[3] < 0), 3] = 0
                        b.loc[(b[24] > 3000) | (b[24] < 0), 24] = 'NaN'
                        b.loc[(b[71] > 3000) | (b[71] < 0), 71] = 'NaN'
                        b.columns=cols2
                        chkdir(path+cur3[0:4]+'/'+cur3[0:7])
                        if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-003S] '+cur3[0:10]+'.txt')):
                            b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-003S] '+cur3[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                        else:
                            b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-003S] '+cur3[0:10]+'.txt',header=True,sep='\t',index=False)
                        flag2=0
                    except Exception as e:
                        flag2=0
                        print(e)
                        print("Failed for",currtime2)
            if(flag2!=0 and j<5):
                print('sleeping for 10 seconds!')            
                time.sleep(10)
        if(flag2==1):
            print('File not there!',cur)
        print('sleeping for a minute!')
    except:
        pass
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        