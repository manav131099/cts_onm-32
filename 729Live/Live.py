from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[MY-006S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=10,microsecond=0)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=0)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
noneflag=1
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgSMP10','AvgHamb','AvgGSi00','AvgTamb','AvgTmod','Act_Pwr-Tot_M1','Act_Pwr-Tot_M2','Act_Pwr-Tot_M3','Act_E-Del_M1','Act_E-Del_M2','Act_E-Del_M3']
while(1):
    try:
        if(flag==1):
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            prev=currtime+datetime.timedelta(days=-1)
            prev=str(prev)
        else:
            curr=datetime.datetime.now(tz)
            currtime=curr.replace(tzinfo=None)
            prev=currtime+datetime.timedelta(days=-1)
            prev=str(prev)
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
        flag2=1
        j=0
        while(j<5 and flag2!=0):
            j=j+1
            files=ftp.nlst()
            for i in files:
                if(re.search(cur,i)):
                    req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                    try:
                        with urllib.request.urlopen(req) as response:
                            s = response.read()
                        df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                        a=df.loc[df[1] == 729] 
                        if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[MY-006S] '+cur[0:10]+'.txt') and noneflag==0):
                            b=a[[2,3,4,5,6,7,21,68,115,36,83,130]] 
                            print(b)
                            df= pd.read_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[MY-006S] '+cur[0:10]+'.txt',sep='\t')
                            df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                            df2.columns = cols2
                            std1=df2['Act_E-Del_M1'].std()
                            std2=df2['Act_E-Del_M2'].std()
                            std3=df2['Act_E-Del_M3'].std()
                            if(std1>10000):
                                b.loc[:, 36] = 'NaN' 
                            if(std2>10000):
                                b.loc[:, 83] = 'NaN'  
                            if(std3>10000):
                                b.loc[:, 130] = 'NaN'
                            b.loc[b[36] == 0, 36] = 'NaN'
                            b.loc[b[83] == 0, 83] = 'NaN'
                            b.loc[b[130] == 0, 130] = 'NaN'
                        else:
                            print("In else")
                            noneflag=1
                            b=a[[2,3,4,5,6,7,21,68,115,36,83,130]] 
                            print(b,prev)
                            dfprev= pd.read_csv(path+prev[0:4]+'/'+prev[0:7]+'/'+'[MY-006S] '+prev[0:10]+'.txt',sep='\t')
                            dfprev2= pd.DataFrame( np.concatenate( (dfprev.values, b.values), axis=0 ) )
                            dfprev2.columns = cols2
                            print(dfprev2.tail())
                            std1=dfprev2['Act_E-Del_M1'].std()
                            std2=dfprev2['Act_E-Del_M2'].std()
                            std3=dfprev2['Act_E-Del_M3'].std()
                            if(std1>10000):
                                b.loc[:, 36] = 'NaN' 
                            if(std2>10000):
                                b.loc[:, 83] = 'NaN'  
                            if(std3>10000):
                                b.loc[:, 130] = 'NaN'  
                            b.loc[b[36] == 0, 36] = 'NaN'
                            b.loc[b[83] == 0, 83] = 'NaN'
                            b.loc[b[130] == 0, 130] = 'NaN' 
                            print(std1)
                            print(std2)
                            print(std3)
                            if(b[36].isnull().values.any() or b[83].isnull().values.any() or b[130].isnull().values.any() or std1>10000 or std2>10000 or std3>10000 or std1==np.nan or std2==np.nan or  std3==np.nan):
                                pass
                            else:
                                noneflag=0
                            flag=0
                        #Taking care of Power
                        b.loc[(b[5] < 0), 5] = 0
                        b.loc[(b[21] > 3000) | (b[21] < 0), 21] = 'NaN'
                        b.loc[(b[68] > 3000) | (b[68] < 0), 68] = 'NaN'
                        b.loc[(b[115] > 3000) | (b[115] < 0), 115] = 'NaN'
                        b.columns=cols2
                        chkdir(path+cur[0:4]+'/'+cur[0:7])
                        if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[MY-006S] '+cur[0:10]+'.txt')):
                            b.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[MY-006S] '+cur[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                        else:
                            b.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[MY-006S] '+cur[0:10]+'.txt',header=True,sep='\t',index=False)
                        flag2=0
                    except Exception as e:
                        flag2=0
                        print(e)
                        print("Failed for",currtime)
            if(flag2!=0 and j<5):
                print('sleeping for 10 seconds!')            
                time.sleep(10)
        if(flag2==1):
            print('File not there!',cur)
        print('sleeping for a minute!')
    except:
        pass
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        