import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import pymsteams
from twilio.rest import Client

path='/home/admin/Dropbox/Gen 1 Data/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
timezones={'IN':'Asia/Calcutta','VN':'Asia/Bangkok','KH':'Asia/Phnom_Penh','TH':'Asia/Bangkok','MY':'Asia/Kuala_Lumpur','SG':'Asia/Singapore'}

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[stations] ''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])

SQL_Query = pd.read_sql_query('SELECT TOP (1000) [O&M_Code],[Webhook] FROM [dbo].[Station_Limits]', connStr)
df_webhook= pd.DataFrame(SQL_Query, columns=['O&M_Code','Webhook'])

connStr.close()
dflifetime=dflifetime.loc[(dflifetime['Alarm_Status']==1),:]
temp_lifetime=(dflifetime.to_dict(orient='records'))


stations={}
for i in temp_lifetime:
    tz=pytz.timezone(timezones[i['Station_Name'].strip()[0:2]])
    stations['['+i['Station_Name'].strip()+'L]']=[i['Station_No_Meters']]
    temp=[]
    for index,j in enumerate(i['Station_Columns'].strip().split(',')):
        if(index>1 and index<(2+i['Station_No_Meters'])): #Skip date and include meters only
            temp2=(j.replace('"','').split('.')[:-1])
            temp.append(' '.join(temp2))
    stations['['+i['Station_Name'].strip()+'L]'].append(temp)
    stations['['+i['Station_Name'].strip()+'L]'].append([0]*(i['Station_No_Meters']))
    stations['['+i['Station_Name'].strip()+'L]'].append([datetime.datetime.now(tz)]*(i['Station_No_Meters']))

def get_text_recipients(stn):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Text_Recipients] ''', connStr)
    df_recipients = pd.DataFrame(SQL_Query, columns=['Name','Number','Site','Country','Status'])
    df_temp = df_recipients.loc[df_recipients['Country'].str.strip()==stn[0:2],]
    connStr.close()
    df_temp = df_temp.loc[(((df_temp['Site'].str.strip()=='All') | (df_temp['Site'].str.strip()==stn)) & (df_temp['Status']==1)),'Number']
    df_temp = df_temp.unique()
    numbers= df_temp.tolist()
    return numbers

    
def sendmail(station,type,data,rec,smsrec,sms_flag):
    if(station[1:3]!='IN'):
        SERVER = "smtp.office365.com"
        FROM = 'operations@cleantechsolar.com'
        recipients = rec# must be a list
        cleantech_rec=[]
        for i in recipients:
            if(i.split('@')[1]=='cleantechsolar.com' or i.split('@')[1]=='comin.com.kh'):
                cleantech_rec.append(i)
        TO=", ".join(cleantech_rec)
        SUBJECT = station+' Network Issue/Power Trip Alarm'
        text2='Last Timestamp Read: '+str(data[0]) +'\n\n Last Power Reading: '+str(data[2])+'\n\n Meter Name: '+str(data[1])
        TEXT = text2
        # Prepare actual message
        message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
        # Send the mail
        import smtplib
        server = smtplib.SMTP(SERVER)
        server.starttls()
        server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
        server.sendmail(FROM, recipients, message)
        server.quit()

def teams_alert(station,data):
    try:
        webhook=df_webhook.loc[df_webhook['O&M_Code'].str.strip()==station[1:-2],'Webhook'].values[0].strip()
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title(station[1:-2]+" Network Issue/Power Trip Alarm")
        myTeamsMessage.text("<pre>Last Timestamp Read: "+str(data[0])+"<br>Last Power Reading: "+str(round(data[2],1))+"<br>Meter Name: "+str(data[1])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

starttime=time.time()
flag=0
while(1):
    tz=pytz.timezone('Asia/Calcutta')
    date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')
    if(datetime.datetime.now(tz).hour>8 and datetime.datetime.now(tz).hour<17):
        print(datetime.datetime.now(tz).strftime("%Y-%m-%d"))
        if(os.path.exists(path+'[IN-301L]/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_MCR TVM 1 TRAFO/[IN-301L]-MFM2-'+date_now[0:10]+'.txt')):
            df=pd.read_csv(path+'[IN-301L]/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_MCR TVM 1 TRAFO/[IN-301L]-MFM2-'+date_now[0:10]+'.txt',sep='\t')
            #Power Alarm
            df_power=df[['W_avg']]
            pow1=df_power.tail(20)
            check_dup = pow1.drop_duplicates()
            date=df.tail(1)['ts'].values
            time_check=datetime.datetime.strptime(date[0],"%Y-%m-%d %H:%M:%S")
            if(((check_dup.empty) and flag==0) and time_check.hour>7):
                recipients=['sai.pranav@cleantechsolar.com']
                print('Sending')
                smsrec=get_text_recipients(i[1:-2])
                sendmail(i,'pow',[date[0],k,last_pow],recipients,smsrec,1)
                stations[i][3][index]=datetime.datetime.now(tz)
                flag=1
            elif(((check_dup.empty) and flag==1) and time_check.hour>7):
                diff=datetime.datetime.now(tz)-stations[i][3][index]
                if((diff.seconds)//60>120):
                    print('Second time')
                    stations[i][3][index]=datetime.datetime.now(tz)
                    recipients=['sai.pranav@cleantechsolar.com']
                    smsrec=get_text_recipients(i[1:-2])
                    sendmail(i,'pow',[date[0],k,last_pow],recipients,smsrec,0)
            else:
                flag==0
    time.sleep(900.0 - ((time.time() - starttime) % 900.0))