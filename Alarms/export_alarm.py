import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pymsteams

tz = pytz.timezone('Asia/Kolkata')
path='/home/admin/Dropbox/Second Gen/[SG-003E]/Preliminary-Settlements/'

def teams_alert(data):
    try:
        webhook='https://cleantechenergycorp.webhook.office.com/webhookb2/0ffc7c20-3de8-44f4-aae2-a0f49becf523@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/d6d33b435d8943f999103f14f874476b/83a767a3-8685-45e8-8c0f-249346c7961b'
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title("SG-003 Export Alert")
        myTeamsMessage.text(data)
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

def sendmail(station,date,export,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec # must be a list
    TO=", ".join(recipients)
    SUBJECT = 'Electricity Export Alert - '+station
    text2='Date: '+str(date) +'\n\nSite: '+str(station) +'\n\nElectricity Exported: '+str(export)+' kWh'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    teams_alert(text2)

date = str(datetime.datetime.now(tz).date())
old_files = []
for m in sorted(os.listdir(path+date[0:4])):
    temp_files =  [f for f in os.listdir(path+date[0:4]+'/'+m) if os.path.isfile(os.path.join(path+date[0:4]+'/'+m, f))]
    old_files= old_files+temp_files
while(1):
    date = str(datetime.datetime.now(tz).date())
    new_files = []
    for m in sorted(os.listdir(path+date[0:4])):
        temp_files =  [f for f in os.listdir(path+date[0:4]+'/'+m) if os.path.isfile(os.path.join(path+date[0:4]+'/'+m, f))]
        new_files= new_files+temp_files
    print(new_files[-1],old_files[-1])
    print(list(set(new_files) - set(old_files)))
    if(len(list(set(new_files) - set(old_files)))>0):
        files_read = list(set(new_files) - set(old_files))
        print(files_read)
        for i in files_read:
            file_date = i.split(' ')[1][:-4]
            print(file_date) 
            df = pd.read_csv(path + file_date[0:4]+'/'+file_date[0:7]+'/'+i,sep='\t')
            export = df['Export Volume (kWh)'].values[0]
            if(export>0):
                sendmail('SG-003',file_date,export,['operationssg@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
        old_files = new_files
    print('sleeping')
    time.sleep(3600)
    
