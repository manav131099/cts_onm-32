import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
import pyodbc
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)

def azure_push(date,value,parameterid,componentid,siteid,bypassflag):
        cursor = connStr.cursor()
        with cursor.execute("UPDATE [Test].[ProcessedData] SET [Value] = {} where [Date] = '{}' AND [ComponentId]={} AND [ParameterId]={} if @@ROWCOUNT = 0 INSERT into [Test].[ProcessedData](Date, Value, ParameterId, ComponentId, SiteId, BypassFlag) values('{}', {}, {}, {}, {}, {}) ".format(str(value),str(date),str(componentid),str(parameterid), str(date),str(value), str(parameterid) ,str(componentid) ,str(siteid) ,str(bypassflag))):
            pass
        connStr.commit()    

#Historical Start Dates can be changed from here
sites_mapped = {'IN-013W':['IN-013W', ['MFM_1_Meter - MLCP 1'], '2020-10-01', ['Date','Frequency','Total Power AC'], ['Date','Irradiance_Avg'], 45, 3564],
                'IN-015L':['712', ['MFM_1_HT Meter'], '2021-02-01', ['ts','Hz_avg','W_avg'], ['Tm','AvgGsi00'], 87, 3527],
                'IN-017A':['IN-009W', ['MFM_1'], '2020-09-01', ['Date','Frequency (Hz)','AC Power (kW)'], ['Date','Irradiation_GHI'], 46, 3569],
                'IN-032W':['711', ['MFM_1'], '2020-09-01', ['Date','Frequency','Total Power AC'], ['Tm','AvgGsi00-02'], 57, 3572],
                'IN-040R':['IN-040R', ['MFM_1'], '2021-07-19', ['time','ac-freq','ac-power'], ['time','irradiance'], 64, 3573]}

#Historical
for site in sites_mapped:
  print(site)
  for meter in sites_mapped[site][1]:
    start_date=datetime.datetime.strptime(sites_mapped[site][2],"%Y-%m-%d").date()
    date_today=datetime.datetime.now().date()
    while(start_date<=date_today):
        date = str(start_date)
        mfm_file="/home/admin/Dropbox/Gen 1 Data/["+site+"]/"+date[0:4]+'/'+date[0:7]+'/'+meter+'/['+site+']-MFM'+meter.split('_')[1]+'-'+date+'.txt'
        if site=='IN-015L':
          wms_file='/home/admin/Dropbox/SerisData/1min/[712]/' + date[:4] + '/' + date[:7] + '/[712] ' + date + '.txt'
        elif site=='IN-032W':
          wms_file='/home/admin/Dropbox/Cleantechsolar/1min/[711]/' + date[:4] + '/' + date[:7] + '/[711] ' + date + '.txt'
        else:
          wms_file='/home/admin/Dropbox/Gen 1 Data/['+sites_mapped[site][0]+']/'+date[0:4]+'/'+date[0:7]+'/WMS_1/['+sites_mapped[site][0]+']-WMS1-'+date+'.txt'
        if(os.path.exists(mfm_file)):
          mfm_df=pd.read_csv(mfm_file,sep='\t')
          mfm_df[sites_mapped[site][3][0]]=pd.to_datetime(mfm_df[sites_mapped[site][3][0]])
          if site=='IN-015L':
            mfm_df[sites_mapped[site][3][2]]=mfm_df[sites_mapped[site][3][2]]*-1
          mask = (mfm_df[sites_mapped[site][3][0]].dt.hour > 6) & (mfm_df[sites_mapped[site][3][0]].dt.hour < 18)
          mfm_df_sh=mfm_df[mask]
          freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
          freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
          if(len(freq_timestamps)>0 and len(freq_timestamps_sh)>0):
              if(os.path.exists(wms_file)):
                wms_df=pd.read_csv(wms_file,sep='\t')
                col = sites_mapped[site][4][1]
                if(len(wms_df)>0 and col in wms_df.columns.tolist()):
                  irr_df=wms_df[[sites_mapped[site][4][0],col]]
                  irr_df.columns = [sites_mapped[site][3][0], col]
                  irr_df[sites_mapped[site][3][0]]=pd.to_datetime(irr_df[sites_mapped[site][3][0]])
                  if site=='IN-015L' or 'IN-032W':
                    irr_df=irr_df.resample('5Min', on=sites_mapped[site][3][0]).mean()
                    irr_df.reset_index(level=0, inplace=True)
                  irr_df=irr_df[irr_df[col].notnull()]
                  mfm_df=mfm_df[sites_mapped[site][3]]
                  irr_timestamps=irr_df.loc[irr_df[col]>20,sites_mapped[site][3][0]]
                  freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                  power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]                        
                  ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                  pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                  if((len(irr_timestamps)>0) and (len(ga_common)>0)):
                      GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                      PA=(float(len(pa_common))/float(len(ga_common)))*100
                      azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                      azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
                  else:
                      mfm_df=mfm_df[mask]
                      freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                      power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                      pa_common=list(set(freq_timestamps) & set(power_timestamps))
                      GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                      PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                      azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                      azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
              else:
                mfm_df=mfm_df[mask]
                freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                pa_common=list(set(freq_timestamps) & set(power_timestamps))
                GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
          else:
            azure_push(date,"NULL",13,sites_mapped[site][6],sites_mapped[site][5],0)
            azure_push(date,"NULL",14,sites_mapped[site][6],sites_mapped[site][5],0)
        else:
          azure_push(date,"NULL",13,sites_mapped[site][6],sites_mapped[site][5],0)
          azure_push(date,"NULL",14,sites_mapped[site][6],sites_mapped[site][5],0)
        start_date=start_date+datetime.timedelta(days=1)

#live
while(1):
  print("Going Live...")
  try:
    connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)
    for site in sites_mapped:
      print(site)
      for meter in sites_mapped[site][1]:
        date = str(datetime.datetime.now().date())
        mfm_file="/home/admin/Dropbox/Gen 1 Data/["+site+"]/"+date[0:4]+'/'+date[0:7]+'/'+meter+'/['+site+']-MFM'+meter.split('_')[1]+'-'+date+'.txt'
        if site=='IN-015L':
          wms_file='/home/admin/Dropbox/SerisData/1min/[712]/' + date[:4] + '/' + date[:7] + '/[712] ' + date + '.txt'
        elif site=='IN-032W':
          wms_file='/home/admin/Dropbox/Cleantechsolar/1min/[711]/' + date[:4] + '/' + date[:7] + '/[711] ' + date + '.txt'
        else:
          wms_file='/home/admin/Dropbox/Gen 1 Data/['+sites_mapped[site][0]+']/'+date[0:4]+'/'+date[0:7]+'/WMS_1/['+sites_mapped[site][0]+']-WMS1-'+date+'.txt'
        if(os.path.exists(mfm_file)):
          mfm_df=pd.read_csv(mfm_file,sep='\t')
          mfm_df[sites_mapped[site][3][0]]=pd.to_datetime(mfm_df[sites_mapped[site][3][0]])
          if site=='IN-015L':
            mfm_df[sites_mapped[site][3][2]]=mfm_df[sites_mapped[site][3][2]]*-1
          mask = (mfm_df[sites_mapped[site][3][0]].dt.hour > 6) & (mfm_df[sites_mapped[site][3][0]].dt.hour < 18)
          mfm_df_sh=mfm_df[mask]
          freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
          freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
          if(len(freq_timestamps)>0 and len(freq_timestamps_sh)>0):
              if(os.path.exists(wms_file)):
                wms_df=pd.read_csv(wms_file,sep='\t')
                col = sites_mapped[site][4][1]
                if(len(wms_df)>0 and col in wms_df.columns.tolist()):
                  irr_df=wms_df[[sites_mapped[site][4][0],col]]
                  irr_df.columns = [sites_mapped[site][3][0], col]
                  irr_df[sites_mapped[site][3][0]]=pd.to_datetime(irr_df[sites_mapped[site][3][0]])
                  if site=='IN-015L' or 'IN-032W':
                    irr_df=irr_df.resample('5Min', on=sites_mapped[site][3][0]).mean()
                    irr_df.reset_index(level=0, inplace=True)
                  irr_df=irr_df[irr_df[col].notnull()]
                  mfm_df=mfm_df[sites_mapped[site][3]]
                  irr_timestamps=irr_df.loc[irr_df[col]>20,sites_mapped[site][3][0]]
                  freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                  power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]                        
                  ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                  pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                  if((len(irr_timestamps)>0) and (len(ga_common)>0)):
                      GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                      PA=(float(len(pa_common))/float(len(ga_common)))*100
                      azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                      azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
                  else:
                      mfm_df=mfm_df[mask]
                      freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                      power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                      pa_common=list(set(freq_timestamps) & set(power_timestamps))
                      GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                      PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                      azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                      azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
              else:
                mfm_df=mfm_df[mask]
                freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                pa_common=list(set(freq_timestamps) & set(power_timestamps))
                GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                azure_push(date,GA,13,sites_mapped[site][6],sites_mapped[site][5],0)
                azure_push(date,PA,14,sites_mapped[site][6],sites_mapped[site][5],0)
          else:
            azure_push(date,"NULL",13,sites_mapped[site][6],sites_mapped[site][5],0)
            azure_push(date,"NULL",14,sites_mapped[site][6],sites_mapped[site][5],0)
        else:
          azure_push(date,"NULL",13,sites_mapped[site][6],sites_mapped[site][5],0)
          azure_push(date,"NULL",14,sites_mapped[site][6],sites_mapped[site][5],0) 
  except Exception as e:
    print(e)
    print('Failed')
  print('Sleeping')
  time.sleep(300)