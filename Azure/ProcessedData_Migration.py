import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import logging
import pyodbc
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

df = pd.read_excel("/home/manav/ctsfork/Azure/Migration Auditing.xlsx")
sites = list(df['O&M'])

def send_mail(stn, body, recipients):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Alert " + str(stn)
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  msg.attach(MIMEText(body,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)

#Fast Upsert Function
def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = connStr.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    connStr.commit()

#Process Start  
for site in sites:
  
  sourcesiteid = pd.read_sql_query("SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] = '" + str(site) + "'", connStr)
  sourcesiteid =  sourcesiteid['Station_Id'].values[0]

  sourcemeterids = pd.read_sql_query("SELECT [Meter_id] FROM [dbo].[Meters] WHERE [Station_Id]=(SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] = '" + site + "')", connStr)
  sourcemeterids = sourcemeterids['Meter_id'].values
  
  destsiteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode] = '" + str(site) + "'", connStr)
  destsiteid =  destsiteid['SiteId'].values[0]
  
  destmeterids = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode = '" + site + "') AND ComponentType='MFM'", connStr) 
  destmeterids = destmeterids['ComponentId'].values
  
  fullsiteprdestmeter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId=69 and SiteId = " + str(destsiteid), connStr)
  fullsiteprdestmeter = fullsiteprdestmeter['ComponentId'].values[0]
    
  print("Site:", site)
  print("Source_Site_ID:", sourcesiteid)
  print("Source_Component_IDs:", sourcemeterids)
  print("Dest_Site_ID:", destsiteid)
  print("Dest_Component_IDs:", destmeterids)
  print("Full_PR_Dest_Component_ID:", fullsiteprdestmeter)
  
  
  sourcemeterrefs = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id] = " + str(sourcesiteid), connStr)
  destmeterrefs = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId] = " + str(destsiteid), connStr)
  sourcedestmetermapping = pd.merge(sourcemeterrefs, destmeterrefs, how='left', left_on=['Reference'], right_on=['MeterReference'])
  metermapping = dict(zip(sourcedestmetermapping.Meter_Id, sourcedestmetermapping.ComponentId))
  print("\nSource Meters and References:\n", sourcemeterrefs)
  print("\nDestination Meters and References:\n", destmeterrefs)
  print("\nSource-Destination Meters Mapping:\n", sourcedestmetermapping)
  
  #Fetching processed data from Stations_Data table
  stations_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [PR], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid), connStr)
  stations_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'PR', 'BypassFlag']
  stations_data['Date'] = stations_data['Date'].dt.date
  print("\nStations Data Table:\n", stations_data)
  
  #Mapping ParameterName to ParameterID
  parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7, 'PR':69}
  
  #Preparing processed data for export to portfolio table
  export = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']) 
  for i in range(len(stations_data)):
    for j in parameter_mapping:
      export = export.append({'Date'        : stations_data['Date'][i],
                              'Value'       : stations_data[j][i],
                              'ParameterId' : parameter_mapping[j],
                              'ComponentId' : int(metermapping[stations_data['Meter_Id'][i]]),
                              'SiteId'      : int(destsiteid),
                              'BypassFlag'  : stations_data['BypassFlag'][i]}, ignore_index = True)
  
  print("\nPortfolio Table Ready for Export:\n", export)

  #Upserting processed Data to the portfolio table
  fast_upsert_processed(site, export)
  send_mail(str(site), 'Processed Data Push Complete', ['manav.gupta@cleantechsolar.com'])
  
  
  ####FIXING FULL SITE PR NOW#######
  #Fetching PR data and their meter information from stations data table
  stations_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid), connStr)
  stations_data['Date'] = stations_data['Date'].dt.date
  dates = stations_data['Date'].unique()
    
  print("\nStations Data PR-Meter Table:\n", stations_data)
  
  #Calculating full site PR and preparing export table
  pr_fix_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
  for date in dates:
    temp = stations_data[stations_data['Date']==date]
    temp['Weight'] = temp['Capacity']*temp['PR']
    temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
    pr_fix_df = pr_fix_df.append({'Date' : date,
                          'Value' : temp_full_pr,
                          'ParameterId' : 69,
                          'ComponentId' : int(fullsiteprdestmeter),
                          'SiteId' : int(destsiteid),
                          'BypassFlag' : 0},
                          ignore_index = True)
  
  print("\nFull Site PR Fixing Table Ready for Export:\n", pr_fix_df)
  
  #Upserting Full Site PR Fixing Table to the portfolio table
  fast_upsert_processed(site, pr_fix_df)
  
  #Deleting extra records
  cursor = connStr.cursor()
  cursor.execute("DELETE FROM [Portfolio].[ProcessedData] WHERE SiteId=" + str(destsiteid) + " and ParameterId=69 AND ComponentId!=" + str(fullsiteprdestmeter))
  connStr.commit()
  
  send_mail(str(site), 'Full Site PR Fix Complete', ['manav.gupta@cleantechsolar.com'])
  
#Disconnecting Server
connStr.close()