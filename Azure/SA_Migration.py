import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import logging
import pyodbc
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib


df = pd.read_excel("/home/manav/ctsfork/Azure/SA Migration.xlsx")
sites = list(df['O&M'])

def send_mail(stn, body, recipients):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Alert " + str(stn)
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  msg.attach(MIMEText(body,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)

#Fast Upsert Function
def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = connStr.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    connStr.commit()

#Process Start  
for site in sites:
  
  sourcesiteid = pd.read_sql_query("SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] = '" + str(site) + "'", connStr)
  sourcesiteid =  sourcesiteid['Station_Id'].values[0]

  sourcemeterids = pd.read_sql_query("SELECT [Meter_id] FROM [dbo].[Meters] WHERE [Station_Id]=(SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] = '" + site + "')", connStr)
  sourcemeterids = sourcemeterids['Meter_id'].values
  
  destsiteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode] = '" + str(site) + "'", connStr)
  destsiteid =  destsiteid['SiteId'].values[0]
  
  destmeterids = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode = '" + site + "') AND ComponentType='MFM'", connStr) 
  destmeterids = destmeterids['ComponentId'].values
  
  print("Site:", site)
  print("Source_Site_ID:", sourcesiteid)
  print("Source_Component_IDs:", sourcemeterids)
  print("Dest_Site_ID:", destsiteid)
  print("Dest_Component_IDs:", destmeterids)
  
  sourcemeterrefs = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id] = " + str(sourcesiteid), connStr)
  destmeterrefs = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId] = " + str(destsiteid), connStr)
  sourcedestmetermapping = pd.merge(sourcemeterrefs, destmeterrefs, how='left', left_on=['Reference'], right_on=['MeterReference'])
  metermapping = dict(zip(sourcedestmetermapping.Meter_Id, sourcedestmetermapping.ComponentId))
  print("\nSource-Destination Meters Mapping:\n", sourcedestmetermapping)
  
  #Fetching processed data from Stations_Data table
  sa_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [GA], [PA], [DA], [Master-Flag] FROM System_Uptime where Station_Id=" + str(sourcesiteid), connStr)
  sa_data.columns = ['Date', 'Meter_Id', 'Grid Availability', 'Plant Availability', 'Data Availability', 'BypassFlag']
  sa_data['Date'] = sa_data['Date'].dt.date
  print("\nStations Data Table:\n", sa_data)
  
  #Mapping ParameterName to ParameterID
  parameter_mapping = {'Grid Availability':13, 'Plant Availability':14, 'Data Availability':1}
  
  #Preparing processed data for export to portfolio table
  export = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']) 
  for i in range(len(sa_data)):
    for j in parameter_mapping:
      export = export.append({'Date'        : sa_data['Date'][i],
                              'Value'       : sa_data[j][i],
                              'ParameterId' : parameter_mapping[j],
                              'ComponentId' : int(metermapping[sa_data['Meter_Id'][i]]),
                              'SiteId'      : int(destsiteid),
                              'BypassFlag'  : sa_data['BypassFlag'][i]}, ignore_index = True)
  
  export['Value'] = export['Value'].where(pd.notnull(export['Value']), None)
  print("\nPortfolio Table Ready for Export:\n", export)
    
  #Upserting processed Data to the portfolio table
  #fast_upsert_processed(site, export)
  #send_mail(str(site), 'Processed Data Push Complete', ['manav.gupta@cleantechsolar.com'])
  
#Disconnecting Server
connStr.close()