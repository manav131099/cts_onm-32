import requests, json
import requests.auth
import pandas as pd
import datetime 
import os
import re 
import time
import pyodbc
import pytz
import sys
import numpy as np

stns = ['TH-051','TH-052']

path = '/home/admin/Dropbox/Second Gen/'
tz = pytz.timezone('Asia/Kuala_Lumpur')
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver = '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

def find_first_date(stn):
    for i in sorted(os.listdir(path+'['+stn+'L]/')):
        for j in sorted(os.listdir(path+'['+stn+'L]/'+i)):
            for k in sorted(os.listdir(path+'['+stn+'L]/'+i+'/'+j)):
                for l in sorted(os.listdir(path+'['+stn+'L]/'+i+'/'+j+'/'+k)):
                    date=l[-14:-4]
                    break
                break
            break
        break
    return date
    
def azure_push(push_date, push_meterid, push_gc, push_sp):
  cursor = connStr.cursor()
  with cursor.execute("UPDATE [dbo].[Solar_Percentage] SET [SolarPercentage] = {}, [GridConsumption] = {} where [Date] = '{}' AND [Meter_Id]={} if @@ROWCOUNT = 0 INSERT into [dbo].[Solar_Percentage](Date, Meter_Id, SolarPercentage, GridConsumption) values('{}', {}, {}, {}) ".format(str(push_sp), str(push_gc), str(push_date), str(push_meterid), str(push_date), str(push_meterid), str(push_sp), str(push_gc))):
    pass
  connStr.commit()

#Hisorical
for stn in stns:
  SQL_Query = pd.read_sql_query('''SELECT [Station_Id], [Station_Name], [Station_Columns], [Station_No_Meters] FROM [dbo].[Stations] where [Station_Name]=''' + "'" + stn + "'", connStr)
  df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','Station_Columns','Station_No_Meters'])
  SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Meters] where [Station_Id] = ''' + str(df_stations['Station_Id'].values[0]), connStr)
  df_meters = pd.DataFrame(SQL_Query, columns=['Meter_id','Station_Id','Country_Id'])
  
  meter_ids = df_meters['Meter_id'].tolist()
  no = df_stations['Station_No_Meters'].values[0]
  cols = df_stations['Station_Columns'].values[0]
  meter_name = []
  for index, j in enumerate(cols.strip().split(',')):
    if(index>1 and index<(2+no)):
      meter_name2=(j.replace('"','').strip().split('.')[:-1])
      meter_name.append(' '.join(meter_name2))
  
  print(stn)    
  print(meter_name)
  
  for no in range(len(meter_name)):
    start_date = datetime.datetime.strptime(find_first_date(stn),"%Y-%m-%d").date()
    date_today = datetime.datetime.now(tz).date()
    while(start_date <= date_today):
      date = str(start_date)
      path_read = path + '['+stn+'L]/' + date[0:4] + '/' + date[0:7] + '/' + meter_name[no] + '/['+stn+'L]-MFM' + meter_name[no].split('_')[1] + '-' + date + '.txt'
      push_date = date
      push_meterid = 'NULL'
      push_gc = 'NULL'
      push_sp = 'NULL'
      if os.path.exists(path_read):
        mfm_df = pd.read_csv(path_read, sep='\t')
        push_date = mfm_df['Date'].values[0]
        push_meterid = meter_ids[no]
        push_gc = mfm_df['Grid Consumption'].values[0]
        push_sp = mfm_df['Solar Percenatge'].values[0]
        if pd.isna(push_gc):
          push_gc = 'NULL'
        if pd.isna(push_sp):
          push_sp = 'NULL'
      azure_push(push_date, push_meterid, push_gc, push_sp)
      start_date = start_date + datetime.timedelta(days=1) 
  
#Live
while(1):
  for stn in stns:
    SQL_Query = pd.read_sql_query('''SELECT [Station_Id], [Station_Name], [Station_Columns], [Station_No_Meters] FROM [dbo].[Stations] where [Station_Name]=''' + "'" + stn + "'", connStr)
    df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','Station_Columns','Station_No_Meters'])
    SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Meters] where [Station_Id] = ''' + str(df_stations['Station_Id'].values[0]), connStr)
    df_meters = pd.DataFrame(SQL_Query, columns=['Meter_id','Station_Id','Country_Id'])
    
    meter_ids = df_meters['Meter_id'].tolist()
    no = df_stations['Station_No_Meters'].values[0]
    cols = df_stations['Station_Columns'].values[0]
    meter_name = []
    for index, j in enumerate(cols.strip().split(',')):
      if(index>1 and index<(2+no)):
        meter_name2=(j.replace('"','').strip().split('.')[:-1])
        meter_name.append(' '.join(meter_name2))
    
    print(stn)    
    print(meter_name)
    
    for no in range(len(meter_name)):
      date_today = datetime.datetime.now(tz).date()
      date = str(date_today)
      path_read = path + '['+stn+'L]/' + date[0:4] + '/' + date[0:7] + '/' + meter_name[no] + '/['+stn+'L]-MFM' + meter_name[no].split('_')[1] + '-' + date + '.txt'
      push_date = date
      push_meterid = 'NULL'
      push_gc = 'NULL'
      push_sp = 'NULL'
      if os.path.exists(path_read):
        mfm_df = pd.read_csv(path_read, sep='\t')
        push_date = mfm_df['Date'].values[0]
        push_meterid = meter_ids[no]
        push_gc = mfm_df['Grid Consumption'].values[0]
        push_sp = mfm_df['Solar Percenatge'].values[0]
        if pd.isna(push_gc):
          push_gc = 'NULL'
        if pd.isna(push_sp):
          push_sp = 'NULL'
      azure_push(push_date, push_meterid, push_gc, push_sp)
  time.sleep(300)