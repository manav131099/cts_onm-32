import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

##global start_tstamp
##global start_tstamp_update

tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()

##read_path = '' ###local
read_path = '/home/admin/Dropbox/Third Gen/[IN-021T]/[IN-021T] lifetime.txt' ###server


#Function to check for new day
def determine_date():
    global last_date    
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date()
    if today_date != last_date:
        last_date = today_date
        print('New day, Deleting old records')
        delete_records(last_date)

#Function delete old records
def delete_records(last_date):
    last_date = last_date + datetime.timedelta(days=-35)
    last_date = last_date.strftime("%Y-%m-%d %H:%M:%S")

    while True:
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        try:
            cursor = cnxn.cursor()
            with cursor.execute("DELETE FROM [Cleantech Meter Readings].dbo.[IN-021T-ALL] WHERE [Tstamp]<?",last_date):
                print('Detetion successful - All records deleted before '+last_date)
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
            
        except Exception as e:
                err_num = e[0]
                print ('ERROR:', err_num,e[1])
                cursor.close()
                cnxn.close()
                if err_num == '08S01':
                    counter = counter + 1
                    print ('Sleeping for 5')
                    time.sleep(300)
                    if counter > 10:
                        exit('Persistent connection error')                    
                    print ('%%%Re-trying connection%%%')             
                else:
                    break
        
    

#Function to read start timestamp
def read_timestamp():
    global start_tstamp
##    file = open('F:/Flexi_final/[IN-021T]/IN021TAA.txt','r') ###local
    file = open('/home/admin/Start/IN021TAA.txt','r') ###server
    time_read = file.read(19)
    start_tstamp = datetime.datetime.strptime(time_read, "%Y-%m-%d %H:%M:%S")

#Function to update start file 
def update_start_file(curr_tstamp):
##    file = open('F:/Flexi_final/[IN-021T]/IN021TAA.txt','w') ###local
    file = open('/home/admin/Start/IN021TAA.txt','w') ###server
    file.write(str(curr_tstamp))
    file.close()

def insert(fields,curr_tstamp):
 
    counter = 0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            update_start_file(fields[7]) ##Check if you want to put it at the start of try
            global start_tstamp_update
            start_tstamp_update = curr_tstamp
            
            with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[IN-021t-ALL] ([Date],[DA],[EacM1],[EacM2],[Yield1],[Yield2],[EacLast],[TmLast],[IrrTot],[TmodMean],[PR1],[PR2],[StnId]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",fields): 
                print('Successfuly Inserted!' + fields[7])                
            
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
        
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num,e[1], fields[7])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                continue




def convert_data_type(fields):
    for i in range(len(fields)):
        if i == 0 or i == 7 or i == 12:
            continue
        if fields[i] == 'NA' or fields[i] == 'NA\n' :
            fields[i] = None
        else:
            fields[i] = float(fields[i])

    return fields
        





read_timestamp()
start_tstamp_update = start_tstamp
while True:

    determine_date()
       
    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found -',read_path)
            time.sleep(600)
            continue

    start_tstamp = start_tstamp_update
    with open(read_path, 'r') as read_file:
        first = 1
        for line in read_file:
            if first == 1:
                first = 0
                continue
            fields =[]
            fields = line.split("\t")
            if fields[7] == "NA":
                continue
            fields[0] = fields[0].replace('"', '').strip()
            fields[7] = fields[7].replace('"', '').strip()
            fields[12] = fields[12].replace('"', '').strip()
    
            curr_tstamp = datetime.datetime.strptime(fields[7], "%Y-%m-%d %H:%M:%S")
            if curr_tstamp > start_tstamp:
                fields = convert_data_type(fields)
                insert(fields,curr_tstamp)
                
    time.sleep(3600)
