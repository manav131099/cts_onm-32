import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

##global start_tstamp
##global start_tstamp_update

tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()

str_val = [0,5,12,13]

##read_path = 'F:/del/FlexiMC_Data/Azure_Fourth_Gen/[MY-001X]/[MY-001X]-lifetime.txt' ###local
read_path = '/home/admin/Dropbox/Fourth_Gen/[MY-001X]/[MY-001X]-lifetime.txt' ###server


#Function to check for new day
def determine_date():
    global last_date    
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date()
    if today_date != last_date:
        last_date = today_date
##        print('New day, Deleting old records')
##        delete_records(last_date)

#Function delete old records
def delete_records(last_date):
    last_date = last_date + datetime.timedelta(days=-35)
    last_date = last_date.strftime("%Y-%m-%d %H:%M:%S")

    while True:
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        try:
            cursor = cnxn.cursor()
            with cursor.execute("DELETE FROM [Cleantech Meter Readings].dbo.[MY-001X-4G] WHERE [Tstamp]<?",last_date):
                print('Detetion successful - All records deleted before '+last_date)
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
            
        except Exception as e:
                err_num = e[0]
                print ('ERROR:', err_num,e[1])
                cursor.close()
                cnxn.close()
                if err_num == '08S01':
                    counter = counter + 1
                    print ('Sleeping for 5')
                    time.sleep(300)
                    if counter > 10:
                        exit('Persistent connection error')                    
                    print ('%%%Re-trying connection%%%')             
                else:
                    break
        
    

#Function to read start timestamp
def read_timestamp():
    global start_tstamp
##    file = open('F:/Flexi_final/[MY-001X]/MY001XA4G.txt','r') ###local
    file = open('/home/admin/Start/MY001XA4G.txt','r') ###server
    time_read = file.read(19)
    start_tstamp = datetime.datetime.strptime(time_read, "%Y-%m-%d %H:%M:%S")

#Function to update start file 
def update_start_file(curr_tstamp):
##    file = open('F:/Flexi_final/[MY-001X]/MY001XA4G.txt','w') ###local
    file = open('/home/admin/Start/MY001XA4G.txt','w') ###server
    file.write(str(curr_tstamp))
    file.close()

def insert(fields,curr_tstamp):
 
    counter = 0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            update_start_file(fields[12])
            global start_tstamp_update
            start_tstamp_update = curr_tstamp

            with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[MY-001X-4G] ([Date1],[Eac1],[Eac2],[DA],[Downtime],[LastTime],[LastReadDel],[DailySpecYield1],[DailySpecYield2],[PR1],[PR2],[GHIMY002],[Tstamp],[StnID]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",fields): 
                print('Successfuly Inserted!' + fields[12])                
            
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
        
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num,e[1], fields[12])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                break




def convert_data_type(fields):
    for i in range(len(fields)):
        if i in str_val:
            continue
        if fields[i] == 'NA' or fields[i] == 'NA\n' :
            fields[i] = None
        else:
            fields[i] = float(fields[i])

    return fields
        


read_timestamp()
start_tstamp_update = start_tstamp
while True:

    determine_date()
       
    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found -',read_path)
            time.sleep(600)
            continue

    start_tstamp = start_tstamp_update
    with open(read_path, 'r') as read_file:
        first = 1
        for line in read_file:
            if first == 1:
                first = 0
                continue
            fields =[]
            fields = line.split("\t")
            for i in str_val:   
                fields[i] = fields[i].replace('"', '').strip()
            if fields[12] == "NA" or fields[0] == "NA":
                continue
            curr_tstamp = datetime.datetime.strptime(fields[12], "%Y-%m-%d %H:%M:%S")
            if curr_tstamp >= start_tstamp:
                fields = convert_data_type(fields)
                insert(fields,curr_tstamp)
                
    time.sleep(1800)
