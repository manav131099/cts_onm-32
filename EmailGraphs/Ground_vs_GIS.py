import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import matplotlib.backends.backend_pdf
from matplotlib.ticker import FormatStrFormatter

gis_cities = ['CHENNAI','NEW_DELHI','PUNE']
met_cities  = ['713','711','721']

for index,city in enumerate(gis_cities):
    path_gis = '/home/admin/Dropbox/GIS_API3/'+gis_cities[index]+'/'+gis_cities[index]+'_AGGREGATE.txt'
    path_met = 'IN-'+met_cities[index]+' Extract Daily.txt'
    df_gis_temp = pd.read_csv(path_gis,sep='\t')
    df_gis_temp['Date']=pd.to_datetime(df_gis_temp['Date'],format='%d-%m-%Y',errors='coerce').dropna()

    df_gis_temp2 = pd.read_csv(path_gis,sep='\t')
    df_gis_temp2['Date']=pd.to_datetime(df_gis_temp2['Date'],format='%Y-%m-%d',errors='coerce').dropna()

    df_gis = df_gis_temp.append(df_gis_temp2)

    df_gis.index=  pd.to_datetime(df_gis['Date'])
    df_gis = df_gis[df_gis.index>='2020-06-01']
    df_met = pd.read_csv(path_met,sep='\t')
    df_met.index = pd.to_datetime(df_met['Date'])
    gis = df_gis.groupby(pd.Grouper(freq="M"))
    met = df_met.groupby(pd.Grouper(freq="M"))

    font = {'size'   : 11}
    matplotlib.rc('font', **font)
    shape_dict = {6:'h',7:'v',8:'>',9:'s',10:'^',11:'o',12:'d',13:'1',14:'2',15:'<',16:'4',17:'p'}

    shape = ['h','v','>','s','^','o','d','1','2','<','4','p']
    cnt=0
    merged_df = df_gis.join(df_met, how='left',lsuffix='_x').dropna()
    merged_df = merged_df[merged_df['DA']>99]

    df_temp = pd.DataFrame()
    pdf = matplotlib.backends.backend_pdf.PdfPages('All_'+city+'.pdf')
    for month,group in gis:
        fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))
        ax = fig3.add_subplot(111)
        group1 = met.get_group(month) 
        group2 = group
        merged = group2.join(group1, how='left',lsuffix='_x').dropna()
        merged = merged[merged['DA']>99]
        X = merged['Smp'].values.reshape(-1, 1)
        Y = merged['GHI'].values.reshape(-1, 1)
        dates = merged['Date'].tolist()
        da =  (merged['PtsRec']/1440)*100 
        da = da.mean().round(2)
        g_sum = merged['Smp'].sum()
        gis_sum = merged['GHI'].sum()
        linear_regressor = LinearRegression()  # create object for the class
        model  = linear_regressor.fit(X, Y)
        r_sq = round(model.score(X, Y),3)
        ax.plot(merged['Smp'], merged['GHI'], marker=shape[cnt], linestyle='', ms=7, label=month)
        asd = pd.DataFrame({'Date':[month],'Value':[r_sq],'Pyranometer':[g_sum],'GIS':[gis_sum],'DA':[da]})
        df_temp = df_temp.append(asd)
        cnt= cnt+1
        #mylabels=['2020-06','2020-07','2020-08','2020-09','2020-10','2020-11','2020-12','2021-01','2020-02']
        ax.legend(loc='lower right',labels=[month.strftime("%b-%y")])
        ax.plot([0,1],[0,1], transform=ax.transAxes,color='black')
        ax.set_ylabel("Satellite Global Horizontal Irradiation [kWh/m2]")
        ax.set_xlabel("Ground Global Horizontal Irradiation [kWh/m2]")
        ttl = ax.set_title(month.strftime("%b-%y"), fontdict={'fontsize': 13, 'fontweight': 'medium'})
        ttl.set_position([.495, 1.01])

        ttl_main=fig3.suptitle('Ground GHI vs Satellite GHI',fontsize=15,x=.511,y=0.95)
        ax.set_xlim(0,7.5)
        ax.annotate('R-sq error: '+str(round(r_sq,3)), (.54, .24),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)
        ax.set_ylim(0,8)
        plt.margins(y=1.1)
        pdf.savefig(fig3, bbox_inches='tight')

    df_temp.to_csv('All_'+city+'.csv',index=False)
    X = merged_df['Smp'].values.reshape(-1, 1)
    Y = merged_df['GHI'].values.reshape(-1, 1)
    linear_regressor = LinearRegression()  # create object for the class
    model  = linear_regressor.fit(X, Y)
    r_sq = model.score(X, Y)
    fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))
    ax = fig3.add_subplot(111)
    ax.plot(merged_df['Smp'], merged_df['GHI'], marker='8', linestyle='', ms=7, label=month)
    asd = pd.DataFrame({'Date':[month],'Value':[r_sq],'Pyranometer':[g_sum],'GIS':[gis_sum],'DA':[da]})
    df_temp = df_temp.append(asd)
    cnt= cnt+1
    #mylabels=['2020-06','2020-07','2020-08','2020-09','2020-10','2020-11','2020-12','2021-01','2020-02']
    ax.legend(loc='lower right',labels=['All Data'])
    ax.plot([0,1],[0,1], transform=ax.transAxes,color='black')
    ax.set_ylabel("Satellite Global Horizontal Irradiation [kWh/m2]")
    ax.set_xlabel("Ground Global Horizontal Irradiation [kWh/m2]")
    ttl = ax.set_title('All Data', fontdict={'fontsize': 13, 'fontweight': 'medium'})
    ttl.set_position([.495, 1.01])

    ttl_main=fig3.suptitle('Ground GHI vs Satellite GHI',fontsize=15,x=.511,y=0.95)
    ax.set_xlim(0,7.5)
    ax.annotate('R-sq error: '+str(round(r_sq,3)), (.54, .24),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)
    ax.set_ylim(0,8)
    plt.margins(y=1.1)
    pdf.savefig(fig3, bbox_inches='tight')
    pdf.close()