import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

font = {'size'   : 11}
matplotlib.rc('font', **font)
fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))
ax = fig3.add_subplot(111)

df_my402 = pd.read_csv('/home/admin/Dropbox/Gen 1 Data/[MY-402L]/2020/2020-12/MFM_1_PV Meter/[MY-402L]-MFM1-2020-12-27.txt',sep='\t')
df_my419 = pd.read_csv('/home/admin/Dropbox/Gen 1 Data/[MY-419L]/2020/2020-12/MFM_1_PV Meter/[MY-419L]-MFM1-2020-12-27.txt',sep='\t')

df_my402['ts'] = pd.to_datetime(df_my402['ts'] )
df_my419['ts'] = pd.to_datetime(df_my419['ts'] )

ax.plot(df_my402['ts'], df_my402['W_avg']/1000,color='blue',label='MY-402 (Huawei)')
ax.plot(df_my419['ts'], df_my419['W_avg']/1000,color='orange',label='MY-419 (Solis)')
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H')) 
ax.set_xlim(datetime.datetime(2020,12,27,0,0,0),datetime.datetime(2020,12,27+1,0,0,0))
ax.set_ylabel("Power [kW]")
ax.set_xlabel("")
ax.legend()
ttl = ax.set_title('2020-12-27', fontdict={'fontsize': 13, 'fontweight': 'medium'})
ttl.set_position([.495, 1.02])
ttl_main=fig3.suptitle('MY-402 & MY-419 Decemeber Pac Graph',fontsize=15,x=.512)

fig3.savefig("Pac Decemeber.png", bbox_inches='tight')