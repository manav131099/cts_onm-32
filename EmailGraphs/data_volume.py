import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf
import matplotlib.dates as mdates
import logging
import matplotlib.colors as mcolors

path='/home/pranav/Size_Processed.csv'
df=pd.read_csv(path)

df_2 = pd.read_csv('/home/pranav/Code/PV Analysis/DFG.csv')
df_2 = df_2[['Date of First Generation (DFG)','Val']]
df_2.index = pd.to_datetime(df_2['Date of First Generation (DFG)'])
df_2 = df_2.resample('1m').sum()
df_2['Cumsum'] = df_2['Val'].cumsum()
df_2['Date'] = df_2.index.strftime('%Y-%b')
df_2 = df_2.iloc[6:]
#Graph Creation
font = {'size'   : 22}
plt.rcParams['axes.facecolor'] = 'white'
csfont = {'fontname':'Arial'}
plt.title('title',**csfont)
plt.xlabel('xlabel', **csfont)
matplotlib.rc('font', **font)
fig, ax = plt.subplots(figsize=(32,20))
ttl = ax.set_title('From 2016-01-01 to 2021-05-31', fontdict={'fontsize': 28})

ttl_main=fig.suptitle('Data Volume',fontsize=35,x=.512,y=0.94)

df.index = pd.to_datetime(df['Date'])
df_year = df.resample('1y').sum()
df_year['Size:'] = df_year['Size:']/1000
df_year = df_year.round(1)
year = df_year['Size:'].tolist()
df = df.resample('1m').sum()
df['Date'] = df.index.strftime('%Y-%b')
df = df.iloc[:-1]
df['Size:'] = df['Size:']/1000
c=['cyan','skyblue','dodgerblue','royalblue','blue','mediumblue','navy']
for i in range(1,7):
    plt.bar(df['Date'].tolist()[(i-1)*12:12*i], df['Size:'].tolist()[(i-1)*12:12*i],color=c[i],width=.8,label=str(2015+i) +' ['+str(year[i-1])+' GB]')
plt.xticks(rotation = 90)
ax.set_xlabel('')
ax.set_ylabel('Volume [GB]',labelpad=8)
ax.set_xlim('2016-Jan','2021-June')
ax.legend()
""" full = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tYearly Totals\n\n'
for index,row in df_year.iterrows():
    full = full+ str(index)[0:4]+': '+str(row['Size:'])+' GB\t\t'
plt.text(0.22, 0.8,full[:-2], transform=plt.gcf().transFigure,weight='bold',bbox=dict(facecolor='none', edgecolor='black')) """
xlims = ax.get_xlim()
ax2 = ax.twinx()
ax2.plot(df['Date'].tolist(),df_2['Cumsum'],color='red',lw=2.5, marker='o')
ax2.set_ylim(0,400)
ax2.set_ylabel('Number of sites',rotation=270,labelpad=28)
fig.savefig("Data_Volume.pdf", bbox_inches='tight') #Graph
