import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

class LineAnnotation(Annotation):
    def __init__(
        self, text, line, x , line_type, xytext=(0, 5), textcoords="offset points", **kwargs
    ):
        assert textcoords.startswith(
            "offset "
        ), "*textcoords* must be 'offset points' or 'offset pixels'"

        self.line = line
        self.xytext = xytext

        # Determine points of line immediately to the left and right of x
        xs, ys = line.get_data()

        assert (
            np.diff(xs) >= 0
        ).all(), "*line* must be a graph with datapoints in increasing x order"

        i = np.clip(bisect(xs, x), 1, len(xs) + 1)
        self.neighbours = n1, n2 = np.asarray([(xs[i - 1], ys[i - 1]), (xs[i], ys[i])])

        # Calculate y by interpolating neighbouring points
        if(line_type == 'below'):
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))-1
        else:
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))
        self.ypos = n1[1] + ((x -.7- n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))
        kwargs = {
            "horizontalalignment": "center",
            "rotation_mode": "anchor",
            **kwargs,
        }
        super().__init__(text, (x, y), xytext=xytext, textcoords=textcoords, **kwargs,size=10)


    def get_rotation(self):

        transData = self.line.get_transform()
        dx, dy = np.diff(transData.transform(self.neighbours), axis=0).squeeze()
        return np.rad2deg(np.arctan2(dy, dx))

    def update_positions(self, renderer):
        xytext = Affine2D().rotate_deg(self.get_rotation()).transform(self.xytext)
        self.set_position(xytext)
        super().update_positions(renderer)




def get_total_eac(date):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT SUM([Eac-MFM]) AS SUM FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM [dbo].[Stations] WHERE Station_Name='"+site+"') AND Date='"+date+"' ", connStr)
    df_sum = pd.DataFrame(SQL_Query, columns=['SUM'])
    return df_sum
    
#Sending Message
def send_mail(date, stn, df, recipients, attachment_path_list=None):
  info ='Date: '+date+'\n\n'
  info = info+"GHI : "+str(df[0]['GHI'].values[0])+'\n\n'
  info = info+"PR : "+str(round(df[0]['PR'].values[0],1))+'\n\n'
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = stn 
  today_g = df_today['GHI'].values[0]
  today_pr = df_today['PR'].values[0]
  print(today_pr)
  if('Upper' in stn):
      info = info+'Alarm type: Upper Corridor\n\n'
      trig_val = round((((today_pr-(slope*today_g+(intercept)))/today_pr))*100,1)
  elif('Lower' in stn):
      info = info+'Alarm type: Lower Corridor\n\n'
      temp_trig = (slope*today_g+(intercept))
      trig_val = round(((temp_trig - today_pr)/temp_trig)*100,1)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  info = info + 'Alarm Trigger Value: '+str(trig_val)+' %\n\n'
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

def line_annotate(text, line, x , y,line_type, *args, **kwargs):
    ax = line.axes
    a = LineAnnotation(text, line, x, line_type, *args, **kwargs)
    if "clip_on" in kwargs:
        a.set_clip_path(ax.patch)
    ax.add_artist(a)
    plt.arrow(x-.7, a.ypos, .1, .9, length_includes_head=True,head_width=.07, head_length=.25,facecolor ='green')
    return a
      
def abline(slope, intercept, df, df_today,color,line_type,prs,temp,year='All'):
    x_vals = np.array((0, 8))
    y_vals = intercept + slope * x_vals
    g = df['GHI'].tolist()
    p = df['PR'].tolist()
    cnt=0
    for index,i in enumerate(prs):
        plt.plot(4.4438, i, '.',color='black',ms=12)
        if(index==0):
            plt.text(4.4438+.1, i-.2,str(round(i,1))+'%')
        else:
            plt.text(4.4438+.1, i-.2,str(round(i,1))+'%')
    for index,i in enumerate(g):
        temp = slope*i+(intercept+0.05*intercept)
        temp2 = slope*i+(intercept-0.05*intercept)
        if(p[index]>temp or p[index]<temp2):
            cnt=cnt+1
    ln = plt.plot(x_vals, y_vals,color=color,label=year) 
    if(year=='2021' and site=='SG-003'):
        line_annotate('Optimum Performance',ln[0],1,temp_gg[0],'above',color='#008000')
        line_annotate('Optimum Performance',ln[0],3.5,temp_gg[1],'above',color='#008000')
        line_annotate('Optimum Performance',ln[0],6.5,temp_gg[2],'above',color='#008000')
    elif(year=='2021' and site=='SG-004'):
        line_annotate('Optimum Performance',ln[0],1,temp_gg[0],'above',color='#008000')
        line_annotate('Optimum Performance',ln[0],3.5,temp_gg[1],'above',color='#008000')
"""     if((today_pr>slope*today_g+(intercept+0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Upper '+site+' PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
    elif((today_pr<slope*today_g+(intercept-0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Lower '+site+' PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
 """

    
font = {'size'   : 11}

matplotlib.rc('font', **font)


site = sys.argv[1]
pr_ghi_columns = sys.argv[2]
start_date = sys.argv[3]
date_today = sys.argv[4]

if(site=='SG-003'):
    path = '/home/admin/Graphs/Graph_Extract/'+site+'/[SG-003] Graph '+date_today+' - PR Evolution - Full Site.txt'
else:
    path='/home/admin/Graphs/Graph_Extract/'+site+'/['+site+'] Graph '+date_today+' - PR Evolution.txt'
file_path = '/home/admin/Dropbox/Second Gen/['+site+'S]/'+date_today[0:4]+'/'+date_today[0:7]+'/['+site+'S] '+date_today+'.txt'


df=pd.read_csv(path,sep='\t')

df=df[df['Date']>=start_date]
df_temp = df.copy()

if(os.path.exists(file_path)):
    df_today = pd.read_csv(file_path,sep='\t')
    df_today = df_today[pr_ghi_columns.split(',')]
    if(site=='SG-003'):
        eac_sg003 = round(df_today['FullSiteProd'].values[0]/1000,1)
        df_today['FullSiteProd'] = df_today['FullSiteProd']/1621.8 
        df_today['FullSiteProd'] = (df_today['FullSiteProd']/df_today['Gsi00'])*100
    elif(site=='SG-004'):
        eac_sg004 = round(df_today['FullSiteProd'].values[0]/1000,1)
        df_today['FullSiteProd'] = df_today['FullSiteProd']/844.8
        df_today['FullSiteProd'] = (df_today['FullSiteProd']/df_today['Gsi00'])*100
    df_today.columns = ['GHI','PR']
else:
    df_today = df[df['Date']==date_today]

df = df[(df['Date']!=date_today)]

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

ax = fig3.add_subplot(111)

shape_dict = {1:'1',2:'2',3:'<',4:'4',5:'p',6:'h',7:'v',8:'>',9:'s',10:'^',11:'o',12:'d',2020:'.',2017:'+',2018:'x',2019:'.',2016:'p'}
temp_dict_2 = {}

today_g = df_today['GHI'].values[0]
today_pr = df_today['PR'].values[0]
plt.vlines(today_g, ymin=0, ymax=today_pr, color='red', linestyle='--')
plt.text(today_g-.14,65, 'GHI = '+str(round(today_g,2))+' kWh/m2', ha='left', va='center',color='red',rotation=90)
plt.text(today_g+.1, today_pr-.3,str(round(today_pr,1))+'%',color='red')
ln5 = ax.scatter(today_g, today_pr, marker='*',color='red',s=60, alpha=0.75, zorder=3)
temp_dict_2[14] = [ln5,date_today]

years = ['2017-12-31','2018-12-31','2019-12-31','2020-12-31','2021-12-31']
c= ['lightblue','cyan','dodgerblue','blue','#008000']
prs=[]
temp_gg = []
for index,i in enumerate(years):
    if(i=='2017-12-31'):
        df_year = df[df['Date']<i]
    else:
        df_year = df[((df['Date']>years[index-1]) & (df['Date']<i))]
    print(df_year)
    df_year = df_year[df_year['PR']>40]
    X = df_year['GHI'].values.reshape(-1, 1)
    Y = df_year['PR'].values.reshape(-1, 1)
    linear_regressor = LinearRegression()  # create object for the class
    linear_regressor.fit(X, Y)  # perform linear regression
    slope=linear_regressor.coef_[0][0]
    intercept=linear_regressor.intercept_[0]
    val = slope*4.4438+intercept
    prs.append(val)
    abline(slope,intercept,df_temp,df_today,c[index],'notall',prs,temp_gg,i[0:4])
    if(i=='2020-12-31'):
        temp_gg.append(slope*1+(intercept))
        temp_gg.append(slope*3.5+(intercept))
        temp_gg.append(slope*6.5+(intercept))
print(temp_gg)
keys = []
vals = []
for i in temp_dict_2:
    keys.append(temp_dict_2[i][0])
    vals.append(temp_dict_2[i][1])
legend = plt.legend(keys,vals,scatterpoints=1,fontsize=11,loc=(0.05,.05),frameon=False)
totlen=len(vals)
for m in range(totlen-1):
    legend.legendHandles[m].set_color('black')

plt.gca().add_artist(legend)

plt.axvline(x=4.4438, color='darkorange', linestyle='--',label='P50 Daily Irradiation')
plt.text(4.3,65, 'GHI = '+str(round(4.4438,2))+' kWh/m2', ha='left', va='center',color='darkorange',rotation=90)
yx=[today_g]
y=[today_pr]
plt.ylim(60,90)
plt.xlim(0,7.5)
ax.set_ylabel("PR [%]")
ax.set_xlabel("GHI [kWh/m2]")
ttl = ax.set_title('From '+str(df['Date'].head(1).values[0])[0:10]+' to '+str(df_temp['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 13, 'fontweight': 'medium'})
ttl.set_position([.495, 1.02])
ttl_main=fig3.suptitle(''+site+' Degradation Evolution',fontsize=15,x=.512,y=0.96)
ax.legend(loc='upper right')

fig3.savefig("/home/admin/Graphs/Graph_Output/"+site+"/["+site+"] Graph "+date_today+" - Degradation Evolution.pdf", bbox_inches='tight')
df_temp.to_csv("/home/admin/Graphs/Graph_Extract/"+site+"/["+site+"] Graph "+date_today+" - Degradation Evolution.txt",sep='\t')


