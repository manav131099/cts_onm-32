import pandas as pd
import pvlib
from pvlib.pvsystem import PVSystem
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS
from pvlib import solarposition, irradiance, atmosphere, pvsystem, inverter, temperature, iam
from pvlib.forecast import GFS, NAM, NDFD, RAP, HRRR
import datetime
import requests.auth
import smtplib
import os
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf
import matplotlib.dates as mdates
from database_operations import *

def send_mail(date,body,recipients,attachment_path_list=None):
	server = smtplib.SMTP("smtp.office365.com")
	server.starttls()
	server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
	msg = MIMEMultipart()
	sender = 'operations@cleantechsolar.com'
	msg['Subject'] = 'Solar Energy Forecasting [IN-069] Prism Colony - '+date
	#if sender is not None:
	msg['From'] = sender
	msg['To'] = ", ".join(recipients)
	if attachment_path_list is not None:
		for each_file_path in attachment_path_list:
			try:
				file_name = each_file_path.split("/")[-1]
				part = MIMEBase('application', "octet-stream")
				part.set_payload(open(each_file_path, "rb").read())
				encoders.encode_base64(part)
				part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
				msg.attach(part)
			except:
				print("could not attache file")
	msg.attach(MIMEText(body,'plain/text'))
	server.sendmail(sender, recipients, msg.as_string())
  
    
def gfs_forecast():
	start = pd.Timestamp(datetime.date.today()+datetime.timedelta(days=1), tz=tz) # today's date
	end = start + pd.Timedelta(days=1) # 7 days from today
	fm = GFS()
	forecast_data = fm.get_processed_data(latitude, longitude, start, end)
	forecast_data = forecast_data.resample('15min').interpolate()
	ghi = forecast_data['ghi']
	dni = forecast_data['dni']
	dhi = forecast_data['dhi']
	temp_air = forecast_data['temp_air']
	wind_speed = forecast_data['wind_speed']
	weather = pd.DataFrame([dni, ghi, dhi, temp_air, wind_speed]).T 
	return weather 

def solcast_forecast():
	headers = {'content-type': 'application/json'}
	url='https://api.solcast.com.au/world_radiation/forecasts?latitude=24.572452&longitude=81.00092&hours=168&api_key=abx5Kr6FEIKbL8a-o9MOWG9WO2pSKy2W'
	r = requests.get(url, headers=headers)
	data=r.json()
	dataframe=pd.DataFrame(data['forecasts'])
	dataframe.to_csv('Forecast.csv')
	solcast_df = pd.DataFrame(data['forecasts'])
	solcast_df.index = pd.to_datetime(solcast_df['period_end'])
	solcast_df.index = solcast_df.index.tz_convert('Asia/Kolkata')
	solcast_df = solcast_df.resample('15min').interpolate()
	gfs_df = gfs_forecast()
	merged_df = pd.merge(gfs_df,solcast_df,left_index=True,right_index=True)
	merged_df = merged_df[['dni_y','ghi_y','dhi_y','air_temp','wind_speed']]
	merged_df.columns = ['dni','ghi','dhi','temp_air','wind_speed']
	return merged_df
  
  

def create_model(area,mps,spi):
  temperature_model_parameters = TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_polymer']
  cec_module = cec_modules['Trina_Solar_TSM_330DD14A']
  if(area == 'Mines'):
    cec_inverter = cec_inverters['TMEIC__PVH_L2500GR']
  else:
    cec_inverter = cec_inverters['Sungrow_Power_Supply_Co___Ltd___SG_60KU_M__480V_']
  system = PVSystem(surface_tilt=surface_tilt, surface_azimuth=surface_azimuth,
                  module_parameters=cec_module,
                  inverter_parameters=cec_inverter,
                  temperature_model_parameters=temperature_model_parameters,
                  surface_type=None, module=None, 
                  module_type='glass_polymer',
                  modules_per_string=mps, 
                  strings_per_inverter=spi, inverter=None, 
                  racking_model='open_rack', 
                  losses_parameters=None, 
                  name='Name of PV system: Satna')
  mc = ModelChain(system, location, name='Satna', 
                  clearsky_model='ineichen', transposition_model='perez', 
                  solar_position_method='nrel_numpy', airmass_model='kastenyoung1989',
                  dc_model=None, #from module specifications
                  ac_model='sandia', #from inverter specifications
                  aoi_model='no_loss', 
                  spectral_model='no_loss', #spectral model 'no loss' otherwise error
                  temperature_model='sapm', losses_model='no_loss')
  return mc

def get_prediction(mc,weather,n,pred_type,date):
	mc.run_model(weather)
	power = mc.ac
	power = power[power>=0]
	df_power = pd.DataFrame({'Timestamp':power.index, 'AC_Power':power.values})
	df_power['AC_Power'] = (df_power['AC_Power']/1000)*n
	irr = mc.effective_irradiance
	ghi = (irr.resample('1d').sum())/4000
	eac = ((power.resample('1d').sum())/4000)*n
	return [ghi.round(1),eac.round(1),df_power]

def get_accuracy(date):
	cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
	site_data = get_processed_data(date, get_site_id('IN-069',cnxn), cnxn)
	cnxn.close()
	actual_eac = site_data.loc[(site_data['ComponentId']==1086) & (site_data['ParameterId']==5),'Value'].values[0]
	df_2 = pd.read_csv(path+'Daily/[IN-069]-Colony-'+date+'.txt',sep='\t')
	yest_gfs = df_2['GFS_EAC'].values[0]
	yest_solcast = df_2['SOLCAST_EAC'].values[0]
	acc_1 = (1-abs(actual_eac - yest_gfs)/actual_eac)*100
	acc_2 = (1-abs(actual_eac - yest_solcast)/actual_eac)*100
	return [acc_1,acc_2,actual_eac,yest_gfs,yest_solcast]

def create_graph(date):
	df = pd.DataFrame()
	for i in sorted(os.listdir(path+'Daily/'))[:-1]:
		if('Colony' in i):
			file_date = i[-14:-4]
			acc = get_accuracy(file_date)
			temp_df = pd.DataFrame({'Date':[file_date],'Accuracy_GFS':[acc[0]],'Accuracy_SOLCAST':[acc[1]]})
			df = df.append(temp_df)
	fig, ax = plt.subplots(figsize=(12.8, 8.8))
	df = df[df['Date']<=date]
	df['Date'] = pd.to_datetime(df['Date'])
	plt.plot(df['Date'].dt.date,df['Accuracy_GFS'],linewidth=1.5,marker='o',label = 'Accuracy_GFS')
	plt.plot(df['Date'].dt.date,df['Accuracy_SOLCAST'],linewidth=1.5,marker='o',label='Accuracy_SOLCAST')
	ax.set_xlabel('')
	ax.set_ylabel('Accuracy [%]')
	ax.set_ylim([0,100])
	#myFmt = mdates.DateFormatter('%Y-%m-%d')
	#ax.xaxis.set_major_formatter(myFmt)
	ax.set_title(date, fontdict={'fontsize': 11, 'fontweight': 'bold'})
	fig.suptitle('Prism Colony Forecast Accuracy',fontsize=11,x=.512, fontweight='bold',y=0.945)
	ax.legend()
	fig.savefig(path+'Graphs/[IN-069]-Colony-'+date+'.pdf', bbox_inches='tight') #Graph


#Initialize
cec_modules = pvlib.pvsystem.retrieve_sam('CECMod')
cec_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
latitude= 24.565261
longitude= 81.002298
altitude=1007
surface_tilt = 14
surface_azimuth = 180 
tz='Asia/Kolkata'
location = Location(latitude=latitude, longitude=longitude, altitude=altitude, tz=tz)
date = (datetime.date.today()+datetime.timedelta(days=1)).strftime("%Y-%m-%d")
prev_date = (datetime.date.today()+datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
path ='/home/admin/Dropbox/CleantechForecasting/[IN-069]/'
#{No of INV:[Area,modules per string,strings per inverter]}
site = {16:['Colony',19,12]}
df_gfs = pd.DataFrame()
df_solcast = pd.DataFrame()
total_gfs = 0
total_solcast = 0
for c in site:
  	#Create Models
	component_info  = site[c]
	mc = create_model(component_info[0],component_info[1],component_info[2])

	#GFS Prediction
	weather = gfs_forecast()
	gfs_prediction = get_prediction(mc,weather,c,'GFS',date)
	total_gfs = total_gfs +  gfs_prediction[1].values[0]
	if(df_gfs.empty):
		df_gfs = gfs_prediction[2]
	else:
		df_gfs = df_gfs+gfs_prediction[2]

	#Solcast Prediction
	weather = solcast_forecast()
	solcast_prediction = get_prediction(mc,weather,c,'SOLCAST',date)
	total_solcast = total_solcast + solcast_prediction[1].values[0]
	if(df_solcast.empty):
		df_solcast = solcast_prediction [2]
	else:
		df_solcast = df_solcast+solcast_prediction[2]

#Saving files
df_gfs.to_csv(path+'15min/'+date+'-GFS.txt',sep='\t',index=False)
df_solcast.to_csv(path+'15min/'+date+'-SOLCAST.txt',sep='\t',index=False)
df = pd.DataFrame({'Date':[date],'GFS_GHI':[gfs_prediction[0].values[0]],'SOLCAST_GHI':[solcast_prediction[0].values[0]],'GFS_EAC':[total_gfs],'SOLCAST_EAC':[total_solcast]})
df.to_csv(path+'Daily/[IN-069]-Colony-'+date+'.txt',sep='\t',index=False)

#Creating Graph and Sending Mail 
body="\nDate: "+date+"\n\n------------------------------\nGFS\n------------------------------\n\n"+"GHI [kWh/m^2]: "+str(gfs_prediction[0].values[0])+"\n\n"+"Forecasted Energy [kWh]: "+str(round(total_gfs,1))+"\n\n"
body=body+"------------------------------\nSOLCAST\n------------------------------\n\n"+"GHI [kWh/m^2]: "+str(solcast_prediction[0].values[0])+"\n\n"+"Forecasted Energy [kWh]: "+str(round(total_solcast,1))+"\n\n"
if(os.path.exists(path+'Daily/[IN-069]-Colony-'+prev_date+'.txt' )):
	acc = get_accuracy(prev_date)
	body=body+"------------------------------\nACCURACY ("+prev_date+")\n------------------------------\n\n"+"Actual Energy [kWh]: "+str(round(acc[2],1))+"\n\n"+"GFS Forecasted Energy [kWh]: "+str(round(acc[3],1))+"\n\nSOLCAST Forecasted Energy [kWh]: "+str(round(acc[4],1))+"\n\nGFS Accuracy [%]: "+str(round(acc[0],1))+"\n\n"+"SOLCAST Accuracy [%] "+str(round(acc[1],1))+"\n\n"
try:
	create_graph(prev_date)
except:
	print('Graph Failed')
send_mail(date,body,['andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com','rohit.jaswal@cleantechsolar.com','sai.pranav@cleantechsolar.com'],attachment_path_list=[path+'15min/'+date+'-SOLCAST.txt',path+'Graphs/[IN-069]-Colony-'+prev_date+'.pdf'])



