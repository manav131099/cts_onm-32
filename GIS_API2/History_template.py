import datetime as DT
import requests
import os
from lxml import etree
import pandas as pd
import csv
from datetime import timedelta 
import shutil
import pytz
tz = pytz.timezone('Asia/Calcutta')

#GHI CODE

today=(DT.datetime.now(tz).date()+DT.timedelta(days=-1))
print("Printing today")
print(today)
api_key = 'HesHutsorc8mnk95sc'
url = 'https://solargis.info/ws/rest/datadelivery/request?key=%s' % api_key
headers = {'Content-Type': 'application/xml'}
xmlfile = open('/home/admin/CODE/GIS_API2/API_req.xml', 'r')
body = xmlfile.read()

start = [ '2021-01-01','2021-02-01','2021-03-01', '2021-04-01','2021-05-01','2021-06-01','2021-07-01','2021-08-01','2021-09-01']

end = [ '2021-01-31','2021-02-28','2021-03-31','2021-04-30','2021-05-31','2021-06-30','2021-07-31','2021-08-31','2021-09-06']

site = [ 'JALKOT']

lat = [18.57331029]

lng = [77.20730606]

tilt = [15]

azimuth = [180]

week_ago = today - DT.timedelta(days=7)
yesterday = today - DT.timedelta(days=1)


day = today.day
print("printing day")
print(day)
month = today.month-1
if(day == 10):
  print("Replacing data")
  last_day = today - timedelta(days = 2) 
  first_day_last = last_day.replace(day=1) 
  delta = last_day - first_day_last
  delta = delta.days
  delta = delta+2
  print(delta)
  first_day = today.replace(day=1) 
  start = [first_day_last, first_day]
  end = [last_day, today]
  print(start)
  print(end)
  print(start[0])
  print(end[0])
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
    print(a, b, c, d, e)
    response = requests.post(url, data=body.format(Start_Date= start[0], End_Date = end[0], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI"), headers=headers)   
    data = []
    ghi_rounded = []
    ts_rounded = []
    root = etree.fromstring(bytes(response.text, encoding='utf-8'))
    for element in root.iter("*"):
      data.append(element.items())
    tuple_list = [item for t in data for item in t]
    first_tuple_elements = []
            
    for a_tuple in tuple_list:
      first_tuple_elements.append(a_tuple[1])
         
    ts = first_tuple_elements[1::2]
    for i in ts:
      x = slice(0, 10)
      ts_rounded.append(i[x])
            
    ghi = first_tuple_elements[2::2]
    for i in ghi:
      i = float(i)
      i = round(i,4)
      ghi_rounded.append(i)
    list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
    df = pd.DataFrame(list_of_tuples) 
    df = df.iloc[1:]
    ts_rounded.pop(0)
    ghi_rounded.pop(0)
    path = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt"
    #df_path = pd.read_csv(path)
    #df_path = df_path.iloc[:-delta]
    path_new = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt"
    lines = open(path).readlines()
    open(path_new, 'w').writelines(lines[:-delta])
      
    file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt", "a")
    #print(file)
    #print("Written")
    for index in range(len(ts_rounded)):
      file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) + "\n")
    file.close() 
    with open(path_new, 'rb') as f2, open(path, 'wb') as f1:
      shutil.copyfileobj(f2, f1)
      
    
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
      #print(yesterday, a, b, c)
      response = requests.post(url, data=body.format(Start_Date= start[1], End_Date = end[1], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI GTI"), headers=headers)      
      data = []
      ghi_rounded = []
      gti_rounded = []
      ts_rounded = []
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
      tuple_list = [item for t in data for item in t] 
      first_tuple_elements = []
      
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
      ts = first_tuple_elements[1::2]
      for i in ts:
        x = slice(0, 10)
        ts_rounded.append(i[x])
      
      ghi = first_tuple_elements[2::2]
      ghi.pop(0)
      for i in ghi:
        i = i.split()
      
        i[0] = float(i[0])
        i[1] = float(i[1])
        i[0] = round(i[0],2)
        i[1] = round(i[1],2)
        ghi_rounded.append(i[0])
        gti_rounded.append(i[1])
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      if(a == 'RAYONG' and e == 0):
        file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_0az_AGGREGATE.txt", "a")
        for index in range(len(ts_rounded)):
            file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
        file.close() 
      else:
        file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
        for index in range(len(ts_rounded)):
            file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
        file.close() 
      
else: 
  for (f,g) in zip(start, end):
      for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
          #print(yesterday, a, b, c)
          response = requests.post(url, data=body.format(Start_Date= f, End_Date = g, latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI"), headers=headers)
                
          data = []
          ghi_rounded = []
          gti_rounded = []
          ts_rounded = []
          root = etree.fromstring(bytes(response.text, encoding='utf-8'))
          for element in root.iter("*"):
              data.append(element.items())
          tuple_list = [item for t in data for item in t] 
          first_tuple_elements = []
      
          for a_tuple in tuple_list:
              first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
          ts = first_tuple_elements[1::2]
          for i in ts:
            x = slice(0, 10)
            ts_rounded.append(i[x])
      
          ghi = first_tuple_elements[2::2]
          ghi.pop(0)
          for i in ghi:
            i = i.split()
      
            i[0] = float(i[0])
            i[1] = float(i[1])
            i[0] = round(i[0],2)
            i[1] = round(i[1],2)
            ghi_rounded.append(i[0])
            gti_rounded.append(i[1])
          list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
          df = pd.DataFrame(list_of_tuples) 
          df = df.iloc[1:]
          ts_rounded.pop(0)
          if(a == 'RAYONG' and e == 0):
            file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_0az_AGGREGATE.txt", "a")
            for index in range(len(ts_rounded)):
                file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
            file.close() 
          else:
            file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
            for index in range(len(ts_rounded)):
                file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
            file.close() 



