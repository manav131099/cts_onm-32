source('/home/admin/CODE/common/aggregate.R')

METERACNAMESAGG = c("ABS_MFM","EMR_MFM","ERD_MFM","SS_MFM", "U21_MFM", "U24_MFM","ABS_Inverter_1","ABS_Inverter_2","ABS_Inverter_3","ABS_Inverter_4","ABS_Inverter_5","EMR_Inverter_1","EMR_Inverter_2","EMR_Inverter_3","ERD_Inverter_1","ERD_Inverter_2","SS_Inverter_1","SS_Inverter_2","SS_Inverter_3","SS_Inverter_4","SS_Inverter_5","U21_Inverter_1","U21_Inverter_2","U21_Inverter_3","U21_Inverter_4","U21_Inverter_5","U21_Inverter_6","U21_Inverter_7","U21_Inverter_8","U21_Inverter_9","U21_Inverter_10","U24_Inverter_1", "U24_Inverter_2")

registerMeterList("IN-006C",METERACNAMESAGG)
for( x in 1 : length(METERACNAMESAGG))
{
		aggNameTemplate = getNameTemplate()
		aggColTemplate = getColumnTemplate()
		{
		if(grepl("WMS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = NA #column for LastRead
			aggColTemplate[4] = NA #column for LastTime
			aggColTemplate[5] = NA #column for Eac-1
			aggColTemplate[6] = NA #column for Eac-2
			aggColTemplate[7] = NA #column for Yld-1
			aggColTemplate[8] = NA #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = 3 #column for Irr
			aggColTemplate[12] = "Self" # IrrSrc Value
			aggColTemplate[13] = 4 #column for Tamb
			aggColTemplate[14] = 5 #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
		}
		else if(grepl("MFM",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-713S-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = NA #column for IA
			#aggColTemplate[17] = 11 #column for GA
			#aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("Inverter",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 8 #column for LastRead
			aggColTemplate[4] = 9 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 6 #column for Yld-1
			aggColTemplate[8] = 7 #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = NA # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = 10 #column for IA
		}
		}
		registerColumnList("IN-006C",METERACNAMESAGG[x],aggNameTemplate,aggColTemplate)
}
