rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN013Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN013CDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(32,27,28,29,30,31,1,12,20,21,22,23,24,25,26,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN013CDigest/aggregateInfo.R')

METERNICKNAMES = c("WMS","MLCP1","MLCP2","SDB1","SDB2","SDB3","INV-1","INV-2","INV-3","INV-4","INV-5","INV-6","INV-7","INV-8","INV-9","INV-10","INV-11","INV-12","INV-13","INV-14",
                   "INV-15","INV-16","INV-17","INV-18","INV-19","INV-20","INV-21","INV-22","INV-23","INV-24","INV-25","INV-26")

METERACNAMES = c("WMS","MLCP1","MLCP2","SDB1","SDB2","SDB3","Inverter_1","Inverter_2","Inverter_3","Inverter_4","Inverter_5","Inverter_6","Inverter_7","Inverter_8","Inverter_9","Inverter_10",
                 "Inverter_11","Inverter_12","Inverter_13","Inverter_14","Inverter_15","Inverter_16","Inverter_17","Inverter_18","Inverter_19","Inverter_20",
                 "Inverter_21","Inverter_22","Inverter_23","Inverter_24","Inverter_25","Inverter_26")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB = "22-02-2017"
DOB2 = "2017-02-22"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-013C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-013C]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  print("###########################################")
  print(pathall)
  print("###########################################")
  path = pathall[1]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: CTS Chennai",sep="")
  body = paste(body,"\n\nLocation: Chennai, India")
  body = paste(body,"\n\nO&M Code: IN-013")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body,"\n\nNumber of Energy Meters: 5")
  body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / 315Wp / 2604")
  body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP25 / 26")
  body = paste(body,"\n\nSite COD: 2017-02-22")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  GTI = NA
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld=c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("MLCP1",pathall[t])|grepl("MLCP2",pathall[t])|grepl("SDB1",pathall[t])|grepl("SDB2",pathall[t])|grepl("SDB3",pathall[t]))
      type=1
    else
      type = 0
    if( t > (1+NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"Inverter_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+6],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else if(type==1 && grepl("MLCP1",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'MLCP1',".txt",sep="")
    }
    else if(type==1 && grepl("MLCP2",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'MLCP2',".txt",sep="")
    }
    else if(type==1 && grepl("SDB1",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'SDB1',".txt",sep="")
    }
    else if(type==1 && grepl("SDB2",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'SDB2',".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'SDB3',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("MLCP1",pathall[t])|grepl("MLCP2",pathall[t])|grepl("SDB1",pathall[t])|grepl("SDB2",pathall[t])|grepl("SDB3",pathall[t]))
        {
          if(grepl("MLCP1",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
          }else if(grepl("MLCP2",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
          }else if(grepl("SDB1",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[3],"\n\n")
          }else if(grepl("SDB2",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[4],"\n\n")
          }else{
            body = paste(body,"System Size [kWp]:",INSTCAPM[5],"\n\n")
          }
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,5],2), nsmall = 2)),"\n\n")
          yld1 = dataread[1,5]*100
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
          yld2 = dataread[1,6]*100
          globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
          idxglobMtYld = idxglobMtYld + 1
          body = paste(body,"PR-1 (GHI) [%]:",round(yld1/GTI,1),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",round(yld2/GTI,1),"\n\n")
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
          body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
          body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
        }
        else
        {
          GTI = as.numeric(dataread[1,3])
          print("Printing GTI")
          print(GTI)
          body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2,)),"\n\n")
          body = paste(body,"Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
          body = paste(body,"Tmod solar hours[C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))

        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx-2)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),2)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
    {
      body = paste(body,">>> Inverter outside range")
    }
  }
  body = paste(body,"\n\nStdev/COV Yields: ",format(round(sddev,2), nsmall = 2)," / ",format(round(covar,1), nsmall = 1),"%",sep="")
  for(t in 1 : noInverters)
  {
    body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
  }
  bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",format(round(TOTALGENCALC,1), nsmall = 1))
  bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh/kWp]:",format(round(TOTALGENCALC/sum(INSTCAPM),1), nsmall = 1))
  bodyac = paste(bodyac,"\n\nSystem Full PR [%]:",format(round(TOTALGENCALC*100/sum(INSTCAPM)/GTI,1), nsmall = 1),"\n\n")
  for(t in 1 : NOMETERS)
  {
    bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2,)),"\n\n")
  }
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)
  bodyac = paste(bodyac,"Stdev/COV Yields:",as.character(format(round(sdm,1), nsmall = 1)),"/",as.character(format(round(covarm,1), nsmall = 1)),"[%]")
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  if(length(filenams)<26)
    return()
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-013C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-013C","Mail",currday)
}


path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-013C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-013C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-013C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-013C]'

if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-013C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I26-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    a=c('MLCP1','MLCP2','SDB1','SDB2','SDB3')
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(stns[t] %in% a)
        type = 0
      if(stns[t] == "WMS")
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')
print("#############################")
print(z)
print("#############################")
prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = getRecipients("IN-013C","m")
  recordTimeMaster("IN-013C","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 32)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      a=c('MLCP1','MLCP2','SDB1','SDB2','SDB3')
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(stns[t] %in% a)
          type = 0
        if(stns[t] == "WMS")
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        else
          days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          print("#############################")
          print(startz)
          print("#############################")
          if(startz > nodays)
            startz = nodays
          print("#############################")
          print(startz)
          print("#############################")
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            print(path2Gfinal)
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
