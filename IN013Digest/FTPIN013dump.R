require('RCurl')
require('R.utils')
require('utils')
FIREMAIL = 0
source('/home/admin/CODE/MasterMail/timestamp.R')
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-013X FTP server down",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-013X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(days,path)
{
 	print('Enter function')
  url = "ftp://IN-021X:FUYEWF6FG@61.8.194.114/"
  filenames = try(withTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	print('try success')
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
	  print('Server up and running...')
		serverupmail()
		FIREMAIL<<-0
	}
	recordTimeMaster("IN-013X","FTPProbe")
	if(class(filenames)!='character')
	{
		print('Cant split filename as not character')
		return(0)
	}
	print('Filenames obtained')
  newfilenames = unlist(strsplit(filenames,"\n"))
	print('Filenames split')
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	tmps = newfilenames[grepl("tmp",newfilenames)]
	print(paste('length of tmps',tmps))
	print(paste('newfilenames length before delete',length(newfilenames)))
	if(length(tmps) > 1)
	{
		idxmtchrm = match(tmps,newfilenames)
		newfilenames = newfilenames[-idxmtchrm]
	}
	print(paste('newfilenames length after delete',length(newfilenames)))
	print('Zips found')
	newfilenamesb = newfilenames[!grepl("4-20 mA",newfilenames)]
	newfilenamesb = newfilenamesb[grepl("Tmod",newfilenamesb)]
	newfilenamesa = newfilenames[grepl("Gsi00",newfilenames)]
	newfilenames = newfilenames[grepl("IN-013",newfilenames)]
	newfilenames = c(newfilenames,newfilenamesa,newfilenamesb)
	days2 = unlist(strsplit(days,"\\."))
	seq1 = seq(from = 1,to = length(days2),by=2)
	days2 = days2[seq1]
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	newfilenames2 = newfilenames2[seq1]
#	tail(newfilenames2)
#	head(newfilenames2)
#	tail(days2)
#	head(days2)
	match = match(days2,newfilenames2)
	print(paste('Length of match',length(match)))
	match = match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[-match]
	if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}
	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")),silent = T)
		if(class(file) == 'try-error')
		{
			print(paste('couldnt download',newfilenames[y]))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
  }
	recordTimeMaster("IN-013X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	return(1)
}
