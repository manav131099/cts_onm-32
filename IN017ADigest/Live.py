import requests, json
import requests.auth
import pandas as pd
import time
import pytz
import datetime
import os
import shutil

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Gen 1 Data/[IN-017A]/'
start_path="/home/admin/Start/IN017A.txt"

components={'MFM_1':['MFM',["1-1"],'MFM1'],'INVERTER_1':['Inverter',["1-1-1"],'I1'],'INVERTER_2':['Inverter',["1-1-2"],'I2'],'INVERTER_3':['Inverter',["1-1-3"],'I3']}

if(os.path.exists(start_path)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(start_path, "w") as file:
        file.write("2020-09-07\n00:00:00")   
with open(start_path) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)

start=datetime.datetime.strptime(startdate, "%Y-%m-%d")

data = {"email":"abhishek.rai@cleantechsolar.com","password":"cleantechsolar2020"}	
r = requests.post("https://apis.eaglesunscada.com/api/v1.0/getToken", data=data)
token_json = r.json()  
auth_token=token_json['token']

while(start.date()<datetime.datetime.now(tz).date()):
    date=start.strftime("%Y-%m-%d")
    endpoint = "https://apis.eaglesunscada.com/api/v1.0/report"
    for i in components:
        data_params = {"plantId":"3187","dateFrom":date,"dateTo":date,"idList":components[i][1],"interval":"1","module":"report","device":components[i][0],"role":"user"}
        headers = {"Authorization": "Bearer "+auth_token, 'Content-type': 'application/json'}
        data=requests.post(endpoint, data=json.dumps(data_params), headers=headers).json()
        df=pd.DataFrame(data['message'])
        if(df.empty):
            pass
        else:
            path_temp=path+date[0:4]+'/'+date[0:7]
            chkdir(path_temp+'/'+i+'/')
            cols = df.columns.tolist()
            cols.insert(0, cols.pop(cols.index('Date')))
            df = df.reindex(columns= cols)
            df=df.sort_values(by='Date')
            print(path_temp+'/'+i+'/'+'[IN-017A]-'+components[i][2]+'-'+date+'.txt')
            df.to_csv(path_temp+'/'+i+'/'+'[IN-017A]-'+components[i][2]+'-'+date+'.txt',sep='\t',index=False,mode='w')
        time.sleep(100)
    start=start+datetime.timedelta(days=+1)


while(1):
    try:
        print('Live')
        date=(datetime.datetime.now(tz)).strftime("%Y-%m-%d")
        date_time=datetime.datetime.now(tz)
        data = {"email":"abhishek.rai@cleantechsolar.com","password":"cleantechsolar2020"}	
        r = requests.post("https://apis.eaglesunscada.com/api/v1.0/getToken", data=data)
        token_json = r.json()  
        auth_token=token_json['token']
        endpoint = "https://apis.eaglesunscada.com/api/v1.0/report"
        for i in components:
            data_params = {"plantId":"3187","dateFrom":date,"dateTo":date,"idList":components[i][1],"interval":"1","module":"report","device":components[i][0],"role":"user"}
            headers = {"Authorization": "Bearer "+auth_token, 'Content-type': 'application/json'}
            data=requests.post(endpoint, data=json.dumps(data_params), headers=headers).json()
            try:
                df=pd.DataFrame(data['message'])
                path_temp=path+date[0:4]+'/'+date[0:7]
                chkdir(path_temp+'/'+i+'/')
                cols = df.columns.tolist()
                cols.insert(0, cols.pop(cols.index('Date')))
                df = df.reindex(columns= cols)
                df=df.sort_values(by='Date')
                df.to_csv(path_temp+'/'+i+'/'+'[IN-017A]-'+components[i][2]+'-'+date+'.txt',sep='\t',index=False,mode='w')
            except:
                print('No data')
            time.sleep(100)
        with open(start_path, "w") as file:
            file.write(str(date)+"\n"+str(date_time.time().replace(microsecond=0)))
    except:
        print('json error')
    time.sleep(600)

