rm(list=ls())
library(stringr)
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN019CMailNew.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN019CDigest/summaryFunctions_new.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
reorderStnPaths = c(12,1,4,5,6,7,8,9,10,11,2,3)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
require('lubridate')
source('/home/admin/CODE/IN019CDigest/aggregateInfo_new.R')

METERNICKNAMES = c("MFM","INV-1","INV-2","INV-3","INV-4","INV-5","INV-6",
                   "INV-7","INV-8","INV-9","INV-10","INV-11")

METERACNAMES = c("MFM","Inverter_1","Inverter_2","Inverter_3",
                 "Inverter_4","Inverter_5","Inverter_6","Inverter_7","Inverter_8",
                 "Inverter_9","Inverter_10","Inverter_11")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

COD = as.Date("2016-12-31", format = "%Y-%m-%d")
currDate <- as.Date(format(Sys.time(), "%Y-%m-%d"), format = "%Y-%m-%d")
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-019C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-019C]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}

sendMail= function(pathall)
{
  filenams = c()
  Cur=Sys.Date()
  body = ""
  body = paste(body,"Site Name: Kanchan",sep="")
  body = paste(body,"\n\nLocation: Bhilwara, India ")
  body = paste(body,"\n\nO&M Code: IN-019")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body,"\n\nNumber of Energy Meters: 1")
  body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar / 265 Wp / 2,382")
  body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP 60 / 11")
  body = paste(body,"\n\nSite COD:",COD)
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(currDate - COD))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round(time_length(difftime(currDate, COD), "years"), 1)))
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  yr = substr(Cur,1,4)	
  yrmon = substr(Cur,1,7)
  path1 = "/home/admin/Dropbox/Second Gen/[IN-065L]"
  path2 = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-019C]"
  filename1 = paste("[IN-065L]-WMS3-",Cur,".txt",sep="")
  filename2 = paste("[IN-019C]-MFM-",Cur,".txt",sep="")
  
  pathRead2 = paste(path1,yr,yrmon,"WMS_3",filename1,sep="/")
  pathRead3 = paste(path2,yr,yrmon,"MFM",filename2,sep="/")
  GTIGreater20=NA
  GTI = DNI = NA
  if(file.exists(pathRead3))
  {
    dataread3 = read.table(pathRead3, sep = "\t", header = T)
    if(nrow(dataread3) > 0)
    {
      yield1 = as.numeric(dataread3[1,5])
      yield2 = as.numeric(dataread3[1,6])
      
      if(file.exists(pathRead2))
      {
        dataread2 = read.table(pathRead2, sep = "\t", header = T)
        GTI2 = as.numeric(dataread2[1,3])
        PR1 = round((yield1*100)/GTI2,1)
        PR2 = round((yield2*100)/GTI2,1)
      }else{
        GTI2='NA'
        PR1 = 'NA'
        PR2 = 'NA'
      }
      body = paste(body,"\n\n_____________________________________________\n",sep="")
      body = paste(body,"WMS")
      body  = paste(body,"\n_____________________________________________\n",sep="")
      body = paste(body,"\nIrradiation (From IN-065L): ",GTI2)
      body = paste(body,"\n\nPR-1 [%]: ",PR1)
      body = paste(body,"\n\nPR-2 [%]: ",PR2)
    }
  }
  
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    if(grepl("MFM",pathall[t]))
      type = 1
    if( t > (NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"Inverter_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+1],".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("MFM",pathall[t]))
        {
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
          body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx-2)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),2)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    body = paste(body,"\n\n Yield ",acname[(t+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
    {
      body = paste(body,">>> Inverter outside range")
    }
  }
  body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-019C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-019C","Mail",currday)
}


path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-019C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-019C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-019C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-019C]'

if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-019C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I11-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(stns[t] == "MFM")
        type = 0
      if(stns[t] == "WMS")
        next
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          len = str_length(days[z])
          cur_date = substr(days[z],len-5,len-4)
          if(months[y] == '2020-06')
            if(cur_date < 13)
              next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = getRecipients("IN-019C","m")
  recordTimeMaster("IN-019C","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 12)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(stns[t] == "MFM")
          type = 0
        if(stns[t] == "WMS")
          next
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        # else
        #   days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          if(t==1)
            print('line 424')
          startz = prevz
          if(startz > nodays)
          {
            print(startz)
            print('line 429')
            startz = nodays-1
          }
          for(z in startz : nodays)
          {
            len = str_length(days[z])
            cur_date = substr(days[z],len-5,len-4)
            if(months[y] == '2020-06')
              if(cur_date < 13)
                next
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
              print('line 438')
            }
            if(is.na(days[z]))
            {
              print('line 442')
              next
            }
            pathdays = paste(pathmon1,days[z],sep = "/")
            if(t == 1)
            {
              print('pathdays check')
              print(pathdays)
            }
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            print(path2Gfinalall)
            print(path2Gmon1)
            print(newlen)
            print(t)
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()