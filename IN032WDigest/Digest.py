import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')

#Change these
stn = '[IN-032W]'
irr_stn = 'NA'    #NA if WMS is present
wms_name = 'NA'   #NA if WMS is present
distance = '0'
components=['WMS_1']
meters = {'MFM_1':'MFM'}
MFMCAP = [652.8]
NO_MFM = len(MFMCAP)
inverters = ['INVERTER_1', 'INVERTER_2', 'INVERTER_3', 'INVERTER_4', 'INVERTER_5', 'INVERTER_6', 'INVERTER_7', 'INVERTER_8', 'INVERTER_9', 'INVERTER_10', 'INVERTER_11', 'INVERTER_12', 'INVERTER_13', 'INVERTER_14', 'INVERTER_15']
INVCAP = [38.4, 38.4, 38.4, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8, 44.8]
user = 'admin'
cod_date = '2017-11-13'
recipients = ['operationsNorthIN@cleantechsolar.com','om-it-digest@cleantechsolar.com']
Site_Name = 'Varun Beverages'
Location = 'Greater Noida, India'
brands = "Module Brand / Model / Nos: JA Solar / 320 Wp / 2040" + "\n\n" + "Inverter Brand / Model / Nos: Huawei / 36KTL / 15" + "\n\n"
budget_pr=75.7
rate=0.008
inv_limit=3
mfm_limit=3

path_read = '/home/admin/Dropbox/Gen 1 Data/' + stn
startpath = '/home/' + user + '/Start/MasterMail/'
path_write_2g = '/home/' + user + '/Dropbox/Second Gen/' + stn
path_write_3g = '/home/' + user + '/Dropbox/Third Gen/' + stn
path_write_4g = '/home/' + user + '/Dropbox/Fourth_Gen/' + stn + '/' + stn + '-lifetime.txt'

#Creating directories if not present
chkdir(path_write_2g)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

#Ordering Components in the series WMS, MFM, Inverters. Done since PR requires GHI to be updated first.


for i in os.listdir(path_read + '/' + start[:4] + '/' + start[:7]):
  if 'WMS' in i:
    components.append(i)

for i in meters:
  components.append(i)

for i in inverters:
  components.append(i)

print(components)

#Creating dataframes for respective components
def create_template(Type, date):
  if(Type == 'INV'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'IA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'LastRead', 'LastTime', 'IA'])
  elif(Type == 'WMS'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'GHI':'NA', 'Tmod':'NA'}, columns =['Date', 'DA', 'GHI', 'Tmod'])
  elif(Type == 'MFM'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'PR1':'NA', 'PR2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'GA':'NA', 'PA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'PR1', 'PR2', 'LastRead', 'LastTime', 'GA', 'PA'])
  return df_template

#Generating site information for Mail
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

#Getting Irradiance Value
def get_irr(date, irr_stn):
  irr_data={'GHI':'NA', 'DA':'NA'}
  gen2 = '/home/admin/Dropbox/Second Gen/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + irr_stn + ' ' + date + '.txt'
  gen1 = '/home/admin/Dropbox/Cleantechsolar/1min/[711]/' + date[:4] + '/' + date[:7] + '/[711] ' + date + '.txt'
 
  if(os.path.exists(gen2)):
    df = pd.read_csv(gen2, sep = '\t')
    irr_data['GHI'] = df['Gsi02'][0]
    irr_data['DA'] = df['DA'][0]
  
  if(os.path.exists(gen1)):
    df = pd.read_csv(gen1, sep = '\t')
    GTIGreater20 = df[['AvgGsi00-02', 'Tm']]
    GTIGreater20 = GTIGreater20[::5]
    GTIGreater20 = GTIGreater20[GTIGreater20['AvgGsi00-02']>20]['Tm']
  else:
    GTIGreater20 = pd.DataFrame({'Tm' : []})
  GTIGreater20 = GTIGreater20.rename("Date")
  return irr_data, GTIGreater20

def add_graphs(stn,budget_pr,rate,cod,inv_limit,date,start):
  os.system(" ".join(["Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R",stn[1:-2],str(budget_pr),str(rate),start,date]))
  os.system(" ".join(["python3 /home/admin/CODE/EmailGraphs/Inverter_CoV_Graph.py",stn[1:-2],date,str(inv_limit),start,'" "','"Inverter CoV"']))
  os.system(" ".join(["python3 /home/admin/CODE/EmailGraphs/Meter_CoV_Graph.py",stn[0][0:-1],date,str(mfm_limit)]))
  meter_graph_path='/home/admin/Graphs/Graph_Output/'+stn[0][0:-1]+'/['+stn[0][0:-1]+'] Graph '+date+' - Meter CoV.pdf'
  meter_graph_extract_path='/home/admin/Graphs/Graph_Extract/'+stn[0][0:-1]+'/['+stn[0][0:-1]+'] Graph '+date+' - Meter CoV.txt'
  pr_graph_path='/home/admin/Graphs/Graph_Output/'+stn[1:-2]+'/['+stn[1:-2]+'] Graph '+date+' - PR Evolution.pdf'
  pr_graph_extract_path='/home/admin/Graphs/Graph_Extract/'+stn[1:-2]+'/['+stn[1:-2]+'] Graph '+date+' - PR Evolution.txt'
  inv_graph_path='/home/admin/Graphs/Graph_Output/'+stn[1:-2]+'/['+stn[1:-2]+'] Graph '+date+' - Inverter CoV.pdf'
  inv_graph_extract_path='/home/admin/Graphs/Graph_Extract/'+stn[1:-2]+'/['+stn[1:-2]+'] Graph '+date+' - Inverter CoV.txt'
  return [inv_graph_path,inv_graph_extract_path] 

#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#checking Stn_Mail.txt
if(os.path.exists(startpath + stn[1:8] + "_Mail.txt")):
  print('Exists')
else:
  try:
    shutil.rmtree(path_write_2g, ignore_errors=True)
    shutil.rmtree(path_write_3g, ignore_errors=True)
    os.remove(path_write_4g)
  except Exception as e:
    print(e)
  with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "_Mail.txt") as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def process(date, stn):
  file_path=[]
  inv_ylds=[]
  Dyield = ''
  Dia = ''
  
  digest_body = site_info(date, Site_Name, Location, stn[1:7], sum(MFMCAP), NO_MFM, cod_date)
  
  #If WMS is not present
  if(irr_stn != 'NA'):
    irr_data, GTIGreater20 = get_irr(date, irr_stn)
    df_2g = pd.DataFrame({'Date': [date], 'DA': [irr_data['DA']], 'GHI' : [irr_data['GHI']]}, columns =['Date', 'DA', 'GHI'])
    GHI = df_2g['GHI'][0]
    digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI [kWh/m^2] (From ' + irr_stn + '): ' + str(df_2g['GHI'][0]) + "\n\n"
    df_merge = df_2g.copy()
    cols_4g = ['Date', 'WMS.DA', 'WMS.GHI']
  else:
    cols_4g = ['Date']

  for i in components:
    chkdir(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      df_3g = pd.read_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', sep='\t')
      
    if(os.path.exists(path_write_4g)):
      df_4g = pd.read_csv(path_write_4g, sep='\t')
      
    if('INV' in i):
      if len(inverters) < 9:	
        file_name = i[0] + i[9]	
      else:	
        file_name = i[0] + i[9:11]
      df_2g = create_template('INV', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        df_2g['DA'] = round(len(df['Total Power AC'].dropna())/2.88, 1)
        df_2g['Eac1'] = round((df['Total Power AC'].sum())/12, 2)
        df_2g['Yld1'] = round((df_2g['Eac1'][0]/INVCAP[int(file_name[1:])-1]),2)
        
        if len(df['Total Energy'].dropna()) > 0:
          dataint = df.dropna(subset=['Total Energy'])
          df_2g['Eac2'] = round((dataint['Total Energy'].tail(1).values[0] - dataint['Total Energy'].head(1).values[0])/100,2)
          df_2g['Yld2'] = round((dataint['Total Energy'].tail(1).values[0] - dataint['Total Energy'].head(1).values[0])/(INVCAP[int(file_name[1:])-1]*100),2)
          df_2g['LastRead'] = round((dataint['Total Energy'].dropna().tail(1).values[0])/100, 2)
          df_2g['LastTime'] = dataint['Date'].tail(1).values[0]
   
        if len(GTIGreater20)>0:
          if len(df[df['Frequency']>40])>0 and 'Frequnecy' in df.columns.tolist():
            FrequencyGreaterthan40 = df[df['Frequency']>40]['Date']
            common = pd.merge(GTIGreater20, FrequencyGreaterthan40, how='inner')
          DCPowerGreater2 = df[df['Total Power AC']>0]['Date']
          common2 = pd.merge(common, DCPowerGreater2, how='inner')
          if len(common) > 0:
            df_2g['IA'] = round((len(common2)*100/len(common)), 1)
        if(df_2g['Eac2'][0]=='NA'):
            pass
        else:
            inv_ylds.append(df_2g['Yld2'][0])
  
      Dyield +=  '\n\nYield INV-' + i[9:] + ': ' + str(df_2g['Yld2'][0])
      Dia += '\n\nInverter Availability INV-' + i[9:] + ' [%]: ' + str(df_2g['IA'][0])
        
    elif('WMS' in i):
      file_name = i[:3] + i[4]
      df_2g = create_template('WMS', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['Irradiance_Avg'].dropna())/2.88, 1)
        
        if len(df['Irradiance_Avg'].dropna()) > 0:
          GHI = round(((df['Irradiance_Avg'].dropna()).sum())/12000, 2)
          df_2g['GHI'] = GHI
        else:
          GHI = 'NA'
                
        GTIGreater20 = df[df['Irradiance_Avg']>20]['Date']
       
      digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI [kWh/m^2]: ' + str(df_2g['GHI'][0]) + '\n\nAvg Tmod [C]: ' + str(df_2g['Tmod'][0]) + '\n\n'
      
    elif('MFM' in i):
      if NO_MFM < 9:
        file_name = i[:3] + i[4]
      else:
        file_name = i[:3] + i[4:6]
      df_2g = create_template('MFM', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', sep='\t')
        
        df_2g['DA'] = round(len(df['Total Power AC'].dropna())/2.88, 1)
        df_2g['Eac1'] = round(df['Total Power AC'].sum()/12,2)
        df_2g['Yld1'] = round((df['Total Power AC'].sum()/(MFMCAP[int(file_name[3:])-1]*12)),2)
        
        if len(df['Active Energy Delivered'].dropna())>0:
          dataint = df.dropna(subset=['Active Energy Delivered'])
          dataint = dataint[dataint['Active Energy Delivered']>0]
          df_2g['Eac2'] = round((dataint['Active Energy Delivered'].tail(1).values[0] - dataint['Active Energy Delivered'].head(1).values[0])/1000,2)
          df_2g['Yld2'] = round(((dataint['Active Energy Delivered'].tail(1).values[0] - dataint['Active Energy Delivered'].head(1).values[0])/MFMCAP[int(file_name[3:])-1])/1000,2)
          df_2g['LastRead'] = round((dataint['Active Energy Delivered'].dropna().tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['Date'].tail(1).values[0]
        
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1'] = round((df_2g['Yld1'][0]*100)/GHI,1)
          
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2'] = round((df_2g['Yld2'][0]*100)/GHI,1)
        
        if len(GTIGreater20)>0:
          FrequencyGreater = df[df['Frequency']>40]['Date']
          PowerGreater = df[df['Total Power AC']>2]['Date']
          common = pd.merge(GTIGreater20, FrequencyGreater, how='inner')
          common2 = pd.merge(common, PowerGreater, how='inner')
          if len(common)>0:
            df_2g['GA'] = round((len(common)*100/len(GTIGreater20)),1)
            df_2g['PA'] = round((len(common2)*100/len(common)),1)
          
      digest_body += '------------------------------\n' + meters[i.strip()] + '\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nSize [kWp]: ' + str(MFMCAP[int(file_name[3:])-1]) + '\n\nEAC method-1 (Pac) [kWh]: ' + str(df_2g['Eac1'][0]) + '\n\nEAC method-2 (Eac) [kWh]: ' + str(df_2g['Eac2'][0]) + '\n\nYield-1 [kWh/kWp]: ' + str(df_2g['Yld1'][0]) + '\n\nYield-2 [kWh/kWp]: ' + str(df_2g['Yld2'][0]) + '\n\nPR-1 (GHI) [%]: ' + str(df_2g['PR1'][0]) + '\n\nPR-2 (GHI) [%]: ' + str(df_2g['PR2'][0]) + '\n\nLast recorded value [kWh]: ' + str(df_2g['LastRead'][0]) + '\n\nLast recorded time: ' + str(df_2g['LastTime'][0]) + '\n\nGrid Availability [%]: ' + str(df_2g['GA'][0]) + '\n\nPlant Availability [%]: ' + str(df_2g['PA'][0]) + '\n\n'      
    
    #Saving 2G 
    df_2g.to_csv(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', na_rep='NA', sep='\t',index=False)
    file_path.append(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
    
    #Saving 3G
    chkdir(path_write_3g + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      if((df_3g['Date'] == date).any()):
        vals = df_2g.values.tolist()
        df_3g.loc[df_3g['Date'] == date,df_3g.columns.tolist()] = vals[0]
        df_3g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='w', index=False, header=True)
      else:
        df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=False)
    else:
      df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=True) #First time
    
    
    #If WMS is present
    if("WMS" in i and irr_stn == 'NA'):
      df_merge = df_2g.copy()
    else:
      df_merge = df_merge.merge(df_2g, on = 'Date')
      
    cols_4g = cols_4g + [i + '.' + n for n in df_2g.columns.tolist()[1:]] 
  df_merge.columns = cols_4g
  
  #Saving 4G
  if not os.path.exists(path_write_4g):
    print('firsttime')
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=True)
  elif((df_4g['Date'] == date).any()):
    vals = df_merge.values.tolist()
    df_4g.loc[df_4g['Date']== date, df_merge.columns.tolist()] = vals[0]
    df_4g.to_csv(path_write_4g, sep = '\t', mode='w', index=False, header=True)
  else:
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=False)
  print('4G Done')
  std=np.std(inv_ylds)
  cov=(std*100/np.mean((inv_ylds)))
  digest_body += '------------------------------\nInverters\n------------------------------' + Dyield +'\n\nStdev/COV Yields: ' +str(round(std,1))+' / '+str(round(cov,1))+'%'+Dia 
  
  return [digest_body, file_path]

#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Historical Processing!')
  processed_data = process(str(startdate)[:10], stn)
  if(bot_type == 'Mail'):
    graph_paths=add_graphs(stn,budget_pr,rate,cod_date,inv_limit,str(startdate)[:10],start)
    processed_data[1]=graph_paths+processed_data[1]
    send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
  date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
  with open(startpath + stn[1:8] + "_Bot.txt", "w") as file:
    file.write(timenow)   
  processed_data = process(date, stn)
  print('Done Processing')
  if(datetime.datetime.now(tz).hour==1 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
    print('Sending')
    date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
    processed_data = process(date_yesterday, stn)
    graph_paths=add_graphs(stn,budget_pr,rate,cod_date,inv_limit,date_yesterday,start)
    processed_data[1]=graph_paths+processed_data[1]
    send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
    with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
        file.write(timenow + "\n" + date_yesterday)   
    startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
  print('Sleeping')
  time.sleep(300)