from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib
import re 
import os
import numpy as np
import gzip
import shutil
import logging
import sqlalchemy as sa
import urllib.request

def azure_push(azure_df):
    params = urllib.parse.quote_plus("DRIVER={ODBC Driver 17 for SQL Server};SERVER=cleantechsolar.database.windows.net;DATABASE=Cleantech Meter Readings;UID=RohanKN;PWD=R@h@nKN1")
    engine = sa.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    azure_df.to_sql("RawData",  con=engine, if_exists="append",index=False,schema="IN-032")

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

def write(Type,cols,path_temp,df,date):
    df.columns=cols
    date=str(date)
    chkdir(path_temp+'/'+Type)
    fileType=Type
    if(Type[0:3]=='INV'):
        fileType='I'+Type.split('_')[1]
    elif(Type[0:3]=='MFM'):
        fileType='MFM'+Type.split('_')[1]
    elif(Type[0:3]=='WMS'):
        fileType='WMS'+Type.split('_')[1]
    df['Date']=pd.to_datetime(df['Date'])
    if(os.path.exists(path_temp+'/'+Type+'/'+'[IN-032W]-'+fileType+'-'+date[0:10]+'.txt')):
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-032W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=False,mode='a')
    else:
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-032W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=True,mode='a')

def preprocess(df,Type,cols):
    df.columns=cols
    df.iloc[:, 1:] = df.iloc[:, 1:].apply(pd.to_numeric)
    if(Type[0:3]=='INV'):
        df['Total Power AC']=df['Total Power AC']/1000
        df['Total Power DC']=df['Total Power DC']/1000
        if('Frequency' in df.columns.tolist()):
           df['Frequency']=df['Frequency']/100
    return df 

def filter_files(names,file_name):
    return [k for k in names if file_name in k]

path='/home/admin/Dropbox/Gen 1 Data/[IN-032W]/'
startpath="/home/admin/Start/IN032W.txt"
logging.basicConfig(filename='/home/pranav/LogsIN0032.txt')

chkdir(path)
tz = pytz.timezone('Asia/Kolkata')
curr=datetime.datetime.now(tz)

print("Start time is",curr)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com', timeout=60)
ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
ftp.cwd('DATA/MODBUS')
mfmcols=['Date','Current A','Current B','Current C','Current N','Current G','Current Avg','Voltage A','Voltage B','Voltage C','Voltage Avg','Voltage A-N','Voltage B-N','Voltage C-N','Voltage L-N','Active Power A','Active Power B','Active Power C','Total Power AC','Reactive Power A','Reactive Power B','Reactive Power C','Reactive Power Total','Apparent Power A','Apparent Power B','Apparent Power C','Apparent Power Total','Power Factor A','Power Factor B','Power Factor C','Power Factor Total','Frequency','Active Energy Delivered','Active Energy Received','Active Energy Delivered + Received','Active Energy Delivered - Received','Reactive Energy Delivered','Reactive Energy Received','Reactive Energy Delivered + Received','Reactive Energy Delivered - Received','Apparent Energy Delivered','Apparent Energy Received','Apparent Energy Delivered + Received','Apparent Energy Delivered - Received']
wmscols=['Date','Irradiance_Min','Irradiance_Max','Irradiance_Avg','Temp_1','Temp_2','Temp_3','Temp_4','Temp_5','Temp_6','Temp_7','Temp_8','Temp_9','Temp_10','Temp_11','Temp_12','Temp_13','Temp_14','Temp_15','Temp_16','Temp_17']
#invcols=['Date','Active Power Total','Output Reactive Power','Total Energy','Grid AB Line Voltage','Grid BC Line Voltage','Grid CA Line Voltage','Grid A Line Current','Grid B Line Current','Grid C Line Current','Frequency','Power Factor','Cabinet Temperature','MPPT1 DC Power','MPPT2 DC Power','MPPT3 DC Power','Inverter Status','PV1 Voltage/PV1','PV1 Current/PV1','PV2 Voltage/PV2','PV2 Current/PV2','PV3 Voltage/PV3','PV3 Current/PV3','PV4 Voltage/PV4','PV4 Current/PV4','PV5 Voltage/PV5','PV5 Current/PV5','PV6 Voltage/PV6','PV6 Current/PV6','E-day','Energy yield collection time previous day','Energy yield previous day','Energy yield collection time previous month','Energy yield previous month','Energy yield collection previous year','Energy yield previous year','PV7 Voltage/PV7','PV7 Current/PV7','PV8 Voltage/PV8','PV8 Current/PV8','Total Power DC']
invcols=['Date','Total Power AC','Output Reactive Power','Total Energy','Voltage A','Voltage B','Voltage C','Current A','Current B','Current C','Frequency','Power Factor','Cabinet Temperature','MPPT1 DC Power','MPPT2 DC Power','MPPT3 DC Power','Inverter Status','PV1 Voltage/PV1','PV1 Current/PV1','PV2 Voltage/PV2','PV2 Current/PV2','PV3 Voltage/PV3','PV3 Current/PV3','PV4 Voltage/PV4','PV4 Current/PV4','PV5 Voltage/PV5','PV5 Current/PV5','PV6 Voltage/PV6','PV6 Current/PV6','E-day','Energy yield collection time previous day','Energy yield previous day','Energy yield collection time previous month','Energy yield previous month','Energy yield collection previous year','Energy yield previous year','PV7 Voltage/PV7','PV7 Current/PV7','PV8 Voltage/PV8','PV8 Current/PV8','Total Power DC']
components={'WD00CC2C':{1:['MFM_1',mfmcols,44],2:['INVERTER_1',invcols,41],3:['INVERTER_2',invcols,41],4:['INVERTER_3',invcols,41],5:['INVERTER_4',invcols,41],6:['INVERTER_5',invcols,41],7:['INVERTER_6',invcols,41],8:['INVERTER_7',invcols,41],9:['INVERTER_8',invcols,41],10:['INVERTER_9',invcols,41],11:['INVERTER_10',invcols,41],12:['INVERTER_11',invcols,41],13:['INVERTER_12',invcols,41],14:['INVERTER_13',invcols,41],15:['INVERTER_14',invcols,41],16:['INVERTER_15',invcols,41],17:['WMS_1',wmscols,10]},
'WD00CC2C_IO':{1:['WMS_1',wmscols,21]}}
parameters=['Date','Type','Current A','Current B','Current C','Voltage A','Voltage B','Voltage C','Total Power AC','Total Power DC','Frequency']

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date[0:6]+'20'+date[6:], '%d/%m/%Y-%H:%M:%S')
        return 1
    except:
        return 0


if(os.path.exists(startpath)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath, "w") as file:
        file.write("2020-09-08\n00:00:00")   
with open(startpath) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)
print(startdate)
print(starttime)

for c in components:
    if(c=='WD00CC2C_IO'):
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
        ftp.cwd('DATA/IO')
        files=ftp.nlst() 
        print('Doing WMS')
    else:
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
        ftp.cwd('DATA/MODBUS')
        files=ftp.nlst() 
    start=startdate+starttime
    start=start.replace('\n','')
    start=datetime.datetime.strptime(start, "%Y-%m-%d%H:%M:%S")
    end=datetime.datetime.now(tz).replace(tzinfo=None)-datetime.timedelta(minutes=1)
    while(start<end):
        try:
            files_temp=filter_files(files,c)
            start_str=start.strftime("%Y%m%d_%H%M")
            start_str='_'+start_str[2:]
            for i in sorted(files_temp):
                if start_str in i:
                    print(start_str)
                    path_temp=path+'20'+start_str[1:3]+'/'+'20'+start_str[1:3]+'-'+start_str[3:5]
                    chkdir(path_temp)
                    if(c=='WD00CC2C_IO'):
                        req = urllib.request.Request('ftp://IN-032W:JfYF8sDxR7fuSMBu@ftpnew.cleantechsolar.com/DATA/IO/'+i)
                    else:
                        req = urllib.request.Request('ftp://IN-032W:JfYF8sDxR7fuSMBu@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i)
                    with urllib.request.urlopen(req) as response:
                        s =  gzip.decompress(response.read())
                    cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
                    df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                    azure_df=pd.DataFrame()
                    for index, row in df.iterrows():
                        if(row[1]=='ADDRMODBUS'):
                            val=row[3]
                        elif(row[1]=='TypeIO'):
                            val=1
                        if(chk_date(row[1])):
                            data=row.to_frame().T.iloc[:,:components[c][int(val)][2]]
                            date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                            if(date.year<2015):
                                continue
                            data[1]=date.replace(second=0)
                            if(c=='WD00CC2C_IO'):#WMS Factor Multiplication
                                data[4]=(1.832*float(data[4].values[0]))- 374.988
                            write(components[c][int(val)][0],components[c][int(val)][1],path_temp,data,date)
            start=start+datetime.timedelta(minutes=1)
        except Exception as e:
            print(e)
            start=start+datetime.timedelta(minutes=1)
            logging.exception("His")
            pass
ftp.close()

print('Historical Done!')
flag=[0,0]
files_sorted=[]
while(1):
    try:
        time_now=datetime.datetime.now(tz)
        print('Live',time_now)
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
        print('Connected!')
        cnt=0
        new_sorted=[]
        for c in sorted(components):#sort to preserve order
            if(c=='WD00CC2C_IO'):
                ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
                ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
                print('Connected!')
                ftp.cwd('DATA/IO')
                files=ftp.nlst() 
            else:
                ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
                ftp.login(user='IN-032W', passwd = 'JfYF8sDxR7fuSMBu')
                print('Connected!')
                ftp.cwd('DATA/MODBUS')
                files=ftp.nlst() 
            if(flag[cnt]==0):
                start=time_now
                files_temp=filter_files(files,c)
                files_sorted.append(sorted(files_temp,reverse=True))
                new_file=[files_sorted[cnt][0]]
                flag[cnt]=1
                print(c)
                time.sleep(1)
            else:
                files_temp=filter_files(files,c)  
                new_sorted.append(sorted(files_temp,reverse=True))
                with open("/home/admin/Start/MasterMail/IN-032W_FTPProbe.txt", "w") as file:
                    file.write(str(time_now.replace(microsecond=0)))
                asd=new_sorted[cnt]
                new_file=list(set(new_sorted[cnt]) - set(files_sorted[cnt]))
                files_sorted[cnt]=new_sorted[cnt]
            for i in sorted(new_file):
                print(flag)
                print(new_file)
                time.sleep(2)
                date=i.split('_')[2]
                path_temp=path+'20'+date[0:2]+'/'+'20'+date[0:2]+'-'+date[2:4]
                chkdir(path_temp)
                if(c=='WD00CC2C_IO'):
                    url='ftp://IN-032W:JfYF8sDxR7fuSMBu@ftpnew.cleantechsolar.com/DATA/IO/'+i
                else:
                    url='ftp://IN-032W:JfYF8sDxR7fuSMBu@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i
                try:
                    response = urllib.request.urlopen(url, timeout=120).read()
                except:
                    print('Fail')
                else:
                    logging.info('Access successful.')
                s =  gzip.decompress(response)
                cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                azure_df=pd.DataFrame()
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS'):
                        val=row[3]
                    elif(row[1]=='TypeIO'):
                        val=1
                    if(chk_date(row[1])):
                        data=row.to_frame().T.iloc[:,:int(components[c][int(val)][2])]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        data[1]=date.replace(second=0)
                        if(c=='WD00CC2C_IO'):#WMS Factor Multiplication
                            data[4]=(1.832*float(data[4].values[0]))- 374.988
                        write(components[c][int(val)][0],components[c][int(val)][1],path_temp,data,date)
                        if(date.year<2015):
                            continue
                        with open(startpath, "w") as file:
                            file.write(str(time_now.date())+"\n"+str(time_now.time().replace(microsecond=0)))
                        with open("/home/admin/Start/MasterMail/IN-032W_FTPNewFiles.txt", "w") as file:
                            file.write(str(time_now.replace(microsecond=0)))
                print('Written!')
            cnt=cnt+1
        ftp.close()
        print('Sleeping')
    except:
        logging.exception('Main Failed')
    time.sleep(10800)

    