rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN047LHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/IN047LDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(9,11,5,6,7,8)

if(RESETHISTORICAL)
{
  system("rm -R \"/home/admin/Dropbox/Second Gen/[IN-047L]\"")
  system("rm -R \"/home/admin/Dropbox/Third Gen/[IN-047L]\"")
  system("rm -R /home/admin/Dropbox/Fourth_Gen/[IN-047L]")
}

path = "/home/admin/Dropbox/Gen 1 Data/[IN-047L]"
pathwrite2G = "/home/admin/Dropbox/Second Gen/[IN-047L]"
pathwrite3G = "/home/admin/Dropbox/Third Gen/[IN-047L]"
pathwrite4G = "/home/admin/Dropbox/Fourth_Gen/[IN-047L]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 4 : length(years))
{
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 7 : length(months))
  {
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    print(length(stns))
    for(z in 1 : length(stns))
    {
      type = 1
      pathstn = paste(pathmon,stns[z],sep="/")
      if(grepl("MFM_1",stns[z]))
        type = 0
      if(grepl("WMS_1",stns[z]))
        type = 2
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()
