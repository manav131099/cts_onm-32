import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"
											
def authenticate():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "shravan1994@gmail.com","password" : "welcome",
                 "client_id" : clientid,"client_secret" : clientsecret}
    												
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"]   	

while True:
    try:
        a=authenticate()
    except:
        print("Authentication API Failed!")
        time.sleep(5)
        continue
    break  	

def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

strtmnth=str(monthdelta(datetime.datetime.now().date(),-1))
tz = pytz.timezone('Asia/Kolkata')					 
stations = [[] for _ in range(3)]
siteId = 3795014
componenturl ="https://api.locusenergy.com/v3/sites/"+ str(siteId) +"/components"
while True:
    try:
        r = requests.get(componenturl, headers={'Authorization': "Bearer "+a})
        data2=r.json()
        temp=data2['components']
    except:
        print("Component API Failed!")
        time.sleep(5)
        continue
    break  
for i in data2['components']:
    if(i['nodeType']=='METER'):
        stations[0].append(i['id'])
    elif(i['nodeType']=='INVERTER'):
        stations[1].append(i['id'])
    elif(i['nodeType']=='WEATHERSTATION'):
        stations[2].append(i['id'])

order = [9,0,1,2,3,4,5,6,7,8,10,11,12,13]  
stations[1] = [stations[1][i] for i in order]
stations[0] = [2566433, 2669746, 2669748]
stations[1] = [2669754,2566434, 2566435, 2566436,2669747,2669749,2669750,2669751,2669752,2669753,2669755,2669756,2669757,2669758]

cols="""AphA_avg,AphA_max,AphA_min,AphB_avg,AphB_max,AphB_min,AphC_avg,AphC_max,AphC_min,DHI_m_avg,DHIh_m_sum,DNI_m_avg,DNIh_m_sum,GHI_m_avg,GHI_m_max,GHI_m_min,
    GHIh_m_sum,Hz_avg,Hz_max,Hz_min,PF_avg,PF_max,PF_min,PI_m,PPVphAB_avg,PPVphAB_max,PPVphAB_min,PPVphBC_avg,PPVphBC_max,PPVphBC_min,PPVphCA_avg,PPVphCA_max,PPVphCA_min,
    PhVphA_avg,PhVphA_max,PhVphA_min,PhVphB_avg,PhVphB_max,PhVphB_min,PhVphC_avg,PhVphC_max,PhVphC_min,THDA%_avg,THDA%_max,THDA%_min,THDV%_avg,THDV%_max,THDV%_min,TotVARhExp_max,
    TotVARhImp_max,TotVAhExp_avg,TotVAhExp_max,TotVAhExp_min,TotVAhImp_avg,TotVAhImp_max,TotVAhImp_min,TotWhExp_max,TotWhImp_max,VA_avg,VA_max,VA_min,VAR_avg,VAR_max,VAR_min,VARphA_avg,
    VARphA_max,VARphA_min,VARphB_avg,VARphB_max,VARphB_min,VARphC_avg,VARphC_max,VARphC_min,VAphA_avg,VAphA_max,VAphA_min,VAphB_avg,VAphB_max,VAphB_min,VAphC_avg,VAphC_max,W_avg,VAphC_min
    ,W_max,W_min,W_m_avg,W_no_avg,W_no_min,W_no_max,Wh_sum,Wh_m_sum,Wh_no_sum,WphA_avg,WphA_max,WphA_min,WphB_avg,WphB_max,WphB_min,WphC_avg,WphC_max,WphC_min,DCA1_avg,
    DCA1_max,DCA1_min,DCA2_avg,DCA2_max,DCA2_min,DCV1_avg,DCV1_max,DCV1_min,DCV2_avg,DCV2_max,DCV2_min,DCW_avg,DCW_max,DCW_min,DCW01_avg,DCW01_max,DCW01_min,DCW02_avg,DCW02_max,
    DCW02_min,HoursRun_avg,HoursRun_max,HoursRun_min,InvEff_avg,InvEff_max,InvEff_min,Tmp_avg,Tmp_max,Tmp_min,Humidity_avg,Humidity_max,Humidity_min,TmpAmb_avg,TmpAmb_max,TmpAmb_min,POAI_avg,
    POAI_max,POAI_min,POAIh_sum,TmpBOM_avg,TmpBOM_max,TmpBOM_min"""

ogcols=['ts','id','AphA_avg', 'AphA_max', 'AphA_min', 'AphB_avg', 'AphB_max', 'AphB_min', 'AphC_avg', 'AphC_max', 'AphC_min', 'DHI_m_avg', 'DHIh_m_sum', 'DNI_m_avg', 'DNIh_m_sum', 'GHI_m_avg', 'GHI_m_max', 'GHI_m_min',
    'GHIh_m_sum', 'Hz_avg', 'Hz_max', 'Hz_min', 'PF_avg', 'PF_max', 'PF_min', 'PI_m', 'PPVphAB_avg', 'PPVphAB_max', 'PPVphAB_min', 'PPVphBC_avg', 'PPVphBC_max', 'PPVphBC_min', 'PPVphCA_avg', 'PPVphCA_max', 'PPVphCA_min',
    'PhVphA_avg', 'PhVphA_max', 'PhVphA_min', 'PhVphB_avg', 'PhVphB_max', 'PhVphB_min', 'PhVphC_avg', 'PhVphC_max', 'PhVphC_min', 'THDA%_avg', 'THDA%_max', 'THDA%_min', 'THDV%_avg', 'THDV%_max', 'THDV%_min', 'TotVARhExp_max', 
    'TotVARhImp_max', 'TotVAhExp_avg', 'TotVAhExp_max', 'TotVAhExp_min', 'TotVAhImp_avg', 'TotVAhImp_max', 'TotVAhImp_min', 'TotWhExp_max', 'TotWhImp_max', 'VA_avg', 'VA_max', 'VA_min', 'VAR_avg', 'VAR_max', 'VAR_min', 'VARphA_avg',
    'VARphA_max', 'VARphA_min', 'VARphB_avg', 'VARphB_max', 'VARphB_min', 'VARphC_avg', 'VARphC_max', 'VARphC_min', 'VAphA_avg', 'VAphA_max', 'VAphA_min', 'VAphB_avg', 'VAphB_max', 'VAphB_min', 'VAphC_avg', 'VAphC_max', 'VAphC_min',
    'W_avg', 'W_max', 'W_min', 'W_m_avg', 'W_no_avg', 'W_no_min', 'W_no_max', 'Wh_sum', 'Wh_m_sum', 'Wh_no_sum', 'WphA_avg', 'WphA_max', 'WphA_min', 'WphB_avg', 'WphB_max', 'WphB_min', 'WphC_avg', 'WphC_max', 'WphC_min', 'DCA1_avg',
    'DCA1_max', 'DCA1_min', 'DCA2_avg', 'DCA2_max', 'DCA2_min', 'DCV1_avg', 'DCV1_max', 'DCV1_min', 'DCV2_avg', 'DCV2_max', 'DCV2_min', 'DCW_avg', 'DCW_max', 'DCW_min', 'DCW01_avg', 'DCW01_max', 'DCW01_min', 'DCW02_avg', 'DCW02_max', 
    'DCW02_min', 'HoursRun_avg', 'HoursRun_max', 'HoursRun_min', 'InvEff_avg', 'InvEff_max', 'InvEff_min', 'Tmp_avg', 'Tmp_max', 'Tmp_min', 'Humidity_avg', 'Humidity_max', 'Humidity_min', 'TmpAmb_avg', 'TmpAmb_max', 'TmpAmb_min', 'POAI_avg',
    'POAI_max', 'POAI_min', 'POAIh_sum', 'TmpBOM_avg', 'TmpBOM_max', 'TmpBOM_min']

startpath="/home/admin/Start/"
path="/home/admin/Dropbox/Gen 1 Data/[IN-048L]"

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
if(os.path.exists(startpath+"IN048L.txt")):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath+"IN048L.txt", "w") as file:
        file.write(strtmnth+"\n00:00:00")   
with open(startpath+"IN048L.txt") as f:
    startdate = f.readline(10)

chkdir(path)
print(("Startdate is ",startdate))
stn='[IN-048L]'
start=startdate
start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
final=start
end=datetime.datetime.now(tz).date()
tot=end-start
tot=tot.days
historicalflag=0
print("Bot Started!")
for key,i in enumerate(stations):
    while True:
        try:
            a=authenticate()
        except:
            print("Authentication API Failed!")
            time.sleep(5)
            continue
        break 
    for key2,j in enumerate(i):
        start=startdate
        start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
        for k in range(0,tot):
            historicalflag=1
            end=start+datetime.timedelta(days=1)
            start2=str(start)
            temppath=path+'/'+start2[0:4]+'/'+start2[0:7]
            chkdir(temppath)
            if(key==0):
                path2=temppath+'/MFM'+'_'+str(key2+1)
                chkdir(path2)
                componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz=Asia/Calcutta&gran=5min&fields="+cols
            elif(key==1):
                path2=temppath+'/INVERTER'+'_'+str(key2+1)
                chkdir(path2)
                componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz=Asia/Calcutta&gran=5min&fields="+cols
            else:
                path2=temppath+'/WMS'+'_'+str(key2+1)
                chkdir(path2)
                componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz=Asia/Calcutta&gran=5min&fields="+cols
            while True:
                try:
                    r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                    data4=r.json()
                    dataframe=pd.DataFrame(data4['data'])
                except:
                    print("Historical Data API Failed!")
                    time.sleep(5)
                    continue
                break      
            newdf=pd.DataFrame(columns=ogcols)      
            cols1=dataframe.columns.tolist()
            for i in cols1:
                if i in ogcols:
                    newdf[i]=dataframe[i]
            newdf['ts']=newdf['ts'].str.replace('T',' ')
            newdf.ts=newdf.ts.str[0:19]
            if(key==0):
                newdf.to_csv(path2+'/'+stn+'-'+'MFM'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
            elif(key==1):
                newdf.to_csv(path2+'/'+stn+'-'+'I'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
            else:
                newdf.to_csv(path2+'/'+stn+'-'+'WMS'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
            start=start+datetime.timedelta(days=1)
            final=start

if(historicalflag==0):
    print("Historical Skipped!")
else:
    print("Historical Done!") 

def getcompid(stnname):
    l=stnname.split('_')
    if(l[0]=='INVERTER'):
        return stations[1][int(l[1])-1]
    elif(l[0]=='MFM'):
        return stations[0][int(l[1])-1]
    elif(l[0]=='WMS'):
        return stations[2][int(l[1])-1]
        
def createstationstructure(finalpath,prevpath):
    for x in os.listdir(prevpath):
        chkdir(finalpath+'/'+x)

def update(path,start,end):
    while True:
        try:
            a=authenticate()
        except:
            time.sleep(5)
            continue
        break 
    start=start.replace(minute=0,second=0)
    start=start.strftime("%Y-%m-%dT%H:%M:%S")
    end=end.replace(minute=0,second=0)
    end=end.strftime("%Y-%m-%dT%H:%M:%S")
    print('Adding data between',start,'and',end)
    for x in os.listdir(path):
        flag2=1
        for y in os.listdir(path+'/'+x):
            if(re.search(start[0:10],y)):
                flag3=1
                j=getcompid(x)
                componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz=Asia/Calcutta&gran=5min&fields="+cols
                while True:
                    try:
                        r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                        data4=r.json()
                        dataframe1=pd.DataFrame(data4['data'])
                    except:
                        print("Live Data API Failed!")
                        time.sleep(5)
                        continue
                    break  
                newdf=pd.DataFrame(columns=ogcols)      
                cols1=dataframe1.columns.tolist()
                for i in cols1:
                    if i in ogcols:
                        newdf[i]=dataframe1[i]                
                newdf['ts']=newdf['ts'].str.replace('T',' ')
                newdf.ts=newdf.ts.str[0:19]
                if(end==end[0:11]+"23:00:00"):
                    flag3=0
                    print("Getting all data again from 11 ")
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+end[0:11]+"00:00:00&end="+str(end)+"&tz=Asia/Calcutta&gran=5min&fields="+cols
                    while True:
                        try:
                            r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                            data4=r.json()
                            dataframe1=pd.DataFrame(data4['data'])
                        except:
                            print("Live Data API Failed!")
                            time.sleep(5)
                            continue
                        break  
                    newdf=pd.DataFrame(columns=ogcols)      
                    cols1=dataframe1.columns.tolist()
                    for i in cols1:
                        if i in ogcols:
                            newdf[i]=dataframe1[i]                
                    newdf['ts']=newdf['ts'].str.replace('T',' ')
                    newdf.ts=newdf.ts.str[0:19]
                if(historicalflag==0  or flag3==0):#In case of failure and you run it again the same day.
                    with open(path+'/'+x+'/'+y, 'w') as f:
                        newdf.to_csv(f, header=True,sep='\t',index=False)
                else:
                    with open(path+'/'+x+'/'+y, 'a') as f:
                        newdf.to_csv(f, header=False,sep='\t',index=False)
                flag2=0
        if(flag2==1):
            j=getcompid(x)
            componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz=Asia/Calcutta&gran=5min&fields="+cols
            while True:
                try: 
                    r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                    data4=r.json()
                    dataframe=pd.DataFrame(data4['data'])
                except:
                    print("Live Data API Failed!")
                    time.sleep(5)
                    continue
                break 
            newdf=pd.DataFrame(columns=ogcols)      
            cols1=dataframe.columns.tolist()
            for i in cols1:
                if i in ogcols:
                    newdf[i]=dataframe[i]                
            newdf['ts']=newdf['ts'].str.replace('T',' ')
            newdf.ts=newdf.ts.str[0:19]
            l=x.split('_')
            if(l[0]=='INVERTER'): 
                b=str(start)
                newdf.to_csv(path+'/'+x+'/'+stn+'-'+'I'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
            elif(l[0]=='MFM'): 
                b=str(start)
                newdf.to_csv(path+'/'+x+'/'+stn+'-'+'MFM'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
            elif(l[0]=='WMS'):
                b=str(start)
                newdf.to_csv(path+'/'+x+'/'+stn+'-'+'WMS'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
    currtime=datetime.datetime.now(tz)
    currtime=curr.replace(tzinfo=None)
    with open(startpath+"MasterMail/IN-048L_FTPNewFiles.txt", "w") as file:
        file.write(str(currtime))  


flag=0
print("RUNNING LIVE!")
while(1):
    if(flag==0):
        start=str(final)
        start=datetime.datetime.strptime(start, "%Y-%m-%d")
        flag=1
    c=str(start)
    temppath=path+'/'+c[0:4]+'/'+c[0:7]
    curr=datetime.datetime.now(tz)
    curr = curr.replace(tzinfo=None)
    print('Time now is',str(curr))
    with open(startpath+"MasterMail/IN-048L_FTPProbe.txt", "w") as file:
        file.write(str(curr))  
    d=str(curr)
    if(d[8:10]=='01'):#New month so create structure.
        chkdir(path+'/'+d[0:4]+'/'+d[0:7])
        createstationstructure(path+'/'+d[0:4]+'/'+d[0:7],path+'/'+c[0:4]+'/'+c[0:7])
        prevmonth=str(monthdelta(curr,-1))
        createstationstructure(path+'/'+d[0:4]+'/'+d[0:7],path+'/'+prevmonth[0:4]+'/'+prevmonth[0:7])
    with open(startpath+"IN048L.txt", "w") as file:
        file.write(str(curr.date())+"\n"+str(curr.time().replace(microsecond=0)))
    diff=curr-start
    diff=diff.seconds/3600
    if(diff<1):
        print("WAITING!")
    else:
        update(temppath,start,curr)
    start=curr
    historicalflag=1
    print('sleeping for an hour')
    time.sleep(3600)
   
	 

	



