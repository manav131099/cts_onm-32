import pyodbc
import pandas as pd
import datetime
import math
import pytz
from database_operations import *
import sys
import os
irr = []

def get_irr_raw_1(date, site_id, irr_site, cnxn):
    component_id = get_main_irr_component(site_id,cnxn)
    if(component_id==None):
        return None
    if(int(irr_site[3])>=4):
        n = group_shell(irr_site[3:6])
        query = pd.read_sql_query("SELECT [Timestamp],[Irradiance] FROM ["+irr_site[0:3]+n+"].[RawData] WHERE [ComponentId]="+str(component_id)+"AND Timestamp BETWEEN '"+str(date)+"' AND'" +str(date)[0:10]+" 23:55:00'", cnxn)
    else:
        query = pd.read_sql_query("SELECT [Timestamp],[Irradiance] FROM ["+irr_site+"].[RawData] WHERE [ComponentId]="+str(component_id)+"AND Timestamp BETWEEN '"+str(date)+"' AND'" +str(date)[0:10]+" 23:55:00'", cnxn)
    df = pd.DataFrame(query)

    return df
    
    
def get_irr_raw_2(date, site_id, irr_site, cnxn):
    component_id = get_main_irr_component(site_id,cnxn)
    if(component_id==None):
        return None
    if(int(irr_site[3])>=4):
        n = group_shell(irr_site[3:6])
        query = pd.read_sql_query("SELECT [Timestamp],[Irradiance_GTI] FROM ["+irr_site[0:3]+n+"].[RawData] WHERE [ComponentId]="+str(component_id)+"AND Timestamp BETWEEN '"+str(date)+"' AND'" +str(date)[0:10]+" 23:55:00'", cnxn)
    else:
        query = pd.read_sql_query("SELECT [Timestamp],[Irradiance_GTI] FROM ["+irr_site+"].[RawData] WHERE [ComponentId]="+str(component_id)+"AND Timestamp BETWEEN '"+str(date)+"' AND'" +str(date)[0:10]+" 23:55:00'", cnxn)
    df = pd.DataFrame(query)

    return df


    
def test(date, param):
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    date = date
    irr_site = 'IN-035'
    site_id = '60'
    component_df_ghi = get_irr_raw_1(date, site_id, irr_site, cnxn)
    component_df_gti = get_irr_raw_2(date, site_id, irr_site, cnxn)
    GHI = (round((component_df_ghi['Irradiance'].sum()/12000),2))
    GTI = (round((component_df_gti['Irradiance_GTI'].sum()/12000),2))
    GMod = round((GHI*2831.4/4745.4) + (GTI*1914/4745.4) ,2)
    if(param == 'GHI'):
        return GHI
    if(param == 'GTI'):
        return GTI
    if(param == 'GMod'):
       return GMod


