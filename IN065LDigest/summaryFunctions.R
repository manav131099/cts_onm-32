require('compiler')

checkdirUnOp = function(path)
{
  if(!file.exists(path))
  {
    dir.create(path)
  }
}

checkdir = cmpfun(checkdirUnOp)

INSTCAP = 4745.4
NOMETERS = 1
NOINVS = 8
getGTIDataUnOp = function(date)
{
  yr = substr(date,1,4)
  yrmon = substr(date,1,7)
  filename = paste("[IN-065L]-WMS3-",date,".txt",sep="")
  filename2 = paste("[IN-035L]-WMS1-",date,".txt",sep="")
  path = "/home/admin/Dropbox/Second Gen/[IN-065L]"
  path2 = "/home/admin/Dropbox/Gen 1 Data/[IN-065L]"
  path3 = "/home/admin/Dropbox/Second Gen/[IN-035L]"
  pathRead = paste(path,yr,yrmon,"WMS_3",filename,sep="/")
  pathRead2= paste(path2,yr,yrmon,"WMS_3",filename,sep="/")
  pathRead3 = paste(path3,yr,yrmon,"WMS_1_WMS",filename2,sep="/")
  GTIGreater20=NA
  GHI = GTI = GMod = NA
  if(file.exists(pathRead3))
  {
    dataread = read.table(pathRead3,sep="\t",header = T)
    if(nrow(dataread) > 0)
    {
         GTI = as.numeric(dataread[1,3])
    }
  }
  if(file.exists(pathRead))
  {
    dataread = read.table(pathRead,sep="\t",header = T)
    if(nrow(dataread) > 0)
    {
      GHI = as.numeric(dataread[1,3])
      GMod = as.numeric(dataread[1,5])
    }
  }
  if(file.exists(pathRead2))#return all timestamps with irr>20
  {
    dataread = read.table(pathRead2,sep="\t",header = T,stringsAsFactors = F)
    dataread=dataread[complete.cases(dataread[which( colnames(dataread)=="POAI_avg" )]),]
    GTIGreater20=dataread[dataread[,which( colnames(dataread)=="POAI_avg")]>20,1]
    
  }
  return(c(GHI,GTI,GMod,GTIGreater20))
}

getGTIData = cmpfun(getGTIDataUnOp)

timetominsUnOp = function(time)
{
  hr = as.numeric(substr(time,12,13))
  min = as.numeric(substr(time,15,16))
  bucket =((hr*60) + min + 1)
  return(bucket)
}

timetomins = cmpfun(timetominsUnOp)

getMFMVolatgeDataUnOp = function(date)
{
  yr = substr(date,1,4)
  yrmon = substr(date,1,7)
  filename = paste("[IN-065L]-MFM3-",date,".txt",sep="")
  path3 = "/home/admin/Dropbox/Gen 1 Data/[IN-065L]"
  pathRead3 = paste(path3,yr,yrmon,"MFM_3",filename,sep="/")
  VoltageGreater150=NA
  if(file.exists(pathRead3))#return all timestamps with Voltage>150
  {
    dataread3 = read.table(pathRead3,sep="\t",header = T,stringsAsFactors = F)
    dataread3=dataread3[complete.cases(dataread3[which( colnames(dataread3)=="PPVphAB_avg" )]),]
    VoltageGreater150=dataread3[dataread3[,which( colnames(dataread3)=="PPVphAB_avg" )]>150,1]
  }
  return(VoltageGreater150)
}

getMFMVolatgeData = cmpfun(getMFMVolatgeDataUnOp)

secondGenDataUnOp = function(pathread, pathwrite,type)
{
  dataread = try(read.table(pathread,sep="\t",header = T,stringsAsFactors=F),silent = T)
  if(class(dataread) == "try-error")
  {
    print(paste('pathread error',pathread))
    date=DA=Eac1=Eac3=Eac2=Yld1=Yld2=LR=Tamb=Tmod=GHI=DTI=GMod=LT=TModSH=TambSH=PR1GHI=PR2GHI=PR1GTI=PR2GTI=PR1GMod=PR2GMod=GA=PA=IA=NA
    {
      if(type == 0)
        data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
                          Yld2 = Yld2,PR1GHI=PR1GHI,PR2GHI=PR2GHI,PR1GTI=PR1GTI,PR2GTI=PR2GTI,PR1GMod=PR1GMod,PR2GMod=PR2GMod,LastRead = LR, LastTime = LT,GA=GA,PA=PA,stringsAsFactors=F)
      else if(type == 1)
        data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
                          Yld1 = Yld1, Yld2 = Yld2,LastRead = LR, LastTime = LT,IA=IA,stringsAsFactors=F)
      else if(type == 2)
        data = data.frame(Date = date,DA = DA,GHI = GHI, GTI = GTI, GMod = GMod, Tamb = Tamb, Tmod = Tmod,TambSH=TambSH,TModSH=TModSH,stringsAsFactors=F)
    }
    write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
    thirdGenData(pathwrite)
    fourthGenData(pathwrite)
    return()	
  }
  date = substr(dataread[1,1],1,10)
  {
    if(type == 0)
    {
      idxPower = which( colnames(dataread)=="W_avg" )
      idxEnergy = which( colnames(dataread)=="TotWhExp_max" )
      
      DA = format(round((length(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])/2.88),1),nsmall=1)
      Eac1 = Eac2 = Yld1 = Yld2 = LR = LT =PR1GHI=PR2GHI=PR1GTI=PR2GTI=PR1GMod=PR2GMod = GA = PA = NA
      
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
      dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
      
      dataIntTr = round(dataInt/1000000,0)
      dataIntTr2 = unique(dataIntTr)
      mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
      dataInt = dataInt[abs(dataIntTr - mode) <= 100]
      dataInt2 = dataInt2[abs(dataIntTr - mode) <= 100]
      
      if(length(dataInt))
      {
        Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])/1000),1) 
        LR = dataInt[length(dataInt)]/1000
        LT = dataInt2[length(dataInt2)]
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPower])),idxPower])
      dataInt = dataInt[abs(dataInt) < 1000000]
      if(length(dataInt))
        Eac1 = format(round((sum(dataInt)/1200),1),nsmall=1)
      
      Yld1 = round(as.numeric(Eac1)/INSTCAP,2)
      Yld2 = round(as.numeric(Eac2)/INSTCAP,2)
      GHI = getGTIData(date)
      GTIGreater20=GHI[-c(1, 2, 3)]
      if(length(GTIGreater20))
      {
        dataread1=dataread[complete.cases(dataread[which( colnames(dataread)=="Hz_avg" )]),]	
        FrequencyGreater=dataread1[dataread1[,which( colnames(dataread)=="Hz_avg" )]>40,1]#Found timestamps having Avg Freq>40 here	
        dataread2=dataread[complete.cases(dataread[which( colnames(dataread)=="W_avg" )]),]	
        PowerGreater=dataread2[dataread2[,which( colnames(dataread)=="W_avg" )]>2,1]#Found timestamps having Power>2 here	
        common=intersect(GTIGreater20,FrequencyGreater)	
        common2=intersect(common,PowerGreater)	
        GA=(length(common)/length(GTIGreater20))*100	
        GA=round(GA,1)	
        PA=(length(common2)/length(common))*100	
        PA=round(PA,1)
      }
      GTI = as.numeric(GHI[2])
      GMod = as.numeric(GHI[3])
      GHI = as.numeric(GHI[1])
      PR1GHI = round(Yld1*100/GHI,1)
      PR2GHI = round(Yld2*100/GHI,1)
      PR1GTI = round(Yld1*100/GTI,1)
      PR2GTI = round(Yld2*100/GTI,1)
      PR1GMod = round(Yld1*100/GMod,1)
      PR2GMod = round(Yld2*100/GMod,1)
      data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1, Yld2 = Yld2, PR1GHI=PR1GHI,PR2GHI=PR2GHI,PR1GTI=PR1GTI,PR2GTI=PR2GTI,
                        PR1GMod=PR1GMod,PR2GMod=PR2GMod, LastRead = LR,LastTime = LT, GA=GA,PA=PA,stringsAsFactors = F)
    }
    else if(type == 1)
    {
      InvNo = unlist(strsplit(pathread,"INVERTER_"))
      InvNo = unlist(strsplit(InvNo[2],"/"))
      InvNo = as.numeric(InvNo[1])
      INSTCAPMT = 930.6
      is.na(InvNo)
      {
        INSTCAPMT = 963.6
      }
      if(InvNo == 3){
        INSTCAPMT = 963.6
      }else if(InvNo == 4){
        INSTCAPMT = 950.4
      }else if(InvNo == 5){
        INSTCAPMT = 950.4
      }else if(InvNo == 7){
        INSTCAPMT = 950.4
      }else if(InvNo == 8){
        INSTCAPMT = 930.6
      }
      idxEnergy = which( colnames(dataread)=="Wh_sum" )
      idxPowerAC = 3
      idxPowerDC = 3
      DA = format(round((length(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])/2.88),1),nsmall=1)
      Eac1 = Eac2 = Eac3 = LR = LT=IA=NA
      GHI = getGTIData(date)
      GTIGreater20=GHI[-c(1, 2, 3)] 
      if(length(GTIGreater20))
      {
        dataread1=dataread[complete.cases(dataread[which( colnames(dataread)=="Hz_avg" )]),]	
        dataread2=dataread[complete.cases(dataread[which( colnames(dataread)=="PhVphB_avg" )]),]	
        if (length(dataread1[dataread1[,which( colnames(dataread1)=="Hz_avg" )]>40,1]))	
        {	
          FrequencyGreaterthan40 = dataread1[dataread1[,which( colnames(dataread1)=="Hz_avg" )]>40,1]	
          common=intersect(GTIGreater20,FrequencyGreaterthan40)	
        }	
        else	
        {	
          VoltageGreaterthan150 = dataread2[dataread2[,which( colnames(dataread2)=="PhVphB_avg" )]>150,1]	
          common=intersect(GTIGreater20,VoltageGreaterthan150)	
        }	
        dataread3=dataread[complete.cases(dataread[which( colnames(dataread)=="W_avg" )]),]	
        DCPowerGreater2=dataread3[dataread3[,which( colnames(dataread3)=="W_avg" )]>0,1]#Found timestamps having DCPower>0 here	
        common2=intersect(common,DCPowerGreater2)	
        IA=(length(common2)/length(common))*100	
        IA=round(IA,1)
      }
      
      dataInt = as.numeric(as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy]))
      dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
      
      dataIntTr = round(dataInt/1000000,0)
      dataIntTr2 = unique(dataIntTr)
      mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
      dataInt = dataInt[abs(dataIntTr - mode) <= 1]
      dataInt2 = dataInt2[abs(dataIntTr - mode) <= 1]
      
      if(length(dataInt))
      {
        Eac2 = round(sum(dataInt)/1000,1)
        LR = dataInt[length(dataInt)]/1000
        LT = dataInt2[length(dataInt2)]
      }
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerAC])),idxPowerAC])
      dataInt = dataInt[abs(dataInt) < 1000000]
      if(length(dataInt))
        Eac1 = format(round((sum(dataInt)/12000),1),nsmall=1)
      
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerDC])),idxPowerDC])
      dataInt = dataInt[abs(dataInt) < 1000000]
      
      if(length(dataInt))
        Eac3 = format(round((sum(dataInt)/12000),1),nsmall=1)
      
      Yld1 = round(as.numeric(Eac1)/INSTCAPMT,2)
      Yld2 = round(as.numeric(Eac2)/INSTCAPMT,2)
      
      data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
                        Yld1 = Yld1, Yld2 = Yld2, LastRead = LR, LastTime = LT,IA=IA,stringsAsFactors = F)
    }
    else if(type == 2)
    {
      idxGHI = which( colnames(dataread)=="POAI_avg" )
      idxGTI = which( colnames(dataread)=="POAI_avg" )
      idxTMod = which( colnames(dataread)=="TmpBOM_avg" )
      idxTamb = 4 
      Tamb = Tmod = GHI = GTI=GMod=TModSH=TambSH=NA
      DA = format(round((length(dataread[complete.cases(as.numeric(dataread[,idxGHI])),idxGHI])/2.88),1),nsmall=1)
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTMod])),idxTMod])
      if(length(dataInt))
        Tmod = round(mean(dataInt),1)
      
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTamb])),idxTamb])
      if(length(dataInt))
        Tamb = round(mean(dataInt),1)
      
      dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxGHI])),idxGHI])
      if(length(dataInt))
        GHI = format(round((sum(dataInt)/12000),2),nsmall=2)
      
      yr = substr(date,1,4)
      yrmon = substr(date,1,7)
      filename11 = paste("[IN-065L]-WMS1-",date,".txt",sep="")
      path11 = "/home/admin/Dropbox/Gen 1 Data/[IN-065L]"
      pathRead11= paste(path11,yr,yrmon,"WMS_1",filename11,sep="/")
      if(file.exists(pathRead11))
      {
        dataread11 = read.table(pathRead11,sep="\t",header = T)
        dataInt = as.numeric(dataread11[complete.cases(as.numeric(dataread11[,idxGTI])),idxGTI])
        if(length(dataInt))
          GTI = format(round((sum(dataInt)/12000),2),nsmall=2)
      }
      #filename2 = paste("[IN-035L]-WMS1-",date,".txt",sep="")
      #path3 = "/home/admin/Dropbox/Second Gen/[IN-035L]"
      #pathRead3 = paste(path3,yr,yrmon,"WMS_1_WMS",filename2,sep="/")
      #if(file.exists(pathRead3))
      #{
      #  dataread11 = read.table(pathRead3,sep="\t",header = T)
      #  print(dataread11)
      #  if(nrow(dataread11) > 0)
      #  {
      #      GTI = as.numeric(dataread11[1,3])
      #  }
      #}

      GMod = round((as.numeric(GHI)*2831.4/4745.4) + (as.numeric(GTI)*1914/4745.4) ,2)
      
      dataInt = dataread[complete.cases(as.numeric(dataread[,idxTamb])),c(1,idxTamb)]
      if(length(dataInt[,1]))
      {
        tmmins = timetomins(as.character(dataInt[,1]))
        dataInt = dataInt[tmmins < 1140,]
        tmmins = tmmins[tmmins < 1140]
        if(length(tmmins))
        {
          dataInt = dataInt[tmmins > 420,2]
          if(length(dataInt))
            TambSH= format(round(mean(as.numeric(dataInt)),1),nsmall=1)
        }
      }
      dataInt = dataread[complete.cases(as.numeric(dataread[,idxTMod])),c(1,idxTMod)]
      if(length(dataInt[,1]))
      {
        tmmins = timetomins(as.character(dataInt[,1]))
        dataInt = dataInt[tmmins < 1140,]
        tmmins = tmmins[tmmins < 1140]
        if(length(tmmins))
        {
          dataInt = dataInt[tmmins > 420,2]
          if(length(dataInt))
            TModSH = format(round(mean(as.numeric(dataInt)),1),nsmall=1)
        }
      }
      data = data.frame(Date = date,DA = DA,GHI = GHI, GTI=GTI, GMod=GMod,Tamb = Tamb, Tmod = Tmod,
                        TambSH = TambSH,TModSH=TModSH,stringsAsFactors=F)
    }
  }
  write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
  print("done writing")
  thirdGenData(pathwrite)
  fourthGenData(pathwrite)
}

secondGenData = cmpfun(secondGenDataUnOp)

thirdGenDataUnOp = function(path2G)
{
  stnnames = unlist(strsplit(path2G,"/"))
  path3G = paste("/home/admin/Dropbox/Third Gen",stnnames[6],sep="/")
  print(path3G)
  checkdir(path3G)
  path3GStn = paste(path3G,stnnames[9],sep="/")
  checkdir(path3GStn)
  pathwrite3GFinal = paste(path3GStn,paste(substr(stnnames[8],1,18),".txt",sep=""),sep="/")
  print(pathwrite3GFinal)
  dataread = read.table(path2G,header = T,sep ="\t",stringsAsFactors=F)
  {
    if(!file.exists(pathwrite3GFinal))
      write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
    else
      write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=F,append=T,sep="\t")
  }
  dataread = read.table(pathwrite3GFinal,header = T,sep="\t",stringsAsFactors=F)
  tm = as.character(dataread[,1])
  tmuniq = unique(tm)
  # The idea here is update third-gen with the most recent readings. So if 
  # there are multiple calls, only take the latest row for the day and 
  # slot that into the third-gen data
  if(length(tm) > length(tmuniq))
  {
    dataread = dataread[-(nrow(dataread)-1),]
  }
  write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
}

thirdGenData = cmpfun(thirdGenDataUnOp)

fourthGenDataUnOp = function(path2G)
{
  order = c("WMS_3","MFM_3","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4","INVERTER_5","INVERTER_6","INVERTER_7","INVERTER_8")
  CONSTANTLEN = c(7,unlist(rep(13,NOMETERS)),unlist(rep(9,NOINVS))) # No of columns for each file excluding date
  stnnames = unlist(strsplit(path2G,"/"))
  rowtemplate = unlist(rep(NA,sum(CONSTANTLEN)+1))
  path4G = paste("/home/admin/Dropbox/Fourth_Gen",stnnames[6],sep="/")
  checkdir(path4G)
  dataread = read.table(path2G,header = T,sep ="\t",stringsAsFactors=F)
  pathfinal = paste(path4G,paste(stnnames[6],"-lifetime.txt",sep=""),sep="/")
  idxmtch = match(stnnames[9],order)
  print(idxmtch)
  start = 2
  if(idxmtch > 1)
  {
    start = 2 + sum(CONSTANTLEN[1:(idxmtch -1)])
  }
  end = start + CONSTANTLEN[idxmtch] - 1
  print(end)
  idxuse = start : end
  colnames2G = colnames(dataread)
  colnames2G = colnames2G[-1]
  colnames2G = paste(stnnames[9],colnames2G,sep="-")
  
  {
    if(!file.exists(pathfinal))
    {
      colnames = rep("Date",length(rowtemplate))
      colnames[idxuse] = colnames2G
      rowtemplate[idxuse] = dataread[1,(2:ncol(dataread))]
      rowtemplate[1] = as.character(dataread[1,1])
      dataExist = data.frame(rowtemplate,stringsAsFactors=F)
      colnames(dataExist) = colnames
      write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
    }
    else
    {
      dataExist = read.table(pathfinal,sep="\t",header=T,stringsAsFactors=F)
      colnames = colnames(dataExist)
      colnames[idxuse] = colnames2G
      dates = as.character(dataExist[,1])
      idxmtchdt = match(as.character(dataread[,1]),dates)
      if(is.finite(idxmtchdt))
      {
        tmp = dataExist[idxmtchdt,]
        tmp[idxuse] = dataread[1,2:ncol(dataread)]
        dataExist[idxmtchdt,] = tmp
        colnames(dataExist) = colnames
        write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
      }
      else
      {
        rowtemplate = unlist(rowtemplate)
        rowtemplate[c(1,idxuse)] = dataread[1,]
        dataExist = data.frame(rowtemplate,stringsAsFactors=F)
        colnames(dataExist) = colnames
        write.table(dataExist,file=pathfinal,row.names =F,col.names=F,sep="\t",append=T)
      }
    }
  }
  
  print(paste("4G Done",path2G))
}

fourthGenData = cmpfun(fourthGenDataUnOp)
