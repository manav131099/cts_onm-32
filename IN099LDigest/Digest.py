import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')

######CHANGES START######
stn = '[IN-099L]'                                             #Enter station code for digest
irr_stn = 'NA'                                               #NA if WMS is present, NotRequired if to run without WMS
wms_name = 'NA'                                                #NA if WMS is present, NotRequired if to run without WMS
distance = '0'                                                #0 if self WMS is present
components = ['WMS_1_Pyranometer-01']                                 #Enter self WMS name if present else empty
meters = ['MFM_1_Billing MFM']                                     #Enter MFM Folder Name
meters_mapped = {'MFM_1_Billing MFM' : ['Billing MFM', 14905.95]}                  #Enter MFM foldername, MFM Display name, MFM capacity 
inverters = ['INVERTER_1', 'INVERTER_2', 'INVERTER_3','INVERTER_4','INVERTER_5', 'INVERTER_6', 'INVERTER_7', 'INVERTER_8','INVERTER_9', 'INVERTER_10', 'INVERTER_11', 'INVERTER_12','INVERTER_13', 'INVERTER_14', 'INVERTER_15', 'INVERTER_16','INVERTER_17', 'INVERTER_18', 'INVERTER_19', 'INVERTER_20','INVERTER_21', 'INVERTER_22', 'INVERTER_23', 'INVERTER_24','INVERTER_25', 'INVERTER_26', 'INVERTER_27', 'INVERTER_28','INVERTER_29', 'INVERTER_30', 'INVERTER_31', 'INVERTER_32','INVERTER_33', 'INVERTER_34','INVERTER_35', 'INVERTER_36','INVERTER_37', 'INVERTER_38', 'INVERTER_39', 'INVERTER_40','INVERTER_41', 'INVERTER_42','INVERTER_43', 'INVERTER_44', 'INVERTER_45', 'INVERTER_46','INVERTER_47', 'INVERTER_48', 'INVERTER_49', 'INVERTER_50','INVERTER_51', 'INVERTER_52','INVERTER_53', 'INVERTER_54', 'INVERTER_55', 'INVERTER_56','INVERTER_57', 'INVERTER_58', 'INVERTER_59', 'INVERTER_60','INVERTER_61', 'INVERTER_62', 'INVERTER_63']
inv_mapped = {'INVERTER_1': ['INV-1',223.65],'INVERTER_2': ['INV-2',223.65],'INVERTER_3':['INV-3',223.65],'INVERTER_4': ['INV-4',223.65],'INVERTER_5': ['INV-5',224.1],'INVERTER_6': ['INV-6',223.65],'INVERTER_7': ['INV-7',223.65],'INVERTER_8': ['INV-8',223.65],'INVERTER_9': ['INV-9',220.5],'INVERTER_10': ['INV-10',220.5],'INVERTER_11': ['INV-11',226.8],'INVERTER_12': ['INV-12',226.8],'INVERTER_13': ['INV-13',219.0],'INVERTER_14': ['INV-14',220.5],'INVERTER_15': ['INV-15',216.0],'INVERTER_16': ['INV-16',216.0],'INVERTER_17': ['INV-17',223.65],'INVERTER_18': ['INV-18',223.65],'INVERTER_19': ['INV-19',223.65],'INVERTER_20': ['INV-20',223.65],'INVERTER_21': ['INV-21',223.65],'INVERTER_22': ['INV-22',223.65],'INVERTER_23': ['INV-23',223.65],'INVERTER_24': ['INV-24',222.75],'INVERTER_25': ['INV-25',220.5],'INVERTER_26': ['INV-26',228.0],'INVERTER_27': ['INV-27',225.9],'INVERTER_28': ['INV-28',220.5],'INVERTER_29':['INV-29',218.2],'INVERTER_30': ['INV-30',216.0],'INVERTER_31': ['INV-31',216.0],'INVERTER_32': ['INV-32',223.65],'INVERTER_33': ['INV-33',223.65],'INVERTER_34':['INV-34',223.65],'INVERTER_35': ['INV-35',223.65],'INVERTER_36': ['INV-36',223.65],'INVERTER_37': ['INV-37',223.65],'INVERTER_38': ['INV-38',223.65],'INVERTER_39': ['INV-39',223.65],'INVERTER_40': ['INV-40',220.5],'INVERTER_41': ['INV-41',220.5],'INVERTER_42': ['INV-42',226.8],'INVERTER_43': ['INV-43',226.95],'INVERTER_44': ['INV-44',220.5],'INVERTER_45': ['INV-45',219.0],'INVERTER_46': ['INV-46',214.65],'INVERTER_47': ['INV-47',214.65],'INVERTER_48': ['INV-48',226.8],'INVERTER_49': ['INV-49',226.8],'INVERTER_50': ['INV-50',225.0],'INVERTER_51': ['INV-51',224.85],'INVERTER_52': ['INV-52',223.65],'INVERTER_53': ['INV-53',223.65],'INVERTER_54': ['INV-54',223.65],'INVERTER_55': ['INV-55',223.65],'INVERTER_56': ['INV-56',220.5],'INVERTER_57': ['INV-57',220.5],'INVERTER_58': ['INV-58',226.8],'INVERTER_59': ['INV-59',226.8],'INVERTER_60': ['INV-60',213.0],'INVERTER_61': ['INV-61',213.0],'INVERTER_62': ['INV-62',213.0],'INVERTER_63': ['INV-63',213.0]}
user = 'admin'   
recipients = ['om-it-digest@cleantechsolar.com','operationsWestIN@cleantechsolar.com','ampm@inspire-ce.com','ctt@inspire-ce.com']
#SiteDetails
Site_Name = 'Prism cement Satna'	
Location = 'Satna, M.P '	
brands = "Module Brand/ Model/ Nos- Canadian Solar/ 350 Wp/355 Wp/360 Wp/365 Wp/ 6600 / 20940 / 9660 / 2130 " + "\n\n" + "Inverter Brand/ Model/ Nos- Huawei / 160/185 kW:- 63" + "\n\n"
cod_date = '2021-02-26'                                              #Enter COD if available else TBC
budget_pr=75
rate=0.008
inv_limit = 3	
mfm_limit = 5
######CHANGES END######

path_read = '/home/admin/Dropbox/Gen 1 Data/' + stn
startpath = '/home/' + user + '/Start/MasterMail/'
path_write_2g = '/home/' + user + '/Dropbox/Second Gen/' + stn
path_write_3g = '/home/' + user + '/Dropbox/Third Gen/' + stn
path_write_4g = '/home/' + user + '/Dropbox/Fourth_Gen/' + stn + '/' + stn + '-lifetime.txt'
NO_MFM = len(meters)
capacity = sum([i[1] for i in meters_mapped.values()])

#Creating directories if not present
chkdir(path_write_2g)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

#Ordering Components in the series WMS, MFM, Inverters
for i in meters:
  components.append(i)

for i in inverters:
  components.append(i)

print(components)

#Creating dataframes for respective components
def create_template(Type, date):
  if(Type == 'INV'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'IA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'LastRead', 'LastTime', 'IA'])
  elif(Type == 'WMS'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'GHI':'NA', 'Tmod':'NA'}, columns =['Date', 'DA', 'GHI', 'Tmod'])
  elif(Type == 'MFM'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'PR1':'NA', 'PR2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'GA':'NA', 'PA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'PR1', 'PR2', 'LastRead', 'LastTime'])
  return df_template

#Generating site information for Mail
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

#Getting Irradiance Value
def get_irr(date, irr_stn):
  irr_data={'GHI':'NA', 'DA':'NA'}
  gen2 = '/home/admin/Dropbox/Second Gen/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt' 
  gen1 = '/home/admin/Dropbox/Gen 1 Data/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt'
  
  if(os.path.exists(gen2)):
    df = pd.read_csv(gen2, sep = '\t')
    irr_data['GHI'] = df['GHI'][0]
    irr_data['DA'] = df['DA'][0]
  
  if(os.path.exists(gen1)):
    df = pd.read_csv(gen1, sep = '\t')
    GHIGreater20 = df[df['POAI_avg']>20]['ts']
  else:	
    GHIGreater20 = pd.DataFrame({'ts' : []})
  
  return irr_data, GHIGreater20

def add_graphs(enddate, startdate):
  os.system(" ".join(["Rscript /home/" + user + "/CODE/EmailGraphs/PR_Graph_Azure.R", stn[1:-2], str(budget_pr), str(rate), cod_date, enddate]))
  if len(meters)>1:
    os.system(" ".join(['python3 /home/' + user + "/CODE/EmailGraphs/Meter_CoV_Graph.py", stn[1:-2], enddate, str(mfm_limit)]))
  if(cod_date == 'TBC'):
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, startdate]))
    #os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/GHI_vs_PR.py", stn[1:-2],'" "', startdate, enddate]))
    if len(inverters)>1:
      os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Inverter_CoV_Graph.py", stn[1:-2], enddate, str(inv_limit), startdate,'" "','"Inverter CoV"']))
  else:
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, cod_date]))
    #os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/GHI_vs_PR.py", stn[1:-2],'" "', cod_date, enddate]))
    if len(inverters)>1:
      os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Inverter_CoV_Graph.py", stn[1:-2], enddate, str(inv_limit), cod_date,'" "','"Inverter CoV"']))
  
  graph_list = ['PR Evolution', 'Inverter CoV', 'Meter CoV', 'Grid Availability', 'Plant Availability', 'Data Availability', 'System Availability']
  path_list = []
  
  for i in range(len(graph_list)):
    check = '/home/' + user + '/Graphs/Graph_Output/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.pdf'
    if(os.path.exists(check)):
      path_list.append(check)
    
    check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.txt'
    if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Daily.txt'
  if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Monthly.txt'
  if(os.path.exists(check)):
      path_list.append(check)
  
  return(path_list)
   
#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#checking Stn_Mail.txt
if(os.path.exists(startpath + stn[1:8] + "_Mail.txt")):
  print('Exists')
else:
  try:
    shutil.rmtree(path_write_2g, ignore_errors=True)	
    shutil.rmtree(path_write_3g, ignore_errors=True)	
    os.remove(path_write_4g)
  except Exception as e:
    print(e)
  with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "_Mail.txt") as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def process(date, stn):
  file_path = []
  inv_ylds = []
  meter_ylds = []
  total_eac = 0
  total_pr = 0
  Dyield = ''
  Dia = ''
  yld2 = 0
  yld1 = 0
  digest_body = site_info(date, Site_Name, Location, stn[1:7], capacity, NO_MFM, cod_date)
  fullsite_pos = len(digest_body)
  
  #If WMS is not present
  if(irr_stn != 'NA'):
    irr_data, GHIGreater20 = get_irr(date, irr_stn)
    df_2g = pd.DataFrame({'Date': [date], 'DA': [irr_data['DA']], 'GHI' : [irr_data['GHI']]}, columns =['Date', 'DA', 'GHI'])
    GHI = df_2g['GHI'][0]
    if irr_stn != 'NotRequired' and wms_name != 'NotRequired':
      digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI [kWh/m^2] (From ' + irr_stn[1:8] + ', ' + distance + ' km from ' + stn[1:8] + '): ' + str(df_2g['GHI'][0]) + "\n\n"
    df_merge = df_2g.copy()
    cols_4g = ['Date', 'WMS.DA', 'WMS.GHI']
  else:
    cols_4g = ['Date']

  for i in components:
    chkdir(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      df_3g = pd.read_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', sep='\t')
      
    if(os.path.exists(path_write_4g)):
      df_4g = pd.read_csv(path_write_4g, sep='\t')
      
    if('INV' in i):
      if len(inverters) < 9:	
        file_name = i[0] + i[9]	
      elif len(inverters) < 99:
        file_name = i[0] + i[9:11]
      else:
        file_name = i[0] + i[9:12]
      df_2g = create_template('INV', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['Wh_sum'].dropna())/2.88, 1)
        df_2g['Eac1'] = round((df['AphA_avg'].sum())/12000, 2)
        df_2g['Yld1'] = round((df_2g['Eac1'][0]/inv_mapped[i][1]), 2)
        
        if len(df['Wh_sum'].dropna()) > 0:
          dataint = df.dropna(subset=['Wh_sum'])
          df_2g['Eac2'] = round(dataint['Wh_sum'].sum()/1000, 2)
          df_2g['LastRead'] = round((dataint['Wh_sum'].tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
        
        if df_2g['Eac2'][0] != 'NA':
          df_2g['Yld2'] = round((df_2g['Eac2'][0]/inv_mapped[i][1]), 2) 
        
        if 'Hz_avg' in df.columns and 'PhVphB_avg' in df.columns and 'W_avg' in df.columns:
          if len(GHIGreater20)>0:
            if len(df[df['Hz_avg']>40])>0:
              FrequencyGreaterthan40 = df[df['Hz_avg']>40]['ts']
              common = pd.merge(GHIGreater20, FrequencyGreaterthan40, how='inner')
            else:
              VoltageGreaterthan150 = df[df['PhVphB_avg']>150]['ts']
              common = pd.merge(GHIGreater20, VoltageGreaterthan150, how='inner')
            DCPowerGreater2 = df[df['W_avg']>0]['ts']
            common2 = pd.merge(common, DCPowerGreater2, how='inner')
            if len(common) > 0:
              df_2g['IA'] = round((len(common2)*100/len(common)), 1)
        if(df_2g['Eac2'][0]=='NA'):	
          pass	
        else:	
          inv_ylds.append(df_2g['Yld2'][0])
  
      Dyield +=  '\n\nYield ' + str(inv_mapped[i][0]) + ': ' + str(df_2g['Yld2'][0])
      if(i[9] == '2'):
        yld2 = df_2g['Yld2'][0]
        
      if(i[9] == '1'):
        yld1 = df_2g['Yld2'][0]
        
 
    elif('WMS' in i):
      file_name = i[:3] + i[4]
      df_2g = create_template('WMS', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['POAI_avg'].dropna())/2.88, 1)
        
        if len(df['POAI_avg'].dropna()) > 0:
          GHI = round(((df['POAI_avg'].dropna()).sum())/12000, 2)
          df_2g['GHI'] = GHI
        else:
          GHI = 'NA'
        
        #if len(df['TmpBOM_avg'].dropna()) > 0:
        #  df_2g['Tmod'] = round((df['TmpBOM_avg'].dropna()).mean(), 1)
        
        GHIGreater20 = df[df['POAI_avg']>20]['ts']
      digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGTI [kWh/m^2]: ' + str(df_2g['GHI'][0]) + '\n\nAvg Tmod [C]: ' + str(df_2g['Tmod'][0]) + '\n\n'
      
    elif('MFM' in i):
      if NO_MFM < 9:
        file_name = i[:3] + i[4]
      elif NO_MFM < 99:
        file_name = i[:3] + i[4:6]
      else:
        file_name = i[:3] + i[4:7]
      df_2g = create_template('MFM', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', sep='\t')
        df_2g['DA'] = round(len(df['W_avg'].dropna())/2.88, 1)
        df_2g['Eac1'] = round(df['W_avg'].sum()/12000,2)
        df_2g['Yld1'] = round((df['W_avg'].sum()/(meters_mapped[i][1]*12000)),2)
        
        if len(df['TotWhExp_max'].dropna())>0:
          dataint = df.dropna(subset=['TotWhExp_max'])
          df_2g['Eac2'] = round((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/1000,2)
          df_2g['Yld2'] = round(((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/meters_mapped[i][1])/1000,2)
          df_2g['LastRead'] = round((dataint['TotWhExp_max'].tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
        
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1'] = round((df_2g['Yld1'][0]*100)/GHI,1)
          
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2'] = round((df_2g['Yld2'][0]*100)/GHI,1)
        
        if(df_2g['Eac2'][0]!='NA'):
            total_eac += df_2g['Eac2'][0]
            meter_ylds.append(df_2g['Yld2'][0])
        
        if(df_2g['PR2'][0]!='NA'):
            total_pr += df_2g['PR2'][0] * meters_mapped[i][1]
        
      digest_body += '------------------------------\n' + meters_mapped[i][0] + '\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nSize [kWp]: ' + str(meters_mapped[i][1]) + '\n\nEAC method-1 (Pac) [kWh]: ' + str(df_2g['Eac1'][0]) + '\n\nEAC method-2 (Eac) [kWh]: ' + str(df_2g['Eac2'][0]) + '\n\nYield-1 [kWh/kWp]: ' + str(df_2g['Yld1'][0]) + '\n\nYield-2 [kWh/kWp]: ' + str(df_2g['Yld2'][0]) + '\n\nPR-1 (GHI) [%]: ' + str(df_2g['PR1'][0]) + '\n\nPR-2 (GHI) [%]: ' + str(df_2g['PR2'][0]) + '\n\nLast recorded value [kWh]: ' + str(df_2g['LastRead'][0]) + '\n\nLast recorded time: ' + str(df_2g['LastTime'][0]) +  '\n\n'      
    
    #Saving 2G 
    df_2g.to_csv(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', na_rep='NA', sep='\t',index=False)
    file_path.append(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
    
    #Saving 3G
    chkdir(path_write_3g + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      if((df_3g['Date'] == date).any()):
        vals = df_2g.values.tolist()
        df_3g.loc[df_3g['Date'] == date,df_3g.columns.tolist()] = vals[0]
        df_3g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='w', index=False, header=True)
      else:
        df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=False)
    else:
      df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=True) #First time
    
    
    #If WMS is present
    if("WMS" in i and irr_stn == 'NA'):
      df_merge = df_2g.copy()
    else:
      df_merge = df_merge.merge(df_2g, on = 'Date')
      
    cols_4g = cols_4g + [i + '.' + n for n in df_2g.columns.tolist()[1:]] 
  df_merge.columns = cols_4g
  
  #Saving 4G
  if not os.path.exists(path_write_4g):
    print('firsttime')
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=True)
  elif((df_4g['Date'] == date).any()):
    vals = df_merge.values.tolist()
    df_4g.loc[df_4g['Date']== date, df_4g.columns.tolist()] = vals[0]
    df_4g.to_csv(path_write_4g, sep = '\t', mode='w', index=False, header=True)
  else:
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=False)
  print('4G Done')
  
  std=np.std(inv_ylds)
  cov=(std*100/np.mean((inv_ylds)))
  
  
  digest_body += '------------------------------\nInverters\n------------------------------' + Dyield + '\n\nStdev/COV Yields: ' + str(round(std,1)) + ' / ' + str(round(cov,1)) + '%' + Dia
  fullsite = '--------------------------\nFull Site\n--------------------------\n\n' + 'System Full Generation [kWh]: ' + str(round(total_eac, 2)) + '\n\nSystem Full Yield [kWh/kWp]: ' + str(round(total_eac/capacity, 2)) + '\n\nSystem Full PR [%]: ' + str(round(total_pr/capacity, 1)) 
  
  if (irr_stn != 'NA' and irr_stn != 'NotRequired'):
    if round(total_pr/capacity, 1)>60 and round(total_pr/capacity, 1)<70:
      fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + ') : 70 >>> PR Floor Activated!'
    elif round(total_pr/capacity, 1)>90:
      fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + ') : 85 >>> PR Ceiling Activated!'
    else:
      fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + ') : ' + str(round(total_pr/capacity, 1))
  
  if NO_MFM > 1:
    stdm = np.std(meter_ylds)
    covm = std*100/np.mean((meter_ylds))
    fullsite += '\n\nStdev/COV Yields: ' + str(round(stdm,1)) + ' / ' + str(round(covm,1)) + '%'
  # fullsite += "\n\n"
  
  digest_body = digest_body[:fullsite_pos] + digest_body[fullsite_pos:]
  return [digest_body, file_path]

#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Historical Processing!')
  processed_data = process(str(startdate)[:10], stn)
  if(bot_type == 'Mail'):
    graph_paths = add_graphs(str(startdate)[:10], start)
    processed_data[1] = graph_paths + processed_data[1]
    send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
  try:
    date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    with open(startpath + stn[1:8] + "_Bot.txt", "w") as file:
      file.write(timenow)   
    processed_data = process(date, stn)
    print('Done Processing')
    if(datetime.datetime.now(tz).hour == 1 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
      print('Sending')
      date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
      processed_data = process(date_yesterday, stn)
      graph_paths = add_graphs(date_yesterday, start)
      processed_data[1] = graph_paths + processed_data[1]
      send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
      with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
          file.write(timenow + "\n" + date_yesterday)   
      startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    print('Sleeping')
  except:
    print('error')
    logging.exception('msg')
  time.sleep(300)