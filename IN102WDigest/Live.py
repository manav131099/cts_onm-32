from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib
import re 
import os
import numpy as np
import gzip
import shutil
import logging
import sqlalchemy as sa
import urllib.request

def azure_push(azure_df):
    params = urllib.parse.quote_plus("DRIVER={ODBC Driver 17 for SQL Server};SERVER=cleantechsolar.database.windows.net;DATABASE=Cleantech Meter Readings;UID=RohanKN;PWD=R@h@nKN1")
    engine = sa.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    azure_df.to_sql("RawData",  con=engine, if_exists="append",index=False,schema="IN-102")

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

def write(Type,cols,path_temp,df,date):
    print(df)
    print(cols)
    df.columns=cols
    date=str(date)
    chkdir(path_temp+'/'+Type)
    fileType=Type
    if(Type[0:3]=='INV'):
        fileType='I'+Type.split('_')[1]
    elif(Type[0:3]=='MFM'):
        fileType='MFM'+Type.split('_')[1]
    df['Date']=pd.to_datetime(df['Date'])
    if(os.path.exists(path_temp+'/'+Type+'/'+'[IN-102W]-'+fileType+'-'+date[0:10]+'.txt')):
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-102W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=False,mode='a')
    else:
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-102W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=True,mode='a')


path='/home/admin/Dropbox/Gen 1 Data/[IN-102W]/'
startpath="/home/admin/Start/IN102W.txt"
#logging.basicConfig(filename='/home/admin/LogsIN102.txt')

chkdir(path)
tz = pytz.timezone('Asia/Kolkata')
curr=datetime.datetime.now(tz)

print("Start time is",curr)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
ftp.login(user='IN-102W', passwd = 'BN1M9N6B4')
ftp.cwd('DATA/MODBUS')
mfmcols=['Date','R-N phase', 'Y-N phase', 'B-N phase', 'Avg voltage', 'R-Y voltage', 'Y-B voltage', 'B-R voltage', 'R phase line current', 'Y phase line current', 'B phase line current', 'Neutral line current', 'Power factor q1', 'Power factor q2', 'Power factor q3', 'Avg power factor', 'R phase active power', 'Y phase active power', 'B phase active power', '3 phase active power', 'R phase reactive power', 'Y phase reactive power', 'B phase reactive power', '3 phase reactive power', 'R phase apparent power', 'Y phase apparent power', 'B phase apparent power', '3 phase apparent power', 'Frequency', 'Active Total Export','Active Total import', 'Apparent Export', 'Reactive import', 'Reactive export']
#wmscols=['Date','Irradiation','Record_Id','Record_Id_2']
invcols=['Date','Product model', 'DSP software version', 'LCD software version', 'AC output type', 'DC output type', 'Total Power AC', 'Total DC output power', 'Total energy', 'Energy this month', 'Energy last month', 'Energy today', 'Energy yesterday', 'Energy this year ', 'Energy last year ', 'DC voltage 1', 'DC current 1', 'DC voltage 2', 'DC current 2', 'DC voltage 3', 'DC current 3', 'DC voltage 4', 'DC current 4', 'Reserved', 'Reserved', 'DC busbar voltage', 'DC half-busbar voltage', 'AB line voltage / A phase voltage', 'BC line voltage / B phase voltage', 'CA line voltage / C phase voltage', 'A phase current', 'B phase current', 'C phase current', 'Reserved', 'Reserved', 'Inverter temperature', 'Grid Frequency', 'Inverter status', 'Reactive power', 'Apparent power', 'Real time power factor', 'Inverter SN_1', 'Inverter SN_2', 'Inverter SN_3', 'Inverter SN_4', 'Grid ON/OFF', 'Total PV voltage', 'Total PV current', 'PV1 current', 'PV2 current', 'PV3 current', 'PV4 current', 'PV5 current', 'PV6 current', 'PV7 current', 'PV8 current', 'PV9 current', 'PV10 current', 'PV11 current', 'PV12 current', 'PV13 current', 'PV14 current', 'PV15 current', 'PV16 current', 'PV17 current', 'PV18 current', 'PV19 current', 'PV20 current']
components={6:['MFM_1',mfmcols,34],1:['INVERTER_1',invcols,68],2:['INVERTER_2',invcols,68],3:['INVERTER_3',invcols,68],4:['INVERTER_4',invcols,68],5:['INVERTER_5',invcols,68]}
parameters=['Date','Type','Current A','Current B','Current C','Voltage A','Voltage B','Voltage C','Total Power AC','Total Power DC','Frequency']

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date[0:6]+'20'+date[6:], '%d/%m/%Y-%H:%M:%S')
        return 1
    except:
        return 0


if(os.path.exists(startpath)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath, "w") as file:
        file.write("2021-08-12\n00:00:00")   
with open(startpath) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)
print(startdate)
print(starttime)

start=startdate+starttime
start=start.replace('\n','')
start=datetime.datetime.strptime(start, "%Y-%m-%d%H:%M:%S")
end=datetime.datetime.now(tz).replace(tzinfo=None)-datetime.timedelta(minutes=1)
print(start)
files=ftp.nlst() 
while(start<end):
    try:
        start_str=start.strftime("%Y%m%d_%H%M")
        start_str='_'+start_str[2:]
        print(start,end)
        for i in sorted(files):
            if start_str in i:
                path_temp=path+'20'+start_str[1:3]+'/'+'20'+start_str[1:3]+'-'+start_str[3:5]
                chkdir(path_temp)
                req = urllib.request.Request('ftp://IN-102W:BN1M9N6B4@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i)
                with urllib.request.urlopen(req) as response:
                    s =  gzip.decompress(response.read())
                cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71]
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                azure_df=pd.DataFrame()
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS'):
                        val=row[3]
                    if(chk_date(row[1])):
                        data=row.to_frame().T.iloc[:,:int(components[int(val)][2])]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        if(date.year<2015):
                            continue
                        data[1]=date.replace(second=0)
                        write(components[int(val)][0],components[int(val)][1],path_temp,data,date)
        start=start+datetime.timedelta(minutes=1)
    except Exception as e:
        print(e)
        start=start+datetime.timedelta(minutes=1)
        logging.exception("His")
        pass
ftp.close()
print('Historical Done!')
flag=0


while(1):
    try:
        time_now=datetime.datetime.now(tz)
        print('Live',time_now)
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-102W', passwd = 'BN1M9N6B4')
        ftp.cwd('DATA/MODBUS')
        print('Connected!')
        if(flag==0):
            start=time_now
            files=ftp.nlst()  
            files_sorted=sorted(files,reverse=True)
            new_file=[files_sorted[0]]
            flag=1
        else:
            files_new=ftp.nlst()  
            with open("/home/admin/Start/MasterMail/IN-102W_FTPProbe.txt", "w") as file:
                file.write(str(time_now.replace(microsecond=0)))
            new_file=list(set(files_new) - set(files))
            print(new_file)
            files=files_new
        for i in sorted(new_file):
            date=i.split('_')[2]
            path_temp=path+'20'+date[0:2]+'/'+'20'+date[0:2]+'-'+date[2:4]
            chkdir(path_temp)
            print('Getting file')
            url='ftp://IN-102W:BN1M9N6B4@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i
            try:
                response = urllib.request.urlopen(url, timeout=120).read()
            except (HTTPError, URLError) as error:
                logging.error('Data of %s not retrieved because %s\nURL: %s', name, error, url)
            except timeout:
                logging.error('socket timed out - URL %s', url)
            else:
                logging.info('Access successful.')
            s =  gzip.decompress(response)
            print('Got file')
            cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71]
            df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
            azure_df=pd.DataFrame()
            for index, row in df.iterrows():
                if(row[1]=='ADDRMODBUS'):
                    val=row[3]
                if(chk_date(row[1])):
                    data=row.to_frame().T.iloc[:,:int(components[int(val)][2])]
                    date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                    if(date.year<2015):
                        continue
                    data[1]=date.replace(second=0)
                    write(components[int(val)][0],components[int(val)][1],path_temp,data,date)
                    with open(startpath, "w") as file:
                        file.write(str(time_now.date())+"\n"+str(time_now.time().replace(microsecond=0)))
        ftp.close()
        print('Sleeping')
    except:
        logging.exception('Main Failed')
    time.sleep(7200)

    