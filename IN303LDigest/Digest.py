import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')

######CHANGES START######
stn = '[IN-303L]'                                             #Enter station code for digest
irr_stn = 'NA'                                                #NA if WMS is present, NotRequired if to run without WMS
wms_name = 'NA'                                               #NA if WMS is present, NotRequired if to run without WMS
distance = '0'                                                #0 if self WMS is present
components =  ['WMS_1_K&Z RT1']                                  #Enter self WMS name if present else empty
meters = ['MFM_2_ICR 01 HT MFM','MFM_3_ICR 02 HT MFM','MFM_4_ICR 03 HT MFM']                                     #Enter MFM Folder Name
meters_mapped = {'MFM_2_ICR 01 HT MFM' : ['MFM_1', 6994.8],'MFM_3_ICR 02 HT MFM' : ['MFM_2', 6874.2],'MFM_4_ICR 03 HT MFM':['MFM_3',6964.65]}                  #Enter MFM foldername, MFM Display name, MFM capacity 
inverters =['INVERTER_1_Inverter 01','INVERTER_2_Inverter 02','INVERTER_3_Inverter 03','INVERTER_4_Inverter 04','INVERTER_5_Inverter 05','INVERTER_6_Inverter 06','INVERTER_7_Inverter 07','INVERTER_8_Inverter 08', 'INVERTER_9_Inverter 09','INVERTER_10_Inverter 10', 'INVERTER_11_Inverter 11', 'INVERTER_12_Inverter 12', 'INVERTER_13_Inverter 13', 'INVERTER_14_Inverter 14', 'INVERTER_15_Inverter 15', 'INVERTER_16_Inverter 16','INVERTER_47_Inverter-17', 'INVERTER_48_Inverter-18', 'INVERTER_49_Inverter-19','INVERTER_50_Inverter-20', 'INVERTER_51_Inverter-21', 'INVERTER_52_Inverter-22', 'INVERTER_53_Inverter-23', 'INVERTER_54_Inverter-24', 'INVERTER_55_Inverter-25', 'INVERTER_56_Inverter-26', 'INVERTER_57_Inverter-27', 'INVERTER_58_Inverter-28', 'INVERTER_59_Inverter-29','INVERTER_60_Inverter-30', 'INVERTER_61_Inverter-31','INVERTER_17_Inverter 32', 'INVERTER_18_Inverter 33', 'INVERTER_19_Inverter 34', 'INVERTER_20_Inverter 35', 'INVERTER_21_Inverter 36', 'INVERTER_22_Inverter 37', 'INVERTER_23_Inverter 38', 'INVERTER_24_Inverter 39', 'INVERTER_25_Inverter 40', 'INVERTER_26_Inverter 41', 'INVERTER_27_Inverter 42', 'INVERTER_28_Inverter 43', 'INVERTER_29_Inverter 44', 'INVERTER_30_Inverter 45', 'INVERTER_31_Inverter 46', 'INVERTER_32_Inverter 47', 'INVERTER_62_Inverter-48', 'INVERTER_63_Inverter-49', 'INVERTER_64_Inverter-50', 'INVERTER_65_Inverter-51', 'INVERTER_66_Inverter-52', 'INVERTER_67_Inverter-53', 'INVERTER_68_Inverter-54', 'INVERTER_69_Inverter-55','INVERTER_93_Inverter 56', 'INVERTER_70_Inverter-57', 'INVERTER_90_Inverter 58',
 'INVERTER_71_Inverter-59', 'INVERTER_72_Inverter-60', 'INVERTER_73_Inverter-61', 'INVERTER_74_Inverter-62', 'INVERTER_91_Inverter 63', 'INVERTER_33_Inverter 64', 'INVERTER_34_Inverter 65', 'INVERTER_35_Inverter 66', 'INVERTER_36_Inverter 67','INVERTER_92_Inverter 68', 'INVERTER_37_Inverter 69', 'INVERTER_38_Inverter 70', 'INVERTER_39_Inverter 71', 'INVERTER_40_Inverter 72', 'INVERTER_41_Inverter 73', 'INVERTER_42_Inverter 74', 'INVERTER_43_Inverter 75', 'INVERTER_44_Inverter 76', 'INVERTER_45_Inverter 77', 'INVERTER_46_Inverter 78', 'INVERTER_75_Inverter-79', 'INVERTER_76_Inverter-80', 'INVERTER_77_Inverter-81', 'INVERTER_78_Inverter-82', 'INVERTER_79_Inverter-83', 'INVERTER_80_Inverter-84', 'INVERTER_81_Inverter-85', 'INVERTER_82_Inverter-86', 'INVERTER_83_Inverter-87', 'INVERTER_84_Inverter-88', 'INVERTER_85_Inverter-89', 'INVERTER_86_Inverter-90', 'INVERTER_87_Inverter-91', 'INVERTER_88_Inverter-92', 'INVERTER_89_Inverter-93'] 

inv_mapped = {'INVERTER_1_Inverter 01': 244.8, 'INVERTER_2_Inverter 02': 234.6, 'INVERTER_3_Inverter 03': 214.2, 'INVERTER_4_Inverter 04': 234.6, 'INVERTER_5_Inverter 05': 224.4, 'INVERTER_6_Inverter 06': 224.4, 'INVERTER_7_Inverter 07': 224.4, 'INVERTER_8_Inverter 08': 224.4, 'INVERTER_9_Inverter 09': 224.4, 'INVERTER_10_Inverter 10': 170.85, 'INVERTER_11_Inverter 11': 231.15, 'INVERTER_12_Inverter 12': 221.1, 'INVERTER_13_Inverter 13': 231.15, 'INVERTER_14_Inverter 14': 231.15, 'INVERTER_15_Inverter 15': 231.15, 'INVERTER_16_Inverter 16': 231.15, 'INVERTER_47_Inverter-17': 231.15, 'INVERTER_48_Inverter-18': 231.15, 'INVERTER_49_Inverter-19': 231.15, 'INVERTER_50_Inverter-20': 231.15, 'INVERTER_51_Inverter-21': 231.15, 'INVERTER_52_Inverter-22': 221.1, 'INVERTER_53_Inverter-23': 221.1, 'INVERTER_54_Inverter-24': 231.15, 'INVERTER_55_Inverter-25': 221.1, 'INVERTER_56_Inverter-26': 221.1, 'INVERTER_57_Inverter-27': 221.1, 'INVERTER_58_Inverter-28': 231.15, 'INVERTER_59_Inverter-29': 221.1, 'INVERTER_60_Inverter-30': 221.1, 'INVERTER_61_Inverter-31': 231.15,'INVERTER_17_Inverter 32': 231.15,'INVERTER_18_Inverter 33': 231.15,
'INVERTER_19_Inverter 34': 231.15,
'INVERTER_20_Inverter 35': 221.1,
'INVERTER_21_Inverter 36': 231.15,
'INVERTER_22_Inverter 37': 221.1,
'INVERTER_23_Inverter 38': 221.1,
'INVERTER_24_Inverter 39': 221.1,
'INVERTER_25_Inverter 40': 231.15,
'INVERTER_26_Inverter 41': 231.15,
 'INVERTER_27_Inverter 42': 221.1,
 'INVERTER_28_Inverter 43': 221.1,
 'INVERTER_29_Inverter 44': 221.1,
 'INVERTER_30_Inverter 45': 231.15,
 'INVERTER_31_Inverter 46': 221.1,
 'INVERTER_32_Inverter 47': 221.1,
 'INVERTER_62_Inverter-48': 221.1,
 'INVERTER_63_Inverter-49': 231.15,
 'INVERTER_64_Inverter-50': 231.15,
 'INVERTER_65_Inverter-51': 231.15,
 'INVERTER_66_Inverter-52': 221.1,
 'INVERTER_67_Inverter-53': 231.15,
 'INVERTER_68_Inverter-54': 221.1,
 'INVERTER_69_Inverter-55': 231.15,
 'INVERTER_93_Inverter 56':160.8,
 'INVERTER_70_Inverter-57': 190.95,
 'INVERTER_90_Inverter 58': 190.95,
 'INVERTER_71_Inverter-59': 231.15,
 'INVERTER_72_Inverter-60': 221.1,
 'INVERTER_73_Inverter-61': 221.1,
 'INVERTER_74_Inverter-62': 231.15,
'INVERTER_91_Inverter 63' :231.15,
 'INVERTER_33_Inverter 64': 221.1,
 'INVERTER_34_Inverter 65': 221.1,
 'INVERTER_35_Inverter 66': 231.15,
 'INVERTER_36_Inverter 67': 221.1,
 'INVERTER_92_Inverter 68': 211.05,
 'INVERTER_37_Inverter 69': 231.15,
 'INVERTER_38_Inverter 70': 231.15,
 'INVERTER_39_Inverter 71': 231.15,
 'INVERTER_40_Inverter 72': 221.1,
 'INVERTER_41_Inverter 73': 231.15,
 'INVERTER_42_Inverter 74': 231.15,
 'INVERTER_43_Inverter 75': 221.1,
 'INVERTER_44_Inverter 76': 221.1,
 'INVERTER_45_Inverter 77': 201,
 'INVERTER_46_Inverter 78': 231.15,
 'INVERTER_75_Inverter-79': 231.15,
 'INVERTER_76_Inverter-80': 221.1,
 'INVERTER_77_Inverter-81': 231.15,
 'INVERTER_78_Inverter-82': 221.1,
 'INVERTER_79_Inverter-83': 221.1,
 'INVERTER_80_Inverter-84': 231.15,
 'INVERTER_81_Inverter-85': 231.15,
 'INVERTER_82_Inverter-86': 231.15,
 'INVERTER_83_Inverter-87': 221.1,
 'INVERTER_84_Inverter-88': 231.15,
 'INVERTER_85_Inverter-89': 221.1,
 'INVERTER_86_Inverter-90': 221.1,
 'INVERTER_87_Inverter-91': 221.1,
 'INVERTER_88_Inverter-92': 221.1,
 'INVERTER_89_Inverter-93': 221.1}

user = 'admin'
recipients = ['om-it-digest@cleantechsolar.com','operationsCentralIN@cleantechsolar.com']
#SiteDetails
Site_Name = 'EXIDE Captive Solar plant'
Location = 'Tuljapur, India '
brands = "Module Brand / Model / Nos:  Canadian Solar / Polycrystalline / 62670" + "\n\n" + "Inverter Brand / Model / Nos: Huawei/ 185 KTL/ 93" + "\n\n"
cod_date = '2020-07-01'                                              #Enter COD if available else TBC
budget_pr=76.8
rate=0.008
inv_limit=3
mfm_limit = 3
######CHANGES END######

path_read = '/home/admin/Dropbox/Gen 1 Data/' + stn
startpath = '/home/' + user + '/Start/MasterMail/'
path_write_2g = '/home/' + user + '/Dropbox/Second Gen/' + stn
path_write_3g = '/home/' + user + '/Dropbox/Third Gen/' + stn
path_write_4g = '/home/' + user + '/Dropbox/Fourth_Gen/' + stn + '/' + stn + '-lifetime.txt'
NO_MFM = len(meters)
capacity = sum([i[1] for i in meters_mapped.values()])

#Creating directories if not present
chkdir(path_write_2g)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

#Ordering Components in the series WMS, MFM, Inverters
for i in meters:
  components.append(i)

for i in inverters:
  components.append(i)

print(components)

#Creating dataframes for respective components
def create_template(Type, date):
  if(Type == 'INV'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'IA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'LastRead', 'LastTime', 'IA'])
  elif(Type == 'WMS'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'GHI':'NA','GTI':'NA', 'Tmod':'NA','Tamb':'NA','Hamb':'NA','WS':'NA','WD':'NA'}, columns =['Date', 'DA', 'GHI','GTI','Tmod','Tamb','Hamb','WS','WD'])
  elif(Type == 'MFM'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'PR1':'NA', 'PR2':'NA', 'PR1_GTI':'NA', 'PR2_GTI':'NA', 'LastRead':'NA', 'LastTime':'NA', 'GA':'NA', 'PA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'PR1', 'PR2', 'PR1_GTI', 'PR2_GTI', 'LastRead', 'LastTime', 'GA', 'PA'])
  return df_template

#Generating site information for Mail
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

#Getting Irradiance Value
def get_irr(date, irr_stn):
  irr_data={'GHI':'NA', 'DA':'NA'}
  gen2 = '/home/admin/Dropbox/Second Gen/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt' 
  gen1 = '/home/admin/Dropbox/Gen 1 Data/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt'
  
  if(os.path.exists(gen2)):
    df = pd.read_csv(gen2, sep = '\t')
    irr_data['GHI'] = df['GHI'][0]
    irr_data['DA'] = df['DA'][0]
  
  if(os.path.exists(gen1)):
    df = pd.read_csv(gen1, sep = '\t')
    GTIGreater20 = df[df['POAI_avg']>20]['ts']
  else:	
    GTIGreater20 = pd.DataFrame({'ts' : []})
  
  return irr_data, GTIGreater20
  
def add_graphs(enddate, startdate):
  os.system(" ".join(["python3 /home/admin/CODE/IN303LDigest/WR.py"]))
  os.system(" ".join(["Rscript /home/" + user + "/CODE/EmailGraphs/PR_Graph_Azure.R", stn[1:-2], str(budget_pr), str(rate), cod_date, enddate]))
  os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Inverter_CoV_Graph.py", stn[1:-2], enddate, str(inv_limit), cod_date,'" "','"Inverter CoV"']))
  os.system(" ".join(['python3 /home/' + user + "/CODE/EmailGraphs/Meter_CoV_Graph.py", stn[1:-2], enddate, str(mfm_limit)]))
  os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/GHI_vs_PR.py", stn[1:-2],'" "', "2021-01-01", enddate]))
  if(cod_date == 'TBC'):
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, startdate]))
  else:
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, cod_date]))
  
  graph_list = ['GHI vs PR','PR Evolution', 'Inverter CoV', 'Meter CoV', 'Grid Availability', 'Plant Availability', 'Data Availability', 'System Availability']
  path_list = []
  
  for i in range(len(graph_list)):
    check = '/home/' + user + '/Graphs/Graph_Output/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.pdf'
    if(os.path.exists(check)):
      path_list.append(check)
    
    check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.txt'
    if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Daily.txt'
  if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Monthly.txt'
  if(os.path.exists(check)):
      path_list.append(check)
      
  check = '/home/admin/Graphs/WINDROSE/[IN-303L] Graph - Wind Rose.pdf'
  if(os.path.exists(check)):
      path_list.append(check)

  return(path_list) 
   
#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#checking Stn_Mail.txt
if(os.path.exists(startpath + stn[1:8] + "_Mail.txt")):
  print('Exists')
else:
  try:
    shutil.rmtree(path_write_2g, ignore_errors=True)	
    shutil.rmtree(path_write_3g, ignore_errors=True)	
    os.remove(path_write_4g)
  except Exception as e:
    print(e)
  with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "_Mail.txt") as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def process(date, stn):
  file_path = []
  inv_ylds = []
  meter_ylds = []
  total_eac = 0
  total_pr = 0
  Dyield = ''
  Dia = ''
  
  digest_body = site_info(date, Site_Name, Location, stn[1:7], capacity, NO_MFM, cod_date)
  fullsite_pos = len(digest_body)
  
  #If WMS is not present
  if(irr_stn != 'NA'):
    irr_data, GTIGreater20 = get_irr(date, irr_stn)
    df_2g = pd.DataFrame({'Date': [date], 'DA': [irr_data['DA']], 'GHI' : [irr_data['GHI']]}, columns =['Date', 'DA', 'GHI'])
    GHI = df_2g['GHI'][0]
    if irr_stn != 'NotRequired' and wms_name != 'NotRequired':
      digest_body += '------------------------------\nWMS from IN-302L\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGTI (Pyranometer) [kWh/m^2] (From ' + irr_stn[1:8] +'): ' + str(df_2g['GHI'][0]) + "\n\n"
    df_merge = df_2g.copy()
    cols_4g = ['Date', 'WMS.DA', 'WMS.GHI']
  else:
    cols_4g = ['Date']

  for i in components:
    chkdir(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      df_3g = pd.read_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', sep='\t')
      
    if(os.path.exists(path_write_4g)):
      df_4g = pd.read_csv(path_write_4g, sep='\t')

    if ('MFM' in i):
      if NO_MFM < 10:
        file_name = i[:3] + i[4]
      elif NO_MFM < 99:
        file_name = i[:3] + i[4:6]
      else:
        file_name = i[:3] + i[4:7]
      df_2g = create_template('MFM', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', sep='\t')
        
        df_2g['DA'] = round(len(df['W_avg'].dropna())/2.88, 1)
        df_2g['Eac1'] = round(df['W_avg'].sum()/12000,2)
        df_2g['Yld1'] = round((df['W_avg'].sum()/(meters_mapped[i][1]*12000)),2)
        
        if len(df['TotWhExp_max'].dropna())>0:
          dataint = df.dropna(subset=['TotWhExp_max'])
          dataint = dataint[dataint['TotWhExp_max'] != 0]
          df_2g['Eac2'] = round((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/1000,2)
          df_2g['Yld2'] = round(((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/meters_mapped[i][1])/1000,2)
          df_2g['LastRead'] = round((dataint['TotWhExp_max'].tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
        
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1'] = round((df_2g['Yld1'][0]*100)/GHI,1)
          
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2'] = round((df_2g['Yld2'][0]*100)/GHI,1)

        if(GTI == 0 or GTI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1_GTI'] = round((df_2g['Yld1'][0]*100)/GTI,1)
          
        if(GTI == 0 or GTI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2_GTI'] = round((df_2g['Yld2'][0]*100)/GTI,1)
        
        if(df_2g['Eac2'][0]!='NA'):
            total_eac += df_2g['Eac2'][0]
            meter_ylds.append(df_2g['Yld2'][0])
        
        if(df_2g['PR2_GTI'][0]!='NA'):
            total_pr += df_2g['PR2_GTI'][0] * meters_mapped[i][1]
        
        if len(GTIGreater20)>0:
          mfm_df = df[['ts', 'W_avg', 'Hz_avg']]
          null_rows = mfm_df[mfm_df.isnull().any(axis=1)]
          GTIGreater20 = GTIGreater20[~GTIGreater20.isin(null_rows['ts'])]
          FrequencyGreater = df[df['Hz_avg']>40]['ts']
          PowerGreater = df[df['W_avg']>2]['ts']
          common = pd.merge(GTIGreater20, FrequencyGreater, how='inner')
          common2 = pd.merge(common, PowerGreater, how='inner')
          if len(common)>0:
            df_2g['GA'] = round((len(common)*100/len(GTIGreater20)),1)
            df_2g['PA'] = round((len(common2)*100/len(common)),1)
        
      digest_body += '------------------------------\n' + meters_mapped[i][0] + '\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nSize [kWp]: ' + str(meters_mapped[i][1]) + '\n\nEAC method-1 (Pac) [kWh]: ' + str(df_2g['Eac1'][0]) + '\n\nEAC method-2 (Eac) [kWh]: ' + str(df_2g['Eac2'][0]) + '\n\nYield-1 [kWh/kWp]: ' + str(df_2g['Yld1'][0]) + '\n\nYield-2 [kWh/kWp]: ' + str(df_2g['Yld2'][0]) + '\n\nPR-1 (GHI) [%]: ' + str(df_2g['PR1'][0]) + '\n\nPR-2 (GHI) [%]: ' + str(df_2g['PR2'][0])+'\n\nPR-1 (GTI) [%]: ' + str(df_2g['PR1_GTI'][0]) + '\n\nPR-2 (GTI) [%]: ' + str(df_2g['PR2_GTI'][0]) + '\n\nLast recorded value [kWh]: ' + str(df_2g['LastRead'][0]) + '\n\nLast recorded time: ' + str(df_2g['LastTime'][0]) + '\n\nGrid Availability [%]: ' + str(df_2g['GA'][0]) + '\n\nPlant Availability [%]: ' + str(df_2g['PA'][0]) + '\n\n'      
      
    elif ('INV' in i):
      if len(inverters) < 9:	
        file_name = i[0] + i[9]	
      elif len(inverters) < 99:
        file_name = i[0] + i.split('_')[1]
      else:
        file_name = i[0] + i[9:12]
      df_2g = create_template('INV', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        if('Wh_sum' in df.columns.tolist()):
          df_2g['DA'] = round(len(df['Wh_sum'].dropna())/2.88, 1)
          df_2g['Eac1'] = round((df['AphA_avg'].sum())/12000, 2)
          df_2g['Yld1'] = round((df_2g['Eac1'][0]/inv_mapped[i]), 2)
          
          if len(df['Wh_sum'].dropna()) > 0:
            dataint = df.dropna(subset=['Wh_sum'])
            df_2g['Eac2'] = round(dataint['Wh_sum'].sum()/1000, 2)
            df_2g['LastRead'] = round((dataint['Wh_sum'].tail(1).values[0])/1000, 2)
            df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
          
          if df_2g['Eac2'][0] != 'NA':
            df_2g['Yld2'] = round((df_2g['Eac2'][0]/inv_mapped[i]), 2) 
          
          if len(GTIGreater20)>0:
            if len(df[df['Hz_avg']>40])>0:
              FrequencyGreaterthan40 = df[df['Hz_avg']>40]['ts']
              common = pd.merge(GTIGreater20, FrequencyGreaterthan40, how='inner')
            else:
              VoltageGreaterthan150 = df[df['PhVphB_avg']>150]['ts']
              common = pd.merge(GTIGreater20, VoltageGreaterthan150, how='inner')
            DCPowerGreater2 = df[df['W_avg']>0]['ts']
            common2 = pd.merge(common, DCPowerGreater2, how='inner')
            if len(common) > 0:
              df_2g['IA'] = round((len(common2)*100/len(common)), 1)
          if(df_2g['Eac2'][0]=='NA'):	
            pass	
          else:	
            inv_ylds.append(df_2g['Yld2'][0])
        if (i == 'INVERTER_71_Inverter 01'):
          Dyield +=  '\n\nYield INV-' + '63' + ': ' + str(df_2g['Yld2'][0])
        else:
          Dyield +=  '\n\nYield INV-' + i[21:] + ': ' + str(df_2g['Yld2'][0])
        Dia += '\n\nInverter Availability INV-' + i[21:] + ' [%]: ' + str(df_2g['IA'][0])
        
    elif ('WMS' in i):
      file_name = i[:3] + i[4]
      df_2g = create_template('WMS', date)
      if (os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        df_2g['DA'] = round(len(df['POAI_avg'].dropna())/2.88, 1)
        if len(df['POAI_avg'].dropna()) > 0 : 
          GHI = round(((df['POAI_avg'].dropna()).sum())/12000, 2)
          df_2g['GHI'] = GHI
        else:
          GHI = 'NA'
        
        if len(df['TmpBOM_avg'].dropna()) > 0:
          df_2g['Tmod'] = round((df['TmpBOM_avg'].dropna()).mean(), 1)
        
        GTIGreater20 = df[df['POAI_avg']>20]['ts']
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_2_Meteocontrol Irradiance Sensor' + '/' + stn + '-' + 'WMS2' + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_2_Meteocontrol Irradiance Sensor' + '/' + stn + '-' + 'WMS2' + '-' + date + '.txt',sep='\t')

        if len(df['POAI_avg'].dropna()) > 0:
          GTI = round(((df['POAI_avg'].dropna()).sum())/12000, 2)
          df_2g['GTI'] = GTI
        else:
          GTI = 'NA'
          PR1_GTI = 'NA'
          PR2_GTI = 'NA'
      
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_3_PT1000 Module Temp. Sensor' + '/' + stn + '-' + 'WMS3' + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_3_PT1000 Module Temp. Sensor' + '/' + stn + '-' + 'WMS3' + '-' + date + '.txt',sep='\t')
        
        if len(df['TmpBOM_avg'].dropna()) > 0:
          df_2g['Tmod'] = round((df['TmpBOM_avg'].dropna()).mean(), 1)
      
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_4_Wind, Humidity and Amb. Temp. Sensor' + '/' + stn + '-' + 'WMS4' + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + 'WMS_4_Wind, Humidity and Amb. Temp. Sensor' + '/' + stn + '-' + 'WMS4' + '-' + date + '.txt',sep='\t')
        
        if 'Humidity_avg' in df.columns.tolist():
          if len(df['Humidity_avg'].dropna()) > 0:
            df_2g['Hamb'] = round((df['Humidity_avg'].dropna()).mean(), 1)

        if 'TmpAmb_avg' in df.columns.tolist():
          if len(df['TmpAmb_avg'].dropna()) > 0:
            df_2g['Tamb'] = round((df['TmpAmb_avg'].dropna()).mean(), 1)

        if 'WndSpd_avg' in df.columns.tolist():
          if len(df['WndSpd_avg'].dropna()) > 0:
            df_2g['WS'] = round((df['WndSpd_avg'].dropna()).mean(), 1)

        if 'WndDir_avg' in df.columns.tolist():
          if len(df['WndDir_avg'].dropna()) > 0:
            df_2g['WD'] = round((df['WndDir_avg'].dropna()).mean(), 1)


      digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI from WMS_1 [kWh/m^2]: ' + str(df_2g['GHI'][0]) + '\n\nGTI from WMS_2 [kWh/m^2]: ' + str(df_2g['GTI'][0]) + '\n\nAvg Tmod [C]: ' + str(df_2g['Tmod'][0]) + '\n\nAvg Tamb [C]: ' + str(df_2g['Tamb'][0])+ '\n\nHumidity: '+str(df_2g['Hamb'][0])+'\n\nAvg Wind Speed [m/s]: ' + str(df_2g['WS'][0])+ '\n\nAvg Wind Direction [o]: ' + str(df_2g['WD'][0])+'\n\n'
      
    
    #Saving 2G 
    df_2g.to_csv(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', na_rep='NA', sep='\t',index=False)
    file_path.append(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
    
    #Saving 3G
    chkdir(path_write_3g + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      if((df_3g['Date'] == date).any()):
        vals = df_2g.values.tolist()
        df_3g.loc[df_3g['Date'] == date,df_3g.columns.tolist()] = vals[0]
        df_3g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='w', index=False, header=True)
      else:
        df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=False)
    else:
      df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=True) #First time
    
    
    #If WMS is present
    if("WMS" in i and irr_stn == 'NA'):
      df_merge = df_2g.copy()
    else:
      df_merge = df_merge.merge(df_2g, on = 'Date')
      
    cols_4g = cols_4g + [i + '.' + n for n in df_2g.columns.tolist()[1:]] 
  df_merge.columns = cols_4g
  
  #Saving 4G
  if not os.path.exists(path_write_4g):
    print('firsttime')
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=True)
  elif((df_4g['Date'] == date).any()):
    vals = df_merge.values.tolist()
    df_4g.loc[df_4g['Date']== date, df_4g.columns.tolist()] = vals[0]
    df_4g.to_csv(path_write_4g, sep = '\t', mode='w', index=False, header=True)
  else:
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=False)
  print('4G Done')
  
  std=np.std(inv_ylds)
  cov=(std*100/np.mean((inv_ylds)))
  digest_body += '------------------------------\nInverters\n------------------------------' + Dyield + '\n\nStdev/COV Yields: ' + str(round(std,1)) + ' / ' + str(round(cov,1)) + '%' + Dia
  malumbra_path = "/home/admin/Dropbox/GIS_API3/MALUMBRA/MALUMBRA_AGGREGATE.txt"
  temp = pd.read_csv(malumbra_path,sep='\t')
  malumbra_gti = temp['GTI'].to_list()[-2]
  yesterday = (datetime.datetime.strptime(date,"%Y-%m-%d") - datetime.timedelta(days=1)).strftime("%Y-%m-%d")
  digest_body += '\n\n----------------------------------------\n'+ str(yesterday) +' MALUMBRA API\n----------------------------------------'+'\n\nMALUMBRA API - GTI [kWh/m^2]: ' + str(malumbra_gti) + '\n\n'
  for i in meters:
    file_name = i[:3] + i[4]
    path = path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt'
    temp = pd.read_csv(path,sep='\t')
    mfm_yld1_global = temp['Yld1'][0]
    mfm_yld2_global = temp['Yld2'][0]
    malumbra_PR1 = round((mfm_yld1_global*100)/malumbra_gti,1)
    malumbra_PR2 = round((mfm_yld2_global*100)/malumbra_gti,1)
    digest_body += '--------------------------\n' + i + '\n--------------------------' + '\n\nPR-1 (MALUMBRA API - GTI) [%]: ' + str(malumbra_PR1)+ '\n\nPR-2 (MALUMBRA API - GTI) [%]: ' + str(malumbra_PR2) +'\n\n'
  
  fullsite = '--------------------------\nFull Site\n--------------------------\n\n' + 'System Full Generation [kWh]: ' + str(round(total_eac, 2)) + '\n\nSystem Full Yield [kWh/kWp]: ' + str(round(total_eac/capacity, 2)) + '\n\nSystem Full PR [%]: ' + str(round(total_pr/capacity, 1))
  
  try:
    os.system(" ".join(["python3 /home/"+user+"/CODE/ReconnectForecasting/generalised_penalty_calculation.py", date, stn[1:-2], "15" , "5"]))
    file_path.insert(0, "/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+stn[1:-2]+"/"+stn+" "+date+" - DSM Penalty.csv")
    enddate = (datetime.datetime.strptime(date, '%Y-%m-%d')+datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    penalty = pd.read_csv("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+stn[1:-2]+"/"+stn+" "+date+" - DSM Penalty.csv")
    penalty_payable = penalty[(penalty['Time Stamp'] >= str(date)) & (penalty['Time Stamp'] < str(enddate))]['Net Penalty payable to State DSM Pool'].sum()
    fullsite += '\n\nPenalty Payable to DISCOM [Rs.]: ' + str(round(penalty_payable, 2))
  except:
    pass
       
  if irr_stn != 'NA':
   if round(total_pr/capacity, 1)>60 and round(total_pr/capacity, 1)<70:
     fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + '): 70 >>> PR Floor Activated!'
   elif round(total_pr/capacity, 1)>90:
     fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + '): 85 >>> PR Ceiling Activated!'
   else:
     fullsite += '\n\nReporting PR (From ' + irr_stn[1:8] + '): ' + str(round(total_pr/capacity, 1))
  
  if NO_MFM > 1:
    stdm = np.std(meter_ylds)
    covm = std*100/np.mean((meter_ylds))
    fullsite += '\n\nStdev/COV Yields: ' + str(round(stdm,1)) + ' / ' + str(round(covm,1)) + '%'
  fullsite += "\n\n"
  
  digest_body = digest_body[:fullsite_pos] + fullsite + digest_body[fullsite_pos:]
  return [digest_body, file_path]

#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Historical Processing!')
  processed_data = process(str(startdate)[:10], stn)
  if(bot_type == 'Mail'):
    graph_paths = add_graphs(str(startdate)[:10], start)
    processed_data[1] = graph_paths + processed_data[1]
    send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
  try:
    date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    with open(startpath + stn[1:8] + "_Bot.txt", "w") as file:
      file.write(timenow)   
    processed_data = process(date, stn)
    print('Done Processing')
    if(datetime.datetime.now(tz).hour == 4 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):  # 
      print('Sending')
      date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
      processed_data = process(date_yesterday, stn)
      graph_paths = add_graphs(date_yesterday, start)
      processed_data[1] = graph_paths + processed_data[1]
      send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
      with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
          file.write(timenow + "\n" + date_yesterday)   
      startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    print('Sleeping')
  except:
    print('error')
    logging.exception('msg')
  time.sleep(300)