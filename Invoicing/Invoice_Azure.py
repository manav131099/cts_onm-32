import pandas as pd
import datetime
import pytz
import pyodbc
import os
from calendar import monthrange
import math



tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/HQDigest/Invoice/'
path_lt='/home/admin/Dropbox/Lifetime/Gen-1/'
path_tesco='/home/admin/Dropbox/Gen 1 Data/'
stations=['[TH-010L]', '[TH-011L]', '[TH-012L]', '[TH-013L]', '[TH-014L]', '[TH-015L]', '[TH-016L]', '[TH-017L]', '[TH-018L]', '[TH-019L]', '[TH-020L]', '[TH-021L]', '[TH-022L]', '[TH-023L]', '[TH-024L]', '[TH-025L]', '[TH-026L]', '[TH-027L]', '[TH-028L]']
invoicing_sites = ['MY-008','MY-015','MY-502','MY-503','MY-511','MY-514','MY-500','MY-471','MY-446','MY-497','MY-456','MY-515','MY-426','IN-076', 'IN-077', 'IN-084', 'IN-094', 'IN-092', 'IN-096', 'IN-098', 'IN-090', 'IN-093', 'IN-100', 'IN-101', 'IN-095','MY-014', 'MY-489', 'MY-451', 'MY-504', 'MY-458', 'MY-478', 'MY-490', 'MY-485', 'MY-465', 'MY-461', 'MY-454', 'MY-473', 'MY-484', 'MY-496', 'MY-494', 'MY-453', 'MY-492', 'MY-493', 'MY-481', 'MY-488', 'MY-486', 'MY-457', 'MY-491', 'MY-483', 'MY-482', 'MY-467', 'MY-495', 'MY-479', 'MY-428', 'MY-468', 'MY-463', 'MY-470', 'MY-464', 'MY-472', 'MY-462', 'MY-498','MY-427', 'MY-434', 'MY-437', 'MY-440', 'MY-441', 'MY-444', 'MY-445', 'MY-450', 'MY-455', 'MY-443', 'MY-423', 'MY-415', 'MY-432', 'MY-431', 'MY-416', 'MY-436', 'MY-452', 'MY-447', 'MY-429', 'MY-438', 'MY-430', 'MY-422','MY-439','MY-424','MY-417','MY-418','MY-419','MY-005','MY-408','MY-414','MY-420','MY-421','MY-410','MY-413','MY-433','MY-411','MY-010','MY-011','MY-012','MY-013','MY-412','MY-409','MY-425','MY-435','MY-430','MY-448','KH-001', 'KH-002', 'KH-003', 'KH-004', 'KH-005', 'KH-006', 'KH-007', 'KH-008', 'KH-009', 'IN-004', 'IN-005', 'IN-006', 'IN-007', 'IN-009', 'IN-011', 'IN-012', 'IN-013', 'IN-013', 'IN-014', 'IN-016', 'IN-017', 'IN-018', 'IN-019', 'IN-020', 'IN-021', 'IN-026', 'IN-027', 'IN-029', 'IN-031', 'IN-032', 'IN-033', 'IN-034', 'IN-037', 'IN-038', 'IN-039', 'IN-040', 'IN-041', 'IN-042', 'IN-043', 'IN-044', 'IN-045', 'IN-046', 'IN-047', 'IN-048', 'IN-049', 'IN-052', 'IN-054', 'IN-055', 'IN-056', 'IN-057', 'IN-058', 'IN-059', 'IN-060', 'IN-061', 'IN-062', 'IN-063', 'IN-064', 'IN-066', 'IN-067', 'IN-068', 'IN-071', 'IN-072', 'IN-073', 'IN-074','IN-076', 'IN-075', 'IN-079', 'IN-080', 'IN-081', 'IN-082', 'IN-083', 'IN-085', 'IN-086', 'IN-087', 'IN-088','IN-090','IN-091','IN-092','IN-093','IN-094','IN-026','IN-046','IN-044', 'MY-001', 'MY-002', 'MY-003', 'MY-004', 'MY-006', 'MY-007', 'MY-009', 'MY-010', 'MY-011', 'MY-401', 'MY-402', 'MY-403', 'MY-404', 'MY-405', 'MY-406', 'MY-407', 'MY-409', 'MY-412', 'SG-001', 'SG-002', 'VN-001', 'VN-002', 'VN-003']
def tesco(timenowdate):
    substations=[['MFM_1_PV Meter','MFM_2_Grid meter'], ['MFM_1_PV meter','MFM_2_Grid Meter'], ['MFM_2_PV Meter','MFM_1_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_2_PV Meter','MFM_1_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV MFM-1','MFM_2_PV MFM-2','MFM_3_EB MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV Meter','MFM_2_Grid Meter'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_2_PV Meter','MFM_1_Grid MFM']]
    for j,i in enumerate(stations):
        try:
            temp=substations[j]
            print(i)
            for index,k in enumerate(temp):
                df=pd.read_csv(path_tesco+i+'/'+timenowdate[0:4]+'/'+timenowdate[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+timenowdate[0:10]+'.txt',sep='\t')
                if(i[1:-2]!='TH-024'):
                    if(index==0):
                        if(i[1:-2]=='TH-010' or i[1:-2]=='TH-017' or i[1:-2]=='TH-021' or i[1:-2]=='TH-022' or i[1:-2]=='TH-014'):
                            frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                        elif(i[1:-2]=='TH-016'):
                            frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'D','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)}
                        else:
                            frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'A','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                    elif(index==1):
                        if(i[1:-2]=='TH-012' or i[1:-2]=='TH-015' or i[1:-2]=='TH-018'):
                            frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)} 
                        else:
                            frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'B','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)}
                else:
                    if(index==0):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'A','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                    elif(index==1):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'B','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                    elif(index==2):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime':df.iloc[240,0] ,'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)} 
                result = pd.DataFrame(frame,columns=['Operation','MeterRef','LastTime','LastRead','ReadingInv'],index=[0])
                result.to_csv(path+'EOD_Meter_Reading_'+timenowdate+'.txt',sep='\t',index=False,mode='a',header=False) 
        except:
            print('Failed')

#Getting data from Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

date=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query('SELECT TOP (1000) [Station_Id],[Station_Name],[GIS_Location] FROM [dbo].[stations] ', connStr)
df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','GIS_Location'])
SQL_Query = pd.read_sql_query('SELECT [Meter_Id],[Reference] FROM [dbo].[Meters] ', connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Meter_Id','Reference'])
SQL_Query = pd.read_sql_query("SELECT [Date],[Station_Id],[Meter_Id],[LastR-MFM] FROM [dbo].[Stations_Data] Where Date='"+date+"'", connStr)
df_stations_data = pd.DataFrame(SQL_Query, columns=['Date','Station_Id','Meter_Id','LastR-MFM'])

#Merging
df_merge=pd.merge(df_meters,df_stations_data,on='Meter_Id',how="left")
df_merge=pd.merge(df_merge,df_stations,on='Station_Id',how="left")

df_merge=df_merge[['Station_Name','Reference','LastR-MFM']]
df_merge['Station_Name']=df_merge['Station_Name'].str.strip()
df_merge['LastTime']=None

#Adding Timestamp
for index,row in df_merge.iterrows():
    try:
        df_temp=pd.read_csv(path_lt+row['Station_Name']+'-LT.txt',sep='\t')
        last_time=df_temp.loc[df_temp['Date']==date,'LastT']
        df_merge.loc[index,'LastTime']=last_time.values[0]
    except:
        pass

df_merge['ReadingInv'] = df_merge['LastR-MFM'].apply(lambda x: round(x, 0))

df_merge.columns=['Operation','MeterRef','LastRead','LastTime','ReadingInv']
df_merge=df_merge[['Operation','MeterRef','LastTime','LastRead','ReadingInv']]

df_merge=df_merge.dropna(subset=['Operation'])
df_merge=df_merge.fillna('NA')
df_merge=df_merge.replace({0:'NA'})
print(df_merge)
for i in stations:
    df_merge = df_merge[df_merge['Operation'] != i[1:-2]]
df_invoicing = pd.DataFrame()
for i in invoicing_sites:
    print(i)
    df_temp = df_merge.loc[df_merge['Operation']==i,:]
    df_invoicing = df_invoicing.append(df_temp)

df_invoicing = df_invoicing.loc[df_invoicing['MeterRef']!='KH-003B',:]

df_invoicing['LastTime'] = pd.to_datetime(df_invoicing['LastTime'], errors='coerce')
df_invoicing = df_invoicing.fillna('NA')
df_invoicing.to_csv(path+'EOD_Meter_Reading_'+date+'.txt',sep='\t',index=False)
tesco(date)