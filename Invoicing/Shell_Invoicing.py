import pandas as pd
import sharepy
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import sys
import numpy as np
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

def send_mail(info,attachment_path_list=None):
    s = smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    #s.set_debuglevel(1)
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    #recipients = ['sai.pranav@cleantechsolar.com']
    recipients = ['Daniel.Boey@meinhardtgroup.com','Navin.Ramudaram@shell.com','Sze-Lin.Toh@shell.com']
    msg['Subject'] = "Shell Retail MY - Monthly Generation Data"
    if sender is not None:
        msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    msg['Cc'] = ", ".join(['operationsMY@cleantechsolar.com'])
    msg['Bcc'] = ", ".join(['sai.pranav@cleantechsolar.com','amira.meliki@cleantechsolar.com'])
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())

##Change this
user = 'admin'

#Authentication means for SP
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
base_path = 'https://cleantechenergycorp.sharepoint.com'
auth = sharepy.connect(base_path, username = username, password = password)

#Authentication means for Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)


#Declaring dates for summary
today = datetime.today()
start = datetime((today + timedelta(days = -28)).year, (today + timedelta(days = -28)).month, 1)
end = start + relativedelta(months=1) + timedelta(days=-1)
month_name=start.strftime("%B")
print("Start Date: ", start.date())
print("End Date  : ", end.date())
print('/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv')
#Fetching and cleaning data from MY SHELL RETAIL
filedownload1 = 'https://cleantechenergycorp.sharepoint.com/sites/CSDC-OMMY/Shared%20Documents/MY-400%20Shell-MY%20Retail/Shell MY-Retail Details.xlsx'
fileread1 = '/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell MY-Retail Details_Raw.xlsx'
response1 = auth.getfile(filedownload1, filename = fileread1)
print(response1)
dfread1 = pd.read_excel(fileread1,engine='openpyxl')
dfread1 = dfread1[['System Code O&M', 'Name ', 'System Size [kWp]', 'COD', 'Project Code']]
dfread1 = dfread1.dropna(subset=['System Code O&M'])
dfread1 = dfread1.dropna(subset=['COD'])

#Fetching and cleaning data from MY SHELL Program Tracker
filedownload2 = 'https://cleantechenergycorp.sharepoint.com/:x:/r/sites/Malaysia/Shared%20Documents/Shell/01%20-%20Program%20Tracker/200420-Program%20tracker-Rev.00.xlsx'
fileread2 = '/home/' + user + '/Dropbox/Customer/Shell_Invoicing/200420-Program tracker-Rev.00.xlsx'
response2 = auth.getfile(filedownload2, filename = fileread2)
dfread2 = pd.read_excel(fileread2,engine='openpyxl')
dfread2.columns = dfread2[16:17].values[0]
dfread2.drop(dfread2.index[0:17], inplace=True)
dfread2.reset_index(drop = True, inplace=True)
dfread2 = dfread2[['Shell Station ID', 'Cleantech reference']]


#Merging both dataframes
dfread = pd.merge(dfread1, dfread2, how='left', left_on='Project Code', right_on='Cleantech reference')
dfread = dfread[['Shell Station ID', 'System Code O&M', 'Name ', 'System Size [kWp]']]


#Creating data frame
Site_Ids = list(dfread['Shell Station ID'])
Site_Ids.insert(0, 'Shell Reference')

Site_Codes = list(dfread['System Code O&M'])
Site_Codes.insert(0, 'Cleantech Reference')

Site_Names = list(dfread['Name '])
Site_Names.insert(0, 'Name')

Site_Capacities = list(dfread['System Size [kWp]'])
Site_Capacities.insert(0, 'Capacity (kWp)')

header = ['Date', 'Daily Generation (kWh)']
#for i in range(len(Site_Codes)-1): header.append('Daily Generation (kWh)')

df = pd.DataFrame([Site_Ids])
df = df.append([Site_Codes])
df = df.append([Site_Names])
df = df.append([Site_Capacities])
df = df.append([header])


##Fetching MFM Folder name and station ID from server
SQL_Query = pd.read_sql_query('''SELECT [Station_Name], [Station_Id], [Station_Columns] FROM [dbo].[Stations] where [Station_Name] LIKE 'MY-4%' OR [Station_Name] LIKE 'MY-5%' ''', connStr)
df_stations = pd.DataFrame(SQL_Query)
MFM_Name = []
Station_id = []
for i in range(1, len(Site_Codes)):
    temp_mfmname = df_stations.loc[(df_stations['Station_Name'].str.strip()==Site_Codes[i]),:]['Station_Columns'].values[0]
    temp_stnid = df_stations.loc[(df_stations['Station_Name'].str.strip()==Site_Codes[i]),:]['Station_Id'].values[0]
    temp_mfmname = ' '.join(temp_mfmname.strip().split(',')[2].replace('"', '').split('.')[:-1])
    MFM_Name.append(temp_mfmname)
    Station_id.append(temp_stnid)


#Creating Summary
while (start <= end):
    curr_date = start.strftime('%Y-%m-%d')
    print("Processing: " + curr_date)
    curr_read = [curr_date]
    
    for i in range(1, len(Site_Codes)):
      fileread = '/home/admin/Dropbox/Gen 1 Data/[' + Site_Codes[i] + 'L]/' + curr_date[:4] + '/' + curr_date[:7] + '/' + MFM_Name[i-1] + '/[' + Site_Codes[i] + 'L]-MFM' + MFM_Name[i-1].split('_')[1] + '-' + curr_date + '.txt'
      eac = 'NA'
      if(os.path.exists(fileread)):
        df_eac = pd.read_csv(fileread, sep='\t')
        df_eac = df_eac['TotWhExp_max'].dropna()
        if(df_eac.empty):
            eac = 'NA'
        else:
            eac = round((df_eac.iloc[len(df_eac)-1]-df_eac.iloc[0])/1000)
      if eac == 'NA' or eac == 0:
        SQL_Query = pd.read_sql_query('''SELECT [Eac-MFM] FROM [dbo].[Stations_Data] where [Station_Id] = ''' + str(Station_id[i-1]) + ''' and [Date] = ' ''' +  str(start) + ''' ' ''', connStr)
        df_eac_server = pd.DataFrame(SQL_Query)
        if(df_eac_server.empty):
            eac = np.nan
        else:
            eac = round(df_eac_server['Eac-MFM'][0])
      if(eac<0):
        eac=0
      curr_read.append(eac)
    
    df = df.append([curr_read])
    start += timedelta(days=1)

df.to_csv('/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv', header = False, index = False)
send_mail('Please find the Generation Report for the month of '+month_name+' attached.',['/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv'])

