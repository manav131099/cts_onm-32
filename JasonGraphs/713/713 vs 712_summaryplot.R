rm(list=ls(all =TRUE))
library(ggplot2)
library(lubridate)
source('/home/admin/CODE/common/math.R')

setwd("~/intern/Data/[713]")
filelist <- dir(pattern = ".txt", recursive= TRUE)

result <- NULL
timemin <- format(as.POSIXct("2016-12-26 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-10-02 17:00:00"), format="%H:%M:%S")

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,20,29)
  gsi <- as.numeric(paste(temp[,5]))
  data[1] <- date
  #data[2] <- round(sum(gsi)/2500,2)
  data[2] <- round(sum(gsi)/60000,2)
  result <- rbind(result,data)
  print(paste(i,' done'))
}

colnames(result) <- c("date","gsi")
rownames(result) <- NULL
result <- result[-1,]
result1 <- data.frame(result)

setwd("~/intern/Data/[712]")
filelist <- dir(pattern = ".txt", recursive= TRUE)
result <- NULL

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,20,29)
  #temp <- temp[condition,]
  gsi <- as.numeric(paste(temp[,3]))
  data[1] <- date
  #data[2] <- round(sum(gsi)/2500,2)
  data[2] <- round(sum(gsi)/60000,2)
  result <- rbind(result,data)
  print(paste(i,' done'))
}

colnames(result) <- c("date","gsi")
rownames(result) <- NULL
result2 <- data.frame(result)
result2 <- result2[-(1:160),]   #removing the starting for 712

result <- cbind(result1,result2[1:length(result1[,1]),2])
rownames(result) <- NULL
colnames(result) <- c("date","gsi713","gsi712")
month <- substr(paste(result[,1]),3,7)
result <- cbind(result,month)
colnames(result) <- c("date","gsi713","gsi712","Month")

write.table(result,"C:/Users/talki/Desktop/cec intern/results/713/712-713_Summary.txt",sep = "\t",row.names = F)

#result <- read.table("~/intern/Results/[713]/1. result/712-713_Summary.txt",header = T)
model <- lm(result[,3]~result[,2])
rsq <- format(as.numeric(summary(model)$r.squared),digits=3,nsmall = 3)

sd1 <- sdp(result[,2], na.rm = T)
sd2 <- sdp(result[,3], na.rm = T)

graph <- ggplot(result, aes(x=gsi713,y=gsi712,group=Month))
grggraph <- graph + geom_point(aes(shape = Month,colour = Month))+
  scale_shape_manual(values=seq(0,(nlevels(result[,4])-1)))+
  theme_bw()+
  geom_abline(intercept = 0, slope = 1)+
  expand_limits(x=0,y=0)+
  coord_cartesian(ylim=c(0,8),xlim=c(0,8))+
  xlab(expression(paste("713 Global Horizontal Irradiation [",kwh/m^2,", daily]")))+
  ylab(expression(paste("712 Global Horizontal Irradiation [",kwh/m^2,", daily]")))+
  ggtitle(paste("713 vs 712 Irradiation - Chennai, India"))+
  annotate("text",label = paste0("R-sq error:",rsq),size=3.6, x = 4, y= 1)+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))
  

grggraph

ggsave(grggraph, filename = paste0("C:/Users/talki/Desktop/cec intern/results/713/IN-713_712_GvsM.pdf"),width =7.92, height =5)


