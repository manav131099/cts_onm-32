### 04/09 [IN-001] EXTRACTION ###

rm(list=ls(all =TRUE))

pathRead <- "~/intern/data/[IN-001T]"
pathwritetxt <- "C:/Users/talki/Desktop/cec intern/results/IN-001T/[IN-001T]_summary.txt"
pathwritecsv <- "C:/Users/talki/Desktop/cec intern/results/IN-001T/[IN-001T]_summary.csv"
setwd(pathRead)
#print("Extracting data..")

filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-06-01 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-10-19 17:00:00"), format="%H:%M:%S")

stationLimit <- 500.24
nameofStation <- "IN-001A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,24,33) 
  
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
   
  #pac - UPTIME
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  pacTemp <- temp[condition,]
  pacTemp <- pacTemp[complete.cases(pacTemp[,2]),]
  pac <- pacTemp[,2]
  numer <- length(pac[pac>0.1])
  pacTemp2 <- pacTemp[pac<0.1,]
  pacTemp2 <- pacTemp2[pacTemp2[,4]>20,]
  denom <- length(pacTemp2[,1])+numer
  flagRate <- numer/denom
  col4[index] <- flagRate*100
  
  #wpac
  gsi <- pacTemp[,4]
  flag <- as.numeric(pac<0.1 & gsi > 20)
  wgsi <- gsi/sum(gsi)
  downloss <- wgsi * flag * 100
  if (length(downloss)>0){
    col5[index] <- 100 - sum(downloss)
  }
  else{
    col5[index] <- NA
  }
  
  #pr
  gsi <- temp[,4]
  eac <- temp[,3][complete.cases(temp[,3])]
  diff <- eac[length(eac)]- eac[1]
  
  if(date >= "2017-06-30")
  {
    col2[index] <- pts/144*100
    pr <- diff/stationLimit/sum(gsi)*6000
  }
  else
  {
    col2[index] <-pts/281*100
    pr <- diff/stationLimit/sum(gsi)*12000
  }
  
  #pr <- diff/stationLimit/sum(gsi)*12000
  if (length(pr)==0){
    col6[index] = NA
  }
  else{
    col6[index] <- pr*100
  }
  
  print(paste(i, " done"))
  index <- index + 1
  
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]

#col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA

print('Starting to save into text/csv file..')

result <- cbind(col0,col1,col2,col3,col4,col5,col6)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points", "Uptime [%]","Weighted Uptime [%]",
                      "Performance Ratio [%]")
result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
datawrite <- result
write.table(result,na = "", pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,na = "", pathwritecsv,row.names = FALSE)

