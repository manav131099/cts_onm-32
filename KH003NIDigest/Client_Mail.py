# KH003S WHILE LOOP FIX


# DEFINE CONSTANTS, PATHS AND IMPORTS
from email.mime import base
import requests, json
import requests.auth
import pandas as pd
import os
from openpyxl import load_workbook
import xlsxwriter
import smtplib
import numpy as np
from email.mime.multipart import MIMEMultipart
import datetime
import pytz
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys
import logging
import time
import shutil


# bot_type = sys.argv[1]
# start = sys.argv[2]
# tz = pytz.timezone('Asia/Calcutta')

tz = pytz.timezone('Asia/Calcutta')

date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
date = date_yesterday

######CHANGES START######
stn = '[KH-003]'                                             #Enter station code for digest
stn1 = '[KH-9003S]'
# irr_stn = 'NotRequired'                                                #NA if WMS is present, NotRequired if to run without WMS
# wms_name = 'NotRequired'                                               #NA if WMS is present, NotRequired if to run without WMS
# distance = '0'                                                #0 if self WMS is present
# components =  ['WMS_1_Irradiance Sensor']                                  #Enter self WMS name if present else empty
# meters = ['MFM_1_Plant Meter']                                     #Enter MFM Folder Name
# meters_mapped = {'MFM_1_Plant Meter' : ['MFM_1', 1001.22]}                  #Enter MFM foldername, MFM Display name, MFM capacity 
# inverters = ['INVERTER_1', 'INVERTER_2', 'INVERTER_3', 'INVERTER_4','INVERTER_5', 'INVERTER_6', 'INVERTER_7', 'INVERTER_8','INVERTER_9', 'INVERTER_10', 'INVERTER_11', 'INVERTER_12','INVERTER_13', 'INVERTER_14', 'INVERTER_15', 'INVERTER_16' ]                                    #Enter Inverter names
# inv_mapped = {'INVERTER_1': 63.36, 'INVERTER_2': 63.36, 'INVERTER_3': 63.36, 'INVERTER_4': 63.36, 'INVERTER_5': 63.36, 'INVERTER_6': 63.36, 'INVERTER_7': 63.36, 'INVERTER_8': 63.36,'INVERTER_9': 63.36, 'INVERTER_10': 63.36, 'INVERTER_11': 63.36, 'INVERTER_12': 63.36,'INVERTER_13': 63.36, 'INVERTER_14': 63.36, 'INVERTER_15': 57.09, 'INVERTER_16': 57.09}    #Enter Inverter names and Capacities
user = 'admin'
recipients = ['operationsKH@cleantechsolar.com', 'om-it-digest@cleantechsolar.com', 'kpengly@coca-cola.com.kh', 'ksophea@coca-cola.com.kh', 'ksreyleak@coca-cola.com.kh', 'ctong@coca-cola.com.kh', 'pthangrak@coca-cola.com.kh', 'edany@coca-cola.com.kh']
# recipients = ['amogh.diwan@cleantechsolar.com', 'anusha.agarwal@cleantechsolar.com']
#SiteDetails
Site_Name = 'Coca-Cola Cambodia'
Location = 'Phnom Penh, Cambodia'
brands = "Module Brand / Model / Nos: Canadian Solar / CS6X-320P / 8200" + "\n\n" + "Inverter Brand / Model / Nos: SMA / STP60-10 / 36" + "\n\n"
cod_date = '2016-10-03'                                              #Enter COD if available else TBC
######CHANGES END######
mail_path =  "/home/"+user+"/Start/MasterMail/KH-9003S_Mail.txt"
NO_MFM = 2
capacity = 2560
if(os.path.exists(mail_path)):
  print('Exists')
else:
  with open(mail_path, "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + date)
    
with open(mail_path) as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")


# WRITE CODE TO GENERATE CSV DATA
def gen_csv(
    date: str, 
    save_file_path: str
    ):
    '''
    Arguments: date -> in string format "%Y-%m-%d"
               save_file_path
    Returns: None
    Help:-
    Collates monthly data till the given data and saves it into a csv whose path == save_file_path
    '''
    df_month = None
    for day in range(1, int(date[8:])+1):
        file_date = date[:8] + str(day).zfill(2)
        df_day = retrieve_data(file_date)
        if day == 1:
            df_month = df_day
        else:
            df_month = df_month.append(df_day)
    folder = "/".join(save_file_path.split('/')[:-1])
    if not os.path.exists(folder):
        os.makedirs(folder)
        print("Created path: \n{folder}".format(folder = folder))
    df_month.to_csv(save_file_path, index=None)

def retrieve_data(date: str):
    """
    Arguments: date -> in string format "%Y-%m-%d"
    Returns: pd.DataFrame columns -> ["date", 'EnergyGeneration', 'LastTime', 'LastRead', 'PR']
             1 Row only
    Help:-
    Retrieves relevant 2g data from file path of format -> '/home/admin/Dropbox/Second Gen/[KH-003S]/{year}/{year}-{month}/[KH-003S] {date}.txt'
    """
    colNames = ["Date", 'LastTime', 'EnergyGeneration', 'LastRead', 'PR']
    colDict = {}
    for c in colNames:
        if c == 'Date':
            colDict[c] = [date]
        else:
            colDict[c] = 'NA'
    df = pd.DataFrame(colDict, columns=colNames)
    paths = [
        '/home/admin/Dropbox/Second Gen/[KH-003S]/{year}/{year}-{month}/[KH-003S] {date}.txt'.format(year=date[:4], month=date[5:7], date=date)
    ]
    for path in paths:
        if not os.path.isfile(path):
            continue
        # print(path)
        data = pd.read_csv(path, sep = '\t')[["Date", "EacCokeMeth2", "LastTimeCoke", "LastReadCoke", "PR2Coke"]]
        df['Date'] = data['Date']
        df['EnergyGeneration'] = data['EacCokeMeth2']
        df['LastTime'] = data['LastTimeCoke']
        df['LastRead'] = data['LastReadCoke']
        df['PR'] = data['PR2Coke']
    return df



#Generating site information for Mail
def site_info(
    date: str, 
    site_name: str =Site_Name,
    location: str =Location, 
    stn: str =stn, 
    capacity: float =capacity, 
    nometers: int =NO_MFM, 
    cod_date: str=cod_date
    ):
    """
    Arguemnts: date
    """
    name = "Site Name: " + site_name + "\n\n"
    loc = "Location: " + location + "\n\n"
    code = "O&M Code: " + stn[1:-1] + "\n\n"
    size = "System Size [kWp]: " + str(capacity) + "\n\n"
    no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
    if cod_date == 'TBC':
        cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
    else:
        cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365)
    info = name + loc + code + size + no_meters + brands + cod
    return info


#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    msg['Subject'] = "Station [KH-9003S] Client Digest " + str(date)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name = each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    server.sendmail(sender, recipients, msg.as_string())

def gen_email_body_data(date):
    """
    Arguments: date: str => format: "%Y-%m-%d"
    Returns: digest_body: str: contains all the information to create the digest email body
    Help:
    Calls the gen_csv function as well creating the required csv email attachement
    """
    df = retrieve_data(date)
    dayofWeek = " ("+datetime.datetime.strptime(date, "%Y-%m-%d").strftime('%A')[:3]+") "
    digest_body = site_info(date)
    digest_body+='\n\n_________________________________________\n\n'+ date + dayofWeek + '\n\n_________________________________________\n\n'
    digest_body+= "Energy Generation [kWh]: "+str(df["EnergyGeneration"][0])+"\n\n"
    digest_body+= "PR [%]: "+str(df['PR'][0]) + "\n\n"
    digest_body+= "Last recorded energy meter reading [kWh]: " + str(df["LastRead"][0])+"\n\n"
    digest_body+= "Last recorded timestamp: "+str(df["LastTime"][0])
    digest_body+='\n\n_________________________________________\n\n'
    save_file_path = "/home/"+user+"/Dropbox/Customer/[KH-9003S]/"+date[:7]+'.csv'
    gen_csv(date, save_file_path)
    return digest_body, save_file_path


if __name__ == '__main__':
    date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
    try:
        digest_body, save_file_path = gen_email_body_data(date_yesterday)
        send_mail(date, stn, digest_body, recipients, [save_file_path])
        print("Sent Mail for {date}".format(date =date_yesterday))
        with open(mail_path, "w") as file:
            timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
            file.write(timenow + "\n" +date_yesterday)
    except Exception as e:
        print(e)



# if __name__ == "__main__":
#     #Historical
#     # CONDITION TO CHECK 2G DATA EXISTENCE
#     DATA_UNAVAILABLE = False
#     dataDateCheck = (startdate+datetime.timedelta(days=1)).strftime("%Y-%m-%d")
#     for day in range(1, int(dataDateCheck[8:])+1):
#         file_date = dataDateCheck[:8] + str(day).zfill(2)
#         data_path = '/home/admin/Dropbox/Second Gen/[KH-003S]/{year}/{year}-{month}/[KH-003S] {date}.txt'.format(year=file_date[:4], month=file_date[5:7], date=file_date)
#         count = 0
#         while not os.path.isfile(data_path) and not DATA_UNAVAILABLE:
#             time.sleep(300)
#             count += 1
#             if count == 3:
#                 DATA_UNAVAILABLE = True
#                 break
#     if DATA_UNAVAILABLE:
#         print("Cannot find 2g data, exiting")
#     else:
            
#         print('Historical Started!')
#         startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
#         while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
#             print('Historical Processing!')
#             digest_body,save_file_path = gen_email_body_data(str(startdate)[:10])
#             if(bot_type == 'Mail'):
#                 print("Sending Mail for {date}".format(date = str(startdate)[:10]))
#                 send_mail(str(startdate)[:10], stn, digest_body, recipients, [save_file_path])
#             startdate = startdate + datetime.timedelta(days = 1)
#         startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
#         with open(mail_path, "w") as file:
#             timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
#             file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
#         print('Historical Done!')

#         while(1):
#             try:
#                 date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
#                 timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
#                 digest_body = gen_email_body_data(date)
#                 print('Done Processing')
#                 if(datetime.datetime.now(tz).hour == 5 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
#                     print('Sending')
#                     date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
#                     digest_body, save_file_path = gen_email_body_data(date_yesterday)
#                     send_mail(date_yesterday, stn, digest_body, recipients, [save_file_path])
#                     with open(mail_path, "w") as file:
#                         file.write(timenow + "\n" + date_yesterday)   
#                     startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
#                 print('Sleeping')
#             except Exception as e:
#                 print('error')
#                 print(e)
#                 logging.exception('msg')
#             time.sleep(300)