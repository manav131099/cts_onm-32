rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .030
InstCap = 25.60
timtomins = function(x)
{
	#print(paste('length received',length(x)))
#	print(paste('first',x[1]))
  hr = unlist(strsplit(x,":"))
	if(length(hr) > 1)
	{
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
	}
	return()
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	dataread2 = dataread
	irr = format(round(abs(sum(dataread2[complete.cases(dataread2[,3]),3]))/60000,2),nsmall=2)
	troom = format(round(abs(mean(dataread2[complete.cases(dataread2[,4]),4])),1),nsmall=1)
	hamb = format(round(abs(mean(dataread2[complete.cases(dataread2[,5]),5])),1),nsmall=1)
	hroom = format(round(abs(mean(dataread2[complete.cases(dataread2[,6]),6])),1),nsmall=1)
	hcab = format(round(abs(mean(dataread2[complete.cases(dataread2[,7]),7])),1),nsmall=1)
	tmod = format(round(abs(mean(dataread2[complete.cases(dataread2[,8]),8])),1),nsmall=1)
	tamb = format(round(abs(mean(dataread2[complete.cases(dataread2[,9]),9])),1),nsmall=1)
	tcab = format(round(abs(mean(dataread2[complete.cases(dataread2[,10]),10])),1),nsmall=1)
	
	datanew = dataread2[complete.cases(dataread2[,9]),]
	timecolumn = as.character(datanew[,1])
	valalone = as.numeric(datanew[,9])
	timetambmax = as.character(timecolumn[match(max(valalone),valalone)])
	tambmax = format(round(max(valalone),1),nsmall=1)

	datanew = dataread2[complete.cases(dataread2[,8]),]
	timecolumn = as.character(datanew[,1])
	valalone = as.numeric(datanew[,8])
	timetmodmax = as.character(timecolumn[match(max(valalone),valalone)])
	tmodmax = format(round(max(valalone),1),nsmall=1)
	
	datanew = dataread2[complete.cases(dataread2[,4]),]
	timecolumn = as.character(datanew[,1])
	valalone = as.numeric(datanew[,4])
	timetroommax = as.character(timecolumn[match(max(valalone),valalone)])
	troommax = format(round(max(valalone),1),nsmall=1)

	datanew = dataread2[complete.cases(dataread2[,10]),]
	timecolumn = as.character(datanew[,1])
	valalone = as.numeric(datanew[,10])
	timetcabmax = as.character(timecolumn[match(max(valalone),valalone)])
	tcabmax = format(round(max(valalone),1),nsmall=1)
	cond = ( dataread2[,39] > 0 &  dataread2[,39] < 1000000000)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,39])),]
	dataread =  dataread[cond,]
	totrowsmissing=lasttsol=lastrsol=Eac12=Eac11=lasttcoke=lastrcoke=Eac22=Eac21="NA"
	if(nrow(dataread) > 0)
	{
  	Eac12 = format(round(as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]),1),nsmall=1)
	lastrsol = as.character(dataread[nrow(dataread),39])
	lasttsol = as.character(dataread[nrow(dataread),1])
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,24])),]
  dataread = dataread[as.numeric(dataread[,24])>0,]
	if(nrow(dataread) > 0)
	{
	Eac11 = format(round(sum(as.numeric(dataread[,24]))/60,1),nsmall=1)
	}
	cond = ( dataread2[,86] > 0 &  dataread2[,86] < 1000000000)
	dataread = dataread2[complete.cases(dataread2[,86]),]
	dataread =  dataread[cond,]
	if(nrow(dataread) > 0)
	{
	Eac22 = format(round(as.numeric(dataread[nrow(dataread),86]) - as.numeric(dataread[1,86]),1),nsmall=1)
	lastrcoke = as.character(dataread[nrow(dataread),86])
	lasttcoke = as.character(dataread[nrow(dataread),1])
	}
	dataread = dataread2[complete.cases(dataread2[,71]),]
  dataread = dataread[as.numeric(dataread[,71])>0,]
  if(nrow(dataread)){
	Eac21 = format(round(sum(as.numeric(dataread[,71]))/60,1),nsmall=1)
	}
	
	dataread = dataread2
	dateuse = substr(as.character(dataread[1,1]),1,10)
	DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
	troomsh=hambsh=hcabsh=tmodsh=tcabsh=hroomsh=tambsh=tambshmx=tcabshmx=troomshmx=tmodshmx=NA
	maxhambsh = minhambsh = timemaxhambsh = timeminhambsh=NA
	if(nrow(dataread) > 0)
	{
  tdx = timtomins(substr(dataread[,1],12,16))
	if(length(tdx) > 1)
	{
  dataread2 = dataread[tdx > 479,]
  tdx = tdx[tdx > 479]
  dataread2 = dataread2[tdx < 1020,]
	troomsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,4]),4])),1),nsmall=1)
	
	datanew = dataread2[complete.cases(dataread2[,5]),]
	timecolumn = as.character(datanew[,1])
	valalone = as.numeric(datanew[,5])
	timemaxhambsh = as.character(timecolumn[match(max(valalone),valalone)])
	timeminhambsh = as.character(timecolumn[match(min(valalone),valalone)])
	maxhambsh = format(round(max(valalone),1),nsmall=1)
	minhambsh = format(round(min(valalone),1),nsmall=1)
	
	hambsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,5]),5])),1),nsmall=1)
	hroomsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,6]),6])),1),nsmall=1)
	hcabsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,7]),7])),1),nsmall=1)
	tmodsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,8]),8])),1),nsmall=1)
	tambsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,9]),9])),1),nsmall=1)
	tcabsh = format(round(abs(mean(dataread2[complete.cases(dataread2[,10]),10])),1),nsmall=1)
	tambshmx = format(round(max(dataread2[complete.cases(dataread2[,9]),9]),1),nsmall=1)
	tmodshmx = format(round(max(dataread2[complete.cases(dataread2[,8]),8]),1),nsmall=1)
	troomshmx = format(round(max(dataread2[complete.cases(dataread2[,4]),4]),1),nsmall=1)
	tcabshmx = format(round(max(dataread2[complete.cases(dataread2[,10]),10]),1),nsmall=1)
	dataread2 = dataread2[complete.cases(dataread2[,24]),]
  missingfactor = 600 - nrow(dataread2)
  dataread3 = dataread2[as.numeric(dataread2[,24]) < ltcutoff,]
	if(length(dataread3[,1]) > 0)
	{
		TIMESTAMPSALARM <<- as.character(dataread3[,1])
	}
  totrowsmissing = format(round((((missingfactor + nrow(dataread3))/6.0)),1),nsmall=1)
	}
	}
	RATIO = round((as.numeric(Eac11)*100/as.numeric(Eac12)),1)
	RATIO2 = round((as.numeric(Eac21)*100/as.numeric(Eac22)),1)
	CABLOSS = format(round((100 * (1 - (as.numeric(Eac22)/as.numeric(Eac12)))),2),nsmall=2)
  
  PR = format(round((as.numeric(Eac11)/(as.numeric(irr)*InstCap)),1),nsmall=1)
	YLD = round((as.numeric(Eac11)/InstCap/100),2)
  PR2 = format(round((as.numeric(Eac12)/(as.numeric(irr)*InstCap)),1),nsmall=1)
	YLD2 = round((as.numeric(Eac12)/InstCap/100),2)
  PRSUBS = format(round((as.numeric(Eac21)/(as.numeric(irr)*InstCap)),1),nsmall=1)
  PRSUBS2 = format(round((as.numeric(Eac22)/(as.numeric(irr)*InstCap)),1),nsmall=1)
	YLDSUBS = round((as.numeric(Eac21)/InstCap/100),2)
	YLDSUBS2 = round((as.numeric(Eac22)/InstCap/100),2)
	
	df = data.frame(Date = dateuse, DA=DA, Downtime=totrowsmissing,Irradiance = irr,
	EacSolMeth1 = as.numeric(Eac11),EacSolMeth2=as.numeric(Eac12),RatioSol = RATIO,
	LastTimeSol=lasttsol,LastReadSol=lastrsol,EacCokeMeth1 = as.numeric(Eac21),
	EacCokeMeth2 = as.numeric(Eac22),RatioCoke=RATIO2,LastTimeCoke=lasttcoke,
	LastReadCoke=lastrcoke,CableLoss=CABLOSS,
	Troom=troom,TroomSH=troomsh,Hamb=hamb,HambSH=hambsh,Hroom=hroom,HroomSH=hroomsh,
	Hcab=hcab,HcabSH=hcabsh,Tmod=tmod,TmodSH=tmodsh,Tamb=tamb,TambSH=tambsh,
	Tcab=tcab,TcabSH=tcabsh,TroomMx=troommax,TroomSHMx=troomshmx,TmodMx=tmodmax,
	TmodSHMx=tmodshmx,TambMax=tambmax,TambSHMx=tambshmx,TcabMx=tcabmax,TcabSHMx=tcabshmx,
	TimeMaxTamb =timetambmax, TimeMaxTmod=timetmodmax,TimeMaxTroom=timetroommax,TimeMaxTCab=timetcabmax,
	HambSHMax = maxhambsh,HambSHMin=minhambsh,TimeMaxHambSH=timemaxhambsh,TimeMinHambSH=timeminhambsh,
	Yld1Sol=YLD,Yld2Sol=YLD2,PR1Sol=PR,PR2Sol=PR2,Yld1Coke=YLDSUBS,Yld2Coke=YLDSUBS2,PR1Coke=PRSUBS,PR2Coke=PRSUBS2,
	stringsAsFactors=F)
  write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
	print(filepathm1)
  dataread1 =try(read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F),silent=F)
	if(class(dataread1)=='try-error')
		return()
	df = dataread1
  {
    if(!file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
    else
    {
		  dataread = read.table(writefilepath,header = T,sep="\t")
			idxmtch = match(as.character(df[,1]),as.character(dataread[,1]))
			{
			if(is.finite(idxmtch))
			{
				dataread[idxmtch,] = df[1,]
				write.table(dataread,file=writefilepath,row.names=F,col.names=T,sep="\t",append = F)
			}
			else{
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
		}
	 }
  }
}

