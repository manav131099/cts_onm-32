# DEFINE CONSTANTS, PATHS AND IMPORTS
from email.mime import base
import requests, json
import requests.auth
import pandas as pd
import os
from openpyxl import load_workbook
import xlsxwriter
import smtplib
import numpy as np
from email.mime.multipart import MIMEMultipart
import datetime
import pytz
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys
import logging
import time
import shutil


tz = pytz.timezone('Asia/Calcutta')

# date = sys.argv[1]
date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
date = date_yesterday
user = 'admin'
recipients = ['operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','ngouv_yangphy@angkormilk.com.kh','Paun_vitou@angkormilk.com.kh','vong_dara@angkormilk.com.kh']  
startpath = '/home/' + user + '/Start/MasterMail/'
save_file_path = "/home/"+user+"/Dropbox/Customer/[KH-9004X]/"+date[:7]+'.csv'
# save_file_path = date[:7]+'.csv'
stn = '[KH-004]'
Site_Name = 'Angkor Dairy'
Location = 'Phnom Penh, Cambodia'
brands = "Module Brand / Model / Nos: Canadian Solar / CS6K-270Wp / 1728 Trina Solar / TSM-PD05 270/288" + "\n\n" + "Inverter Brand / Model / Nos:  SMA / STP60-10 / 6" + "\n\n"
cod_date = '2017-03-14'

def site_info(date, site_name=Site_Name, location=Location, stn=stn[1:-1], cod_date=cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(554.32) + " kWp\n\n"
  no_meters = "Number of Energy Meters: " + str(2) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

# WRITE CODE TO GENERATE EMAIL BODY DATA
def gen_email_body_data(date):
    df = retrieve_data(date)
    dayofWeek = " ("+datetime.datetime.strptime(date, "%Y-%m-%d").strftime('%A')[:3]+") "
    digest_body = site_info(date)
    digest_body += 'Full site generation [kWh]: '+ str(round(df['MSB1_SolarPowerMeth2'][0] + df['MSB2_SolarPowerMeth2'][0],2))+'\n\n'
    digest_body += "Date: "+date+ dayofWeek
    meter_names = ["MSB-01 Solar", "MSB-02 Solar"]
    for meter in meter_names:
        prefix = "MSB1_" if "01" in meter else "MSB2_"
        digest_body+='\n\n_________________________________________\n\n'+ date + dayofWeek + meter + '\n\n_________________________________________\n\n'
        digest_body+= "Energy Generation [kWh]: "+str(df[prefix+"SolarPowerMeth2"][0])+"\n\n"
        digest_body+= "Last recorded timestamp: "+str(df[prefix+"LastTime"][0])+"\n\n"
        digest_body+= "Last recorded energy meter reading [kWh]: " + str(round(df[prefix+"LastRead"][0], 3))
    digest_body+='\n\n_________________________________________\n\n'
    gen_csv(date)
    return digest_body
# WRITE CODE TO GENERATE CSV DATA
def gen_csv(date):
    df_month = None
    for day in range(1, int(date[8:])+1):
        file_date = date[:8] + str(day).zfill(2)
        df_day = retrieve_data(file_date)
        if day == 1:
            df_month = df_day
        else:
            df_month = df_month.append(df_day)
    df_month['Total Generation'] = df_month['MSB1_'+'SolarPowerMeth2'] + df_month['MSB2_'+'SolarPowerMeth2']
    df_month.to_csv(save_file_path, index=None)

def retrieve_data(date):
    baseColList = ['SolarPowerMeth2', 'LastTime', 'LastRead']
    colNames = ["date"] + ["MSB1_" + x for x in baseColList] + ["MSB2_"+x for x in baseColList]
    colDict = {}
    for c in colNames:
        if c == 'date':
            colDict[c] = [date]
        else:
            colDict[c] = 'NA'
    df = pd.DataFrame(colDict, columns=colNames)
    paths = [
        '/home/admin/Dropbox/Second Gen/[KH-004X]/{year}/{year}-{month}/MSB-01 Solar/[KH-004X-M3] {date}.txt'.format(year=date[:4], month=date[5:7], date=date),
        '/home/admin/Dropbox/Second Gen/[KH-004X]/{year}/{year}-{month}/MSB-02 Solar/[KH-004X-M6] {date}.txt'.format(year=date[:4], month=date[5:7], date=date),
    ]
    for path in paths:
        if not os.path.isfile(path):
            continue
        prefix = "MSB1_" if "MSB-01" in path else "MSB2_"
        data = pd.read_csv(path, sep = '\t')
        for col in baseColList:
            df[prefix+col] = data[col]
            if col == 'LastRead':
                df[prefix+col] = df[prefix+col].apply(lambda x: round(x, 3))
    return df
#CALL ALL THE FUNCTIONS TOGETHER

# WRITE CODE TO SEND MAIL
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  stnSub = '[KH-9004X]'
  msg['Subject'] = "Station " + stnSub + " Client Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())



if __name__ == '__main__':
    date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
    try:
        digest_body = gen_email_body_data(date_yesterday)
        send_mail(date, stn, digest_body, recipients, [save_file_path])
        print("Sent Mail for {date}".format(date =date_yesterday))
    except Exception as e:
        print(e)
    # mail_path = startpath + "KH-9004X_Mail.txt"
    # if not os.path.exists(mail_path):
    #     with open(mail_path, "w") as file:
    #         timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    #         file.write(timenow + "\n" + date)
    # with open(mail_path, "r") as f:
    #     startdate = f.readlines()[1].strip()
    # print(startdate)
    # startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")
    # while(1):
    #     try:
    #         datenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    #         timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    #         print('Processing')
    #         digest_body = gen_email_body_data()
    #         print('Done Processing')
    #         if(datetime.datetime.now(tz).hour == 16 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):  # 
    #             print('Sending')
    #             date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
    #             send_mail(date, stn, digest_body, recipients, [save_file_path])
    #             with open(mail_path, "w") as file:
    #                 file.write(timenow + "\n" + date_yesterday)
    #             startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    #         print('Sleeping')
    #     except Exception as e:
    #         print('error')
    #         print(e)
    #     time.sleep(300)