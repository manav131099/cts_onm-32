errHandle = file('/home/admin/Logs/LogsKH007Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH007XDigest/HistoricalAnalysis2G3GKH007.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/KH007XDigest/aggregateInfo.R')
initDigest = function(df)
{
 body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Eac-1 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nEac-2 [kWh]: ",as.character(df[,4]),sep="")
  body = paste(body,"\n\nDA [%]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nDowntime [%]: ",as.character(df[,5]),sep="")
  body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(df[,6]),sep="")
  body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(df[,7]),sep="")
  body = paste(body,"\n\nGrid Availability [%]:",as.character(df[,13]))	
  body = paste(body,"\n\nPlant Availability [%]:",as.character(df[,14]))
  body = paste(body,"\n\nLast reading energy meter [kWh]: ",as.character(df[,12]),sep="")
  body = paste(body,"\n\nLast time energy meter: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nIrradiance (from KH-001S) [kWh/m2]: ",as.character(df[,10]),sep="")
  body = paste(body,"\n\nPR-1 [%]: ",as.character(df[,8]),sep="")
  body = paste(body,"\n\nPR-2 [%]: ",as.character(df[,9]),sep="")
  return(body)
}

printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n_________________________________________\n\n")
		body = paste(body,paste("Timestamps where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}

sendMail = function(df1,pth1,pth5)
{
  filetosendpath = c(pth1,pth5)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: GMAC",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia")
	body = paste(body,"\n\nO&M Code: KH-007")
	body = paste(body,"\n\nSystem Size: 53.76 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 1")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6X-320P / 168")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP 25000TL-30 / 2")
	body = paste(body,"\n\nSite COD:",DOB)
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))
  
	body = paste(body,initDigest(df1))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)
  graph_command1=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R","KH-007",72,0.008,"2018-11-28",substr(currday,11,20),sep=" ")
  system(graph_command1)
  graph_path1=paste('/home/admin/Graphs/Graph_Output/KH-007/[KH-007] Graph ',substr(currday,11,20),' - PR Evolution.pdf',sep="")
  graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/KH-007/[KH-007] Graph ',substr(currday,11,20),' - PR Evolution.txt',sep="")
  filetosendpath=c(graph_path1,graph_extract_path1,filetosendpath)
	while(1)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-007X] Digest",substr(currday,11,20)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		Sys.sleep(180)
		next
	}
	break
	}
	recordTimeMaster("KH-007X","Mail",substr(currday,11,20))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients = getRecipients("KH-007X","m")
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','meangcheav@teams-cambodia.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("KH-007X","m")
  recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','meangcheav@teams-cambodia.com')
  recordTimeMaster("KH-007X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-007X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      pathdays = pathmonths
      writepath2Gdays = writepath2Gmon
      checkdir(writepath2Gdays)
      days = dir(pathdays)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 if(grepl("Copy",c(days[t])))
				 {
				 	daycop = c(days[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
				 if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				 {
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 print(paste('Processing',days[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         df1 = secondGenData(readpath,writepath2Gfinal)
				thirdGenData(writepath2Gfinal,writepath3Gfinal)
  if(sendmail ==0)
  {
    Sys.sleep(3600)
    next
  }
	print('Sending Mail')
  sendMail(df1,writepath2Gfinal,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
