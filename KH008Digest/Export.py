import datetime
import os
import pandas as pd
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.ticker as ticker
from datetime import datetime
import matplotlib.dates as mdates

from datetime import datetime as dt
c=0
flag = 0
allLines = []
export = []
export_new = []
date = []
date_new = []
max_export = []
neg = []
max_exp = 0
today = dt.today()
today  = str(today)
year = today[0:4]
print(year)
month = today[0:7]
dt = today[0:10]
print("Printing dt")
print(dt)
print(month)
fd = datetime.today().replace(day=1)
fd = str(fd)
first_date = fd[0:10]

path = '/home/admin/Dropbox/Gen 1 Data/[KH-008X]/2021/2021-01/Load/'
fileList = os.listdir(path)
for file in sorted(fileList):
   date.append(file[13: 23])
   file = os.path.join(path, file)
   with open(file) as f:
     df1 = pd.read_csv(file,sep='\t')
     df1["Active.Power..W."] = df1["Active.Power..W."].fillna(0) 
     pw = df1["Active.Power..W."]
     E = any(pw<0)
     export_new.append(E)
     if(E):
       export.append(1) 
       for i in pw:
         if(i<0):
           neg.append(i*-1)
       max_exp = max(neg)
       max_export.append(max_exp)
     if(not(E)):
       export.append(0)
       max_exp = 0
       max_export.append(max_exp)
for i in date:
   i = datetime.strptime(i, '%Y-%m-%d')
   date_new.append(i)
df = pd.DataFrame(list(zip(date_new, export, max_export)),columns=['Date','Export','Max_Export'])

fig, ax = plt.subplots(figsize=(55, 30))
tick_spacing  = 1
y = df['Export']
plt.rcParams.update({'font.size': 60})
x = df['Date']
plt.yticks([1.0, 0.0], ["Export - Y (1)",
                        "Export - N (0)"])  
#ax.tick_params(axis='both', which='major', labelsize=20)
ax.tick_params(labelsize=60)
ax.scatter(x,y, color = "red", s = 850)
ax.set_xlim(x[0], x.iloc[-1])
ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))


plt.title('Daily Export - [KH-008X]\n\nFrom '+ first_date +' to '+dt, fontsize = 60)
ax.grid(axis='y')
plt.xticks(rotation=90)

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
fig.savefig('/home/admin/Jason/cec intern/results/[KH-008X]/Graph [KH-008X] Daily Export - '+dt+'.pdf', dpi=fig.dpi)
#plt.show()


fig, ax = plt.subplots(figsize=(55, 30))
tick_spacing  = 1
df['Max_Export'] = df['Max_Export']/1000
y = df['Max_Export']
plt.rcParams.update({'font.size': 60})
x = df['Date']
  
ax.scatter(x,y,  color = 'red',s = 750)
ax.set_xlim(x[0], x.iloc[-1])
ax.set_ylim(-100, 7000)
ax.set_ylabel('Power AC [kW]', fontsize = 60)
ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))

plt.title('Max Export - [KH-008X]\n\nFrom '+ first_date +' to '+dt, fontsize = 60)
ax.grid(axis='y')
plt.xticks(rotation=90)

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
fig.savefig('/home/admin/Jason/cec intern/results/[KH-008X]/Graph [KH-008X] Max Export - '+dt+'.pdf', dpi=fig.dpi)
#plt.show()
