from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib
import re 
import os
import numpy as np
import gzip
from zipfile import ZipFile
import shutil
import logging
import sqlalchemy as sa
import urllib.request

user = 'manav'

def chkdir(path):
    if(os.path.isdir(path)):
        return
    else:
        os.makedirs(path)
        
writepath = '/home/' + user + '/Dropbox/Gen 1 Data/[KH-008X]/'
startpath = '/home/' + user + '/Start/KH008X_History.txt'
chkdir(writepath)
tz = pytz.timezone('Asia/Kolkata')
curr = datetime.datetime.now(tz)

if(os.path.exists(startpath)):
    pass
else:
    try:
        shutil.rmtree(writepath, ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath, "w") as file:
        file.write("2021-05-01\n00:00:00")   

with open(startpath) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)

start = startdate + ' ' + starttime
start = datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
end = datetime.datetime.now(tz).replace(tzinfo=None) - datetime.timedelta(minutes=1)

print("Bot Start time is: " + str(curr))
print("Data fetching from datetime: " + str(start))
print("Data fetching to datetime: " + str(end))

ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
ftp.login(user='KH-008X', passwd = 'sqMmrU4NMpwikeMe')
files=ftp.nlst()

while(start<end):
  try:
    start_str = start.strftime("%Y%m%d%H%M")
    for i in sorted(files):
      if start_str in i and i[-3:] == 'zip':
        path_temp = writepath + str(start)[:4] + '/' + str(start)[:7] + '/' + i.split('_')[0] + '/'
        chkdir(path_temp)
        req = urllib.request.Request('ftp://KH-008X:sqMmrU4NMpwikeMe@ftpnew.cleantechsolar.com/' + i)
        
        with urllib.request.urlopen(req) as response:
          with ZipFile(io.BytesIO(response.read())) as zfile:
            with zfile.open(i[:-3] + 'csv') as myfile:
              df = pd.read_csv(myfile, skiprows=6)
              df = df.drop(columns=['Error', 'UTC Offset (minutes)'])
              df['Local Time Stamp']=pd.to_datetime(df['Local Time Stamp'])
              if i.split('_')[0] == 'Pond':
                meterno = 1
              elif i.split('_')[0] == 'Load':
                meterno = 6
              elif i.split('_')[0] == 'R02':
                meterno = 4
              elif i.split('_')[0] == 'R03':
                meterno = 5
              elif i.split('_')[0] == 'R06':
                meterno = 2
              elif i.split('_')[0] == 'R11':
                meterno = 3
              elif i.split('_')[0] == 'WHR':
                meterno = 7
              
              if(os.path.exists(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt')):
                  df.to_csv(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt', sep='\t', index=False, header=False, mode='a')
              else:
                  df.to_csv(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt', sep='\t', index=False, header=True, mode='a')
                      
              with open(startpath, "w") as file:
                file.write(str(start.date())+"\n"+str(start.time().replace(microsecond=0)))
    start = start + datetime.timedelta(minutes=5)
  except Exception as e:
    print(e)
    start = start + datetime.timedelta(minutes=5)
    logging.exception("His")
    pass
    exit()
    
ftp.close()
print('Historical Done!')
flag=0

while(1):
  try:
    time_now = datetime.datetime.now(tz)
    print('Going Live...')
    ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
    ftp.login(user='KH-008X', passwd = 'sqMmrU4NMpwikeMe')
    print('Connected!')
    files_now = ftp.nlst()
    files_now = sorted(files_now)
    new_files=list(set(files_now) - set(files))
    print(new_files)
    files = files_now
    for i in sorted(new_files):
      if i[-3:] == 'zip'
        datepart = i.split('_')[1]
        path_temp = writepath + datepart[:4] + '/' + datepart[:4] + '-' + datepart[4:6] + '/' + i.split('_')[0] + '/'
        chkdir(path_temp)
        req = urllib.request.Request('ftp://KH-008X:sqMmrU4NMpwikeMe@ftpnew.cleantechsolar.com/' + i)
        
        with urllib.request.urlopen(req) as response:
          with ZipFile(io.BytesIO(response.read())) as zfile:
            with zfile.open(i[:-3] + 'csv') as myfile:
              df = pd.read_csv(myfile, skiprows=6)
              df = df.drop(columns=['Error', 'UTC Offset (minutes)'])
              df['Local Time Stamp']=pd.to_datetime(df['Local Time Stamp'])
              if i.split('_')[0] == 'Pond':
                meterno = 1
              elif i.split('_')[0] == 'Load':
                meterno = 6
              elif i.split('_')[0] == 'R02':
                meterno = 4
              elif i.split('_')[0] == 'R03':
                meterno = 5
              elif i.split('_')[0] == 'R06':
                meterno = 2
              elif i.split('_')[0] == 'R11':
                meterno = 3
              elif i.split('_')[0] == 'WHR':
                meterno = 7
              
              if(os.path.exists(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt')):
                  df.to_csv(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt', sep='\t', index=False, header=False, mode='a')
              else:
                  df.to_csv(path_temp +'[KH-008X-M' + str(meterno) + '] ' + str(start)[:10] + '.txt', sep='\t', index=False, header=True, mode='a')
                  
              with open(startpath, "w") as file:
                file.write(str(time_now.date())+"\n"+str(time_now.time().replace(microsecond=0)))
    
    ftp.close()
    print('Sleeping')
  except:
    logging.exception('Main Failed')
  time.sleep(3600)