import requests

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"

def authenticate_my():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "server.request1@cleantechsolar.com","password" : "welcome",
                 "client_id" : clientid,"client_secret" : clientsecret}
    												
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"]  

def authenticate_sg():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "Cleantech Solar Singapore","password" : "cssingapore",
                 "client_id" : clientid,"client_secret" : clientsecret}								
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"]  

import requests
from concurrent.futures import ThreadPoolExecutor
auth_sg = authenticate_sg()
auth_my = authenticate_my()
list_of_urls = ['https://api.locusenergy.com/v3/components/2677515/data?start=2021-06-30T00:00:00&end=2021-06-30T19:00:00&tz=Asia/Singapore&gran=5min&fields=AphA_avg,AphA_max,AphA_min,AphB_avg,AphB_max','https://api.locusenergy.com/v3/components/2641710/data?start=2021-06-30T00:00:00&end=2021-06-30T19:50:00&tz=Asia/Kuala_Lumpur&gran=5min&fields=ts,POAI_avg,POAI_max,POAI_min,POAIh_sum,TmpBOM_avg,TmpBOM_max,TmpBOM_min']
def get_url(url):
    if('Singapore' in url):
        print('SG')
        return requests.get(url, headers={'Authorization': "Bearer "+auth_sg})
    else:
        print('MY')
        return requests.get(url, headers={'Authorization': "Bearer "+auth_my})
with ThreadPoolExecutor(max_workers=50) as pool:
    print(list(pool.map(get_url,list_of_urls)))
