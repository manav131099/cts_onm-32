import pyodbc
import pandas as pd
import datetime
import math
import pytz
from database_operations import *
import sys
import os

def test(date):
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    date = date
    irr_site = 'MY-002'
    site_id = '136'
    component_df = get_irr_raw(date, site_id, irr_site, cnxn)
    return(round(sum(component_df['Irradiance']/12000),2))    