import requests, json
import requests.auth
import pandas as pd
import datetime 
import os
import re 
import time
import shutil
import pytz
import sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf
import matplotlib.dates as mdates
import logging
import pyodbc
from matplotlib.dates import DateFormatter
from matplotlib.dates import HourLocator

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

date = sys.argv[1]

dates = [date]


font = {'size'   : 12}
plt.rcParams['axes.facecolor'] = 'white'
csfont = {'fontname':'Arial'}
plt.title('title',**csfont)
plt.xlabel('xlabel', **csfont)
matplotlib.rc('font', **font)
fig, axs = plt.subplots(2,2,figsize=(12.8, 8.8))


for index,i in enumerate(dates):
    f_date = datetime.datetime.strptime(i,'%Y-%m-%d').strftime('%y%m%d')
    pdf = matplotlib.backends.backend_pdf.PdfPages("/home/admin/Graphs/Graph_Output/MY-006/MY-006 - "+f_date+".pdf")
    df_cs = pd.read_csv('/home/pranav/March 1-min/'+f_date[2:4]+'-'+f_date[4:]+'.txt',sep='\t')
    df = pd.read_csv('/home/admin/Dropbox/Cleantechsolar/1min/[729]/'+i[0:4]+'/'+i[0:7]+'/[729] '+i+'.txt',sep='\t')
    year = datetime.datetime.strptime(i,'%Y-%m-%d').strftime('%Y')
    month = datetime.datetime.strptime(i,'%Y-%m-%d').strftime('%m')
    day = datetime.datetime.strptime(i,'%Y-%m-%d').strftime('%d')
    df_cs['Time'] = pd.to_datetime(df_cs['Time']).apply(lambda dt: dt.replace(year=int(year),month=int(month),day=int(day)))
    df_cs['pow1'] = df_cs['CSIrr']*1615.6*.001
    df_cs['pow2'] = df_cs['CSIrr']*1579.5*.001
    df_cs['pow3'] = df_cs['CSIrr']*858.2*.001
    try:
        df['Time'] = pd.to_datetime(df['Tm'])
    except:
        df['Time'] = pd.to_datetime(df['Time Stamp'])
    irr = df['AvgGSi00']
    print(irr)
    pow_1 = df['Act_Pwr-Tot_M1']
    pow_2 = df['Act_Pwr-Tot_M2']
    pow_3 = df['Act_Pwr-Tot_M3']
    axs[0, 0].plot(df['Time'], irr, 'tab:orange',label='Actual Irradiance')
    axs[0, 0].set_title('Irradiance',size=12)
    axs[0, 0].set_xlim(datetime.datetime(year=int(year),month=int(month),day=int(day)),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[0, 0].plot(df_cs['Time'], df_cs['CSIrr'], color='grey',label='Clear Sky Irradiance',linestyle='dotted')
    axs[0, 0].set_ylim(bottom=0)
    axs[0, 1].plot(df['Time'], pow_1, 'tab:blue',label='Meter 1 Pac')
    axs[0, 1].set_title('Meter 1 Pac [1,615.60 kWp]',size=12)
    axs[0, 1].plot(df_cs['Time'], df_cs['pow1'], color='grey',label='Clear Sky Pac',linestyle='dotted')
    axs[1, 0].plot(df['Time'], pow_2, 'tab:green',label='Meter 2 Pac')
    axs[1, 0].plot(df_cs['Time'], df_cs['pow2'], color='grey',label='Clear Sky Pac',linestyle='dotted')
    axs[1, 0].set_title('Meter 2 Pac [1,579.50 kWp]',size=12)
    axs[1, 1].plot(df['Time'], pow_3, 'tab:red',label='Meter 3 Pac')
    axs[1, 1].set_title('Meter 3 Pac [858.20 kWp]',size=12)
    axs[1, 1].plot(df_cs['Time'], df_cs['pow3'], color='grey',label='Clear Sky Pac',linestyle='dotted')
    axs[0, 1].set_ylim(0,1800)
    axs[1, 0].set_ylim(0,1800)
    axs[1, 1].set_ylim(0,1800)
    axs[0, 0].legend(loc='upper right',prop={'size': 10})
    axs[1, 0].legend(loc='upper right',prop={'size': 10})
    axs[1, 1].legend(loc='upper right', prop={'size': 10})
    axs[0, 1].legend(loc='upper right', prop={'size': 10})
    axs[0, 1].text(datetime.datetime(int(year),int(month),int(day),1,0,0),1600,str(round((1615.6/1615.6)*100,1))+"% of max" ,color='red',size=10)
    axs[1, 0].text(datetime.datetime(int(year),int(month),int(day),1,0,0),1600,str(round((1579.5/1579.5)*100,1))+"% of max" ,color='red',size=10)
    axs[1, 1].text(datetime.datetime(int(year),int(month),int(day),1,0,0),1600,str(round((858.2/858.2)*100,1))+"% of max" ,color='red',size=10)
    axs[1, 0].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[0, 1].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[1, 1].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
for i in axs:
    for j in i:
        j.xaxis.set_major_formatter(mdates.DateFormatter('%H')) 

for index,ax in enumerate(axs.flat):
    if index == 0:
        ax.set(xlabel='', ylabel='Irradiance [W/m$^2$]')
    elif(index == 1):
        ax.set(xlabel='', ylabel='Power [kW]')    
    else:
        ax.set(xlabel='Time', ylabel='Power [kW]')



fig.suptitle('Coca-Cola Enstek - '+f_date)
pdf.savefig(fig, bbox_inches='tight')







fig, axs = plt.subplots(2,2,figsize=(12.8, 8.8))


for index,i in enumerate(dates):
    df = pd.read_csv('/home/admin/Dropbox/Cleantechsolar/1min/[729]/'+i[0:4]+'/'+i[0:7]+'/[729] '+i+'.txt',sep='\t')
    print(df)
    df['Time'] = pd.to_datetime(df['Tm'])
    print(df.index)
    freq1 = df.loc[df['Freq_M1']>0,'Freq_M1']
    freq1_time = df.loc[df['Freq_M1']>0,'Time']
    freq2 = df.loc[df['Freq_M2']>0,'Freq_M2']
    freq2_time = df.loc[df['Freq_M2']>0,'Time']
    freq3 = df.loc[df['Freq_M3']>0,'Freq_M3']
    freq3_time = df.loc[df['Freq_M3']>0,'Time']
    pow_1 = df['V-LL-Avg_M1']
    v1 = df.loc[df['V-LL-Avg_M1']>370,'V-LL-Avg_M1']
    v1_time = df.loc[df['V-LL-Avg_M1']>370,'Time']
    v2 = df.loc[df['V-LL-Avg_M2']>370,'V-LL-Avg_M2']
    v2_time = df.loc[df['V-LL-Avg_M2']>370,'Time']
    v3 = df.loc[df['V-LL-Avg_M3']>370,'V-LL-Avg_M3']
    v3_time = df.loc[df['V-LL-Avg_M3']>370,'Time']
    pow_2 = df['V-LL-Avg_M2']
    pow_3 = df['V-LL-Avg_M3']
    axs[0, 0].plot(freq1_time, freq1,label='Meter 1',color='blue')
    axs[0, 0].plot(freq2_time, freq2,label='Meter 2',color='darkblue')
    axs[0, 0].plot(freq3_time, freq3,label='Meter 3',color='lightblue')
    axs[0, 0].legend(prop={'size': 10},loc='upper left')
    axs[0, 0].axhline(y=50, color='red', linestyle='-',linewidth=2.5)
    axs[0, 0].axhline(y=50.5, color='red', linestyle='--',linewidth=2.5)
    axs[0, 0].axhline(y=49.5, color='red', linestyle='--',linewidth=2.5)
    axs[0, 0].text(datetime.datetime(int(year),int(month),int(day),16,0,0),50.4,"50.5 Hz" ,color='red')
    axs[0, 0].text(datetime.datetime(int(year),int(month),int(day),16,0,0),49.55,"49.5 Hz" ,color='red')
    axs[0, 0].set_ylim(49.4,50.6)
    axs[0, 0].set_title('Frequency',size=12)
    axs[0, 1].plot(v1_time, v1, 'tab:orange')
    axs[0, 1].set_title('Meter 1 V-LL-Avg',size=12)
    axs[0, 1].set_ylim(370,460)
    axs[0, 1].axhline(y=415, color='red', linestyle='-',linewidth=2.5)
    axs[0, 1].axhline(y=456.5, color='red', linestyle='--',linewidth=2.5)
    axs[0, 1].axhline(y=373.5, color='red', linestyle='--',linewidth=2.5)
    axs[0, 1].text(datetime.datetime(int(year),int(month),int(day),16,0,0),450,"415 V +10%" ,color='red')
    axs[0, 1].text(datetime.datetime(int(year),int(month),int(day),16,0,0),377,"415 V -10%" ,color='red')
    axs[1, 0].plot(v2_time, v2, 'tab:green')
    axs[1, 0].set_title('Meter 2 V-LL-Avg',size=12)
    axs[1, 0].set_ylim(370,460)
    axs[1, 0].axhline(y=415, color='red', linestyle='-',linewidth=2.5)
    axs[1, 0].axhline(y=456.5, color='red', linestyle='--',linewidth=2.5)
    axs[1, 0].axhline(y=373.5, color='red', linestyle='--',linewidth=2.5)
    axs[1, 0].text(datetime.datetime(int(year),int(month),int(day),16,0,0),450,"415 V +10%" ,color='red')
    axs[1, 0].text(datetime.datetime(int(year),int(month),int(day),16,0,0),377,"415 V -10%" ,color='red')
    axs[1, 1].plot(v3_time, v3, 'tab:brown')
    axs[1, 1].set_title('Meter 3 V-LL-Avg',size=12)
    axs[1, 1].set_ylim(370,460)
    axs[0, 0].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[1, 0].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[0, 1].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[1, 1].set_xlim(datetime.datetime(int(year),int(month),int(day),0,0,0),datetime.datetime(int(year),int(month),int(day)+1,0,0,0))
    axs[1, 1].axhline(y=415, color='red', linestyle='-',linewidth=2.5)
    axs[1, 1].axhline(y=456.5, color='red', linestyle='--',linewidth=2.5)
    axs[1, 1].axhline(y=373.5, color='red', linestyle='--',linewidth=2.5)
    axs[1, 1].text(datetime.datetime(int(year),int(month),int(day),16,0,0),450,"415 V +10%" ,color='red')
    axs[1, 1].text(datetime.datetime(int(year),int(month),int(day),16,0,0),377,"415 V -10%" ,color='red')


for i in axs:
    for index,j in enumerate(i):
        j.xaxis.set_major_formatter(mdates.DateFormatter('%H')) 



for index,ax in enumerate(axs.flat):
    if index == 0:
        ax.set(xlabel='', ylabel='Frequency [Hz]')
    elif(index == 1):
        ax.set(xlabel='', ylabel='Voltage [V]')    
    else:
        ax.set(xlabel='Time', ylabel='Voltage [V]')


fig.suptitle('Coca-Cola Enstek - '+f_date)
pdf.savefig(fig, bbox_inches='tight')
pdf.close()
