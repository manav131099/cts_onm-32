rm(list = ls())

path = "/home/admin/Dropbox/GIS/Raw Files"

days = dir(path)

days = days[grepl("_Chumphon_",days)]
#days = days[grepl("201805",days)]
#days = days[-2]
print(length(days))

dateall = wsmean = wdmean = wsmeansh = wdmeansh = DA = c()

idxglobal = 1
for(x in 1 : length(days))
{
   pathdata = paste(path,days[x],sep="/")
	 dataread = read.table(pathdata,header =T,sep = ";")
	 date = as.character(dataread[,1])
	 dateparse = unique(date)
	 for(y in 1 : length(dateparse))
	 {
	 	idxuse = which(date %in% dateparse[y])
		dateall[idxglobal] = gsub("\\.","-",dateparse[y])
		temp = unlist(strsplit(dateall[idxglobal],"-"))
		dateall[idxglobal] = paste(temp[3],temp[2],temp[1],sep="-")
		wsmean[idxglobal] = wdmean[idxglobal] = wsmeansh[idxglobal] = wdmeansh[idxglobal] = DA[idxglobal] =NA
		if(length(idxuse))
		{
		   datasub = as.numeric(dataread[idxuse,10])
		   datasub2 = as.numeric(dataread[idxuse,11])
		   datasub3 = as.numeric(dataread[idxuse,3])
			 datasub4 = datasub[datasub3 > 0]
			 datasub5 = datasub2[datasub3 > 0]

			 wsmean[idxglobal] = round(mean(datasub),1)
			 wdmean[idxglobal] = round(mean(datasub2),1)
			 
			 if(length(datasub4))
			 {
			 	wsmeansh[idxglobal] = round(mean(datasub4),1)
			 	wdmeansh[idxglobal] = round(mean(datasub5),1)
			 }

			 DA[idxglobal] = round(length(idxuse)/.24,1)
		}
		idxglobal = idxglobal + 1
	 }
}

dys = unique(dateall)
idx = match(dys,dateall)
dateall = dateall[idx]

wsmean = wsmean[idx]
wdmean = wdmean[idx]
wdmeansh = wdmeansh[idx]
wsmeansh = wsmeansh[idx]
DA = DA[idx]

df = data.frame(Date=dateall,WSMean=wsmean,WDMean=wdmean,WSMeanSH=wsmeansh,WDMeanSH=wdmeansh,DA=DA)
df = df[order(df[,1]),]

write.table(df,file="/tmp/THChumphon_WS.txt",row.names =F,col.names =T,append=F,sep="\t")
