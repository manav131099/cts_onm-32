import pytz
import os
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions

user = sys.argv[1]
site = 'IN-076'
locus_parameters = []
azure_locus_parameters = []
component_id=[] 
auth = locus_authenticate(site)
new_component_df = pd.read_csv('/home/' + user + '/ComponentInfo.csv')

cnxn =  pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
site_id = get_site_id(site,cnxn)
locus_metadata_df = get_locus_metadata(site,cnxn)
locus_site_id = locus_metadata_df['LocusSiteId'].values[0]
print(new_component_df)
print(locus_metadata_df)
for index,row in new_component_df.iterrows():
    if(row['LocusId'] in locus_metadata_df['LocusComponentId'].tolist()):
        print('Component already present!')
    else:
        print(row['ComponentName'])
        cleantech_name = row['ComponentName']
        cleantech_type = row['ComponentType']
        data_available = get_locus_parameters(auth, row['LocusId'])
        locus_parameters.append(data_available) 
        temp_id = add_component_locus(cleantech_name, cleantech_type, row['LocusId'], ','.join(data_available) , site_id, locus_site_id, cnxn)
        if(cleantech_type=='MFM'):
            size = row['Size']
            meter_ref = row['MeterRef']
            salesforce_ref = row['SalesforceRef']
            add_mfm_info_auto(temp_id, site_id, size, meter_ref, salesforce_ref, cnxn)
        elif(cleantech_type=='INV'):
            size =row['Size']
            add_inv_info_auto(temp_id, site_id, size, cnxn)
        elif(cleantech_type=='SMB'):
            size =row['Size']
            add_smb_info_auto(temp_id, site_id, size, cnxn)
        component_id.append(temp_id)

for i in locus_parameters:
    print(i)
    temp=[]
    for j in i:
        print(j)
        if(('avg' in j) or ('TotWhExp_max' in j) or ('TotWhImp_max' in j) or ('Wh_sum' in j)):
            temp.append(j)
    azure_locus_parameters.append(temp)

        
#Add Locus and Cleantech Parameters/Metadata
for index,c in enumerate(component_id):
    print(c)
    for i in azure_locus_parameters[index]:
        all_parameters = get_all_parameters(cnxn)
        if(i!='ts'):
            if(i in all_parameters['MPName'].tolist()):
                parameter_id = all_parameters.loc[all_parameters['MPName'] == i,'ParameterId'].values[0]
                print(i)
                add_cleantech_metadata(parameter_id, c, site_id, cnxn)
            else:
                res = input("Add Parameter "+i+"? (y or n)")
                print(res)
                if(res=='y'):
                    cs_name = input("Enter Cleantech Parameter Name:")
                    parameter_id = add_parameter(i, cs_name, 'Raw', cnxn)
                    print(i)
                    add_cleantech_metadata(parameter_id, c, site_id, cnxn)
                elif(res=='n'):
                    print('Parameter not added')

parameter_df = get_parameter(site, cnxn)
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
raw_cols = raw_parameter_df['CSName'].unique().tolist()
add_column(site,raw_cols,cnxn)
cnxn.close()
