import pytz
import os
from ftplib import FTP
import time
import pytz
import io
import urllib.request
import re 
import gzip
import shutil
import logging
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions

#Go through SERIS metadata.
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

site = 'ID-001'
site_id = get_site_id(site,cnxn)
print(site_id)
irr_center = 'Self'
data_granularity = get_data_granularity(site,cnxn)
locus_parameters = []
azure_locus_parameters = []
component_id = []
tz = pytz.timezone("Asia/Calcutta")
main_path = '/home/pranav/New_Architecture/RawData/'

df_cols = pd.read_csv("Seris_Cols.csv")
print(df_cols)

components = {'Pyranometer':['WMSIRR'],'Silicon Sensor 1':['WMSIRR'],'Silicon Sensor 2':['WMSIRR'],'MFM-1':['MFM',2979,'ID-001A','a0T2x0000004KE5EAM'],'MFM-2':['MFM',1875.6,'ID-001B','a0T2x0000049lPeEAI']}
all_parameters = get_all_parameters(cnxn)

""" for j in components:
    cols = df_cols.loc[df_cols['Component']==j,'CSName'].tolist()
    
    if(components[j][0]=='WMSIRR'):
        c_id = add_component(j, 'WMSIRR', site_id,cnxn)
    elif(components[j][0]=='MFM'):
        c_id = add_component(j, 'MFM', site_id,cnxn)
        summary_order = get_summary_order(cnxn)
        country_id = get_country_id(site,cnxn)
        print((c_id, site_id, components[j][1], components[j][2], components[j][3], country_id, summary_order))
        add_mfm_info_auto(c_id, site_id, components[j][1], components[j][2], components[j][3], country_id, summary_order,cnxn)
    elif(components[j][0]=='OTH'):
        c_id = add_component(j, 'OTH', site_id,cnxn)
    print((component_id, site_id, ','.join(cols), cnxn))
    add_serismetadata(c_id, site_id, ','.join(cols), cnxn)
    all_parameters = get_all_parameters(cnxn)

    for i in cols:
        if(i in all_parameters['CSName'].tolist()):
            parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
            print(parameter_id)
            add_cleantech_metadata(parameter_id, c_id, site_id,cnxn)
        else:
            print('Parameter Skipped!') """

if(irr_center=='Self'):
    irr_site_id = site_id
else:
    irr_site_id = get_site_id(irr_center,cnxn)

add_irr_center(site_id,irr_site_id,None,cnxn)
#Need WMS First for PR calculation so sorted in get_ceantec_components function
component_df = get_cleantech_components(site,cnxn)
print(component_df)
cleantech_component_id = component_df['ComponentId'].tolist()
all_parameter_df = get_all_parameters(cnxn)


for index,row in component_df.iterrows():
    #if silicon or pyranometer in name then use this else
    c_name = row['ComponentName']
    processed_parameters_dict = {"WMSIRR":["DA","GHI"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
    for i in processed_parameters_dict[row['ComponentType']]:
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, row['ComponentId'], site_id,cnxn)
        except:
            pass

#Adding MFM, INV COV
mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]


if(len(mfm_component_df)>1):
    last_mfm = mfm_component_df.tail(1)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id,cnxn)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id,cnxn)

parameter_df = get_parameter(site,cnxn)

#Getting Capacities
mfm_capacity_df = get_mfm_capacities(site,cnxn) 


#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

#Processed Parameters
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

#Creatin Table
create_table(site,raw_parameter_df['CSName'].unique().tolist(),cnxn) 
cnxn.close()




