import pytz
import os
from ftplib import FTP
import time
import pytz
import io
import urllib.request
import re 
import gzip
import shutil
import logging
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
site = 'IN-102'
tz = pytz.timezone("Asia/Calcutta")
auto_site_df = pd.read_csv('/home/pranav/ctsfork/NewArchitecture/Generalinfo.csv', sep=',', error_bad_lines=False, index_col=False, dtype='unicode')
df_cols = pd.read_csv("/home/pranav/ctsfork/NewArchitecture/Cols.csv")
site_id = get_site_id(site,cnxn)
data_granularity = get_data_granularity(site,cnxn)
main_info_df = auto_site_df.loc[auto_site_df['O&M Reference']==site,:]
irr_center = main_info_df['IrrCenter'].values[0].strip()
mfmcols = df_cols.loc[df_cols['ComponentType']=='MFM','CSName'].tolist()
invcols = df_cols.loc[df_cols['ComponentType']=='INV','CSName'].tolist()
wmscols = df_cols.loc[df_cols['ComponentType']=='WMS','CSName'].tolist()
components={'WD00E340':{6:['MFM_1',mfmcols,93.94],1:['INVERTER_1',invcols,130.41],2:['INVERTER_2',invcols,130.41],3:['INVERTER_3',invcols,130.41],4:['INVERTER_4',invcols,129.03],5:['INVERTER_5',invcols,130.065]}}

meter_info = {'WD00D47C':['IN-0102A','a0T28000005oArTEAU']}

#Adding Components
for j in components:
    for i in components[j]:
        if(components[j][i][0][0:3] == 'INV'):
            print(i)
            all_parameters = get_all_parameters(cnxn)
            c_id = add_component(components[j][i][0], 'INV', site_id,cnxn)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]),cnxn)
            add_inv_info_auto(c_id, site_id, components[j][i][2],cnxn)
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    try:
                        add_cleantech_metadata(parameter_id, c_id, site_id,cnxn)
                    except:
                        print('Already Added')
                else:
                    print('Parameter Skipped!')
        elif(components[j][i][0][0:3] == 'WMS'):
            all_parameters = get_all_parameters(cnxn)
            c_id = add_component(components[j][i][0], 'WMSIRR', site_id,cnxn)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]),cnxn)
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    try:
                        add_cleantech_metadata(parameter_id, c_id, site_id,cnxn)
                    except:
                        print('Already Added')
                else:
                    print('Parameter Skipped!')
        else:
            all_parameters = get_all_parameters(cnxn)
            print('MFM',i)
            c_id = add_component(components[j][i][0], 'MFM', site_id)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]),cnxn)
            add_mfm_info_auto(c_id, site_id, components[j][i][2], meter_info[j][0], meter_info[j][1])
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    try:
                        add_cleantech_metadata(parameter_id, c_id, site_id,cnxn)
                    except:
                        print('Already Added')
                else:
                    print('Parameter Skipped!')

if(irr_center=='Self'):
    irr_site_id = site_id
else:
    irr_site_id = get_site_id(irr_center,cnxn)
add_irr_center(site_id,irr_site_id,None,cnxn)

#Need WMS First for PR calculation so sorted in get_ceantec_components function
component_df = get_cleantech_components(site,cnxn)
print(component_df)
all_parameter_df = get_all_parameters(cnxn)

#Add processed Parameters
if(irr_center=='Self'):
    processed_parameters_dict = {"WMS":["DA","GHI","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability']}
else:
    processed_parameters_dict = {"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability']}

for index,row in component_df.iterrows():
    #if silicon or pyranometer in name then use this else
    c_name = row['ComponentName']
    processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
    for i in processed_parameters_dict[row['ComponentType']]:
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, row['ComponentId'], site_id,cnxn)
        except:
            pass

#Adding MFM, INV COV
inv_component_df = component_df.loc[component_df['ComponentType']=='INV',:]
mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]

if(len(inv_component_df)>1):
    last_inv = inv_component_df.tail(1)
    print(last_inv)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id,cnxn)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id,cnxn)

if(len(mfm_component_df)>1):
    last_mfm = mfm_component_df.tail(1)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id,cnxn)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id,cnxn)

parameter_df = get_parameter(site,cnxn)

#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

#Processed Parameters
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

#Creatin Table
create_table(site,raw_parameter_df['CSName'].unique().tolist(),cnxn) 

cnxn.close()

