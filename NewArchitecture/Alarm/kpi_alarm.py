import time
import pytz
import pymsteams
from database_operations import *
import numpy as np
import math

def teams_alert(site,site_id,date,pr,inv_cov,mfm_cov,full_eac,alarm_type):
    try:
        webhook = webhook_df.loc[webhook_df['SiteId']==site_id,'Webhook'].values[0].strip()
        #webhook='https://cleantechenergycorp.webhook.office.com/webhookb2/264f6275-69ea-4733-80c7-c29cea53c91e@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/c9279a813fe54af1a676d5af5371ac12/b91f3ec7-71fb-437f-8eab-f5905ff0c963'
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title(site+" "+alarm_type+" ALERT")
        text = "<pre>Date: "+str(date)+"<br>PR: "+str(round(pr,1))+"<br>Energy: "+str(round(full_eac,1))+"<br>MFM CoV: "+str(round(mfm_cov,1))+"<br>INV CoV: "+str(round(inv_cov,1))+"</pre>"
        myTeamsMessage.text(text.replace('nan','NULL'))
        myTeamsMessage.send()
        time.sleep(5)
    except Exception as e:
        print(e)
        print('Failed')

def get_pr(data_df,pr_type,site_code):
    if(pr_type == 'GTI'):
        p_no=307
    else:
        p_no=69
    if(not data_df[data_df['ParameterId']==p_no].empty):
        pr = data_df.loc[data_df['ParameterId']==p_no,'Value'].values[0]
        if(pr==None or np.isnan(pr)):
            return np.nan
        else:
            if(int(site_code[3])>=4):
                print('Shell')
                irr_code = get_irr_center(get_site_id(site_code,cnxn),cnxn)
                print(irr_code)
                if(site_code != irr_code):
                    if(pr>60 and pr<70):
                        print('PR is',pr,site_code,irr_code)
                        pr = 70
            print('PR is',pr,site_code)
            return pr
    else:
        return np.nan

def get_cov(data_df,c_type):
    if(c_type=='INV'):
        if(not data_df[(data_df['ParameterId']==12) & (data_df['ComponentType']=='INV')].empty):
            inv_cov = data_df.loc[(data_df['ParameterId']==12) & (data_df['ComponentType']=='INV'),'Value'].values[0] 
            if(inv_cov==None or np.isnan(inv_cov)):
                return np.nan
            else:   
                return round(inv_cov,2)
        else:
            return np.nan
    elif(c_type=='MFM'):
        if(not data_df[(data_df['ParameterId']==12) & (data_df['ComponentType']=='MFM')].empty):
            mfm_cov = data_df.loc[(data_df['ParameterId']==12) & (data_df['ComponentType']=='MFM'),'Value'].values[0]
            if(mfm_cov==None or np.isnan(mfm_cov)):
                return np.nan
            else:
                return round(mfm_cov,2)
        else:
            return np.nan

def check_pr(pr,limit):
    if(math.isnan(pr) or pr==0):
        return -1
    else:
        if(pr<limit):
            return 1
        else:
            return 0 

def check_cov(cov,limit,c_type):
    if(math.isnan(cov)):
        return -1
    if(c_type=='INV'):
        if(cov>limit):
            return 1
        else:
            return 0 
    elif(c_type=='MFM'):
        if(cov>limit):
            return 2
        else:
            return 0 

def get_exception_data(df,site):
    if(site=='IN-012'):
        mfm_capacity_df = get_mfm_capacities(site, cnxn)
        df = pd.merge(df, mfm_capacity_df,  how='left', left_on=['ComponentId'], right_on = ['ComponentId'], suffixes=('', '_dup2'))
        df['Value_Temp'] = df['Value']*df['Size']
        full_yld = df.loc[(df['CSName']=='Yield-2') & (df['ComponentType']=='MFM') & (df['Value_Temp']!=0),'Value_Temp'].sum()/df.loc[(df['CSName']=='Yield-2') & (df['ComponentType']=='MFM') & (df['Value_Temp']!=0),'Size'].sum()
        full_pr = (full_yld*100)/df.loc[(df['CSName']=='GHI') | (df['CSName']=='GTI'),'Value'].values[0]
        covyields = df.loc[(df['ComponentType']=='MFM') & (df['CSName']=='Yield-2') & (df['Value']!=0),'Value'].tolist()
        mfm_cov = (np.std(covyields)*100)/np.mean(covyields)
        inv_cov = get_cov(df,'INV')
        return [full_pr,inv_cov,mfm_cov]


tz=pytz.timezone('Asia/Calcutta')
date = (datetime.datetime.now(tz) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
print(date)
limit_df = get_limit_data('asd',cnxn)
site_df = get_sites('All',cnxn)
site_df = site_df[site_df['SiteCode']!='IN-027']
webhook_df =  get_webhooks(cnxn)

for index,row in site_df.iterrows():
    try:
        site_id = get_site_id(row['SiteCode'],cnxn)
        data_df = get_processed_data(date, site_id, cnxn)
        if(row['SiteCode']=='IN-012'):
            pr_type = 'GHI'
        elif(data_df['ParameterId'].isin([307]).any()):
            data_df = data_df[data_df['ParameterId'].isin([5, 12, 307])] 
            pr_type = 'GTI'
        else:
            data_df = data_df[data_df['ParameterId'].isin([5, 12, 69])]
            pr_type = 'GHI'
        site_limit = limit_df[limit_df['SiteId']==row['SiteId']]
        if not site_limit.empty:
            pr_limit = limit_df.loc[limit_df['SiteId']==row['SiteId'],'PRLimit'].values[0]
            inv_limit = limit_df.loc[limit_df['SiteId']==row['SiteId'],'INVLimit'].values[0]
            mfm_limit = limit_df.loc[limit_df['SiteId']==row['SiteId'],'MFMLimit'].values[0]
            full_eac = data_df.loc[(data_df['CSName']=='EAC method-2') & (data_df['ComponentType']=='MFM'),'Value'].sum()
        if(row['SiteCode']=='IN-012'):
            exception_data = get_exception_data(data_df,row['SiteCode'])
            print(exception_data)
            pr = exception_data[0]
            inv_cov = exception_data[1]
            mfm_cov = exception_data[2]
            pr_status = check_pr(pr,pr_limit)
        else:
            pr = get_pr(data_df,pr_type,row['SiteCode'])
            inv_cov = get_cov(data_df,'INV')
            mfm_cov = get_cov(data_df,'MFM')
            pr_status = check_pr(pr,pr_limit)
        if(pr_status==1): 
            teams_alert(row['SiteCode'],site_id,date,pr,inv_cov,mfm_cov,full_eac,'PR')
            pass
        else:
            inv_cov_status = check_cov(inv_cov,inv_limit,'INV')
            if(inv_cov_status==1):
                teams_alert(row['SiteCode'],site_id,date,pr,inv_cov,mfm_cov,full_eac,'INV COV')
                pass
            else:
                mfm_cov_status = check_cov(mfm_cov,mfm_limit,'MFM')
                if(mfm_cov_status==2):
                    teams_alert(row['SiteCode'],site_id,date,pr,inv_cov,mfm_cov,full_eac,'MFM COV')
                    pass
    except Exception as e:
        print(e)
        print('Failed')
cnxn.close()



