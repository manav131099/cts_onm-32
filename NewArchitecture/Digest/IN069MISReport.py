import pytz
import sharepy
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import smtplib
import numpy as np
import os
import sys
import datetime
import xlsxwriter
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *

curr_date=sys.argv[1]
user='admin'
tz=pytz.timezone('Asia/Bangkok')

#Authentication means for Azure
server='cleantechsolar.database.windows.net'
database='Cleantech Meter Readings'
username='RohanKN'
password='R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
cnxn=pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+password)

def send_mail(info,attachment_path_list=None):
    s=smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg=MIMEMultipart()
    sender='operations@cleantechsolar.com'
    recipients=['operationsWestIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','ampm@inspire-ce.com','ctt@inspire-ce.com','om.prakash@prismjohnson.in','pcl.energy@prismjohnson.in','jabir.khan@prismjohnson.in','manish.singh@prismjohnson.in','kumar.akash@hrjohnsonindia.com','narendra.singhai@prismjohnson.in','gaurav.khatri@prismjohnson.in','sanjeeva@prismjohnson.in','ashok.singh@prismjohnson.in','anik.mukherjee@prismjohnson.in','sanjay.singh@prismjohnson.in','shivdayal.singh@prismjohnson.in']
    msg['Subject']="Station [IN-9069L] MIS Report "+ str(curr_date)
    if sender is not None:
        msg['From']=sender
    msg['To']=", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part=MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())
    
def excel_style(col):
    """ Convert given row and column number to an Excel-style cell name. """
    LETTERS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col+=1
    result=[]
    while col:
        col, rem=divmod(col-1, 26)
        result[:0]=LETTERS[rem]
    return ''.join(result)


#Getting Data
components=get_cleantech_components('IN-069', cnxn)
inverters=components[components.ComponentType=='INV'].sort_values('ComponentId')[:21]
mfm=components[components.ComponentType=='MFM'].sort_values('ComponentId')

master_df_inverter=None
for index, row in inverters.iterrows():
    end_date=str(datetime.datetime.strptime(curr_date, "%Y-%m-%d").date() + datetime.timedelta(days=1))
    df=get_raw_data_2(curr_date, end_date, 'IN-069', [str(row['ComponentId'])], ['Timestamp', 'AC_Power', 'AC_Energy'], cnxn)
    df=df.fillna(0)
    df['Timestamp']=pd.to_datetime(df['Timestamp'])
    df_Power=pd.DataFrame(df.resample('H', on='Timestamp').AC_Power.mean())
    df_Energy=pd.DataFrame(df.resample('H', on='Timestamp').AC_Energy.sum())
    df_Power.columns=[row['ComponentName']+'_wavg']    
    df_Energy.columns=[row['ComponentName']+'_whsum']      
    df_Power.index=df_Energy.index
    df=pd.concat([df_Power, df_Energy], axis=1) 
    if master_df_inverter is None:
        master_df_inverter=df
    else:
        df.index=master_df_inverter.index
        master_df_inverter=pd.concat([master_df_inverter, df], axis=1)

master_df_mfm=pd.DataFrame(index=master_df_inverter.index)
for index, row in mfm.iterrows():
    end_date=str(datetime.datetime.strptime(curr_date, "%Y-%m-%d").date() + datetime.timedelta(days=1))
    df=get_raw_data_2(curr_date, end_date, 'IN-069', [str(row['ComponentId'])], ['Timestamp', 'Export_Meter_Reading'], cnxn)
    df=df.fillna(0)
    df.set_index('Timestamp', inplace=True)
    mfm_df_grouped=df.groupby(df.index.hour)
    df=pd.DataFrame(index=master_df_mfm.index, columns=[row['ComponentName']])
    for hour in mfm_df_grouped.groups.keys():            
        hour_df=mfm_df_grouped.get_group(hour)['Export_Meter_Reading']
        df.iloc[hour]=hour_df.iloc[-1]-hour_df.iloc[0]
    master_df_mfm=pd.concat([master_df_mfm, df], axis=1)

master_df=pd.concat([master_df_inverter, master_df_mfm], axis=1)
master_df=pd.DataFrame(master_df.iloc[5:(7+12)+1])
master_df.replace(np.nan, 0, inplace=True)

#Formatting File
master_df.index=master_df.index.strftime('%H:%M')
column_list=[]
for i in range(1, 22):
    column_list.append('INVERTER_{i}_wavg'.format(i=i))
    column_list.append('INVERTER_{i}_whsum'.format(i=i))
for i in [1, 2, 3, 7, 9]:
    column_list.append('MFM_{i}'.format(i=i))

master_df.columns=column_list
COLUMN_NAMES_HEADER_EXCEL_INVERTER=["COLONY AREA - INVERTER {i}".format(i=i) for i in range(1,17)]
# inverter 17 to 21 have been mapped as follows
# Inverter 17 – Unit I, Inverter 02
# Inverter 18 – Unit I, Inverter 01
# Inverter 19 – Unit II, Inverter 01
# Inverter 20 – Unit II, Inverter 02
# Inverter 21 – Unit II, Inverter 03
# hard coding of the flip of inverter 17 and 18 in the columns
new_col_order=list(master_df.columns)
new_col_order=new_col_order[:32] + new_col_order[34:36] + new_col_order[32:34]  + new_col_order[36:]
master_df=master_df[new_col_order]
COLUMN_NAMES_HEADER_EXCEL_INVERTER.extend(['UNIT I - INVERTER 01', 'UNIT I - INVERTER 02', 'UNIT II - INVERTER 01', 'UNIT II - INVERTER 02', 'UNIT II - INVERTER 03'])
COLUMN_NAMES_HEADER_EXCEL_MFM=['MFM_1 (Unit 2)', 'MFM_2 (Colony Area)', 'MFM_3 (Unit 1)', 'MFM_4 (School)', 'MFM_5 (Admin)']

# create workbook and worksheet
workbook=xlsxwriter.Workbook('/home/'+user+'/Dropbox/Customer/[IN-9069L]/MIS Report.xlsx')
worksheet=workbook.add_worksheet()

#SET UP FRAME and FORMATS FOR SHEET
bold=workbook.add_format({'bold': True, 'align': 'center'})
num_format =workbook.add_format({'num_format': '#,###0.00'})

# -------------------------------------- ROW 1----------------------------------------------
# writing the headers
worksheet.set_column('A:{end_col}'.format(end_col=excel_style(21*2+5+1)),20)
# WRITE COLUMN NAMES
row, col=0,0 
worksheet.write("A1", "Date", bold)
col += 1
#writing inverter name headers and merging two columns for it
for name in COLUMN_NAMES_HEADER_EXCEL_INVERTER:
    range_str="{fc}{r}:{tc}{r}".format(fc=excel_style(col), r=row+1, tc=excel_style(col+1))
    worksheet.merge_range(range_str, name, bold)
    col += 2

worksheet.set_column('{c}:{ce}'.format(c=excel_style(col), ce=excel_style(col+5)), 20)
for name in COLUMN_NAMES_HEADER_EXCEL_MFM:
    worksheet.write("{c}{r}".format(r=row+1,c=excel_style(col)), name, bold)
    col += 1

# -------------------------------------- ROW 2 ------------------------------------------
for column in master_df.columns:
    master_df[column]=master_df[column].apply(lambda x: round(x/1000, 2))
row+=1
col=0
worksheet.write("A2", curr_date)
col += 1
value_format=workbook.add_format({'align': 'center'})
for col in range(1,1+21*2):
    worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "PAC (kW)" if col%2==1 else "ENERGY (kWh)", value_format)
start=col+1
for col in range(start, start+5):
    worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "ENERGY (kWh)", value_format)

#feed values by looping over the data frame
#------------------ ROW 3 ONWARDS, FILLING IN THE DATA --------------------------------------- 
col=0
row += 1
for i in range(master_df.shape[0]):
    worksheet.write('{c}{r}'.format(c=excel_style(col),r=row+1), master_df.index[i])
    col+=1
    for j in range(master_df.shape[1]):
        pos='{c}{r}'.format(c=excel_style(col), r=row+1)
        col+=1
        worksheet.write(pos,master_df.iloc[i, j])
    col=0
    row+=1
for i in range(master_df.shape[1]+1):
    if i == 0:
        worksheet.write(row, i, "Total", value_format)
    else:
        worksheet.write(row, i, master_df.iloc[:, i-1].sum(), num_format)

workbook.close()
send_mail("Dear Sir,\n\nPlease find the MIS Report for "+str(curr_date)+".\n\nRegards,\nTeam Cleantech\n ",['/home/'+user+'/Dropbox/Customer/[IN-9069L]/MIS Report.xlsx'])