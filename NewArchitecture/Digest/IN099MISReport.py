import pytz
import sharepy
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import smtplib
import numpy as np
import os
import sys
import datetime
import xlsxwriter
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *

curr_date=sys.argv[1]
user='admin'
tz=pytz.timezone('Asia/Bangkok')

#Authentication means for Azure
server='cleantechsolar.database.windows.net'
database='Cleantech Meter Readings'
username='RohanKN'
password='R@h@nKN1'
driver='{ODBC Driver 17 for SQL Server}'
cnxn=pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+password)

def send_mail(info,attachment_path_list=None):
    s=smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg=MIMEMultipart()
    sender='operations@cleantechsolar.com'
    recipients=['operationsWestIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','om.prakash@prismjohnson.in','pcl.energy@prismjohnson.in','jabir.khan@prismjohnson.in','manish.singh@prismjohnson.in','kumar.akash@hrjohnsonindia.com','narendra.singhai@prismjohnson.in','gaurav.khatri@prismjohnson.in','sanjeeva@prismjohnson.in','ashok.singh@prismjohnson.in','anik.mukherjee@prismjohnson.in','sanjay.singh@prismjohnson.in','shivdayal.singh@prismjohnson.in']
    msg['Subject']="Station [IN-9099L] MIS Report "+ str(curr_date)
    if sender is not None:
        msg['From']=sender
    msg['To']=", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part=MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())
    
def excel_style(col):
    """ Convert given row and column number to an Excel-style cell name. """
    LETTERS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col+=1
    result=[]
    while col:
        col, rem=divmod(col-1, 26)
        result[:0]=LETTERS[rem]
    return ''.join(result)
    
def col_cmp(col1, col2):
    c1v=int(col1.split('_')[1])
    c2v=int(col2.split('_')[1])
    if c1v < c2v:
        return -1
    elif c1v > c2v:
        return 1
    return 0

#Getting Data
components=get_cleantech_components('IN-099', cnxn)
inverters=components[components.ComponentType=='INV'].sort_values('ComponentId')
inverters["ComponentName"]=inverters["ComponentName"].str.replace('-', '_')
mfm=components[components.ComponentType=='MFM'].sort_values('ComponentId')

master_df_inverter=None
for index, row in inverters.iterrows():
    end_date=str(datetime.datetime.strptime(curr_date, "%Y-%m-%d").date()+ datetime.timedelta(days=1))
    df=get_raw_data_2(curr_date, end_date, 'IN-099', [str(row['ComponentId'])], ['Timestamp', 'AC_Power', 'AC_Energy'], cnxn)
    df=df.fillna(0)
    df['Timestamp']=pd.to_datetime(df['Timestamp'])
    df_Power=pd.DataFrame(df.resample('H', on='Timestamp').AC_Power.mean())
    df_Energy=pd.DataFrame(df.resample('H', on='Timestamp').AC_Energy.sum())
    df_Power.columns=[row['ComponentName']+'_wavg']    
    df_Energy.columns=[row['ComponentName']+'_whsum']      
    df_Power.index=df_Energy.index
    df=pd.concat([df_Power, df_Energy], axis=1) 
    if master_df_inverter is None:
        master_df_inverter=df
    else:
        df.index=master_df_inverter.index
        master_df_inverter=pd.concat([master_df_inverter, df], axis=1)

master_df_mfm=pd.DataFrame(index=master_df_inverter.index)
for index, row in mfm.iterrows():
    end_date=str(datetime.datetime.strptime(curr_date, "%Y-%m-%d").date()+ datetime.timedelta(days=1))
    df=get_raw_data_2(curr_date, end_date, 'IN-099', [str(row['ComponentId'])], ['Timestamp', 'Export_Meter_Reading'], cnxn)
    df=df.fillna(0)
    df.set_index('Timestamp', inplace=True)
    mfm_df_grouped=df.groupby(df.index.hour)
    df=pd.DataFrame(index=master_df_mfm.index, columns=[row['ComponentName']])
    for hour in mfm_df_grouped.groups.keys():            
        hour_df=mfm_df_grouped.get_group(hour)['Export_Meter_Reading']
        df.iloc[hour]=hour_df.iloc[-1]-hour_df.iloc[0]
    master_df_mfm=pd.concat([master_df_mfm, df], axis=1)

master_df=pd.concat([master_df_inverter, master_df_mfm], axis=1)
master_df=pd.DataFrame(master_df.iloc[5:(7+12)+1])
master_df.replace(np.nan, 0, inplace=True)

#Formatting File
master_df.index=master_df.index.strftime('%H:%M')
COLUMN_NAMES_HEADER_EXCEL_INVERTER=["INVERTER {i}".format(i=i) for i in range(1, 64)]
COLUMN_NAMES_HEADER_EXCEL_INVERTER=list(dict.fromkeys(COLUMN_NAMES_HEADER_EXCEL_INVERTER))
COLUMN_NAMES_HEADER_EXCEL_MFM=['Billing MFM']

# create workbook and worksheet
workbook=xlsxwriter.Workbook('/home/'+user+'/Dropbox/Customer/[IN-9099L]/MIS Report.xlsx')
worksheet=workbook.add_worksheet()

#SET UP FRAME and FORMATS FOR SHEET
bold=workbook.add_format({'bold': True, 'align': 'center'})
num_format=workbook.add_format({'num_format': '#,###0.00'})


# -------------------------------------- ROW 1----------------------------------------------
# writing the headers
worksheet.set_column('A:{end_col}'.format(end_col=excel_style(2*len(COLUMN_NAMES_HEADER_EXCEL_INVERTER)+1)),15)
# WRITE COLUMN NAMES
row, col=0,0 
worksheet.write("A1", "Date", bold)
col+=1
#writing inverter name headers and merging two columns for it
for name in COLUMN_NAMES_HEADER_EXCEL_INVERTER:
    range_str="{fc}{r}:{tc}{r}".format(fc=excel_style(col), r=row+1, tc=excel_style(col+1))
    worksheet.merge_range(range_str, name, bold)
    col+=2

# worksheet.set_column('{c}:{ce}'.format(c=excel_style(col), ce=excel_style(col)), 20)
for name in COLUMN_NAMES_HEADER_EXCEL_MFM:
    worksheet.write("{c}{r}".format(r=row+1,c=excel_style(col)), name, bold)
    col+=1

# -------------------------------------- ROW 2 ------------------------------------------
master_df=(master_df/1000).round(2)
#for column in master_df.columns:
#    master_df[column]=master_df[column].apply(lambda x: round(x/1000, 2))
row+=1
col=0
worksheet.write("A2", curr_date)
col+=1
value_format=workbook.add_format({'align': 'center'})
for col in range(1,len(COLUMN_NAMES_HEADER_EXCEL_INVERTER)*2):
    worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "PAC (kW)" if col%2==1 else "ENERGY (kWh)", value_format)
start=col+1
for col in range(start, start+2):
    worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "ENERGY (kWh)", value_format)
#feed values by looping over the data frame

# #------------------ ROW 3 ONWARDS, FILLING IN THE DATA --------------------------------------- 
col=0
row+=1
for i in range(master_df.shape[0]):
    worksheet.write('{c}{r}'.format(c=excel_style(col),r=row+1), master_df.index[i])
    col+=1
    for j in range(master_df.shape[1]):
        pos='{c}{r}'.format(c=excel_style(col), r=row+1)
        col+=1
        worksheet.write(pos,master_df.iloc[i, j])
    col=0
    row+=1
for i in range(master_df.shape[1]+1):
    if i==0:
        worksheet.write(row, i, "Total", value_format)
    else:
        worksheet.write(row, i, master_df.iloc[:, i-1].sum(), num_format)
workbook.close()

send_mail("Dear Sir,\n\nPlease find the MIS Report for "+str(curr_date)+".\n\nRegards,\nTeam Cleantech\n ",['/home/'+user+'/Dropbox/Customer/[IN-9099L]/MIS Report.xlsx'])