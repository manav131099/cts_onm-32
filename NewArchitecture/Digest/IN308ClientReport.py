import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import math
import numpy as np
import logging
import pyodbc
import sharepy
from openpyxl import load_workbook
import xlsxwriter
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from datetime import date
from datetime import timedelta

date = sys.argv[1]
tz = pytz.timezone('Asia/Calcutta')
  
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

tdf = pd.read_sql_query("SELECT * FROM [Portfolio].ProcessedData WHERE  SiteId = (SELECT SiteId FROM [Portfolio].Site WHERE SiteCode='IN-308') AND ParameterId=5 AND ComponentId=3932 AND Date>'2021-04-01' ORDER BY Date", connStr)
  
def send_mail(date, recipients, attachment_path_list=None):
     server = smtplib.SMTP("smtp.office365.com")
     server.starttls()
     server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
     msg = MIMEMultipart()
     sender = 'operations@cleantechsolar.com'
     msg['Subject'] = "Station [IN-9308L] Client Digest "+ str(date)
     msg['From'] = sender
     msg['To'] = ", ".join(recipients)
     email_body = "Dear Sir,\n\nPlease find the Generation Data for "+str(date)+".\n\nRegards,\nTeam Cleantech\n "
     if attachment_path_list is not None:
         for each_file_path in attachment_path_list:
             file_name = each_file_path.split("/")[-1]
             part = MIMEBase('application', "octet-stream")
             part.set_payload(open(each_file_path, "rb").read())
             encoders.encode_base64(part)
             part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
             msg.attach(part)
     msg.attach(MIMEText(email_body))
     server.sendmail(sender, recipients, msg.as_string())

recipients = ['operationsCentralIN@cleantechsolar.com', 'om-it-digest@cleantechsolar.com', 'padwal.dattatreya@cie-india.com']

def format_col_width(ws):
    ws.column_dimensions['A'].width = 20
    ws.column_dimensions['B'].width = 20

path_write = '/home/admin/Dropbox/Customer/[IN-9308L]/Mahindra Generation Data.xlsx'
workbook = xlsxwriter.Workbook(path_write)
workbook.close()
book = load_workbook(path_write)
writer = pd.ExcelWriter(path_write, engine = 'openpyxl',options={'strings_to_numbers': True})
writer.book = book
tdf = tdf.fillna(0)
tdf['Date'] = pd.to_datetime(tdf['Date'])
tdf['months'] = tdf['Date'].apply(lambda x:x.strftime('%B %y'))
tdf['Date'] = tdf['Date'].apply(lambda x:x.strftime('%d-%m-%Y'))
months = list(tdf.months.unique())

for i in months:
    sheet_name = i
    df = pd.DataFrame()
    df = df.fillna(0)
    df['Date'] = tdf[tdf['months']==i]['Date']
    df['Mahindra CIE block-3'] = tdf[tdf['months']==i]['Value']
    if(datetime.datetime.now(tz).strftime('%Y-%m-%d')==pd.datetime.strptime(df.Date.values[-1],"%d-%m-%Y").strftime("%Y-%m-%d")):
        df = df[:-1]
    finalrow = pd.DataFrame({'Date':'Total','Mahindra CIE block-3': round(df['Mahindra CIE block-3'].sum(), 2)},index=[len(df)])
    df = pd.concat([df,finalrow],axis=0)
    df['Mahindra CIE block-3'] = df['Mahindra CIE block-3'].apply(lambda x: "{:,}".format(x))
    df.to_excel(writer,sheet_name=sheet_name,index=False)
    workbook  = writer.book
    worksheet = writer.sheets[sheet_name]
    format_col_width(worksheet)
    writer.save()
try:
    std=book.get_sheet_by_name('Sheet1')
    book.remove_sheet(std)
except:
    pass
writer.save()
writer.close()

send_mail(date,recipients,[path_write])