import time
from database_operations import *
import pytz
import os
import logging

tz = pytz.timezone( "Asia/Calcutta")

while(1):

    date = (datetime.datetime.now(tz) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
    print('Time now:',datetime.datetime.now(tz))
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    site_df = get_sites('Locus',cnxn)
    site_df = site_df[site_df['OverallStatus']==1]
    site_df['Flag'] = 1
    generic_df = site_df[site_df['DigestTypeStatus']==0]
    custom_df = site_df[site_df['DigestTypeStatus']==1]
    client_df = site_df[site_df['ClientDigestStatus']>=1]
    print(custom_df)
    cnxn.close()
    if(datetime.datetime.now(tz).hour==1):
        print('Resetting all flags')
        generic_df['Flag'] = 0 
        custom_df['Flag'] = 0 
        client_df['Flag'] = 0 
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','MYSH', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','SGSH', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','THSH', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','LOTUS1', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','LOTUS2', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/RegionalMasterDigest.py','LPF', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/ThailandDailySummary.py', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/IN069MISReport.py', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/IN099MISReport.py', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/IN303ClientReport.py', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/IN307ClientReport.py', date]))
        os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/IN308ClientReport.py', date]))
    time_now = datetime.datetime.now(tz).strftime("%Y-%m-%d %H:%M:%S")
    start_date = (datetime.datetime.now(tz)).replace(tzinfo=None)
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

#Client Generic Digests if 1 / Custom Digests if 2

    for index,row in client_df.iterrows():
        try:
            site_id = get_site_id(row['SiteCode'],cnxn) 
            last_mail_date = get_bot_metadata(row['SiteCode'],'Mail','Client', cnxn)
            if((start_date-last_mail_date).days>1 and row['Flag']==0):
                print('Sending Client Digest',row['SiteCode'])
                if(row['ClientDigestStatus']==1):
                     os.system(" ".join(['python3 /home/pranav/ctsfork/NewArchitecture/Digest/ClientDigest.py','Production', row['SiteCode'],date]))
                else:
                    os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/'+row['SiteCode'].replace('-','')+'ClientDigest.py','Production', row['SiteCode'],date]))
                client_df.loc[index,'Flag']=1
            if((start_date-last_mail_date).days)<=1:
                print("/home/admin/Start/MasterMail/"+ row['SiteCode'].split('-')[0]+'-9'+row['SiteCode'].split('-')[1] + "L_Bot.txt")
                with open("/home/admin/Start/MasterMail/"+ row['SiteCode'].split('-')[0]+'-9'+row['SiteCode'].split('-')[1] + "L_Bot.txt", "w") as file:
                    file.write(time_now) 
        except Exception as e:
            logging.exception('Failed')
            print(e)

#Internal Custom Digests

    for index,row in custom_df.iterrows():
        try:
            site_id = get_site_id(row['SiteCode'],cnxn) 
            last_mail_date = get_bot_metadata(row['SiteCode'],'Mail','Internal', cnxn)
            if((start_date-last_mail_date).days>1 and row['Flag']==0):
                print('Sending Digest',row['SiteCode'])
                os.system(" ".join(['python3 /home/admin/CODE/NewArchitecture/Digest/'+row['SiteCode'].replace('-','')+'Digest.py','Production', row['SiteCode'],date]))
                custom_df.loc[index,'Flag']=1
            if((start_date-last_mail_date).days)<=1:
                with open("/home/admin/Start/MasterMail/"+ row['SiteCode'] + "L_Bot.txt", "w") as file:
                    file.write(time_now) 
        except Exception as e:
            logging.exception('Failed')
            print(e)

#Internal Generic Digests
    for index,row in generic_df.iterrows():
        try:
            site_id = get_site_id(row['SiteCode'],cnxn)
            last_mail_date = get_bot_metadata(row['SiteCode'],'Mail','Internal', cnxn)
            if((start_date-last_mail_date).days>1 and row['Flag']==0):
                print('Sending Digest',row['SiteCode'])
                os.system(" ".join(['python3 /home/pranav/ctsfork/NewArchitecture/Digest/GenericDigest.py','Production', row['SiteCode'],date]))
                generic_df.loc[index,'Flag']=1
            if((start_date-last_mail_date).days)<=1:
                with open("/home/admin/Start/MasterMail/"+ row['SiteCode'] + "L_Bot.txt", "w") as file:
                    file.write(time_now) 
        except Exception as e:
            logging.exception('Failed')
            print(e)
    cnxn.close()
    print('Sleeping')
    time.sleep(3600)

  