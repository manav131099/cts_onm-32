import smtplib
import numpy as np
import os
import datetime
import sys
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *
#from process_functions import *
#import process_functions

country=sys.argv[1]
date=sys.argv[2]
cnxn=pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+password)

def send_mail(date, info, recipients, attachment_path_list=None):
  server=smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg=MIMEMultipart()
  sender='operations@cleantechsolar.com'
  if country=='MYSH':
    msg['Subject']="SHELL MALAYSIA - MASTER DIGEST "+str(date)
  elif country=='SGSH':
    msg['Subject']="SHELL SINGAPORE - MASTER DIGEST "+str(date)
  elif country=='THSH':
    msg['Subject']="SHELL THAILAND - MASTER DIGEST "+str(date)
  elif country=='LOTUS1':
    msg['Subject']="LOTUS PHASE-1 MASTER DIGEST "+str(date)
  elif country=='LOTUS2':
    msg['Subject']="LOTUS PHASE-2 MASTER DIGEST "+str(date)
  elif country=='LPF':
    msg['Subject']="LPF MASTER DIGEST "+str(date)
  #if sender is not None:
  msg['From']=sender
  msg['To']=", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name=each_file_path.split("/")[-1]
        part=MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  part2=MIMEText(info, "html")
  msg.attach(part2)
  server.sendmail(sender, recipients, msg.as_string())

def add_html_info(info,header,value,body_type):
    if(body_type=='header'):
        temp_info='<tr class="heading"><td>'+header+'</td><td>'+value+'</td></tr>'
    elif(body_type=='item'):
        temp_info='<tr class="item"><td>'+header+'</td><td>'+value+'</td></tr>'          
    info=info+temp_info
    return info

gen_info="""<!doctype html>
     <html>
     <head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
    </head>

    <body>
    <div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
       
        <img src="http://cleantechsolar.com/content/uploads/Logo_black_600.png" style="height: 55px;width:260px;border:0;" height="55" width="260"> <br>
    """

if country=='MYSH':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] WHERE [SiteCode] LIKE 'MY-4%' or [SiteCode] LIKE 'MY-5%' or [SiteCode] LIKE 'MY-6%'", cnxn)
elif country=='SGSH':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] WHERE [SiteCode] LIKE 'SG-4%' or [SiteCode] LIKE 'SG-5%' or [SiteCode] LIKE 'SG-6%'", cnxn)
elif country=='THSH':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] WHERE [SiteCode] LIKE 'TH-4%' or [SiteCode] LIKE 'TH-5%' or [SiteCode] LIKE 'TH-6%'", cnxn)
elif country=='LOTUS1':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] where SiteCode between 'TH-010' and 'TH-028'", cnxn)
elif country=='LOTUS2':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] where SiteCode between 'TH-029' and 'TH-044'", cnxn)
elif country=='LPF':
  query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] where SiteCode between 'TH-060' and 'TH-066'", cnxn)
  
df1=pd.DataFrame(query, columns=['SiteId', 'SiteCode'])
query=pd.read_sql_query("SELECT  [SiteId], [SiteName] FROM [Portfolio].[SiteInformation]", cnxn)
df2=pd.DataFrame(query, columns=['SiteId', 'SiteName'])
df=pd.merge(df1, df2, how='left')

gen_info+='<tr></tr><tr></tr>'+add_html_info('', 'General Information', '', 'header')
digest_body=''
capacities=[]
total_sites=len(df)
energy_gen=[]
yields=[]
pr=[]
no_gen=0
irrs=[]
irrs_all=[]
sitewithirr=0

for site in range(len(df)):
  print(df['SiteCode'][site])
  flag=0
  digest_body+=add_html_info('', df['SiteCode'][site]+' - '+df['SiteName'][site], '', 'header')
  digest_body+=add_html_info('', 'Installed Capacity [kWp]:', '{0:,}'.format(round(get_mfm_capacities(df['SiteCode'][site], cnxn)['Size'].values.sum(), 2)), 'item')
  capacities.append(get_mfm_capacities(df['SiteCode'][site], cnxn)['Size'].values.sum())
  
  df1=get_processed_data(date, df['SiteId'][site], cnxn)
  main_irr_id=get_main_irr_component(df['SiteId'][site], cnxn)
  irr_data_df=get_processed_data(date, get_site_id(get_irr_center(df['SiteId'][site], cnxn), cnxn), cnxn)
  irr_data_df=irr_data_df[irr_data_df["ComponentType"]=='WMSIRR']
  irr_data_df.fillna(value=np.nan, inplace=True)
  try:
    GHI=irr_data_df[(irr_data_df['ComponentType']=='WMSIRR') & (irr_data_df['ComponentId']==main_irr_id) & (irr_data_df['CSName']=='GTI')]['Value'].values[0]
  except:
    GHI=irr_data_df[(irr_data_df['ComponentType']=='WMSIRR') & (irr_data_df['ComponentId']==main_irr_id) & (irr_data_df['CSName']=='GHI')]['Value'].values[0]
  if GHI==None:
    GHI=np.nan
  
  if df1['ComponentType'].str.contains('WMSIRR').any():
    sitewithirr+=1
    irrs.append(GHI)
  else:
    flag=1
    
  irrs_all.append(GHI)
    
  df1=df1[df1["ComponentType"]=='MFM'][['CSName', 'Value', 'ComponentId', 'ParameterId', 'DigestOrder']]
  if 'Full Site PR (GTI)' in df1["CSName"]:
    df1=df1[df1["CSName"].isin(['EAC method-2', 'Yield-2', 'Full Site PR (GTI)', 'Last recorded value', 'Last recorded time'])]
  else:
    df1=df1[df1["CSName"].isin(['EAC method-2', 'Yield-2', 'Full Site PR (GHI)', 'Last recorded value', 'Last recorded time'])]
  df2=get_processed_template(date, df['SiteId'][site], cnxn)
  df2=df2[['ComponentId', 'ParameterId', 'Rounding', 'Unit']]
  merged_df=pd.merge(df1, df2,  how='left', left_on=['ComponentId', 'ParameterId'], right_on=['ComponentId', 'ParameterId'], suffixes=('', '_dup'))
  merged_df.fillna(value=np.nan, inplace=True)
  merged_df.loc[(merged_df['CSName']=='Full Site PR (GHI)'), 'DigestOrder']=10
  merged_df=merged_df.sort_values(by=['ComponentId', 'DigestOrder'])
  
  if flag==1:
    digest_body+=add_html_info('', 'Irradiation [kWh/m2] (From '  + get_irr_center(df['SiteId'][site], cnxn) + ', ' + str(get_rpr_distance(df['SiteCode'][site], cnxn)) + ' km away): ', '{0:,}'.format(round(GHI, 2)), 'item')
  else:
    digest_body+=add_html_info('', 'Irradiation [kWh/m2]: ', '{0:,}'.format(round(GHI, 2)), 'item')
  
  for index, row in merged_df.iterrows():
    if(row['CSName']=='EAC method-2'):
      energy_gen.append(row['Value'])
      if(np.isnan(row['Value'])):
        no_gen+=1
    if(row['CSName']=='Full Site PR (GHI)'):
      pr.append(row['Value'])
      
    if(row['CSName']=='Yield-2'):
      yields.append(row['Value'])
      
    if(row['CSName']=='Last recorded time'):
      if(np.isnan(row['Value'])):
          digest_body+=add_html_info('', str(row['CSName'])+': ', str(row['Value']), 'item')
      else:
          digest_body+=add_html_info('', str(row['CSName'])+': ', datetime.datetime.fromtimestamp(row['Value']).strftime('%Y-%m-%d %H:%M:%S'), 'item')
    else:
      digest_body+=add_html_info('', str(row['CSName'])+' ['+str(row['Unit'])+']: ', '{0:,}'.format(round(row['Value'], int(row['Rounding']))), 'item')
    
    if(row['CSName']=='Full Site PR (GHI)'):
      if flag==1:
        if round(row['Value'], 1)>60 and round(row['Value'], 1)<70:
          digest_body+=add_html_info('', 'Reporting PR (From ' + str(get_irr_center(df['SiteId'][site], cnxn)) + '):', '70 >>> PR Floor Activated!', 'item')
        elif round(row['Value'], 1)>90:
          digest_body+=add_html_info('', 'Reporting PR (From ' + str(get_irr_center(df['SiteId'][site], cnxn)) + '):', '85 >>> PR Ceiling Activated!', 'item')
        else:
          digest_body+=add_html_info('', 'Reporting PR (From ' + str(get_irr_center(df['SiteId'][site], cnxn)) + '):', '{0:,}'.format(round(row['Value'], 1)), 'item')

yields=np.nan_to_num(yields)
irrs=np.nan_to_num(irrs)
cov_yields=((np.nanstd(yields))*100)/np.nanmean(yields)
cov_irr=((np.nanstd(irrs))*100)/np.nanmean(irrs)

gen_info+=add_html_info('', 'Total Sites Running:', str(total_sites), 'item')
gen_info+=add_html_info('', 'Total Installed Capacity [kWp]:', '{0:,}'.format(round((sum(capacities)), 2)), 'item')
gen_info+=add_html_info('', 'Total Energy Generated [kWh]:', '{0:,}'.format(round((np.nansum(energy_gen)), 1)), 'item')
gen_info+=add_html_info('', 'Total Yield [kWh/kWp]:', '{0:,}'.format(round((np.nansum(energy_gen)/sum(capacities)), 2)), 'item')
gen_info+=add_html_info('', 'Stdev / COV Yields:', '{0:,}'.format(round((np.nanstd(yields)), 2))+' / '+'{0:,}'.format(round(cov_yields, 1))+'%', 'item')
gen_info+=add_html_info('', 'Number of Sites with Irradiance Sensor:', str(sitewithirr), 'item')
try:
  gen_info+=add_html_info('', 'Average Irradiation at sites with Sensors [kWh/m2]:', '{0:,}'.format(round(np.nansum(irrs)/sitewithirr, 2)), 'item')
except:
  gen_info+=add_html_info('', 'Average Irradiation at sites with Sensors [kWh/m2]:', 'nan', 'item')
gen_info+=add_html_info('', 'Stdev / COV Irradiation [%]:', '{0:,}'.format(round((np.nanstd(irrs)), 2))+' / '+'{0:,}'.format(round(cov_irr, 1))+'%', 'item')
try:
  gen_info+=add_html_info('', 'Average Portfolio PR [%]:', '{0:,}'.format(round(((np.nansum(energy_gen)/sum(capacities))*100/(np.nanmean(irrs_all))), 1)), 'item')
except:
  gen_info+=add_html_info('', 'Average Portfolio PR [%]:', 'nan', 'item')
gen_info+=add_html_info('', 'Number of Sites with PR > 80%:', str(sum(1 for x in pr if x>80)), 'item')
gen_info+=add_html_info('', 'Number of Sites with 70% < PR < 80%:', str(sum(1 for x in pr if (x>70 and x<80))), 'item')
gen_info+=add_html_info('', 'Number of Sites with PR < 70%:', str(sum(1 for x in pr if x<70)), 'item')
gen_info+=add_html_info('', 'Number of Sites with No Generation:', str(no_gen), 'item')

info=gen_info+digest_body

if country=='MYSH':
  recipients=['operationsMY@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com','arnaud.ayral@cleantechsolar.com','lucas.ferrand@cleantechsolar.com']
elif country=='SGSH':
  recipients=['operationsSG@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com']
elif country=='THSH':
  recipients=['operationsTH@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com']
elif country=='LOTUS1':
  recipients=['operationsTH@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com']
elif country=='LOTUS2':
  recipients=['operationsTH@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com']
elif country=='LPF':
  recipients=['operationsTH@cleantechsolar.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com']

#recipients=['manav.gupta@cleantechsolar.com']

send_mail(date, info, recipients)
