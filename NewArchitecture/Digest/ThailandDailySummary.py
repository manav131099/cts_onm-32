import pytz
import sharepy
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import smtplib
import numpy as np
import os
import sys
from datetime import datetime, timedelta
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *
import openpyxl

curr_date=sys.argv[1]
user='admin'
tz=pytz.timezone('Asia/Bangkok')

#Authentication means for Azure
server='cleantechsolar.database.windows.net'
database='Cleantech Meter Readings'
username='RohanKN'
password='R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
cnxn=pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+password)

def send_mail(info,attachment_path_list=None):
    s=smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg=MIMEMultipart()
    sender='operations@cleantechsolar.com'
    recipients=['operationsTH@cleantechsolar.com','om-interns@cleantechsolar.com', 'om-it-digest@cleantechsolar.com']
    msg['Subject']="TH Daily Summary " + str(curr_date)
    if sender is not None:
        msg['From']=sender
    msg['To']=", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part=MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())

sites=['TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028','TH-029','TH-030','TH-031','TH-032','TH-033','TH-034','TH-035','TH-036','TH-037','TH-038','TH-039','TH-040','TH-041','TH-042','TH-043','TH-044','TH-060','TH-061','TH-062','TH-063','TH-064','TH-065','TH-066']

df=pd.DataFrame()

for site in sites:
  print(site)
  temp=get_processed_data(curr_date, get_site_id(site, cnxn), cnxn)[['ComponentType', 'ComponentName', 'CSName', 'Value', 'DigestOrder']]
  temp=temp.sort_values(by=['ComponentName', 'DigestOrder'])
  try:
    try:
      if site=='TH-018':
        GHI=temp.loc[(temp['ComponentName']=='Silicon Sensor 1') & (temp['CSName']=='GHI'), 'Value'].values[0]
      else:
        GHI=temp.loc[(temp['ComponentName']=='Pyranometer') & (temp['CSName']=='GHI'), 'Value'].values[0]
    except:
      GHI=temp.loc[(temp['CSName']=='GHI'), 'Value'].values[0]
  except:
    GHI=np.nan
  temp=temp.loc[((temp['ComponentType']=='MFM') & ((temp['CSName']=='Last recorded value') | (temp['CSName']=='Last recorded time'))), :]
  if len(temp)>2:
    values=temp.Value.to_list()[:2]
    values.insert(0, GHI)
    values.insert(0, site)
    df=df.append([values])
    values=temp.Value.to_list()[2:]
    values.insert(0, GHI)
    values.insert(0, site)
    df=df.append([values])    
  else:
    values=temp.Value.to_list()
    values.insert(0, GHI)
    values.insert(0, site)
    df=df.append([values])
  
df=df.round(2)
df.columns=['Site Name', 'GHI', 'Last Recorded Value', 'Last Recorded Time']
df['Last Recorded Time'] = pd.to_datetime(df['Last Recorded Time'], unit='s').dt.strftime('%Y-%m-%d %H:%M:%S')
df.to_csv('/home/'+user+'/Dropbox/Regional/LOTUS/LOTUS Daily Summary '+str(curr_date)+'.csv', index=False)

#----------------Daily PR Vals-------------------------------------------#

hist_start = '2021-09-01'

#TH-901 exception site
def get_exception_val(meter, curr_date):
    path = "/home/admin/Dropbox/Fourth_Gen/[TH-901L]/[TH-901L]-lifetime.txt"
    cap_MFM1 = 66.03
    cap_MFM2 = 264.12
    
    df = pd.read_csv(path, sep='\t')
    df = df[['Date', 'MFM_1_PV Meter Roof.PR2', 'MFM_2_PV Meter Farm.PR2']]
    df['Full_PR'] = round(((df['MFM_1_PV Meter Roof.PR2'] * cap_MFM1) + (df['MFM_2_PV Meter Farm.PR2'] * cap_MFM2)) / (cap_MFM1 + cap_MFM2),1)
    df = df.rename({'MFM_1_PV Meter Roof.PR2': 'TH-901-1','MFM_2_PV Meter Farm.PR2': 'TH-901-2', 'Full_PR':'TH-901'}, axis=1)
    pr = df[str(meter)].loc[df.Date == str(curr_date)].values[0]
    return pr

#TH-003 exception site
other_exception = ['TH-003F','TH-003G','TH-003']
def get_other_exception_val(meter, curr_date):
    path = f"/home/admin/Dropbox/Third Gen/[TH-003X]/{str(curr_date)[:4]}/[TH-003X] {str(curr_date)[:7]}.txt"
    cap_MFM1 = 533
    cap_MFM2 = 461.5
    df = pd.read_csv(path, sep='\t')
    df = df[['Date', 'PR2M2', 'PR2M4']]
    df['Full_PR'] = round(((df['PR2M2'] * cap_MFM1) + (df['PR2M4'] * cap_MFM2)) / (cap_MFM1 + cap_MFM2),1)
    df = df.rename({'PR2M2': 'TH-003F','PR2M4': 'TH-003G', 'Full_PR':'TH-003'}, axis=1)
    pr = df[str(meter)].loc[df.Date == str(curr_date)].values[0]
    return pr

def get_pr_val(meter):
    #TH-901 meters
    if meter in exception_meters:        
          lst = []
          for i in range(delta.days + 1):
              day = sdate + timedelta(days=i)
              c_date = day.date()
              a = get_exception_val(meter,c_date)
              lst.append(a)
          return lst
    #TH-003 meters
    elif meter in other_exception:        
          lst = []
          for i in range(delta.days + 1):
              day = sdate + timedelta(days=i)
              c_date = day.date()
              a = get_other_exception_val(meter,c_date)
              lst.append(a)
          return lst
    #TH-007 has 2 comp with full PR (used TOP 1)
    elif meter in ['TH-007']:
          pr = pd.read_sql_query(f"Select Value from Portfolio.ProcessedData where ComponentId in (Select TOP 1 ComponentId from Portfolio.Meter where MeterReference like '{meter}%') and ParameterId =69 and Date  >= '{hist_start}' and Date <= '{curr_date}' order by date", cnxn)
          if (pr.shape[0] != 0):
                pr = round(pr.Value,1).to_list()
                return pr
    else:
        #single meter
        if (len(meter) == 6):
            pr = pd.read_sql_query(f"Select Value from Portfolio.ProcessedData where ComponentId in (Select ComponentId from Portfolio.Meter where MeterReference like '{meter}%') and ParameterId =69 and Date  >= '{hist_start}' and Date <= '{curr_date}' order by date", cnxn)
            #blank date check
            date = pd.read_sql_query(f"Select Top 1 Date from Portfolio.ProcessedData where ComponentId in (Select ComponentId from Portfolio.Meter where MeterReference like '{meter}%') and ParameterId =69 and Date  >= '{hist_start}' and Date <= '{curr_date}' order by date", cnxn)
            diff_dates = start_date_check(date.Date[0])
            emp = []
            emp.extend([None]*diff_dates)
            if (pr.shape[0] != 0):
                if pr.Value[0] is not None:
                    pr = round(pr.Value,1).to_list()
                    lst = emp+pr
                    return lst
            
        #diff meters
        elif (len(meter) == 7):
            pr = pd.read_sql_query(f"Select Value from Portfolio.ProcessedData where ComponentId = (Select ComponentId from Portfolio.Meter where MeterReference ='{meter}') and ParameterId =9 and Date  >= '{hist_start}' and Date <= '{curr_date}' order by date", cnxn)
            if (pr.shape[0] != 0):
                pr = round(pr.Value,1).to_list()
                return pr   

def start_date_check(date):
    old = datetime.datetime.strptime(hist_start, '%Y-%m-%d').date()
    delta = date - old
    return delta.days
    

def get_sites():
    sites = pd.read_sql_query("SELECT SiteId FROM [Portfolio].[Site] where SiteCode Like '%TH%' order by SiteCode", cnxn)
    sites = sites.SiteId.to_list()
    return sites


def get_meters(sites):
    meter_cols = ['Site']
    for site in sites:
        meter_num = pd.read_sql_query(f"Select MeterReference from Portfolio.Meter where SiteId = {site}", cnxn)
        #for more than 1 meter
        if(meter_num.shape[0] > 1):
            for i in range(meter_num.shape[0]):
                meter_cols.append(meter_num.MeterReference[i])
            meter_cols.append(meter_num.MeterReference[0][:6])
                                                                    
        # for 1 meter
        else:
            meter_cols.append(meter_num.MeterReference[0][:6])                                                                                        
    return meter_cols

def get_size(sites):
    size_row = ['System Size(kWp)']
    for site in sites:
        size_num = pd.read_sql_query(f"Select Size from Portfolio.Meter where SiteId = {site}", cnxn)
        #for more than 1 meter
        if(size_num.shape[0] > 1):
            for i in range(size_num.shape[0]):
                size_row.append(size_num.Size[i]) 
            size_row.append(sum(size_num.Size))                                                                    
        # for 1 meter
        else:
            size_row.append(size_num.Size[0])                                                                                          
    return size_row

sites = get_sites()
meter_cols = get_meters(sites)
exception_meters = ['TH-901-1', 'TH-901-2', 'TH-901']

for i in exception_meters:
    meter_cols.append(i)


df = pd.DataFrame(columns = meter_cols)
final_df = pd.DataFrame(columns = meter_cols)
size_cols = get_size(sites)
exception_sizes = [66.03, 264.12, 330.15]
for i in exception_sizes:
    size_cols.append(i)

df.loc[len(df)] = size_cols

sdate =  datetime.datetime.strptime(hist_start, '%Y-%m-%d')  # start date
edate = datetime.datetime.strptime(curr_date, '%Y-%m-%d')   # end date

delta = edate - sdate       # as timedelta
list_date = ['System Size (kWp)']
for i in range(delta.days + 1):
              day = sdate + timedelta(days=i)
              c_date = day.date()
              list_date.append(str(c_date))

final_df['Site'] = list_date

meter_cols.pop(0)


for meter in meter_cols:
  print(meter)
  a = get_pr_val(meter)
  a.insert(0, df[meter][0])
  print(a)
  final_df[meter] = a

final_df.to_excel('/home/'+user+'/Dropbox/Regional/TH PR Portfolio '+str(curr_date)+'.xlsx', index=False)
send_mail('Please find the daily summary for TH sites attached.',['/home/'+user+'/Dropbox/Regional/LOTUS/LOTUS Daily Summary '+str(curr_date)+'.csv', '/home/'+user+'/Dropbox/Regional/TH PR Portfolio '+str(curr_date)+'.xlsx'])

