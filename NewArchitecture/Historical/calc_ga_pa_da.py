import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *
import process_functions
from pvlib import location
from pvlib import irradiance
from pvlib import clearsky, atmosphere, solarposition
from pvlib.location import Location

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

def calc_ga_2(df): #For NON-OA Sites
    if (('Frequency' in df.columns.tolist())  and not df.empty):
        mfm_df = df
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        mask = df['ghi']>50
        mfm_sh_df = df[mask]
        totlen = len(mfm_sh_df)
        mfm_sh_df = mfm_sh_df[['Timestamp','Frequency']].dropna()
        if(len(mfm_sh_df)<12):
            return None
        if(site[3]!='3'):
            if('Frequency' in mfm_sh_df.columns.tolist()):
                freq_timestamps = mfm_sh_df.loc[mfm_sh_df['Frequency']>40,'Timestamp']
            else:
                freq_timestamps = mfm_sh_df.loc[mfm_sh_df['AC_Phase_A_Voltage']>300,'Timestamp']
            if(len(freq_timestamps)>0):
                GA = (float(len(freq_timestamps))/float(totlen))*100
                return GA
            else:
                return None
    else:
        return None

def calc_pa_2(df,site):
    if (('AC_Power' in df.columns.tolist()) and ('Frequency' in df.columns.tolist())  and not df.empty): #is not none in case no WMS 
        mfm_df = df
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        """ month = mfm_df['Timestamp'].dt.month.values[0]
        if(month <=6):
            mask = (mfm_df['Timestamp'].dt.hour > 7) & (mfm_df['Timestamp'].dt.hour < 17)#Change later to 18
        else:
            mask = (mfm_df['Timestamp'].dt.hour > 7) & (mfm_df['Timestamp'].dt.hour < 17) """
        mask = df['ghi']>50
        mfm_sh_df = df[mask]
        mfm_sh_df = mfm_sh_df[['Timestamp','Frequency','AC_Power']].dropna()
        oa_flag = 0 #If OA site has no Irradiance
        if(len(mfm_sh_df)<12):
            return None
        if(site[3]=='3' and site!='IN-304'):    
            wms_df = pd.DataFrame()
            wms_df['Timestamp'] = pd.to_datetime(wms_df['Timestamp'])
            wms_df=wms_df[wms_df['Irradiance'].notnull()]
            irr_timestamps=wms_df.loc[wms_df['Irradiance']>20,'Timestamp']
            if(len(irr_timestamps)>0):
                null_rows = mfm_df[mfm_df.isnull().any(axis=1)]
                mfm_df_temp = mfm_df[['Timestamp','Frequency','AC_Power']].dropna()
                wms_df=wms_df[~wms_df['Timestamp'].isin(null_rows['Timestamp'])]
                irr_timestamps=wms_df.loc[wms_df['Irradiance']>20,'Timestamp']
                freq_timestamps=mfm_df_temp.loc[mfm_df_temp['Frequency']>40,'Timestamp']
                power_timestamps=mfm_df_temp.loc[mfm_df_temp['AC_Power']>2,'Timestamp']
                pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                ga_common = list(set(freq_timestamps) & set(irr_timestamps)) 
                if(len(ga_common)>0):
                    PA = (float(len(pa_common))/float(len(ga_common)))*100
                    return PA
                else:
                    return None
            else:
                oa_flag = 1
        else:
            oa_flag = 1
        if(site[3]!='3' or oa_flag == 1):
            power_timestamps = mfm_sh_df.loc[mfm_sh_df['AC_Power']>2,'Timestamp']
            if('Frequency' in mfm_sh_df.columns.tolist()):
                freq_timestamps = mfm_sh_df.loc[mfm_sh_df['Frequency']>40,'Timestamp']
            else:
                freq_timestamps = mfm_sh_df.loc[mfm_sh_df['AC_Phase_A_Voltage']>300,'Timestamp']
            pa_common=list(set(freq_timestamps) & set(power_timestamps))
            if(len(freq_timestamps)>0):
                PA = (float(len(pa_common))/float(len(freq_timestamps)))*100
                return PA
            else:
                print('g')
                return None
    else:
        return None

sites=[]
df_loc = pd.read_csv('/home/admin/public/Locations.csv')
for i in range(426,568):
    sites.append('MY-'+str(i))
print(sites)

for site in sites:
    print(site)
    if(not os.path.exists('/home/admin/public/'+site+'.txt')):
        print('Getting Raw Data')
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        df_component = get_cleantech_components(site,cnxn)
        c_id = df_component.loc[(df_component['ComponentType']=='MFM'),'ComponentId'].tolist()
        c_id = map(str, c_id)
        df = get_raw_data_2('2019-01-01', '2021-08-31', site, c_id,['Timestamp','ComponentId','AC_Power','Frequency','AC_Phase_A_Voltage'],cnxn)
        print(df.tail())
        df.to_csv('/home/admin/public/'+site+'.txt',sep='\t',index=False)
        cnxn.close()
    lat = float(df_loc[df_loc['O&M Reference']==site]['Exact Location'].values[0].split(',')[0])
    long = float(df_loc[df_loc['O&M Reference']==site]['Exact Location'].values[0].split(',')[1])
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    site_id = get_site_id(site,cnxn)
    df_final = pd.DataFrame()
    df_component = get_cleantech_components(site,cnxn)
    mfm_ids = df_component.loc[(df_component['ComponentType']=='MFM'),'ComponentId'].tolist()
    df = pd.read_csv('/home/admin/public/'+site+'.txt',sep='\t')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    if(site[0:2]=='IN'):
        zone = 'Asia/Kolkata'
    elif(site[0:2]=='TH'):
        zone = 'Asia/Bangkok'
    elif(site[0:2]=='MY'):
        zone = 'Asia/Kuala_Lumpur'
    elif(site[0:2]=='SG'):
        zone = 'Asia/Singapore'
    elif(site[0:2]=='KH'):
        zone = 'Asia/Phnom_Penh'
    tus = Location(lat, long, zone)
    times = pd.date_range(start=datetime.datetime.strftime(df['Timestamp'].min(),'%Y-%m-%d'), end=datetime.datetime.strftime(df['Timestamp'].max(),'%Y-%m-%d'), freq='5min', tz=tus.tz)
    cs = tus.get_clearsky(times)  # ineichen with climatology table by default
    cs = cs.reset_index().rename({'index':'Timestamp'}, axis = 'columns')
    cs['Timestamp'] = cs['Timestamp'].dt.tz_localize(None)
    df = pd.merge(df,cs,on='Timestamp',how='left')
    for i,j in df.groupby(df['Timestamp'].dt.date):
        for n in mfm_ids:
            mfm_df = j[j['ComponentId']==n]
            ga = calc_ga_2(mfm_df)
            pa = calc_pa_2(mfm_df,site)
            df_temp = pd.DataFrame({'Date':[i],'Value':[pa],'ParameterId':[14],'ComponentId':[n],'SiteId':[site_id]})
            df_temp2 = pd.DataFrame({'Date':[i],'Value':[ga],'ParameterId':[13],'ComponentId':[n],'SiteId':[site_id]})
            df_final = df_final.append(df_temp)
            df_final = df_final.append(df_temp2)
    print(df_final['Value'].mean())
    
    df_final = df_final.replace({np.nan: None})
    df_final['Date'] = pd.to_datetime(df_final['Date'])
    df_final = df_final[(df_final['Date']!=datetime.datetime(2021,7,30)) | (df_final['Date']!=datetime.datetime(2021,8,30))]
    #df_final.to_csv(site+'PA.txt',sep='\t')
    print(df_final.tail())
    fast_upsert_processed(site,df_final)
    cnxn.close()