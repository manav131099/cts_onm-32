import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib
from ftplib import FTP
import pytz
import urllib.request
import gzip
import io
import time
from socket import timeout

def process_data(date,df,site_id):
    date = date.strftime('%Y-%m-%d')
    component_id = df['ComponentId'].unique()
    for c in component_id:
        component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==c,:]
        temp_comp_df = df.loc[df['ComponentId']==c,:].dropna(axis=1, how='all')
        if('Irradiance' in df.columns.tolist()):
            wms_raw_data = df.loc[df['ComponentId']==main_irr_component,['Timestamp','Irradiance']]
        elif('Irradiance_GTI' in df.columns.tolist()):
            wms_raw_data = df[df['ComponentId']==main_irr_component,['Timestamp','Irradiance_GTI']].unique()
        else:
            wms_raw_data = pd.DataFrame()
        component_type = component_df.loc[component_df['ComponentId']==c,'ComponentType'].values[0]
        if(component_type.strip()=='MFM'):
            capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==c,'Size'].values[0]
        elif(component_type.strip()=='INV'):
            capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==c,'Size'].values[0]
        elif(component_type.strip()=='WMSIRR'):
            capacity = 0 
        elif(component_type.strip()=='WMS'):
            capacity = 0    
        for index,row in component_parameters_df.iterrows():
            params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
            if(len(params)>1):
                result=getattr(process_functions, function_mapping[row['CSName']][0])(df=temp_comp_df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=1,irr_center_parameter_df=irr_center_parameter_df)
            else:
                result=getattr(process_functions, function_mapping[row['CSName']][0])(df=temp_comp_df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=1,irr_center_parameter_df=irr_center_parameter_df)
            upsert_processed(date,result,int(row['ParameterId']),int(c),int(site_id))

site='IN-015'
irr_center='Self'
tz = pytz.timezone('Asia/Kolkata')
digest_flag=0
main_path = '/home/admin/Dropbox/Cleantechsolar/1min/[712]/'

site_id = get_site_id(site) 
main_irr_component = get_main_irr_component(site_id)
component_df = get_cleantech_components(site)
seris_metadata_df = get_seris_metadata(site)
component_df = pd.merge(component_df,seris_metadata_df,on='ComponentId',how='left')

mfm_capacity_df = get_mfm_capacities(site) 
inv_capacity_df = get_inv_capacities(site) 

parameter_df = get_parameter(site)
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['CSName'],raw_parameter_df['CSName']))
parameter_name_mapping['Timestamp']='Timestamp'
parameter_name_mapping['ComponentId']='ComponentId' #This is mapped to LocusId initially
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

if(irr_center == 'Self'):
    irr_center_component_df = component_df
    irr_center_parameter_df = get_parameter(site)
else:
    irr_center_component_df = get_cleantech_components(irr_center)
    irr_center_parameter_df = get_parameter(irr_center)

print(component_df)

for j in sorted(os.listdir(main_path)):
    for k in sorted(os.listdir(main_path+'/'+j)):
        if(k[0:4]!='2018' and k[0:4]!='2017' and k[0:4]!='2016' and k!='2019-02' and k!='2019-01'):
            for l in sorted(os.listdir(main_path+'/'+j+'/'+k)):
                print(l)
                df = pd.read_csv(main_path+'/'+j+'/'+k+'/'+l,sep='\t')
                if('Rec_ID' not in df.columns.tolist()):
                    df.insert(1, 'Rec_ID',0)
                timestamp = df['Tm']
                append_df  = pd.DataFrame()
                for index,row in component_df.iterrows():
                    start = int(row['ColumnData'].split('-')[0])-1
                    end = int(row['ColumnData'].split('-')[1])
                    c_id = row['ComponentId']
                    azure_data = df.iloc[:,start:end]
                    azure_data['Timestamp'] = timestamp
                    azure_data.columns = row['DataAvailable'].split(',') + ['Timestamp'] 
                    azure_data.columns = azure_data.columns.to_series().map(parameter_name_mapping)
                    azure_data = azure_data[azure_data.columns.dropna()]
                    col = azure_data.pop("Timestamp")
                    azure_data.insert(0, col.name, col)
                    azure_data['ComponentId'] = c_id
                    col = azure_data.pop("ComponentId")
                    azure_data.insert(1, col.name, col)
                    if(index == 0):
                        append_df = azure_data
                    else:
                        append_df = append_df.append(azure_data,sort=False)
                append_df['Timestamp'] = pd.to_datetime(append_df['Timestamp'])
                append_df['AC_Power'] = append_df['AC_Power']*1000*-1
                append_df['Export_Meter_Reading'] = append_df['Export_Meter_Reading']*1000
                append_df['Import_Meter_Reading'] = append_df['Import_Meter_Reading']*1000
                append_df  = append_df .replace({np.nan: None})
                fast_upsert_final(site,append_df)
                process_data(append_df['Timestamp'].dt.date.values[0],append_df,site_id)
                update_botmetadata(site_id,l[6:-4],'Raw') 



 
