import os
import sys
from time import altzone
import numpy as np
import pytz
import process_functions
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *

class SiteData:
    def __init__(self, site_code):
        self.site_code = site_code
        self.site_id = get_site_id(site_code, cnxn) 
        chkdir(main_path+site_code) 
        self.irr_center = get_irr_center(self.site_id, cnxn)
        self.component_df = get_cleantech_components(site_code, cnxn)
        self.locus_metadata_df = get_locus_metadata(site_code, cnxn)
        self.parameter_df = get_parameter(site_code, cnxn)
        self.mfm_capacity_df = get_mfm_capacities(site_code, cnxn) 
        self.inv_capacity_df = get_inv_capacities(site_code, cnxn) 

        #Parameters
        self.raw_parameter_df = self.parameter_df.loc[self.parameter_df['ParameterType']=='Raw',:]
        self.parameter_name_mapping = dict(zip(self.raw_parameter_df['MPName'],self.raw_parameter_df['CSName']))
        if('POAI_avg' in list(self.parameter_name_mapping.keys()) and 'GHI_avg' in list(self.parameter_name_mapping.keys())):
            self.parameter_name_mapping['POAI_avg'] = 'Irradiance_GTI'
        self.parameter_name_mapping = column_mismatch(site_code,self.parameter_name_mapping)
        self.parameter_name_mapping['ts']='Timestamp'
        self.parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially
        self.processed_parameter_df = self.parameter_df.loc[self.parameter_df['ParameterType']=='Processed',:]

        #Getting Irr center components
        self.irr_center_component_df = get_cleantech_components(self.irr_center, cnxn)
        self.irr_center_parameter_df = get_parameter(self.irr_center, cnxn)



def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def rounddown(n):
    rem=n%10
    return (n-rem)

def get_tz(site):
    if(site[0:2]=='MY'):
        timezone = "Asia/Kuala_Lumpur"
    elif(site[0:2]=='TH'):
        timezone = "Asia/Bangkok"
    elif(site[0:2]=='VN'):
        timezone = "Asia/Saigon"
    elif(site[0:2]=='PH'):
        timezone = "Asia/Manila"
    elif(site[0:2]=='KH'):
        timezone = "Asia/Phnom_Penh"
    else:
        timezone = "Asia/Calcutta"
    return timezone

def process_data(date,df,component_id,component_df,site_id,processed_parameter_df,mfm_capacity_df, inv_capacity_df,irr_center_component_df,irr_center_parameter_df,site,cnxn):
    date = date.strftime('%Y-%m-%d')
    component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==component_id,:]
    component_type = component_df.loc[component_df['ComponentId']==component_id,'ComponentType'].values[0]
    if(component_type.strip()=='MFM'):
        capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='INV'):
        capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='WMSIRR'):
        capacity = 0 
    elif(component_type.strip()=='WMS'):
        capacity = 0    
    elif(component_type.strip()=='IMFM'):
        capacity = 0 
    for index,row in component_parameters_df.iterrows():
        params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
        if(len(params)>1):
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        else:
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,component_id=component_id,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        upsert_processed(date,result,row['ParameterId'],component_id,site_id,cnxn)

def save_file(df,site):
    grouped_df = df.groupby('ComponentName')
    for name,group in sorted(grouped_df):
        group['ts']=group['ts'].str.replace('T',' ')
        group.ts=pd.to_datetime(group.ts.str[0:19])
        date_grouped_df=group.groupby(group['ts'].dt.date)
        for name2,group2 in sorted(date_grouped_df):
            chkdir(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name))
            group2.to_csv(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name)+'/'+str(name2)+'-'+str(name)+'.txt',sep='\t',index=False)

def get_azure_data(site_instance,start_date,end_date,auth):
    locus_parameter = list(site_instance.parameter_name_mapping.keys())
    locus_id = site_instance.locus_metadata_df.loc[site_instance.locus_metadata_df['ComponentId']==i,'LocusComponentId'].values[0]
    locus_data_available = site_instance.locus_metadata_df.loc[site_instance.locus_metadata_df['ComponentId']==i,'DataAvailable'].values[0]
    timezone = get_tz(site_instance.site_code[0:2])
    data=get_locus_data(auth,start_date,end_date,locus_id,locus_data_available,timezone)
    if('statusCode' in data.columns.tolist() and data['statusCode'].values[0]==401): #If auth fails
        auth = locus_authenticate(site_instance.site_code)
        data=get_locus_data(auth,start_date,end_date,locus_id,locus_data_available,timezone)
    elif('statusCode' in data.columns.tolist() and data['statusCode'].values[0]==403):
        print('Locus ComponentId incorrect!')
        return [data]
    print(component_type, flush=True)
    azure_data = data.loc[:,data.columns.isin(locus_parameter)]
    azure_data.columns = azure_data.columns.to_series().map(site_instance.parameter_name_mapping)	
    col = azure_data.pop('Timestamp')
    azure_data.insert(0, 'Timestamp', col)
    azure_data.loc[:, 'Timestamp'] = azure_data['Timestamp'].str.replace('T',' ')
    azure_data.loc[:, 'Timestamp']  = azure_data['Timestamp'].str[0:19]
    azure_data = data_mismatch(site_instance.site_code,azure_data,component_type)
    return [data,azure_data]

def raw_upsert(site_instance,azure_data,component_id):
    azure_data['ComponentId']=component_id
    data['ComponentId']=component_id
    col = azure_data.pop("ComponentId")
    if(int(site_instance.site_code[3])>=4): #For shell need SiteId
        azure_data['SiteId']=site_instance.site_id
        col2 = azure_data.pop("SiteId")
        azure_data.insert(1, col.name, col)
        azure_data.insert(2, col2.name, col2)
    else:
        azure_data.insert(1, col.name, col)
    azure_data = azure_data.replace({np.nan: None})
    fast_upsert_final(site_instance.site_code,azure_data,cnxn)

           
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
country_code = sys.argv[1]
site_df = get_sites('Locus', cnxn)
irr_info_df = get_irr_info(cnxn)
all_parameter_df = get_all_parameters(cnxn)
merged_site_df = pd.merge(site_df, irr_info_df, left_on='SiteId', right_on='SiteId', how='left')
merged_site_df = merged_site_df.fillna(0)
merged_site_df['IrradiationCenterSiteId'] = merged_site_df['IrradiationCenterSiteId'].astype('int')
merged_site_df['SensorPresent'] = (merged_site_df['IrradiationCenterSiteId'] == merged_site_df['SiteId']).astype(int)
merged_site_df = merged_site_df.sort_values(by=['SensorPresent'], ascending=False)
merged_site_df = merged_site_df[merged_site_df['SiteCode']!='IN-035']
merged_site_df = merged_site_df.iloc[257:,:]
print(merged_site_df)
#merged_site_df = merged_site_df[merged_site_df['SiteCode'].str[0:2]==country_code]
print(merged_site_df.to_string(), flush=True)
tz = pytz.timezone(get_tz(country_code))
main_path = '/home/admin/public/RawData/'
cnxn.close()

for index,row in merged_site_df.iterrows():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    site_instance = SiteData(row['SiteCode'])
    date_today = datetime.datetime(2021,6,18)
    try:
        historical_start_date = datetime.datetime(2021,6,16)
    except:
        historical_start_date = datetime.datetime(2021,3,1)
    print('Site: ',row['SiteCode'],'Start date: ',historical_start_date, flush=True)
    if(site_instance.site_code=='IN-043' or site_instance.site_code=='SG-007'):
        print('Already Done', flush=True)
        continue
    while(historical_start_date.date()<date_today.date()):
            if((date_today-historical_start_date).days<30):
                temp_end_date = date_today.replace(microsecond=0)
            else:
                temp_end_date = monthdelta(historical_start_date,1)
            save_file_df = pd.DataFrame()
            print(historical_start_date,temp_end_date, flush=True)
            auth = locus_authenticate(site_instance.site_code)
            cleantech_component_id = site_instance.component_df['ComponentId'].tolist()
            for index,i in enumerate(cleantech_component_id):
                print(site_instance.component_df.loc[site_instance.component_df['ComponentId']==i,'ComponentName'].values[0], flush=True)
                temp_day=historical_start_date
                component_type = site_instance.component_df.loc[site_instance.component_df['ComponentId']==i,'ComponentType'].values[0]
                if(component_type=='IMFM'):
                    while(temp_day<temp_end_date):
                        process_data(temp_day, pd.DataFrame(), i , site_instance.component_df, site_instance.site_id, site_instance.processed_parameter_df, site_instance.mfm_capacity_df, site_instance.inv_capacity_df,site_instance.irr_center_component_df,site_instance.irr_center_parameter_df ,site_instance.site_code, cnxn)
                        temp_day=temp_day+datetime.timedelta(days=1)
                else:
                    data = get_azure_data(site_instance,historical_start_date.date(),temp_end_date.date(),auth)
                    
                    if('statusCode' in data[0].columns.tolist()  and data[0]['statusCode'].values[0]==403):
                        continue
                    azure_data = data[1]
                    data = data[0]
                    while(temp_day<temp_end_date):
                        print(temp_day)
                        if(component_type=='MFM'):
                            wms_raw_data = get_irr_raw(temp_day, site_instance.site_id, site_instance.irr_center, cnxn)
                        else:
                            wms_raw_data = pd.DataFrame()
                        data_temp=azure_data[((azure_data['Timestamp']>=(temp_day.strftime("%Y-%m-%d"))) & (azure_data['Timestamp']<(temp_day+datetime.timedelta(days=1)).strftime("%Y-%m-%d")))]
                        if(temp_day>=date_today):
                            print('Not processing')
                            temp_day=temp_day+datetime.timedelta(days=1)
                            continue
                        process_data(temp_day, data_temp, i , site_instance.component_df, site_instance.site_id, site_instance.processed_parameter_df, site_instance.mfm_capacity_df, site_instance.inv_capacity_df,site_instance.irr_center_component_df,site_instance.irr_center_parameter_df ,site_instance.site_code, cnxn)
                        temp_day=temp_day+datetime.timedelta(days=1)
                    raw_upsert(site_instance,azure_data,i)
                    if(index==0):  
                        save_file_df = data
                    else:
                        save_file_df = save_file_df.append(data,sort=False)
            save_file_df = save_file_df.merge(site_instance.component_df,how='left',on='ComponentId')
            save_file(save_file_df,site_instance.site_code)  
            historical_start_date=temp_end_date
    cnxn.close()

