import pyodbc
import pandas as pd
import datetime
import math
import sys
sys.dont_write_bytecode = True

def get_details(user):
    if(user=='admin'):
        with open("/home/admin/Backup/Credentials/admin.txt", "r") as f:
            username = f.readline().replace('\n','')
            password = f.readline().replace('\n','')
    else:
        with open("/home/admin/Backup/Credentials/user.txt", "r") as f:
            username = f.readline().replace('\n','')
            password = f.readline().replace('\n','') 
    return [username,password]

try:
    user = sys.argv[-1]
    server = 'cleantechsolar.database.windows.net'
    database = 'Cleantech Meter Readings'
    username = get_details(user)[0]
    password =  get_details(user)[1]
    driver= '{ODBC Driver 17 for SQL Server}'
except:
    user = 'admin'
    server = 'cleantechsolar.database.windows.net'
    database = 'Cleantech Meter Readings'
    username = get_details(user)[0]
    password =  get_details(user)[1]
    driver= '{ODBC Driver 17 for SQL Server}'
    
#-----------------------PUT operations------------------------#

def add_site(site,monitoring_provider,alarm_status, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("BEGIN IF NOT EXISTS ( SELECT * FROM [Portfolio].[Site] WHERE SiteCode=?) BEGIN INSERT INTO [Portfolio].[Site] ([SiteCode],[MonitoringProvider],[AlarmStatus]) VALUES (?, ?, ?) END END",[site,site,monitoring_provider,alarm_status]):
        pass
    cnxn.commit() 
    

def add_site_information(site_id, site_name, location, module_brand, inverter_brand, cod, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[SiteInformation] ([SiteId],[SiteName],[Location],[ModuleInfo],[InverterInfo],[COD]) VALUES (?, ?, ?, ?, ?, ?)",[str(site_id),str(site_name),str(location),str(module_brand),str(inverter_brand),str(cod)]):
        print('Added!')
    cnxn.commit() 

def add_irr_center(site_id,irr_site_id,distance, cnxn):
    cursor = cnxn.cursor()
    try:
        with cursor.execute("INSERT INTO [Portfolio].[IrrInfo] ([SiteId],[IrradiationCenterSiteId],[SensorDistance]) VALUES (?, ?, ?)",[str(site_id),str(irr_site_id),distance]):
            print('Added!')
    except:
        print('Already Added!')
    cnxn.commit() 
    

def add_irr_component(site_id,c_id, cnxn):
    cursor = cnxn.cursor()
    try:
        with cursor.execute("UPDATE [Portfolio].[IrrInfo] SET [MainIrradiationSensor]=? WHERE [SiteId]=? ",[str(c_id),str(site_id)]):
            print('Updated Main Sensor!')
    except:
        print('Update Failed!')
    cnxn.commit() 

def add_component_locus(cleantech_name, cleantech_type, locus_component_id, data_available, site_id, locus_site_id, cnxn):
    cursor = cnxn.cursor()
    component_id = add_component(cleantech_name, cleantech_type, site_id,cnxn)
    with cursor.execute("INSERT INTO [Portfolio].[LocusMetadata] ([SiteId],[ComponentId],[LocusSiteId],[LocusComponentId],[DataAvailable]) VALUES (?, ? , ? , ?, ?)",[str(site_id),str(component_id),str(locus_site_id),str(locus_component_id),data_available.strip()]):
        print('Added!')
    cursor.close()
    return component_id

def add_component(cleantech_name, cleantech_type, site_id, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Component] ([SiteId],[ComponentName],[ComponentType]) VALUES (?,?,?)",[str(site_id),cleantech_name,cleantech_type]):
        print('Added!')
    cursor.execute("SELECT @@IDENTITY AS ID;")
    component_id = cursor.fetchone()[0]
    cursor.close()
    return component_id

def add_parameter(mp_name, cs_name, parameter_type, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Parameter] ([MPName],[CSName],[ParameterType]) VALUES (?,?,?)",[mp_name,cs_name,parameter_type]):
        print('Added!')
    cursor.execute("SELECT @@IDENTITY AS ID;")
    parameter_id = cursor.fetchone()[0]
    cursor.close()
    return parameter_id

def add_cleantech_metadata(parameter_id, component_id, site_id, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[CleantechMetadata] ([ParameterId],[ComponentId],[SiteId]) VALUES (?, ? , ?)",[str(parameter_id),str(component_id),str(site_id)]):
        print('Added!')
    cnxn.commit() 

def add_mfm_info(component_id, site_id, cnxn):
    size = input("Enter Meter Capacity:")
    meter_ref = input("Enter Meter Reference:")
    salesforce_ref = input("Enter SF Reference:")
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Meter] ([SiteId],[ComponentId],[Size],[MeterReference],[SalesforceReference]) VALUES (?, ? , ?, ?, ?)",[str(site_id),str(component_id),str(size),str(meter_ref),str(salesforce_ref)]):
        print('Added!')
    cnxn.commit() 

def add_mfm_info_auto(component_id, site_id, size, meter_ref, salesforce_ref, country_id, summary_order,cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Meter] ([SiteId],[ComponentId],[Size],[MeterReference],[SalesforceReference],[CountryId],[SummaryOrder]) VALUES (?, ? , ?, ?, ?, ?, ?)",[str(site_id),str(component_id),str(size),str(meter_ref),str(salesforce_ref),str(country_id),str(summary_order)]):
        print('Added!')
    cnxn.commit() 

def add_inv_info(component_id, site_id, cnxn):
    size = input("Enter Inverter Capacity:")
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Inverter] ([SiteId],[ComponentId],[Size]) VALUES (?, ? , ?)",[str(site_id),str(component_id),float(size)]):
        print('Added!')
    cnxn.commit() 

def add_inv_info_auto(component_id, site_id, size, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[Inverter] ([SiteId],[ComponentId],[Size]) VALUES (?, ? , ?)",[str(site_id),str(component_id),float(size)]):
        print('Added!')
    cnxn.commit() 
    
def add_smb_info_auto(component_id, site_id, size, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[SMB] ([SiteId],[ComponentId],[Size]) VALUES (?, ? , ?)",[str(site_id),str(component_id),float(size)]):
        print('Added!')
    cnxn.commit()

def add_webdynmetadata(component_id, site_id, webdyn_id, data_available, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[WebdynMetadata] ([SiteId],[ComponentId],[WebdynComponentId],[DataAvailable]) VALUES (?, ? , ?, ?)",[str(site_id),str(component_id),str(webdyn_id),str(data_available)]):
        print('Added!')
    cnxn.commit()  

def add_serismetadata(component_id, site_id, data, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[SerisMetadata] ([SiteId],[ComponentId],[DataAvailable]) VALUES (?, ?, ?)",[str(site_id),str(component_id),str(data)]):
        print('Added!')
    cnxn.commit() 

def update_botmetadata(site_id,last_time,bot_type,digest_type,cnxn):
    cursor = cnxn.cursor()
    site_id=int(site_id)
    with cursor.execute("UPDATE [Portfolio].[BotMetadata] SET [LastTime] = ? WHERE [SiteId]=? AND [BotType]=? AND [DigestType] = ? if @@ROWCOUNT = 0 INSERT into [Portfolio].[BotMetadata]([SiteId], [LastTime], [BotType],[DigestType]) values(?, ?, ?, ?) ",[last_time,site_id,bot_type,digest_type,site_id,last_time,bot_type,digest_type]):
        print('Success')
    cnxn.commit()

def upsert_status_data(reset,status,last_time,site_id,component_id,cnxn):
    if(reset == 1):
        cursor = cnxn.cursor()
        print(last_time)
        with cursor.execute("UPDATE [Portfolio].[SiteStatus] SET [LastTime] = ?,[Status] = ? ",[last_time,status]):
            print('Success')
        cnxn.commit()    
    else:
        cursor = cnxn.cursor()
        site_id=int(site_id)
        with cursor.execute("UPDATE [Portfolio].[SiteStatus] SET [LastTime] = ?,[Status] = ? WHERE [SiteId]=? AND [ComponentId]=? if @@ROWCOUNT = 0 INSERT into [Portfolio].[SiteStatus]([SiteId],[ComponentId],[LastTime]) values(?, ?, ?) ",[last_time,status,site_id,component_id,site_id,component_id,last_time]):
            print('Success')
        cnxn.commit()  

def add_column(site,col, cnxn):
    cursor = cnxn.cursor()
    if(int(site[3])>=4):
        n = group_shell(site[3:6])
        print("ALTER TABLE ["+site[0:3]+n+"].[RawData] ADD "+col+"FLOAT")
        with cursor.execute("ALTER TABLE ["+site[0:3]+n+"].[RawData] ADD "+col+" FLOAT"):
            print('Added Column')
    else:
        for c in col:
            print("ALTER TABLE ["+site+"].[RawData] ADD "+c+" FLOAT")
            try:
                with cursor.execute("ALTER TABLE ["+site+"].[RawData] ADD "+c+" FLOAT"):
                    print('Added Column')   
            except:
                print('Already there')  

def add_limit(site_id,pr_limit,inv_limit,mfm_limit,cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO  [Portfolio].[Limit] VALUES (?,?,?,?,?,?,?,?)",[str(site_id),str(pr_limit),str(mfm_limit),str(inv_limit),'2020-01-01','GHI','0.008',75]):
        print('Inserted Limit!')

def add_webhook(site_id,webhook,cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO  [Portfolio].[Webhook] VALUES (?,?)",[str(site_id),str(webhook)]):
        print('Inserted Webhook!')

def add_recipient(site_id,email,digest_type,cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO  [Portfolio].[MailRecipient] VALUES (?,?,?)",[str(site_id),str(email),digest_type]):
        print('Inserted Email!')

def add_attachment_metadata(site_id,graph_id,cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("INSERT INTO [Portfolio].[AttachmentMetadata] VALUES ("+str(site_id)+","+str(graph_id)+")"): 
        print('Added Graph!')
     
#-----------------------GET operations------------------------#

def get_sites(provider, cnxn):
    if(provider=='All'):
        query = pd.read_sql_query("SELECT  [SiteId],[SiteCode],[AlarmStatus],[MonitoringProvider],[DataGranularity],[OverallStatus],[DigestTypeStatus],[ClientDigestStatus] FROM [Portfolio].[Site]", cnxn)
    else:
        query = pd.read_sql_query("SELECT  [SiteId],[SiteCode],[AlarmStatus],[MonitoringProvider],[DataGranularity],[OverallStatus],[DigestTypeStatus],[ClientDigestStatus] FROM [Portfolio].[Site] WHERE [MonitoringProvider]='"+str(provider)+"'", cnxn)
    df = pd.DataFrame(query, columns=['SiteId','SiteCode','AlarmStatus','MonitoringProvider','DataGranularity','OverallStatus','DigestTypeStatus','ClientDigestStatus'])
    return df

def get_site_information(site, cnxn):
    query = pd.read_sql_query('SELECT  [SiteId],[SiteName],[Location],[ModuleInfo],[InverterInfo],[COD] FROM [Portfolio].[SiteInformation] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query, columns=['SiteId','SiteName','Location','ModuleInfo','InverterInfo','COD'])
    return df

def get_site_id(site, cnxn):
    query = pd.read_sql_query('''SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['SiteId'])
    return df['SiteId'].values[0]

def get_data_granularity(site, cnxn):
    query = pd.read_sql_query('''SELECT [DataGranularity] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['DataGranularity'])
    return df['DataGranularity'].values[0]

def get_irr_center(site_id, cnxn):
    query = pd.read_sql_query('''SELECT [SiteCode] FROM [Portfolio].[Site] WHERE SiteId = (SELECT [IrradiationCenterSiteId] FROM [Portfolio].[IrrInfo] WHERE [SiteId]='''+'\''+str(site_id)+'\')', cnxn)
    df = pd.DataFrame(query)
    return df['SiteCode'].values[0]

def get_main_irr_component(site_id, cnxn):
    query = pd.read_sql_query('''SELECT [MainIrradiationSensor] FROM [Portfolio].[IrrInfo] WHERE [SiteId]='''+'\''+str(site_id)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['MainIrradiationSensor'])
    return df['MainIrradiationSensor'].values[0]

def get_irr_info(cnxn):
    query = pd.read_sql_query("SELECT * FROM [Portfolio].[IrrInfo]", cnxn)
    df = pd.DataFrame(query)
    return df

def get_cleantech_components(site, cnxn):
    if(isinstance(site,int)):
        query = pd.read_sql_query('SELECT  [ComponentId],[ComponentName],[ComponentType] FROM [Portfolio].[Component] WHERE [SiteId]='+str(site), cnxn)
    else:
        query = pd.read_sql_query('''SELECT  [ComponentId],[ComponentName],[ComponentType] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\') ORDER BY [ComponentType] DESC,[ComponentName]', cnxn)
    df = pd.DataFrame(query, columns=['ComponentId','ComponentName','ComponentType'])
    return df

def get_locus_metadata(site, cnxn):
    query = pd.read_sql_query('''SELECT  [ComponentId],[LocusComponentId],[DataAvailable],[LocusSiteId] FROM [Portfolio].[LocusMetadata] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query, columns=['ComponentId','LocusComponentId','DataAvailable','LocusSiteId'])
    return df

def get_seris_metadata(site, cnxn):
    query = pd.read_sql_query('''SELECT * FROM [Portfolio].[SerisMetadata] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query)
    return df

def get_webdyn_metadata(site, cnxn):
    query = pd.read_sql_query('''SELECT  * FROM [Portfolio].[WebdynMetadata] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query)
    return df

def get_all_parameters(cnxn):
    query = pd.read_sql_query('''SELECT [ParameterId],[MPName],[CSName] FROM [Portfolio].[Parameter]''', cnxn)
    df = pd.DataFrame(query, columns=['ParameterId','MPName','CSName'])
    return df

def get_parameter(site, cnxn):
    query = pd.read_sql_query('''SELECT [Portfolio].[Parameter].[ParameterId],[ComponentId],[Portfolio].[Parameter].[MPName],[Portfolio].[Parameter].[CSName],[Portfolio].[Parameter].[ParameterType],[DigestOrder]
                                FROM [Portfolio].[Parameter]
                                INNER JOIN [Portfolio].[CleantechMetadata]
                                ON [Portfolio].[Parameter].[ParameterId] = [Portfolio].[CleantechMetadata].[ParameterId] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query, columns=['ParameterId','ParameterType','ComponentId','MPName','CSName','DigestOrder'])
    return df

def get_mfm_capacities(site, cnxn):
    if(site=='All'):
        query = pd.read_sql_query('SELECT * FROM [Portfolio].[Meter]', cnxn)
        df = pd.DataFrame(query, columns=['SiteId','ComponentId','Size','MeterReference'])
    else:
        query = pd.read_sql_query('''SELECT * FROM [Portfolio].[Meter] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
        df = pd.DataFrame(query, columns=['SiteId','ComponentId','Size','MeterReference'])
    return df

def get_inv_capacities(site, cnxn):
    query = pd.read_sql_query('''SELECT * FROM [Portfolio].[Inverter] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query, columns=['SiteId','ComponentId','Size'])
    return df

def get_smb_capacities(site, cnxn):
    query = pd.read_sql_query('''SELECT * FROM [Portfolio].[SMB] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\')', cnxn)
    df = pd.DataFrame(query, columns=['SiteId','ComponentId','Size'])
    return df


def get_bot_metadata(site,bot_type,digest_type,cnxn):
    query = pd.read_sql_query('''SELECT * FROM [Portfolio].[BotMetadata] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\') AND BotType=\''+bot_type+'\' AND DigestType=\''+digest_type+'\'', cnxn)
    df = pd.DataFrame(query)
    if(df.empty):
        update_botmetadata(get_site_id(site,cnxn),'2021-07-01',bot_type,digest_type,cnxn) 
        query = pd.read_sql_query('''SELECT * FROM [Portfolio].[BotMetadata] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\') AND BotType=\''+bot_type+'\' AND DigestType=\''+digest_type+'\'', cnxn)
        df = pd.DataFrame(query)
    return df['LastTime'].dt.to_pydatetime()[0]

def get_irr(date, component_df, irr_type, no, parameter_df, cnxn):
    wms = component_df.loc[component_df['ComponentType'].str.strip()=='WMSIRR',:]
    if(len(wms)==0):
        return 0
    elif(len(wms)==1):   
        if(irr_type=='GTI'):
            component_id = wms['ComponentId'].values[0]
            query = pd.read_sql_query("SELECT [Value] FROM [Portfolio].[ProcessedData] WHERE [ComponentId]="+str(component_id)+"AND [ParameterId]=304 AND Date='"+str(date)+"'", cnxn)
        else:
            component_id = wms['ComponentId'].values[0]
            query = pd.read_sql_query("SELECT [Value] FROM [Portfolio].[ProcessedData] WHERE [ComponentId]="+str(component_id)+"AND [ParameterId]=2 AND Date='"+str(date)+"'", cnxn)
    elif(len(wms)>1):
        merged_df = wms.merge(parameter_df,how='left',on='ComponentId')
        if(irr_type=='GTI'):
            print('GETTING GTI')
            #Check if GTI is recorded
            component_id = merged_df.loc[merged_df['CSName']=='GTI','ComponentId'].values.tolist()[no]
            query = pd.read_sql_query("SELECT [Value] FROM [Portfolio].[ProcessedData] WHERE [ComponentId]="+str(component_id)+"AND [ParameterId]=304 AND Date='"+str(date)+"'", cnxn)
        else:
            #Check if GHI is recorded
            component_id = merged_df.loc[merged_df['CSName']=='GHI','ComponentId'].values.tolist()[no]
            query = pd.read_sql_query("SELECT [Value] FROM [Portfolio].[ProcessedData] WHERE [ComponentId]="+str(component_id)+"AND [ParameterId]=2 AND Date='"+str(date)+"'", cnxn)
    df = pd.DataFrame(query, columns=['Value'])
    if df.empty:
        return 0
    else:
        return df['Value'].values[0]

def get_irr_raw(date, site_id, irr_site, cnxn):
    cur = cnxn.cursor()
    component_id = get_main_irr_component(site_id,cnxn)
    if(component_id==None or irr_site[3]=='7'):
        return None
    if(int(irr_site[3])>=4):
        n = group_shell(irr_site[3:6])
        table_name = "["+irr_site[0:3]+n+"].[RawData]"
    else:
        table_name = "["+irr_site+"].[RawData]"
    if cur.execute("SELECT TABLE_SCHEMA FROM information_schema.tables where TABLE_SCHEMA='"+table_name[1:-11]+"' and TABLE_NAME='RawData'").fetchall():
        query = pd.read_sql_query("SELECT [Timestamp],[Irradiance] FROM "+table_name+" WHERE [ComponentId]="+str(component_id)+"AND Timestamp BETWEEN '"+str(date)+"' AND'" +str(date)[0:10]+" 23:55:00'", cnxn)
        df = pd.DataFrame(query)
    else:
        df = pd.DataFrame()
    return df

def get_ylds(date, component_df, site_id, component_type, cnxn):
    if(component_type=='MFM'):
        inverter_ids = component_df.loc[component_df['ComponentType']=='MFM','ComponentId'].tolist()
    elif(component_type=='INV'):
        inverter_ids = component_df.loc[component_df['ComponentType']=='INV','ComponentId'].tolist()
    elif(component_type=='SMB'):
        inverter_ids = component_df.loc[component_df['ComponentType']=='SMB','ComponentId'].tolist()
    data = get_processed_data(date,site_id,cnxn)
    inv_ylds=[]
    for i in inverter_ids:
        if((data['ComponentId'] == i).any()):
            inv_yld = data.loc[((data['ComponentId'] == i) & (data['ParameterId'] == 7)),'Value'].values[0]
            if(inv_yld==None or math.isnan(inv_yld)):
                pass
            else:
                inv_ylds.append(inv_yld)
    return inv_ylds

def get_pr(date, component_df, site_id, mfm_capacity_df,pr_type,irr_center_parameter_df, cnxn):
    data = get_processed_data(date,site_id,cnxn)
    irr_id = get_main_irr_component(site_id,cnxn)
    pos = 0
    wms_df = component_df.loc[component_df['ComponentType']=='WMSIRR',:]
    wms_df = wms_df.merge(irr_center_parameter_df,how='left',on='ComponentId')
    wms_df = wms_df[wms_df['CSName']==pr_type]
    wms_df.reset_index(drop=True,inplace=True)
    if(len(wms_df)>1):
        pos = wms_df.loc[wms_df['ComponentId']==irr_id,:].index.item()
    merged_df = mfm_capacity_df.merge(data,how='left',on='ComponentId')
    if(pr_type == 'GTI'):
        if(pos==0):
            merged_df = merged_df.loc[merged_df['ParameterId']==306,:]
        elif(pos==1):
            merged_df = merged_df.loc[merged_df['ParameterId']==346,:]
        elif(pos==2):
            merged_df = merged_df.loc[merged_df['ParameterId']==347,:]
    else:
        #If sensor 1 then 9 else 344 for 2 else 345 for 3
        if(pos==0):
            merged_df = merged_df.loc[merged_df['ParameterId']==9,:]
        elif(pos==1):
            merged_df = merged_df.loc[merged_df['ParameterId']==344,:]
        elif(pos==2):
            merged_df = merged_df.loc[merged_df['ParameterId']==345,:]
    return merged_df 
    

def get_processed_data(date, site_id, cnxn):
    query = pd.read_sql_query("SELECT [Date],[Value],[CSName],[Portfolio].[ProcessedData].[ComponentId],[ComponentName],[Portfolio].[Component].[ComponentType],[Rounding],[Portfolio].[ProcessedData].[ParameterId],[DigestOrder] FROM [Portfolio].[ProcessedData] INNER JOIN [Portfolio].[Parameter] ON [Portfolio].[ProcessedData].ParameterId = [Portfolio].[Parameter].ParameterId  INNER JOIN [Portfolio].[Component] ON [Portfolio].[ProcessedData].ComponentId = [Portfolio].[Component].ComponentId  WHERE [Portfolio].[ProcessedData].SiteId="+str(site_id)+" AND Date='"+date+"' ORDER BY [Portfolio].[Component].[ComponentType] DESC,[Portfolio].[Component].ComponentName ASC", cnxn)
    df = pd.DataFrame(query, columns=['Date','ComponentName','CSName','Value','ComponentType','ComponentId','ParameterId','DigestOrder'])
    return df   

def get_processed_template(date, site_id, cnxn):
    query = pd.read_sql_query("SELECT TOP (1000) [CleantechMetadata].[SiteId],[Portfolio].[Component].[ComponentId],[CSName],[Portfolio].[Component].[ComponentName],[Portfolio].[Component].[ComponentType],[Portfolio].[Parameter].ParameterId,[Rounding],[Unit] FROM [Portfolio].[CleantechMetadata] INNER JOIN [Portfolio].[Parameter] ON [Portfolio].[Parameter].ParameterId = [Portfolio].[CleantechMetadata].ParameterId INNER JOIN [Portfolio].[Component] ON [Portfolio].[Component].ComponentId = [Portfolio].[CleantechMetadata].ComponentId WHERE [CleantechMetadata].SiteId="+str(site_id)+" AND Parameter.ParameterType='Processed' ORDER BY [Portfolio].[Component].ComponentType DESC,[Portfolio].[Component].ComponentName ASC", cnxn)
    df = pd.DataFrame(query, columns=['ComponentName','CSName','ComponentType','ComponentId','ParameterId','Rounding','Unit'])
    df['Date']=date
    return df  

def get_columns(site, cnxn):
    if(int(site[3])>=4):
        n = group_shell(site[3:6])
        query = pd.read_sql_query("SELECT TOP (1) * FROM ["+site[0:3]+n+"].[RawData]", cnxn)
        df = pd.DataFrame(query)
    else:
        query = pd.read_sql_query("SELECT TOP (1) * FROM ["+site+"].[RawData]", cnxn)
        df = pd.DataFrame(query)
    return df.columns.tolist()  

def  get_provider(site, cnxn):
    query = pd.read_sql_query('''SELECT * FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query)
    return df['MonitoringProvider'].values[0]

def  get_attachments(site, cnxn):
    query = pd.read_sql_query('''SELECT SiteCode,AttachmentName,AttachmentCommand FROM [Portfolio].[AttachmentMetadata] INNER JOIN Portfolio.Site ON Portfolio.Site.SiteId = [Portfolio].[AttachmentMetadata].SiteId INNER JOIN Portfolio.Attachment ON Portfolio.[Attachment].[AttachmentId] = [Portfolio].[AttachmentMetadata].[AttachmentId] WHERE [SiteCode]='''+'\''+str(site)+'\' ORDER BY [AttachmentOrder] ASC', cnxn)
    df = pd.DataFrame(query)
    return df

def get_import_reference(c_id,cnxn):
    query = pd.read_sql_query("SELECT [ReferenceMeter] FROM [Portfolio].[ImportMeter] WHERE ComponentId="+str(c_id), cnxn)
    df = pd.DataFrame(query)
    return df['ReferenceMeter'].values[0]

def get_invoice_data(date,cnxn):
    query = pd.read_sql_query("SELECT * FROM [Portfolio].[ProcessedData] WHERE Date='"+str(date)+"' AND (ParameterId = 10 OR ParameterId = 379)", cnxn)
    df = pd.DataFrame(query)
    return df

def get_status_data(site_id,component_id,cnxn):
    query = pd.read_sql_query("SELECT * FROM [Portfolio].[SiteStatus] WHERE SiteId="+str(site_id)+" AND [ComponentId] = "+str(component_id) , cnxn)
    df = pd.DataFrame(query)
    print(df)
    return df

def get_webhooks(cnxn):
    query = pd.read_sql_query("SELECT * FROM [Portfolio].[Webhook]" , cnxn)
    df = pd.DataFrame(query)
    return df

def get_country_id(site,cnxn):
    query = pd.read_sql_query("SELECT * FROM [dbo].[countries]" , cnxn)
    df = pd.DataFrame(query)
    print(df)
    return df.loc[df['code'].str.strip() == site[0:2],'id'].values[0]

def get_summary_order(cnxn):
    query = pd.read_sql_query("SELECT [SummaryOrder] FROM [Portfolio].[Meter] ORDER BY [SummaryOrder] ASC" , cnxn)
    df = pd.DataFrame(query)
    return df.tail(1).values[0][0]+1
        
def fast_upsert_final(site,df, cnxn):
    str_site=site.replace('-','') #For Shell Sites
    cols=df.columns.tolist()
    if(int(site[3])>=4):
        n = group_shell(site[3:6])
        query_1='DECLARE @timestamp_'+str_site+' datetime,'+'@componentid_'+str_site+' int,'+'@siteid_'+str_site+' int,'
        query_vals='SET @timestamp_'+str_site+' =?\n'+'SET @componentid_'+str_site+ '=?\n'+'SET @siteid_'+str_site+ '=?\n'
        query_2='UPDATE ['+site[0:3]+n+'].[RawData] SET [Timestamp]=@timestamp_'+str_site+',[ComponentId]=@componentid_'+str_site+','+'[SiteId]=@siteid_'+str_site+','
        query_3_1='INSERT INTO ['+site[0:3]+n+'].[RawData]([Timestamp],[ComponentId],[SiteId],'
        query_3_2='VALUES(@timestamp_'+str_site+',@componentid_'+str_site+','+'@siteid_'+str_site+','
        m=3
    else:
        query_1='DECLARE @timestamp_'+str_site+' datetime,'+'@componentid_'+str_site+' int,'
        query_vals='SET @timestamp_'+str_site+' =?\n'+'SET @componentid_'+str_site+ '=?\n'
        query_2='UPDATE ['+site+'].[RawData] SET [Timestamp]=@timestamp_'+str_site+',[ComponentId]=@componentid_'+str_site+','
        query_3_1='INSERT INTO ['+site+'].[RawData]([Timestamp],[ComponentId],'
        query_3_2='VALUES(@timestamp_'+str_site+',@componentid_'+str_site+','
        m=2
    for i in cols[m:]:
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i+'_'+str_site+'=?\n'
        query_1=query_1+'@'+i+'_'+str_site+' float,'
        query_2=query_2+'['+i+']=@'+i+'_'+str_site+','
        query_3_1=query_3_1+'['+i+'],'
        query_3_2=query_3_2+'@'+i+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Timestamp] = @timestamp_'+str_site+' AND [ComponentId]=@componentid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    try:
        cursor = cnxn.cursor()
        cursor.fast_executemany = True
        cursor.executemany(final_query, df.values.tolist())
        cnxn.commit()
    except Exception as e:
        cursor = cnxn.cursor()
        cursor.executemany(final_query, df.values.tolist())
        cnxn.commit()
    
def fast_upsert_rawcalculated(df, cnxn):
    str_site='Portfolio'
    cols=df.columns.tolist()
    query_1='DECLARE @timestamp_'+str_site+' datetime,'+'@componentid_'+str_site+' int,'+'@siteid_'+str_site+' int,'
    query_vals='SET @timestamp_'+str_site+' =?\n'+'SET @componentid_'+str_site+ '=?\n'+'SET @siteid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[RawCalculated] SET [Timestamp]=@timestamp_'+str_site+',[ComponentId]=@componentid_'+str_site+','+'[SiteId]=@siteid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[RawCalculated] ([Timestamp],[ComponentId],[SiteId],'
    query_3_2='VALUES(@timestamp_'+str_site+',@componentid_'+str_site+','+'@siteid_'+str_site+','
    m=3
    for i in cols[m:]:
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i+'_'+str_site+'=?\n'
        query_1=query_1+'@'+i+'_'+str_site+' float,'
        query_2=query_2+'['+i+']=@'+i+'_'+str_site+','
        query_3_1=query_3_1+'['+i+'],'
        query_3_2=query_3_2+'@'+i+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Timestamp] = @timestamp_'+str_site+' AND [ComponentId]=@componentid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    try:
        cursor = cnxn.cursor()
        cursor.fast_executemany = True
        cursor.executemany(final_query, df.values.tolist())
        cnxn.commit()
    except Exception as e:
        cursor = cnxn.cursor()
        cursor.executemany(final_query, df.values.tolist())
        cnxn.commit()

def upsert_processed(date,value,parameter_id,component_id,site_id, cnxn,bypassflag=0,masterflag=0):
    cursor = cnxn.cursor()
    site_id=int(site_id)
    if(pd.isna(value)):
        value=None
    with cursor.execute("UPDATE [Portfolio].[ProcessedData] SET [Value] = ?,[SiteId] = ?,[ComponentId] = ?,[ParameterId] = ?,[BypassFlag]=?,[MasterFlag]=?  where [Date] = ? AND [ComponentId]=? AND [ParameterId]=? AND [SiteId]=? if @@ROWCOUNT = 0 INSERT into [Portfolio].[ProcessedData]([Date], [Value], [SiteId], [ComponentId], [ParameterId],[BypassFlag],[MasterFlag]) values(?, ?, ?, ?, ?, ?, ?) ",[value, site_id, component_id, parameter_id, bypassflag, masterflag, date, component_id,parameter_id, site_id , date , value, site_id , component_id,parameter_id, bypassflag, masterflag]):
        pass
    cnxn.commit()
    
def create_table(site,parameters, cnxn):
    if(int(site[3])>=4): #For Shell Sites
        n = group_shell(site[3:6])
        query = 'CREATE TABLE ['+site[0:3]+n+'].[RawData]([Timestamp] [datetime] NOT NULL,ComponentId int FOREIGN KEY REFERENCES [Portfolio].[Component](ComponentId) NOT NULL,SiteId int FOREIGN KEY REFERENCES [Portfolio].[Site](SiteId) NOT NULL'
        print(query)
        for i in parameters:
            query=query+',['+i+'] float NULL'
        query=query+' PRIMARY KEY NONCLUSTERED ([Timestamp],[ComponentId]))\nCREATE CLUSTERED COLUMNSTORE INDEX cci_'+site[0:2]+n+' ON ['+site[0:3]+n+'].[RawData]'
        query_2 = 'CREATE SCHEMA ['+site[0:3]+n+']'
    else:
        query = 'CREATE TABLE ['+str(site)+'].[RawData]([Timestamp] [datetime] NOT NULL,ComponentId int FOREIGN KEY REFERENCES [Portfolio].[Component](ComponentId) NOT NULL'
        for i in parameters:
            query=query+',['+i+'] float NULL'
        query=query+' PRIMARY KEY NONCLUSTERED ([Timestamp],[ComponentId]))\nCREATE CLUSTERED COLUMNSTORE INDEX cci_'+str(site.replace('-',''))+' ON ['+str(site)+'].[RawData]'
        query_2 = 'CREATE SCHEMA ['+site+']'
    cursor = cnxn.cursor()
    try:
        with cursor.execute(query_2):
            print('Schema created.')
    except:
        print('Schema already created.')
    try:
        with cursor.execute(query):
            print('Table created.')
        with cursor.execute(" CREATE CLUSTERED COLUMNSTORE INDEX cci_"+site.replace('-',"")+" ON  ["+site+"].RawData WITH (DROP_EXISTING =  ON,MAXDOP = 1)"):
            print('Updated!'+site)
    except Exception as e:
        print(e)
        print('Table already created.')
    cnxn.commit()    


#--------------------------Misc-----------------------------------

def delete_site(site_id, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute( "DECLARE @sql NVARCHAR(MAX) = N'';\n;WITH x AS\n (\nSELECT DISTINCT obj =\nQUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.'\n + QUOTENAME(OBJECT_NAME(parent_object_id))\n FROM sys.foreign_keys\n)\nSELECT @sql += N'ALTER TABLE ' + obj + '  NOCHECK CONSTRAINT ALL;\n' FROM x;\nEXEC sp_executesql @sql;\n"):
        print('Disabled All Constraints!')
    with cursor.execute("DELETE  FROM [Portfolio].[SerisMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Seris Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[LocusMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Locus Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[WebdynMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Webdyn Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[SerisMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Seris Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[CleantechMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Component Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[AttachmentMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted Attachement Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[SiteStatus] WHERE SiteId="+str(site_id)):
        print('Deleted Site Status!')
    with cursor.execute("DELETE  FROM [Portfolio].[Meter] WHERE SiteId="+str(site_id)):
        print('Deleted Meters!')
    with cursor.execute("DELETE  FROM [Portfolio].[Inverter] WHERE SiteId="+str(site_id)):
        print('Deleted Inverters!')
    with cursor.execute("DELETE  FROM [Portfolio].[BotMetadata] WHERE SiteId="+str(site_id)):
        print('Deleted BotMetadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[SiteInformation] WHERE SiteId="+str(site_id)):
        print('Deleted SiteInformation!')
    with cursor.execute("DELETE  FROM [Portfolio].[IrrInfo] WHERE SiteId="+str(site_id)):
        print('Deleted IrrInfo!')
    with cursor.execute("DELETE  FROM [Portfolio].[ProcessedData] WHERE SiteId="+str(site_id)):
        print('Deleted ProcessedData!')
    with cursor.execute("DELETE  FROM [Portfolio].[Component] WHERE SiteId="+str(site_id)):
        print('Deleted Components!')
    with cursor.execute("DELETE  FROM [Portfolio].[MailRecipient] WHERE SiteId="+str(site_id)):
        print('Deleted Recipients!')
    with cursor.execute("DELETE  FROM [Portfolio].[Webhook] WHERE SiteId="+str(site_id)):
        print('Deleted Webhook!')
    with cursor.execute("DELETE  FROM [Portfolio].[Limit] WHERE SiteId="+str(site_id)):
        print('Deleted Limits!')
    with cursor.execute("DELETE  FROM [Portfolio].[Site] WHERE SiteId="+str(site_id)):
        print('Deleted Site!')
    with cursor.execute( "DECLARE @sql NVARCHAR(MAX) = N'';\n;WITH x AS\n (\nSELECT DISTINCT obj =\nQUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.'\n + QUOTENAME(OBJECT_NAME(parent_object_id))\n FROM sys.foreign_keys\n)\nSELECT @sql += N'ALTER TABLE ' + obj + '  CHECK CONSTRAINT ALL;\n' FROM x;\nEXEC sp_executesql @sql;\n"):
        print('Enabled All Constraints!')

def delete_component(site,c_id, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute( "DECLARE @sql NVARCHAR(MAX) = N'';\n;WITH x AS\n (\nSELECT DISTINCT obj =\nQUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.'\n + QUOTENAME(OBJECT_NAME(parent_object_id))\n FROM sys.foreign_keys\n)\nSELECT @sql += N'ALTER TABLE ' + obj + '  NOCHECK CONSTRAINT ALL;\n' FROM x;\nEXEC sp_executesql @sql;\n"):
        print('Disabled All Constraints!')
    with cursor.execute("DELETE  FROM [Portfolio].[SerisMetadata] WHERE ComponentId="+str(c_id)):
        print('Deleted Seris Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[LocusMetadata] WHERE ComponentId="+str(c_id)):
        print('Deleted Locus Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[WebdynMetadata] WHERE ComponentId="+str(c_id)):
        print('Deleted Webdyn Metadata!')
    with cursor.execute("DELETE  FROM [Portfolio].[CleantechMetadata] WHERE ComponentId="+str(c_id)):
        print('Deleted Components!')
    with cursor.execute("DELETE  FROM [Portfolio].[Meter] WHERE ComponentId="+str(c_id)):
        print('Deleted Meters!')
    with cursor.execute("DELETE  FROM [Portfolio].[Inverter] WHERE ComponentId="+str(c_id)):
        print('Deleted Inverters!')
    with cursor.execute("DELETE  FROM [Portfolio].[ProcessedData] WHERE ComponentId="+str(c_id)):
        print('Deleted ProcessedData!')
    with cursor.execute("DELETE  FROM [Portfolio].[Component] WHERE ComponentId="+str(c_id)):
        print('Deleted Components!')
    with cursor.execute( "DECLARE @sql NVARCHAR(MAX) = N'';\n;WITH x AS\n (\nSELECT DISTINCT obj =\nQUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.'\n + QUOTENAME(OBJECT_NAME(parent_object_id))\n FROM sys.foreign_keys\n)\nSELECT @sql += N'ALTER TABLE ' + obj + '  CHECK CONSTRAINT ALL;\n' FROM x;\nEXEC sp_executesql @sql;\n"):
        print('Enabled All Constraints!')

def unmap_parameter(c_id,p_id, cnxn):
    cursor = cnxn.cursor()
    with cursor.execute("DELETE  FROM [Portfolio].[CleantechMetadata] WHERE ComponentId="+str(c_id)+"AND ParameterId="+str(p_id)):
        print('Removed Mapping!') 
    with cursor.execute("DELETE  FROM [Portfolio].[ProcessedData] WHERE ComponentId="+str(c_id) +"AND ParameterId="+str(p_id)):
        print('Deleted Processed Data!')    

def get_raw_data(date, time, site, cnxn):
    try:
        date = date.strftime('%Y-%m-%d')
    except:
        pass
    df = pd.DataFrame()
    if(int(site[3])>=4):
        for chunk in pd.read_sql_query("SELECT [Timestamp],[ComponentId],[AC_Power] FROM ["+site[0:3]+group_shell(site[3:6])+"].[RawData] WHERE [Timestamp] BETWEEN '"+date+" 00:00:00' AND '"+date+" "+time+"'", cnxn, chunksize = 5000):
            df = df.append(chunk)
    else:
        for chunk in pd.read_sql_query("SELECT [Timestamp],[ComponentId],[AC_Power] FROM ["+site+"].[RawData] WHERE [Timestamp] BETWEEN '"+date+" 00:00:00' AND '"+date+" "+time+"'", cnxn, chunksize = 5000):
            df = df.append(chunk)
    return df  

def get_raw_data_2(date, date2, site, c_id, parameters, cnxn):
    parameters = ','.join(parameters)
    c_id = ' OR ComponentId='.join(c_id)
    try:
        date = date.strftime('%Y-%m-%d')
    except:
        pass
    df = pd.DataFrame()
    if(int(site[3])>=4):
        for chunk in pd.read_sql_query("SELECT "+parameters+" FROM ["+site[0:3]+group_shell(site[3:6])+"].[RawData] WHERE [Timestamp] >= '"+date+" 00:00:00' AND  [Timestamp] <'"+date2+"' AND (ComponentId = "+str(c_id)+') ORDER BY [Timestamp] ASC', cnxn, chunksize = 5000):
            df = df.append(chunk)
    else:
        for chunk in pd.read_sql_query("SELECT "+parameters+" FROM ["+site+"].[RawData] WHERE [Timestamp] >= '"+date+" 00:00:00' AND [Timestamp] < '"+date2+"' AND (ComponentId = "+str(c_id)+') ORDER BY [Timestamp] ASC', cnxn, chunksize = 5000):
            df = df.append(chunk)
    return df

def get_raw_power_data(date, site, cnxn):
    try:
        date = date.strftime('%Y-%m-%d')
    except:
        pass
    df = pd.DataFrame()
    for chunk in pd.read_sql_query("SELECT [Timestamp],[ComponentId],[AC_Power] FROM ["+site+"].[RawData] WHERE [Timestamp] BETWEEN '"+date+" 00:00:00' AND '"+date+" 23:59:00'", cnxn, chunksize = 5000):
        df = df.append(chunk)
    return df  

def get_processed_data_fullsite(site_id, cnxn):
    query = pd.read_sql_query('SELECT * FROM [Portfolio].[ProcessedData] WHERE [SiteId] = '+str(site_id), cnxn)
    df = pd.DataFrame(query)
    return df 

def get_processed_data_component(component_id, cnxn):
    query = pd.read_sql_query('SELECT * FROM [Portfolio].[ProcessedData] WHERE [ComponentId] = '+str(component_id), cnxn)
    df = pd.DataFrame(query)
    return df 

def get_cov_data(site_id, c_id, cnxn):
    query = pd.read_sql_query('SELECT [Date],[Value] FROM [Portfolio].[ProcessedData] WHERE [SiteId] = '+str(site_id)+' AND [ComponentId] = '+str(c_id)+' AND [ParameterId] = 12 ORDER BY Date ASC', cnxn)
    df = pd.DataFrame(query)
    return df 

def get_avl_data(site_id, cnxn):
    query = pd.read_sql_query('SELECT [Date],[Value],Portfolio.ProcessedData.[ComponentId],[ParameterId] FROM [Portfolio].[ProcessedData] INNER JOIN Portfolio.Component ON Portfolio.Component.ComponentId = Portfolio.ProcessedData.ComponentId WHERE ComponentType=\'MFM\' AND Portfolio.ProcessedData.[SiteId] = '+str(site_id)+' AND ([ParameterId] = 1 OR [ParameterId] = 13 OR [ParameterId] = 14)  ORDER BY Date ASC', cnxn)
    df = pd.DataFrame(query)
    return df 

def get_limit_data(site_id,cnxn):
    query = pd.read_sql_query('SELECT * FROM [Portfolio].[Limit]', cnxn)
    df = pd.DataFrame(query)
    return df 

def get_mail_recipients(site,digest_type,cnxn):
    query = pd.read_sql_query('SELECT * FROM [Portfolio].[MailRecipient] WHERE [DigestType] = \''+digest_type+'\' AND SiteId = (SELECT SiteId FROM Portfolio.Site WHERE SiteCode=\''+site+'\')', cnxn)
    df = pd.DataFrame(query)
    return df['Email'].str.strip().tolist()
    
def get_rpr_distance(site, cnxn):
    query = pd.read_sql_query('SELECT [SensorDistance] FROM [Portfolio].[IrrInfo] where SiteId = (SELECT SiteId FROM Portfolio.Site WHERE SiteCode=\''+site+'\')', cnxn)
    df = pd.DataFrame(query)
    return df['SensorDistance'].values[0]

def get_text_recipients(site, cnxn):
    SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Text_Recipients] ''', cnxn)
    df_recipients = pd.DataFrame(SQL_Query, columns=['Name','Number','Site','Country','Status'])
    df_temp = df_recipients.loc[df_recipients['Country'].str.strip()==site[0:2],]
    df_temp = df_temp.loc[(((df_temp['Site'].str.strip()=='All') | (df_temp['Site'].str.strip()==site)) & (df_temp['Status']==1)),'Number']
    df_temp = df_temp.unique()
    numbers= df_temp.tolist()
    return numbers

def group_shell(n):
    m = int(n[0])
    n = int(n[1:])
    if(n==0):
        return str(m)+"00"
    elif(n>0 and n<=20):
        return str(m)+"20"
    elif(n>20 and n<=40):
        return str(m)+"40"
    elif(n>40 and n<=60):
        return str(m)+"60"
    elif(n>60 and n<=80):
        return str(m)+"80"
    elif(n>80):
        return str(m+1)+"00"






    
