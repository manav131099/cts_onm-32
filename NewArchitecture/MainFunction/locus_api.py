import pandas as pd
pd.options.mode.chained_assignment = None  
import requests
import time
import datetime

def locus_authenticate(site):
    if(site[0:2]=='TH' or site[0:2]=='IN'):
        accessurl ="https://api.locusenergy.com/oauth/token"
        clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
        clientsecret = "7abff4f549770bc025df5eb371ce6857"
    else:
        accessurl ="https://api.locusenergy.com/oauth/token"
        clientid = "58a0afab59d50c37feeb9fd19ff97439"
        clientsecret = "5daae88434c3122708c3066faa49a418"
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    if(site[0:2]=='MY'):
        username = "server.request1@cleantechsolar.com"
        password = "welcome"
    elif(site[0:2]=='SG'):
        username = "Cleantech Solar Singapore"
        password = "cssingapore"
    else:
        username = "shravan1994@gmail.com"
        password = "welcome"
    post_data = {"grant_type" : "password","username" : username,"password" : password,
                 "client_id" : clientid,"client_secret" : clientsecret}										
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"] 

def get_locus_component_id(auth,site_id):
    components = {}
    components_url ="https://api.locusenergy.com/v3/sites/"+ str(int(site_id)) +"/components"
    while True:
        try:
            r = requests.get(components_url, headers={'Authorization': "Bearer "+auth})
            data=r.json()
            data=data['components']
        except:
            print("Component API Failed!")
            time.sleep(5)
            continue
        break  
    for i in data:
        if((i['nodeType']=='METER' or i['nodeType']=='TRANSFORMER') and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
             components[i['id']]=i['name']
        elif(i['nodeType']=='INVERTER' or i['nodeType']=='COMBINER' and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
            components[i['id']]=i['name']
        elif(i['nodeType']=='WEATHERSTATION' and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
            components[i['id']]=i['name']
    return components

def get_locus_parameters(auth,component_id):
    k=[]	
    print(component_id)
    componenturl2 ="https://api.locusenergy.com/v3/components/"+str(int(component_id))+"/dataavailable"	
    while True:	
        try:	
            r = requests.get(componenturl2, headers={'Authorization': "Bearer "+auth})	
            data3=r.json()	
            for i in data3['baseFields']:	
                if(i['source']!='Modeled'):
                    for j in i['aggregations']:	
                        k.append(j['shortName'])
        except:	
            print("Columns API Failed!")	
            print(data3)
            time.sleep(5)	
            continue	
        break 		
    arr=['ts']+k	
    return arr
  
def get_locus_data(auth,start_date,end_date,component_id,locus_parameter,timezone):
    start_date = start_date.strftime('%Y-%m-%dT%H:%M:%S')
    end_date = end_date.strftime('%Y-%m-%dT%H:%M:%S')
    url = "https://api.locusenergy.com/v3/components/"+str(int(component_id))+"/data?start="+start_date+"&end="+end_date+"&tz="+timezone+"&gran=5min&fields="+locus_parameter	
    while True:	
        try:	
            r = requests.get(url, headers={'Authorization': "Bearer "+auth})
            data = r.json()
            df = pd.DataFrame(data['data'])
        except Exception as e:
            try:
                print('API Failed!')
                print(data)
                if(data['statusCode']==401 or data['statusCode']==403):
                    print(pd.DataFrame(data,index=[0]))
                    return pd.DataFrame(data,index=[0])	
                time.sleep(5)	
                continue	
            except:
                continue
        break 
    return df