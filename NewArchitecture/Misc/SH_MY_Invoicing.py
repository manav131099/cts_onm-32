import sharepy
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import smtplib
import numpy as np
import os
import sys
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *

user='admin'
    
#Authentication means for SP
username='test-account@cleantechsolar.com'
password='Cleantechsolarpv@123'
base_path='https://cleantechenergycorp.sharepoint.com'
auth=sharepy.connect(base_path, username=username, password=password)

#Authentication means for Azure
server='cleantechsolar.database.windows.net'
database='Cleantech Meter Readings'
username='RohanKN'
password='R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
cnxn=pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+password)

def send_mail(info,attachment_path_list=None):
    s=smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg=MIMEMultipart()
    sender='operations@cleantechsolar.com'
    #recipients=['manav.gupta@cleantechsolar.com','sai.pranav@cleantechsolar.com']
    recipients=['Daniel.Boey@meinhardtgroup.com','Navin.Ramudaram@shell.com','Sze-Lin.Toh@shell.com']
    msg['Subject']="Shell Retail MY - Monthly Generation Data"
    if sender is not None:
        msg['From']=sender
    msg['To']=", ".join(recipients)
    msg['Cc']=", ".join(['operationsMY@cleantechsolar.com'])
    msg['Bcc']=", ".join(['sai.pranav@cleantechsolar.com','amira.meliki@cleantechsolar.com'])
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part=MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())

query=pd.read_sql_query("SELECT  [SiteId], [SiteCode] FROM [Portfolio].[Site] WHERE [SiteCode] LIKE 'MY-4%' or [SiteCode] LIKE 'MY-5%' or [SiteCode] LIKE 'MY-6%'", cnxn)
dfread1=pd.DataFrame(query, columns=['SiteId', 'SiteCode'])
query=pd.read_sql_query("SELECT  [SiteId], [SiteName] FROM [Portfolio].[SiteInformation]", cnxn)
dfread2=pd.DataFrame(query, columns=['SiteId', 'SiteName'])
dfread=pd.merge(dfread1, dfread2, how='left')

#Declaring dates for summary
today=datetime.datetime.today()
start=datetime.datetime((today + datetime.timedelta(days=-28)).year, (today + datetime.timedelta(days=-28)).month, 1)
end=start + relativedelta(months=1) + datetime.timedelta(days=-1)
month_name=start.strftime("%B")
print("Start Date: ", start.date())
print("End Date  : ", end.date())

#Fetching and cleaning data from MY SHELL RETAIL
filedownload1='https://cleantechenergycorp.sharepoint.com/sites/CSDC-OMMY/Shared%20Documents/MY-400%20Shell-MY%20Retail/Shell MY-Retail Details.xlsx'
fileread1='/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell MY-Retail Details_Raw.xlsx'
response1=auth.getfile(filedownload1, filename=fileread1)
dfread3=pd.read_excel(fileread1,engine='openpyxl')
dfread3=dfread3.dropna(subset=['COD'])
dfread3=dfread3[['System Code O&M', 'System Size [kWp]', 'Project Code']]
dfread=pd.merge(dfread, dfread3, how='right', left_on='SiteCode', right_on='System Code O&M')
dfread=dfread.drop(['System Code O&M'], axis=1)


#Fetching and cleaning data from MY SHELL Program Tracker
filedownload2='https://cleantechenergycorp.sharepoint.com/:x:/r/sites/Malaysia/Shared%20Documents/Shell/01%20-%20Program%20Tracker/200420-Program%20tracker-Rev.00.xlsx'
fileread2='/home/' + user + '/Dropbox/Customer/Shell_Invoicing/200420-Program tracker-Rev.00.xlsx'
response2=auth.getfile(filedownload2, filename=fileread2)
dfread4=pd.read_excel(fileread2,engine='openpyxl')
dfread4.columns=dfread4[16:17].values[0]
dfread4.drop(dfread4.index[0:17], inplace=True)
dfread4.reset_index(drop=True, inplace=True)
dfread4=dfread4[['Shell Station ID', 'Cleantech reference']]

dfread=pd.merge(dfread, dfread4, how='left', left_on='Project Code', right_on='Cleantech reference')
dfread=dfread[['SiteId', 'SiteCode', 'SiteName', 'System Size [kWp]', 'Shell Station ID']]

#Creating data frame
Site_Ids=list(dfread['Shell Station ID'])
Site_Ids.insert(0, 'Shell Reference')

Site_Codes=list(dfread['SiteCode'])
Site_Codes.insert(0, 'Cleantech Reference')

Site_Names=list(dfread['SiteName'])
Site_Names.insert(0, 'Name')

Site_Capacities=list(dfread['System Size [kWp]'])
Site_Capacities.insert(0, 'Capacity (kWp)')

header=['Date', 'Daily Generation (kWh)']

dfwrite=pd.DataFrame([Site_Ids])
dfwrite=dfwrite.append([Site_Codes])
dfwrite=dfwrite.append([Site_Names])
dfwrite=dfwrite.append([Site_Capacities])
dfwrite=dfwrite.append([header])

#Creating Summary
while (start <= end):
    curr_date=start.strftime('%Y-%m-%d')
    print("Processing: " + curr_date)
    curr_read=[curr_date]
    
    query=pd.read_sql_query("SELECT [Value], Portfolio.Site.SiteCode FROM [Portfolio].[ProcessedData] INNER JOIN Portfolio.Site on Portfolio.ProcessedData.SiteId = Portfolio.Site.SiteId where (SiteCode like 'MY-4%' or SiteCode like 'MY-5%' or SiteCode like 'MY-6%') and parameterid=5 and date='"+curr_date+"' ORDER BY SiteCode", cnxn)
    temp=pd.DataFrame(query, columns=['Value', 'SiteCode'])
    temp=temp.round({'Value': 2})
    temp2=pd.merge(dfread, temp, how='left')
    curr_read+=temp2.Value.tolist()
    dfwrite=dfwrite.append([curr_read])
    start += datetime.timedelta(days=1)

dfwrite.to_csv('/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv', header=False, index=False)
send_mail('Please find the Generation Report for the month of '+month_name+' attached.',['/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv'])