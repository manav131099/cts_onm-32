import pyodbc
import pandas as pd
import datetime
import math
import pytz
from database_operations import *
import sys
import os

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

site = sys.argv[1]
day = sys.argv[2]

public_holiday_list = ["2018-04-06",    #Chakri Day
  "2018-04-12",    #Songkran 4
  "2018-04-13",    #Songkran
  "2018-04-14",    #Songkran 2
  "2018-04-15",    #Songkran 3
  "2018-04-16",    #Songkran obs
  "2018-05-01",    #Labour day
  "2018-05-14",    #Royal ploughing ceremony
  "2018-05-29",    #Visakha Bucha
  "2018-07-27",   #Asaalha Bucha
  "2018-08-12",   #Queen's bday
  "2018-08-13",   #Queen's bday obs
  "2018-10-13",   #Anniversary of Death of King Bhumibol
  "2018-10-15",   #Anniversary of Death of King Bhumibol obs
  "2018-10-23",   #Chulalongkorn Day
  "2018-12-05",   #Father's day
  "2018-12-10",   #Constitution Day
  "2018-12-31",
  "2019-01-01",
  "2019-02-19",
  "2019-04-15",
  "2019-05-01",
  "2019-07-16",
  "2019-07-17",
  "2019-08-12",
  "2019-10-23",
  "2019-12-05",
  "2019-12-10",
  "2019-12-31",
  "2020-01-01",
  "2020-04-06",
  "2020-05-01",
  "2020-05-04",
  "2020-05-06",
  "2020-06-03",
  "2020-07-06",
  "2020-07-28",
  "2020-08-12",
  "2020-10-13",
  "2020-10-23",
  "2020-12-10",
  "2020-12-31",
  "2021-01-01",
  "2021-02-12",
  "2021-02-26",
  "2021-04-06",
  "2021-04-12",
  "2021-04-13",
  "2021-04-14",
  "2021-04-15",
  "2021-05-04",
  "2021-05-26",
  "2021-06-03",
  "2021-07-27",
  "2021-07-28",
  "2021-08-12",
  "2021-09-24",
  "2021-10-13",
  "2021-12-10",
  "2021-12-31"]
public_holiday_list = pd.to_datetime(public_holiday_list)
tz = pytz.timezone('Asia/Bangkok')
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

date = (datetime.datetime.now(tz)+datetime.timedelta(days=-0)).date()
date1 = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).date()
print(date)
#date=datetime.datetime.strptime(day, '%Y-%m-%d').date()
first_day = date.replace(day=1) 
component_df = get_cleantech_components(site,cnxn)
mfm_comp_codes = component_df[component_df['ComponentType']=='MFM']['ComponentId'].values
Invoice_dff = pd.DataFrame()
c = 1
for mfm in mfm_comp_codes:
    #df = pd.read_sql_query("Select * from [Cleantech Meter Readings].["+site+"].RawData where (ComponentId="+str(mfm)+"and Timestamp>"+str(first_day)+";",cnxn)
    if(site=='TH-401' or site=='TH-402' or site=='TH-403' or site=='TH-404'):
        df = pd.read_sql_query("SELECT Timestamp, AC_Power, Export_Meter_Reading FROM [Cleantech Meter Readings].[TH-420].RawData WHERE (ComponentId = "+str(mfm)+" AND Timestamp > "+str(first_day)+");",cnxn)
    else:
         df = pd.read_sql_query("SELECT Timestamp, AC_Power, Export_Meter_Reading FROM [Cleantech Meter Readings].["+site+"].RawData WHERE (ComponentId = "+str(mfm)+" AND Timestamp > "+str(first_day)+");",cnxn)
    df['date'] = df['Timestamp']
    Invoice_df = pd.DataFrame()
    for i in range(len(df)):
        df['date'].iloc[i] = df['date'].iloc[i].strftime("%Y-%m-%d")
    for day in range(1,date.day):
        temp_date = datetime.datetime.strptime(str(date.year) +'-'+ str(date.month)+'-'+str(day) + " 00:00:00","%Y-%m-%d %H:%M:%S")
        try:
            temp_df = df.groupby('date',as_index=False).get_group(temp_date.strftime("%Y-%m-%d"))
            temp_df = temp_df.sort_values(by='Timestamp')
            print(temp_df)
            DA = len(temp_df)*100/288
            timemin = temp_date.strftime("%Y-%m-%d") + " 05:59:00"
            timemax = temp_date.strftime("%Y-%m-%d") + " 20:00:00"
            time_ll = temp_date.strftime("%Y-%m-%d") + " 09:00:00"
            time_hl = temp_date.strftime("%Y-%m-%d") + " 22:00:00"
            ddf = temp_df[temp_df['Timestamp'] >= timemin]
            ddf = ddf[ddf['Timestamp'] < timemax]
            DA_2 = len(ddf)*100/168
            total_eac = temp_df[temp_df['AC_Power'] > 0]['AC_Power'].sum()/10000
            pac_temp = temp_df[temp_df['Timestamp']>time_ll]
            pac_temp = pac_temp[pac_temp['Timestamp']<time_hl]
            current_date = temp_date.strftime("%Y-%m-%d")
            d = temp_date.strftime("%w")
            weekday = temp_date.strftime("%a")
            if (current_date in public_holiday_list or weekday == 'Sat' or weekday == 'Sun' ):
                print("Printing weekday")
                print(weekday)
                peak_eac = 0
                nonpeak_eac = total_eac
                holiday = 'Y'
            else:
                peak_eac = pac_temp[pac_temp['AC_Power'] > 0]['AC_Power'].sum()/10000
                nonpeak_eac = total_eac - peak_eac
                holiday = 'N'
            d = temp_date.strftime("%w")
            weekday = temp_date.strftime("%a")
            if (d == 5 or d == 6):
                peak_eac = 0
                nonpeak_eac = total_eac
            temp_df = temp_df.sort_values(by='Timestamp')
            pk_eac_per = (peak_eac/total_eac)*100
            offpk_eac_per = (nonpeak_eac/total_eac)*100
            total_eac_m2 =  temp_df.Export_Meter_Reading.values[-1] - temp_df.Export_Meter_Reading.values[0]
            invoice_row = {}
            invoice_row['Date'] = current_date
            invoice_row['Day'] = weekday
            invoice_row['Holiday [Y/N]'] = holiday
            invoice_row['DA (%)'] = DA
            invoice_row['DA_6AM_TO_8PM (%)'] = DA_2
            invoice_row['Eac delivered (kWh)'] = round(temp_df.Export_Meter_Reading.values[-1]/1000,2)
            invoice_row['Eac2 (kWh)'] = round(total_eac_m2/1000,2)
            invoice_row['Peak_Eac (kWh)'] = round(peak_eac,2)
            invoice_row['Offpeak_Eac (kWh)'] = round(nonpeak_eac,2)
            invoice_row['Peak_Eac (%)'] = round(pk_eac_per,2)
            invoice_row['Offpeak_Eac (%)'] = round(offpk_eac_per,2)
            invoice_row['Eac1 (kWh)'] = total_eac
            invoice_row['Time'] = temp_df['Timestamp'].to_list()[-1].strftime("%Y-%m-%d %H:%M:%S")
            invoice_row = pd.DataFrame(invoice_row,index=[0])
            Invoice_df = pd.concat([Invoice_df,invoice_row],axis=0)
        except:
            print(temp_date)
            pass
    if(c==1):
        Invoice_dff = Invoice_df
        c = c + 1
    else:
        Invoice_dff = Invoice_dff.merge(Invoice_df[['Date','DA (%)','DA_6AM_TO_8PM (%)','Eac delivered (kWh)','Eac2 (kWh)','Peak_Eac (kWh)','Offpeak_Eac (kWh)','Peak_Eac (%)','Offpeak_Eac (%)','Eac1 (kWh)','Time']], how='left',on='Date',suffixes=('','.MFM'+str(c)))
        c = c + 1

chkdir("/home/admin/TH_INVOICE/"+site+"/")
Invoice_dff.to_csv("/home/admin/TH_INVOICE/"+site+"/["+site+"] TH Invoicing summary "+str(date1.strftime("%Y-%m-%d"))+".txt",sep='\t',index=None)


    






