from datetime import date
import pytz
import time
from database_operations import *
import os
import numpy as np

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def get_all_data(table,cnxn):
    query = pd.read_sql_query('SELECT  * FROM [Portfolio].['+table+']', cnxn)
    df = pd.DataFrame(query)
    return df

def get_all_processed_data(site_id,cnxn):
    query = pd.read_sql_query('SELECT  * FROM [Portfolio].[ProcessedData] WHERE SiteId='+str(site_id), cnxn)
    df = pd.DataFrame(query)
    return df

tz = pytz.timezone('Asia/Kolkata')
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
tables = ['Component','ImportMeter','IrrInfo','Inverter','Meter','Limit','Site','CleantechMetadata','LocusMetadata','SiteInformation','Parameter','Attachment','AttachmentMetadata']
date_now = datetime.datetime.strftime(datetime.datetime.now(tz),"%Y-%m-%d")

for t in tables:
    chkdir('/home/admin/Backup/'+t)
    df = get_all_data(t,cnxn)
    print('/home/admin/Backup/'+t+'/'+t+'_Backup_'+date_now+'.txt')
    df.to_csv('/home/admin/Backup/'+t+'/'+t+'_Backup_'+date_now+'.txt',sep='\t',index=False)

if datetime.datetime.now().day == 1:
    site_df = get_sites('All',cnxn)
    for index,row in site_df.iterrows():
        df = get_all_processed_data(row['SiteId'],cnxn)
        chkdir('/home/admin/Backup/ProcessedData/'+date_now[0:7])
        df.to_csv('/home/admin/Backup/ProcessedData/'+date_now[0:7]+'/'+row['SiteCode']+'_Backup.txt',sep='\t',index=False)                                                         
cnxn.close()