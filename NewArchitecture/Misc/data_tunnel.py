import pytz
import time
from database_operations import *
import os
import numpy as np
pd.set_option('mode.chained_assignment',None)

def get_source_id(site):
    query = pd.read_sql_query('''SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['Station_Id'])
    return df['Station_Id'].values[0]
    
def get_source_mfm(site):
    query = pd.read_sql_query("SELECT [Meter_id] FROM [dbo].[Meters] WHERE [Station_Id]=(SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] ="+"'"+site+"')", cnxn)
    df = pd.DataFrame(query, columns=['Meter_id'])
    return df['Meter_id'].values

def get_dest_id(site):
    query = pd.read_sql_query('''SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['SiteId'])
    return df['SiteId'].values[0]

def get_dest_mfm(site):
    query = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode ="+"'"+site+"')" +" AND ComponentType='MFM'", cnxn)
    df = pd.DataFrame(query, columns=['ComponentId'])
    return df['ComponentId'].values  

#Upsert Function
def upsert(Date, Value, ParameterId, ComponentId, SiteId, BypassFlag):
  cursor = cnxn.cursor()
  with cursor.execute("UPDATE [Portfolio].[ProcessedData] SET [Value]=?, [ParameterId]=?, [ComponentId]=?, [SiteId]=?, [BypassFlag]=? where [Date]=? AND [ComponentId]=? AND [ParameterId]=? if @@ROWCOUNT = 0 INSERT into [Portfolio].[ProcessedData]([Date], [Value], [ParameterId], [ComponentId], [SiteId], [BypassFlag]) values(?, ?, ?, ?, ?, ?)", [Value, ParameterId, ComponentId, SiteId, BypassFlag, Date, ComponentId, ParameterId, Date, Value, ParameterId, ComponentId, SiteId, BypassFlag]):
    pass
  cnxn.commit()

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

def calc_ga():
    parameter_mapping = {'GA':13, 'PA':14}
    final = pd.DataFrame()
    for site in sites_mapped:
        temp = pd.DataFrame()
        for meter in sites_mapped[site][1]:
            date_today=(datetime.datetime.now()).date()
            start_date=date_today
            date = str(start_date)
            mfm_file="/home/admin/Dropbox/Gen 1 Data/["+site+"]/"+date[0:4]+'/'+date[0:7]+'/'+meter+'/['+site+']-MFM'+meter.split('_')[1]+'-'+date+'.txt'
            if site=='IN-015L':
                wms_file='/home/admin/Dropbox/SerisData/1min/[712]/' + date[:4] + '/' + date[:7] + '/[712] ' + date + '.txt'
            elif site=='IN-032W':
                wms_file='/home/admin/Dropbox/Cleantechsolar/1min/[711]/' + date[:4] + '/' + date[:7] + '/[711] ' + date + '.txt'
            else:
                wms_file='/home/admin/Dropbox/Gen 1 Data/['+sites_mapped[site][0]+']/'+date[0:4]+'/'+date[0:7]+'/WMS_1/['+sites_mapped[site][0]+']-WMS1-'+date+'.txt'
            if(os.path.exists(mfm_file)):
                mfm_df=pd.read_csv(mfm_file,sep='\t')
                mfm_df[sites_mapped[site][3][0]]=pd.to_datetime(mfm_df[sites_mapped[site][3][0]])
                if site=='IN-015L':
                    mfm_df[sites_mapped[site][3][2]]=mfm_df[sites_mapped[site][3][2]]*-1
                mask = (mfm_df[sites_mapped[site][3][0]].dt.hour > 6) & (mfm_df[sites_mapped[site][3][0]].dt.hour < 18)
                mfm_df_sh=mfm_df[mask]
                freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                if(len(freq_timestamps)>0 and len(freq_timestamps_sh)>0):
                    if(os.path.exists(wms_file)):
                        wms_df=pd.read_csv(wms_file,sep='\t')
                        col = sites_mapped[site][4][1]
                        if(len(wms_df)>0 and col in wms_df.columns.tolist()):
                            irr_df=wms_df[[sites_mapped[site][4][0],col]]
                            irr_df.columns = [sites_mapped[site][3][0], col]
                            irr_df[sites_mapped[site][3][0]]=pd.to_datetime(irr_df[sites_mapped[site][3][0]])
                            if site=='IN-015L' or 'IN-032W':
                                irr_df=irr_df.resample('5Min', on=sites_mapped[site][3][0]).mean()
                                irr_df.reset_index(level=0, inplace=True)
                            irr_df=irr_df[irr_df[col].notnull()]
                            mfm_df=mfm_df[sites_mapped[site][3]]
                            irr_timestamps=irr_df.loc[irr_df[col]>20,sites_mapped[site][3][0]]
                            freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                            power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]                        
                            ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                            pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                            if((len(irr_timestamps)>0) and (len(ga_common)>0)):
                                GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                                PA=(float(len(pa_common))/float(len(ga_common)))*100
                                temp = temp.append([[date, site, meter, GA, PA]], ignore_index = True)
                            else:
                                mfm_df=mfm_df[mask]
                                freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                                power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                                pa_common=list(set(freq_timestamps) & set(power_timestamps))
                                GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                                PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                                temp = temp.append([[date, site, meter, GA, PA]], ignore_index = True)
                    else:
                        mfm_df=mfm_df[mask]
                        freq_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][1]]>40,sites_mapped[site][3][0]]
                        power_timestamps=mfm_df.loc[mfm_df[sites_mapped[site][3][2]]>2,sites_mapped[site][3][0]]
                        pa_common=list(set(freq_timestamps) & set(power_timestamps))
                        GA=(float(len(freq_timestamps))/float(len(mfm_df[sites_mapped[site][3][0]])))*100
                        PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                        temp = temp.append([[date, site, meter, GA, PA]], ignore_index = True)
                else:
                    temp = temp.append([[date, site, meter, 'NULL', 'NULL']], ignore_index = True)
            else:
                temp = temp.append([[date, site, meter, 'NULL', 'NULL']], ignore_index = True)

     
        temp.columns = ['Date', 'Site', 'Meter', 'GA', 'PA']   
        export = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
        destsiteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode] = '" + str(site[:-1]) + "'", cnxn)['SiteId'].values[0]
            
        for i in range(len(temp)):
            for j in parameter_mapping:
                if site=='IN-013W':
                    if temp['Meter'][i] == 'MFM_1_Meter - MLCP 1':
                        compid=3564
                    elif temp['Meter'][i] == 'MFM_2_Meter - MLCP 2':
                        compid=3565
                    elif temp['Meter'][i] == 'MFM_3_Meter- SDB 1':
                        compid=3566
                    elif temp['Meter'][i] == 'MFM_4_Meter- SDB 2':
                        compid=3567
                    elif temp['Meter'][i] == 'MFM_5_Meter- SDB 3':
                        compid=3568
                elif site=='IN-015L':
                    compid=3527
                elif site=='IN-017A':
                    compid=3569
                elif site=='IN-032W':
                    compid=3572
                elif site=='IN-040R':
                    compid=3573
                export = export.append({'Date'        : temp['Date'][i],
                                        'Value'       : temp[j][i],
                                        'ParameterId' : parameter_mapping[j],
                                        'ComponentId' : int(compid),
                                        'SiteId'      : int(destsiteid),
                                        'BypassFlag'  : 0}, ignore_index = True)
        final = final.append(export)
        final = final.replace({"NULL": None})
        final = final.replace({np.nan: None})
    return final

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
seris_site_df = get_sites('Seris',cnxn)
ebx_site_df = get_sites('Ebx',cnxn)
webdyn_site_df = get_sites('Webdyn',cnxn)
avisolar_site_df = get_sites('Avisolar',cnxn)
resync_site_df = get_sites('Resync',cnxn)
locus_site_df = get_sites('Locus',cnxn)
none_site_df = get_sites('All',cnxn)
none_site_df = none_site_df[none_site_df['SiteCode']=='PH-002']
locus_sites = ['IN-015','IN-065','KH-009']
locus_site_df = locus_site_df[locus_site_df['SiteCode'].isin(locus_sites)]
site_df = ebx_site_df.append(seris_site_df).append(webdyn_site_df).append(avisolar_site_df).append(resync_site_df).append(locus_site_df).append(none_site_df)
print(site_df)

##Historical Start Dates can be changed from here
sites_mapped = {'IN-013W':['IN-013W', ['MFM_1_Meter - MLCP 1'], '2020-10-01', ['Date','Frequency','Total Power AC'], ['Date','Irradiance_Avg']],
                'IN-015L':['712', ['MFM_1_HT Meter'], '2021-02-01', ['ts','Hz_avg','W_avg'], ['Tm','AvgGsi00']],
                'IN-017A':['IN-009W', ['MFM_1'], '2020-09-01', ['Date','Frequency (Hz)','AC Power (kW)'], ['Date','Irradiation_GHI']],
                'IN-032W':['711', ['MFM_1'], '2020-09-01', ['Date','Frequency','Total Power AC'], ['Tm','AvgGsi00-02']],
                'IN-040R':['IN-040R', ['MFM_1'], '2021-07-19', ['time','ac-freq','ac-power'], ['time','irradiance']]}

""" for index,row in site_df.iterrows():
    site=row['SiteCode']
    sourcesiteid = get_source_id(site)
    sourcecomponentid = get_source_mfm(site)
    destinationsiteid = get_dest_id(site)
    destinationcomponentid = get_dest_mfm(site)
    print(site)
    #Eac, Yld, LastR Upsert
    sourcesiteid = get_source_id(site)
    sourcecomponentid = get_source_mfm(site)
    destinationsiteid = get_dest_id(site)
    destinationcomponentid = get_dest_mfm(site)
    source_meters = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id]=" + str(sourcesiteid), cnxn)
    dest_meters = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId]=" + str(destinationsiteid), cnxn)
    source_dest_merge = pd.merge(source_meters, dest_meters, how='left', left_on=['Reference'], right_on=['MeterReference'])
    meterid_mapping = dict(zip(source_dest_merge.Meter_Id, source_dest_merge.ComponentId))
    print("Source_Dest_MeterID_Mapping:", meterid_mapping)
    source_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid), cnxn)
    source_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'BypassFlag']
    source_data['Date'] = source_data['Date'].dt.date
    parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7}

    final = pd.DataFrame()
    for i in meterid_mapping:
        df_temp = source_data.copy()
        for j in parameter_mapping:
            final2 = df_temp[df_temp['Meter_Id'] == i]
            final2 = final2[['Date','Meter_Id',j,'BypassFlag']]
            final2['Meter_Id'] = meterid_mapping[i]
            final2['ParameterId'] = parameter_mapping[j]
            final2['SiteId'] = destinationsiteid
            final2 = final2[['Date',j,'ParameterId','Meter_Id','SiteId', 'BypassFlag']]
            final2.columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']
            final = final.append(final2)
            fast_upsert_processed(site,final2) 
        print(final['ParameterId'].unique())
    print(final)
    #fast_upsert_processed(site,final) 
    print('Done')

    
    #PR Upsert
    full_site_meter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId=69 and SiteId=" + str(destinationsiteid), cnxn)
    dest_ComponentId = full_site_meter['ComponentId'].values[0]
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid), cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    dates = source_data['Date'].unique()
    for date in dates:
        temp = source_data[source_data['Date']==date]
        temp['Weight'] = temp['Capacity']*temp['PR']
        temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
        final_df = final_df.append({'Date' : date,'Value' : temp_full_pr, 'ParameterId' : 69,'ComponentId' : int(dest_ComponentId),'SiteId' : int(destinationsiteid),'BypassFlag' : 0},ignore_index = True)
    print(final_df)
    fast_upsert_processed(site, final_df)
    cursor = cnxn.cursor()
    cursor.execute("DELETE FROM [Portfolio].[ProcessedData] WHERE SiteId=" + str(destinationsiteid) + " and ParameterId=69 AND ComponentId!=" + str(dest_ComponentId))
    cnxn.commit()

    #GHI Upsert
    wms_id = get_main_irr_component(destinationsiteid,cnxn)
    dest_ComponentId = wms_id
    print(dest_ComponentId)
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.GHI FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid), cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    final_df = source_data.drop_duplicates(subset=['Date'])
    final_df['ParameterId'] = 2
    final_df['SiteId'] = destinationsiteid
    final_df['BypassFlag'] = 0
    final_df['ComponentId'] = wms_id
    final_df = final_df[['Date','GHI','ParameterId','ComponentId','SiteId', 'BypassFlag']]
    final_df.columns = ['Date', 'Value', 'ParameterId','ComponentId', 'SiteId', 'BypassFlag']
    print(final_df)
    fast_upsert_processed(site, final_df)
    print('Done')


cnxn.close() """



while(1):
    date_last_14 = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    print(date_last_14)
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    for index,row in site_df.iterrows():
        try:
            site=row['SiteCode']
            print(site)
            if(site[3]=='7'):
                continue
            #EAC,LastR Upsert
            sourcesiteid = get_source_id(site)
            sourcecomponentid = get_source_mfm(site)
            destinationsiteid = get_dest_id(site)
            destinationcomponentid = get_dest_mfm(site)
            source_meters = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id]=" + str(sourcesiteid), cnxn)
            dest_meters = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId]=" + str(destinationsiteid), cnxn)
            dest_meters_2 = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[ImportMeter] where [SiteId]=" + str(destinationsiteid), cnxn)
            dest_meters = dest_meters.append(dest_meters_2)
            source_dest_merge = pd.merge(source_meters, dest_meters, how='left', left_on=['Reference'], right_on=['MeterReference'])
            meterid_mapping = dict(zip(source_dest_merge.Meter_Id, source_dest_merge.ComponentId))
            print("Source_Dest_MeterID_Mapping:", meterid_mapping)
            source_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
            source_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'BypassFlag']
            print(source_data)
            if(source_data.empty):
                continue
            try:
                source_data['Date'] = source_data['Date'].dt.date
                parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7}
                final = pd.DataFrame()
                for i in meterid_mapping:
                    df_temp = source_data.copy()
                    for j in parameter_mapping:
                        final2 = df_temp[df_temp['Meter_Id'] == i]
                        final2 = final2[['Date','Meter_Id',j,'BypassFlag']]
                        final2['Meter_Id'] = meterid_mapping[i]
                        final2['ParameterId'] = parameter_mapping[j]
                        final2['SiteId'] = destinationsiteid
                        final2 = final2[['Date',j,'ParameterId','Meter_Id','SiteId', 'BypassFlag']]
                        final2.columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']
                        final = final.append(final2)
                fast_upsert_processed(site,final) 
            except:
                print('EAC Failed!')
            try:
                if(site == 'IN-069' or site == 'IN-099' or site == 'IN-301' or site == 'IN-302'):
                    p_id = 307
                else:
                    p_id = 69
                #PR Upsert
                full_site_meter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId="+str(p_id)+" and SiteId=" + str(destinationsiteid), cnxn)
                dest_ComponentId = full_site_meter['ComponentId'].values[0]
                final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
                source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
                source_data['Date'] = source_data['Date'].dt.date
                dates = source_data['Date'].unique()
                for date in dates:
                    temp = source_data[source_data['Date']==date]
                    temp['Weight'] = temp['Capacity']*temp['PR']
                    temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
                    final_df = final_df.append({'Date' : date,'Value' : temp_full_pr, 'ParameterId' : p_id,'ComponentId' : int(dest_ComponentId),'SiteId' : int(destinationsiteid),'BypassFlag' : 0},ignore_index = True)
                print(final_df)
                final_df = final_df[final_df['Value']>=0]
                fast_upsert_processed(site, final_df)
            except Exception as e:
                print(e)
                print('PR Failed')

            try:
                #GHI Upsert
                wms_id = get_main_irr_component(destinationsiteid,cnxn)
                irr_center = get_irr_center(destinationsiteid, cnxn)
                if(get_site_id(irr_center,cnxn) == destinationsiteid):
                    dest_ComponentId = wms_id
                    print(dest_ComponentId)
                    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
                    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.GHI FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
                    source_data['Date'] = source_data['Date'].dt.date
                    final_df = source_data.drop_duplicates(subset=['Date'])
                    final_df['ParameterId'] = 2
                    final_df['SiteId'] = destinationsiteid
                    final_df['BypassFlag'] = 0
                    final_df['ComponentId'] = wms_id
                    final_df = final_df[['Date','GHI','ParameterId','ComponentId','SiteId', 'BypassFlag']]
                    final_df.columns = ['Date', 'Value', 'ParameterId','ComponentId', 'SiteId', 'BypassFlag']
                    final_df = final_df[final_df['Value']>=0]
                    fast_upsert_processed(site, final_df)
                    print('Done')
            except:
                print('GHI Failed')
        except:
            print('Failed')
    try:
        export = calc_ga()
        print(export)
        if len(export)>0:
            fast_upsert_processed(site, export)
    except Exception as e:
        print(e)
        print("GA Failed")
    cnxn.close()
    print('Sleeping!')
    time.sleep(3600)


