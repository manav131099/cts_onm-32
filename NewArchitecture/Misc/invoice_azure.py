import pytz
import os
import numpy as np
import sys
from database_operations import *

tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/HQDigest/Invoice/'
path_lt='/home/admin/Dropbox/Lifetime/Gen-1/'
invoicing_sites = ['SG-401','SG-402','SG-403','SG-404','SG-405','MY-528''MY-516','MY-531','MY-525','MY-508','MY-460','MY-499','MY-542','MY-517','MY-521','MY-519','MY-532','MY-529','MY-501','MY-535','MY-524','MY-527','MY-477','MY-487','MY-480','MY-476','MY-449','MY-506','MY-008','MY-015','MY-502','MY-503','MY-511','MY-514','MY-500','MY-471','MY-446','MY-497','MY-456','MY-515','MY-426','IN-076', 'IN-077', 'IN-084', 'IN-094', 'IN-092', 'IN-096', 'IN-098', 'IN-090', 'IN-093', 'IN-100', 'IN-101', 'IN-095','MY-014', 'MY-489', 'MY-451', 'MY-504', 'MY-458', 'MY-478', 'MY-490', 'MY-485', 'MY-465', 'MY-461', 'MY-454', 'MY-473', 'MY-484', 'MY-496', 'MY-494', 'MY-453', 'MY-492', 'MY-493', 'MY-481', 'MY-488', 'MY-486', 'MY-457', 'MY-491', 'MY-483', 'MY-482', 'MY-467', 'MY-495', 'MY-479', 'MY-428', 'MY-468', 'MY-463', 'MY-470', 'MY-464', 'MY-472', 'MY-462', 'MY-498','MY-427', 'MY-434', 'MY-437', 'MY-440', 'MY-441', 'MY-444', 'MY-445', 'MY-450', 'MY-455', 'MY-443', 'MY-423', 'MY-415', 'MY-432', 'MY-431', 'MY-416', 'MY-436', 'MY-452', 'MY-447', 'MY-429', 'MY-438', 'MY-430', 'MY-422','MY-439','MY-424','MY-417','MY-418','MY-419','MY-005','MY-408','MY-414','MY-420','MY-421','MY-410','MY-413','MY-433','MY-411','MY-010','MY-011','MY-012','MY-013','MY-412','MY-409','MY-425','MY-435','MY-430','MY-448','KH-001', 'KH-002', 'KH-003', 'KH-004', 'KH-005', 'KH-006', 'KH-007', 'KH-008', 'KH-009', 'IN-004', 'IN-005', 'IN-006', 'IN-007', 'IN-009', 'IN-011', 'IN-012', 'IN-013', 'IN-013', 'IN-014', 'IN-016', 'IN-017', 'IN-018', 'IN-019', 'IN-020', 'IN-021', 'IN-026', 'IN-027', 'IN-029', 'IN-031', 'IN-032', 'IN-033', 'IN-034', 'IN-037', 'IN-038', 'IN-039', 'IN-040', 'IN-041', 'IN-042', 'IN-043', 'IN-044', 'IN-045', 'IN-046', 'IN-047', 'IN-048', 'IN-049', 'IN-052', 'IN-054', 'IN-055', 'IN-056', 'IN-057', 'IN-058', 'IN-059', 'IN-060', 'IN-061', 'IN-062', 'IN-063', 'IN-064', 'IN-066', 'IN-067', 'IN-068', 'IN-071', 'IN-072', 'IN-073', 'IN-074','IN-076', 'IN-075', 'IN-079', 'IN-080', 'IN-081', 'IN-082', 'IN-083', 'IN-085', 'IN-086', 'IN-087', 'IN-088','IN-090','IN-091','IN-092','IN-093','IN-094','IN-026','IN-046','IN-044', 'MY-001', 'MY-002', 'MY-003', 'MY-004', 'MY-006', 'MY-007', 'MY-009', 'MY-010', 'MY-011', 'MY-401', 'MY-402', 'MY-403', 'MY-404', 'MY-405', 'MY-406', 'MY-407', 'MY-409', 'MY-412', 'SG-001', 'SG-002', 'VN-001', 'VN-002', 'VN-003']

for i in range(1,4):
    date=(datetime.datetime.now(tz)+datetime.timedelta(days=-i)).strftime('%Y-%m-%d')
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    df_date = get_invoice_data(date,cnxn)
    df_sites = get_sites('All',cnxn)
    df_sites = df_sites[(df_sites['MonitoringProvider']=='Webdyn')|(df_sites['MonitoringProvider']=='Ebx')|(df_sites['MonitoringProvider']=='Seris')]
    df_meters = get_mfm_capacities('All',cnxn)
    cnxn.close()
    df_merge=pd.merge(df_meters,df_date,on='ComponentId',how="left")
    df_merge = df_merge[['MeterReference','Value','ParameterId']]
    df_invoicing = df_merge.pivot( index = 'MeterReference',columns='ParameterId', values=['Value'])
    df_invoicing.columns = df_invoicing.columns.get_level_values(1)
    df_invoicing.index.name = 'MeterReference'
    df_invoicing.reset_index(level=0, inplace=True)
    df_invoicing.columns = ['MeterReference','NA','LastRead','LastTime']
    df_invoicing = df_invoicing[['MeterReference','LastTime','LastRead']]
    df_invoicing.insert(0, "Operation", df_invoicing['MeterReference'].str[:-1] , True)
    df_invoicing['ReadingInv'] = df_invoicing['LastRead'].apply(lambda x: round(x, 0))
    df_invoicing['LastTime'] = pd.to_datetime(df_invoicing['LastTime'],unit='s')
    df_invoicing = df_invoicing.fillna('NA')
    df_invoicing = df_invoicing.replace(0,'NA')
    df_invoicing  = df_invoicing [df_invoicing['Operation'].isin(invoicing_sites)]
    #Removing KH-003B since it's a backup MFM
    df_invoicing  = df_invoicing [df_invoicing['MeterReference']!='KH-003B']
    #Adding Timestamp for non Locus 
    for index,row in df_sites.iterrows():
        if(os.path.exists(path_lt+row['SiteCode']+'-LT.txt')):
            df_temp=pd.read_csv(path_lt+row['SiteCode']+'-LT.txt',sep='\t')
            last_time=df_temp.loc[df_temp['Date']==date,'LastT']
            if(df_invoicing.loc[df_invoicing['Operation']==row['SiteCode'],'LastTime'].empty or last_time.empty):
                pass
            else:
                df_invoicing.loc[df_invoicing['Operation']==row['SiteCode'],'LastTime']=last_time.values[0]
    df_invoicing = df_invoicing.fillna('NA')
    df_invoicing = df_invoicing.replace(0,'NA')
    df_invoicing.to_csv(path+'EOD_Meter_Reading_'+date+'.txt',sep='\t',index=False) 


