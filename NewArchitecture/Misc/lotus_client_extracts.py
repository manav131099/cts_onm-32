import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pysftp

def chkdir(path):
    if os.path.isdir(path)==False:
        os.makedirs(path)
        
def connect_server(date_now):  #To send all the files in one go.
  cnopts=pysftp.CnOpts()
  cnopts.hostkeys=None
  with pysftp.Connection('203.154.74.118', username='pvsolar', password='go3D8e#4S', cnopts=cnopts) as s:
    for stn in stations:
      if os.path.exists(path_write+'SOLAR_'+str(stations[stn])+'_'+date_now+'.csv'):
        s.put(path_write+'SOLAR_'+str(stations[stn])+'_'+date_now+'.csv')
        print('done')
      else:
        print("The file does not exist")

user='admin'
tz=pytz.timezone('Asia/Bangkok')
path='/home/admin/public/RawData/'
path_write='/home/'+user+'/Dropbox/Customer/TESCO_CSV/'
chkdir(path_write)
stations={'TH-010': 1088, 'TH-011': 5034, 'TH-012': 1099, 'TH-013': 5010, 'TH-014': 1059, 'TH-015': 1012, 'TH-016': 5027, 'TH-017': 1042, 'TH-018': 1067, 'TH-019': 5518, 'TH-020': 1007, 'TH-021': 1502, 'TH-022': 1501, 'TH-023': 1096, 'TH-024': 5002, 'TH-025': 5003, 'TH-026': 5007, 'TH-027': 5026, 'TH-028': 5512,'TH-029': 'DC6', 'TH-030': 1090, 'TH-031': 1045, 'TH-032': 1021, 'TH-033': 1052, 'TH-034': 1124, 'TH-035': 1113, 'TH-036': 5020, 'TH-037': 5031, 'TH-038': 1140, 'TH-039': 1138, 'TH-040': 1110, 'TH-041': 5006, 'TH-042': 5028, 'TH-043': 5017, 'TH-044': 5019, 'TH-060': 1017, 'TH-061': 1074, 'TH-062': 1002, 'TH-063': 1016, 'TH-064': 1030, 'TH-065': 1058, 'TH-066': 5013}

while(1):
  for stn in stations:
    df_upload=pd.DataFrame(columns=['Date','TotWhNet_max'])
    print('Processing: '+stn)
    try:
      for day in range(30,0,-1):
        pvmeters=[]
        date_now=(datetime.datetime.now(tz)+datetime.timedelta(days=-day)).strftime('%Y-%m-%d')
        print(date_now)
        meters=os.listdir(path+stn+'/'+date_now[0:4]+'/'+date_now[0:7])
        for meter in meters:
          if 'pv' in meter.lower():
            pvmeters.append(meter)
        for pos, item in enumerate(pvmeters):
          if os.path.exists(path+stn+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+item+'/'+date_now+'-'+item+'.txt'):
            temp=pd.read_csv(path+stn+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+item+'/'+date_now+'-'+item+'.txt', sep='\t')
            if 'ts' and 'TotWhNet_max' in temp.columns:
              temp=temp[['ts','TotWhNet_max']]
            else:
              continue
            if pos==0:
              df=temp
            else:
              df['TotWhNet_max']+=temp['TotWhNet_max']
        df=df.rename(columns={'ts':'Date'})
        df=df.iloc[::3, :]
        df_upload=df_upload.append(df)
      df_upload['TotWhNet_max']=df_upload['TotWhNet_max'].diff(1)/1000
      df_upload=df_upload.rename(columns={'TotWhNet_max':'Solar'})
      date_now=date_now.replace('-', '')
      df_upload.to_csv(path_write+'SOLAR_'+str(stations[stn])+'_'+date_now+'1.csv',index=False)
    except:
      print("Failed")

  if(datetime.datetime.now(tz).hour==3):
    connect_server(date_now)
    print('Uploaded!')
  print('Sleeping for an hour')
  time.sleep(3600)