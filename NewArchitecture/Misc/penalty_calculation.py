import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import math
import numpy as np
import logging
import pyodbc
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

date = sys.argv[1]
site = sys.argv[2]
PA = sys.argv[3]
PPA = sys.argv[4]

user = 'admin'
path = '/home/admin/Dropbox/ReconnectForecasting/' + site 

def datetime(x):
    return(day+' '+x[:5])

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)
df1 = pd.DataFrame()
df2 = pd.DataFrame()

station_columns = pd.read_sql_query("SELECT Station_Columns FROM [dbo].[Stations] WHERE Station_Name='" + str(site) + "'", connStr)
components = station_columns.values[0][0].strip().split(',')
components = components[2:-2]                        
components = components[:len(components)//2]         
for i in range(len(components)):
  if 'Import' not in components[i]:
    components[i] = components[i].split('.')[0]
    components[i] = components[i].replace('"', '')
  else:
    components.remove(components[i])

#Get site id
site_id = pd.read_sql_query("Select SiteId from Portfolio.Site where SiteCode = '" + str(site) + "'", connStr)
site_id = site_id.values[0][0]

#Get site comps compid
site_comps = pd.read_sql_query("Select * from Portfolio.Component where SiteId = '" + str(site_id) + "' and ComponentType = 'MFM'", connStr)
site_comps = site_comps.ComponentId.to_list()

print(components)
print(site_comps)

#Raw table
raw_table = '['+site + '].RawData'

for year in os.listdir(path):
  for monthyear in os.listdir(path+'/'+year):
    for day in os.listdir(path+'/'+year+'/'+monthyear):
    #path values 3 cols
      filepath = path+'/'+year+'/'+monthyear+'/'+day+'/'+sorted(os.listdir(path+'/'+year+'/'+monthyear+'/'+day))[-1]
      rev_number = filepath[-8:-5]
      temp1 = pd.read_excel(filepath, engine='openpyxl')
      temp1 = temp1.drop([0,1,2,3,4])
      temp1 = temp1[[temp1.columns[2], temp1.columns[4]]]
      temp1[temp1.columns[0]] = temp1[temp1.columns[0]].apply(datetime)
      temp1['File Revision Number'] = rev_number
      df1 = pd.concat([df1, temp1])
                      
      #path2 vals 2 cols [Timestamp, AC_Power]
      tempmerge = pd.DataFrame()
      for i in range(len(site_comps)):
        query = f"Select Timestamp, AC_Power from {raw_table} where ComponentId = {site_comps[i]} and Timestamp BETWEEN '"+str(day)+"' AND DATEADD(day, 1, '"+str(day)+"')"
        temp = pd.read_sql_query(query,connStr)
        if (tempmerge.empty):
          tempmerge = temp
        else:
          tempmerge = pd.merge(tempmerge,temp,on='Timestamp', how = 'inner')
      df2 = pd.concat([df2, tempmerge])

df1.reset_index(drop=True, inplace=True)
df1.columns = ['Time Stamp','Forecasted Generation (MW)', 'File Revision Number']
df1['Time Stamp']= pd.to_datetime(df1['Time Stamp'])

cols = list(df2.columns)[1:]
df2['W_Tot'] = 0
for i in cols:
  df2['W_Tot'] += df2[i]

df2.reset_index(drop=True, inplace=True)
df2 = df2[['Timestamp','W_Tot']]
df2['Timestamp']= pd.to_datetime(df2['Timestamp'])
df2.loc[df2.W_Tot < 0, 'W_Tot'] = 0
df2 = df2.resample('15Min', on='Timestamp').mean()
df2.reset_index(level=0, inplace=True)
df2['W_Tot'] = df2['W_Tot']/1000000
df2.columns = ['Time Stamp','Actual Generation (MW)']
df = pd.merge(df1, df2, how='left')

df['Deviation (MW)'] = df['Actual Generation (MW)'] - df['Forecasted Generation (MW)']
df['Absolute Deviation'] = df['Deviation (MW)'].abs()
df['Plant Availability (MW)'] = int(PA)
df['Error %'] = df['Absolute Deviation']*100/df['Plant Availability (MW)']
df['Total Deviation'] = df['Absolute Deviation']*250

df['Deviation in Units (within +/-15%)'] = 0
df.loc[df['Error %'] > 15, 'Deviation in Units (within +/-15%)'] = df['Plant Availability (MW)']*0.15*250
df.loc[df['Error %'] <= 15, 'Deviation in Units (within +/-15%)'] = df['Absolute Deviation']*250
df['Deviation in Units (within +/-15%)'] = df['Deviation in Units (within +/-15%)']

df['Deviation in Units (+/-15% to +/-25%)'] = 0
df.loc[(df['Error %'] > 15) & (df['Error %'] <= 25), 'Deviation in Units (+/-15% to +/-25%)'] = df['Plant Availability (MW)']*(df['Error %']-15)*250/100
df.loc[df['Error %'] > 25, 'Deviation in Units (+/-15% to +/-25%)'] = df['Plant Availability (MW)']*0.1*250
df['Deviation in Units (+/-15% to +/-25%)'] = df['Deviation in Units (+/-15% to +/-25%)']

df['Deviation in Units (+/-25% to +/-35%)'] = 0
df.loc[(df['Error %'] > 25) & (df['Error %'] <= 35), 'Deviation in Units (+/-25% to +/-35%)'] = df['Plant Availability (MW)']*(df['Error %']-25)*250/100
df.loc[df['Error %'] > 35, 'Deviation in Units (+/-25% to +/-35%)'] = df['Plant Availability (MW)']*0.1*250
df['Deviation in Units (+/-25% to +/-35%)'] = df['Deviation in Units (+/-25% to +/-35%)']

df['Deviation in Units (> +/-35%)'] = 0
df.loc[df['Error %'] > 35, 'Deviation in Units (> +/-35%)'] = df['Plant Availability (MW)']*(df['Error %']-35)*250/100
df['Deviation in Units (> +/-35%)'] = df['Deviation in Units (> +/-35%)']

df['PPA Rate (Rs.)'] = int(PPA)
df['Net Penalty payable to State DSM Pool'] = (df['Deviation in Units (+/-15% to +/-25%)']*0.5 + df['Deviation in Units (+/-25% to +/-35%)'] + df['Deviation in Units (> +/-35%)']*1.5) 
df['Gross Revenue as per Actual Generation (Rs.)'] = (df['Actual Generation (MW)']*df['PPA Rate (Rs.)']*250)
df['Net Revenue Realized from Actual Generation (Rs.)'] = df['Gross Revenue as per Actual Generation (Rs.)'] - df['Net Penalty payable to State DSM Pool']
df['Month No.'] = df['Time Stamp'].astype(str).str[5:7].astype(int)

df['File Revision Number'] = df.pop('File Revision Number')

if os.path.isdir("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+site+"/") == False:
  os.makedirs("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+site+"/")

df.to_csv("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+site+"/["+site+"L] "+date+" - DSM Penalty.csv", index=False)