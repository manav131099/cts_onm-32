#!/bin/bash
RUNALL=1
x=""

#------------------------------SERIS------------------------------
if [ "$1" == "Seris_Sharepoint" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/SerisSharepoint/Seris_Sharepoint.py > /home/admin/Logs/LogsSeris_Sharepoint.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Sharepoint"
	sleep 10
fi

if [ "$1" == "IN-711S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/711Digest/MailDigest711.R &
	y=`echo $!`
	x="$x\n$y --IN-711S_Mail"
	sleep 10
fi

if [ "$1" == "KH-714S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/714Digest/MailDigest714.R &
	y=`echo $!`
	x="$x\n$y --KH-714S_Mail"
	sleep 10
fi

if [ "$1" == "KH-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003NIDigest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003S_Mail"
	sleep 10
fi

if [ "$1" == "IN-015S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN015Digest/MailDigestIN015.R &
	y=`echo $!`
	x="$x\n$y --IN-015S_Mail"
	sleep 10
fi

if [ "$1" == "IN-713S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/713Digest/MailDigest713.R &
	y=`echo $!`
	x="$x\n$y --IN-713S_Mail"
	sleep 10
fi

if [ "$1" == "SG-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/716Digest/MailDigest716.R &
	y=`echo $!`
	x="$x\n$y --SG-003S_Mail"
	sleep 10
fi

if [ "$1" == "SG-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004SDigest/MailDigest717.R &
	y=`echo $!`
	x="$x\n$y --SG-004S_Mail"
	sleep 10
fi


if [ "$1" == "IN-036S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN036Digest/MailDigestIN036.R &
	y=`echo $!`
	x="$x\n$y --IN-036S_Mail"
	sleep 10
fi

if [ "$1" == "IN-721S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/721Digest/MailDigest721.R &
	y=`echo $!`
	x="$x\n$y --IN-721S_Mail"
	sleep 10
fi

if [ "$1" == "SG-724S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG724Digest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-724S_Mail"
	sleep 60 
fi

if [ "$1" == "SG-725S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG725Digest/MailDigest725.R &
	y=`echo $!`
	x="$x\n$y --SG-725S_Mail"
	sleep 60 
fi


if [ "$1" == "SG-726S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG726Digest/MailDigest726.R &
	y=`echo $!`
	x="$x\n$y --SG-726S_Mail"
	sleep 60 
fi


if [ "$1" == "SG-727S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG727Digest/MailDigest727.R &
	y=`echo $!`
	x="$x\n$y --SG-727S_Mail"
	sleep 60 
fi


if [ "$1" == "VN-001S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001SDigest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-001S_Mail"
	sleep 10
fi

if [ "$1" == "VN-722S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN722Digest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-722S_Mail"
	sleep 10
fi

if [ "$1" == "MY-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY004SDigest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-004S_Mail"
	sleep 10
fi

if [ "$1" == "MY-720S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY720Digest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-720S_Mail"
	sleep 10
fi

if [ "$1" == "MY-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY006SDigest/MailDigest729.R &
	y=`echo $!`
	x="$x\n$y --MY-006S_Mail"
	sleep 10
fi

if [ "$1" == "KH-008S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008SDigest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008S_Mail"
	sleep 10
fi


if [ "$1" == "SERIS3G_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SERIS_3G/SERIS_3G.R &
	y=`echo $!`
	x="$x\n$y --SERIS3G"
	sleep 10
fi

#------------------------------WEBDYN------------------------------

if [ "$1" == "IN-003W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN003WDigest/Live.py  > /home/admin/Logs/LogsIN003WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-003W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-004W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN004WDigest/Live.py  > /home/admin/Logs/LogsIN004WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-004W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-005W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN005WDigest/Live.py  > /home/admin/Logs/LogsIN005WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-005W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-006W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN006WDigest/Live.py  > /home/admin/Logs/LogsIN006WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-006W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-007W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN007WDigest/Live.py  > /home/admin/Logs/LogsIN007WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-007W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-009W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN009WDigest/Live.py  > /home/admin/Logs/LogsIN009WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-009W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-013W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN013WDigest/Live.py  > /home/admin/Logs/LogsIN013WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-013W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-029W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN029WDigest/Live.py  > /home/admin/Logs/LogsIN029WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-029W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-032W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN032WDigest/Live.py  > /home/admin/Logs/LogsIN032WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-102W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN102WDigest/Live.py  > /home/admin/Logs/LogsIN102WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-102W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-003W_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN003WDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-003W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-004W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN004WDigest/Digest.py "Mail" "2020-12-01"> /home/admin/Logs/LogsIN004WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-004W_Mail"
    sleep 10
fi

if [ "$1" == "IN-004W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN004WDigest/Client_Digest.py "Mail" "2020-12-01"> /home/admin/Logs/LogsIN004WClientMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-004W_Mail"
    sleep 10
fi

if [ "$1" == "IN-005W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN005WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN005WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-005W_Mail"
    sleep 10
fi

if [ "$1" == "IN-006W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN006WDigest/Digest.py "Mail" "2021-01-28"> /home/admin/Logs/LogsIN006WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-006W_Mail"
    sleep 10
fi

if [ "$1" == "IN-006W_Live" ] || [ $RUNALL == 1 ] 
then
    python3 /home/admin/CODE/NewArchitecture/Live/webdyn_live.py > /home/admin/Logs/LogsIN006WLive.txt 2>&1 &
    y=`echo $!`
    x="$x\n$y --IN-006W_Live"
    sleep 10
fi

if [ "$1" == "IN-007W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN007WDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsIN007WMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-007W_Mail"
    sleep 10
fi

if [ "$1" == "IN-009W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN009WDigest/Digest.py "Mail" "2020-11-08"> /home/admin/Logs/LogsIN009WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-009W_Mail"
    sleep 10
fi

if [ "$1" == "IN-013W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN013WDigest/Digest.py "Mail" "2020-09-30"> /home/admin/Logs/LogsIN013WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-013W_Mail"
    sleep 10
fi

if [ "$1" == "IN-029W_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN029WDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsIN029WMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-029W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-032W_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN032WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN032WMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-102W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN102WDigest/Digest.py "Mail" "2021-08-10"> /home/admin/Logs/LogsIN102WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-102W_Mail"
    sleep 10
fi

#------------------------------AVISOLAR------------------------------

if [ "$1" == "IN-017A_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN017ADigest/Live.py  > /home/admin/Logs/LogsIN017AHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-017A_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-036A_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN036ADigest/Live.py  > /home/admin/Logs/LogsIN036AHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036A_Gen_1"
	sleep 10 
fi

if [ "$1" == "IN-017A_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN017ADigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN017AMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-017A_Mail"
    sleep 10
fi

if [ "$1" == "IN-036A_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN036ADigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN036AMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-036A_Mail"
    sleep 10
fi

#------------------------------RESYNC------------------------------

if [ "$1" == "IN-040R_History" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/IN040RDigest/Live.py &
	y=`echo $!`
	x="$x\n$y --IN-040R_History"
	sleep 30
fi

if [ "$1" == "IN-040R_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN040RDigest/Digest.py "Mail" "2020-10-04"> /home/admin/Logs/LogsIN040RMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-040R_Mail"
	sleep 20 
fi

#------------------------------LOCUS------------------------------
if [ "$1" == "IN-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3899851" "[IN-015L]" "Asia/Kolkata" "Asia/Calcutta" "2021-02-01"> /home/admin/Logs/LogsIN015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-015L_History"
	sleep 30
fi

if [ "$1" == "IN-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3814767" "[IN-065L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-065L_History"
	sleep 30
fi

if [ "$1" == "KH-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3848700" "[KH-009L]" "Asia/Phnom_Penh" "Asia/Phnom_Penh" "2020-02-01"> /home/admin/Logs/LogsKH009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-009L_History"
	sleep 30
fi

if [ "$1" == "PH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3830382" "[PH-002L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-002L_History"
	sleep 30
fi

if [ "$1" == "TH-901L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3907709" "[TH-901L]" "Asia/Bangkok" "Asia/Bangkok" "2021-03-01"> /home/admin/Logs/LogsTH901LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-901L_History"
	sleep 30
fi

if [ "$1" == "IN-015L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN015LDigest/Digest.py "Mail" "2021-02-10"> /home/admin/Logs/LogsIN015LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-015L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-065L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN065LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-065L_Mail"
	sleep 20 
fi

if [ "$1" == "KH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --KH-009L_Mail"
	sleep 10
fi

if [ "$1" == "PH-002L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/PH002LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsPH002LMail.txt &
   	y=`echo $!`
    x="$x\n$y --PH-002L_Mail"
    sleep 10
fi

if [ "$1" == "TH-901L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH901LDigest/Digest.py "Mail" "2021-03-01"> /home/admin/Logs/LogsTH901LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-901L_Mail"
fi


#------------------------------EBX------------------------------

if [ "$1" == "IN-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/LalruCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_History"
	sleep 10
fi

if [ "$1" == "IN-010X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/DelhiCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_History"
	sleep 10
fi

if [ "$1" == "SG-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/SG001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_History"
	sleep 10
fi

if [ "$1" == "SG-002X_History" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/SG002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_History"
	sleep 10
fi

if [ "$1" == "SG-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/SG004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_History"
	sleep 10
fi

if [ "$1" == "SG-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/SG006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_History"
	sleep 10
fi


if [ "$1" == "SG-003EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/SG003EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_History"
	sleep 10
fi

if [ "$1" == "SG-003EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/SG003EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/SG007EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/SG007EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/SG008EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/SG008EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_History"
	sleep 10
fi

if [ "$1" == "KH-002X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/KH002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_History"
	sleep 10
fi

if [ "$1" == "KH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003Digest/KH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_History"
	sleep 10
fi

if [ "$1" == "KH-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/KH004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_History"
	sleep 10
fi

if [ "$1" == "KH-005X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/KH005CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_History"
	sleep 10
fi

if [ "$1" == "KH-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/KH006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_History"
	sleep 10
fi

if [ "$1" == "KH-007X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/KH007CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_History"
	sleep 10
fi

if [ "$1" == "KH-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/KH008CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_History"
	sleep 10
fi

if [ "$1" == "TH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/TH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_History"
	sleep 10
fi

if [ "$1" == "MY-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MY001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_History"
	sleep 10
fi

if [ "$1" == "MY-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MY003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_History"
	sleep 10
fi

if [ "$1" == "VN-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/VN001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_History"
	sleep 10
fi

if [ "$1" == "IN-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/MailDigestLalru.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_Mail"
	sleep 10
fi

if [ "$1" == "IN-010X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/MailDigestDelhi.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_Mail"
	sleep 10
fi

if [ "$1" == "KH-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH001Digest/MailDigestKH001.R &
	y=`echo $!`
	x="$x\n$y --KH-001X_Mail"
	sleep 10
fi

if [ "$1" == "KH-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/MailDigestKH002.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/MailDigestSG001.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_Mail"
	sleep 10
fi

if [ "$1" == "SG-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/MailDigestSG002.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_Mail"
	sleep 10
fi

if [ "$1" == "VN-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/MailDigestVN001.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/MailDigestSG003EP.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/MailDigestSG003EF.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/MailDigestSG007EP.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/MailDigestSG007EF.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_Mail"
	sleep 10
fi

if [ "$1" == "TH-001L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-001L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/MailDigestSG008EP.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/MailDigestSG008EF.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/MailDigestSG004.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/MailDigestSG006.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_Mail"
	sleep 10
fi


if [ "$1" == "IN-018X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018Digest/MailDigestIN018.R &
	y=`echo $!`
	x="$x\n$y --IN-018X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006_Lifetime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SG006Lifetime.py > /home/admin/Logs/LogsSG006LifetimeHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-006_Lifetime"
	sleep 10
fi

if [ "$1" == "KH-003X_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/KH003Digest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_Mail"
	sleep 10
fi

if [ "$1" == "KH-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/MailDigestKH004.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_Mail"
	sleep 10
fi

if [ "$1" == "KH-005X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/MailDigestKH005.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_Mail"
	sleep 10
fi

if [ "$1" == "KH-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/MailDigestKH006.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_Mail"
	sleep 10
fi

if [ "$1" == "KH-007X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/MailDigestKH007.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_Mail"
	sleep 10
fi

if [ "$1" == "KH-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_Mail"
	sleep 10
fi

if [ "$1" == "TH-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/MailDigestTH003.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_Mail"
	sleep 10
fi

if [ "$1" == "MY-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MailDigestMY001.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_Mail"
	sleep 10
fi

if [ "$1" == "MY-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MailDigestMY003.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_Mail"
	sleep 10
fi

#MISC

if [ "$1" == "KH003_Merge_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/KH003_Merge_Live.py > /home/admin/Logs/LogsKH003MergeLive.txt &
	y=`echo $!`
	x="$x\n$y --KH003_Merge_Live"
	sleep 10
fi

if [ "$1" == "Reconnect_Forecasting_Ftp" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/ReconnectForecasting/ftp_fetch.py > /home/admin/Logs/LogsReconnectForecastingFTP.txt &
	y=`echo $!`
	x="$x\n$y --Reconnect_Forecasting_Ftp"
	sleep 10
fi

if [ "$1" == "SerisLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SerisLifetimeMaster.py > /home/admin/Logs/LogsSerisLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --SerisLifetimeMaster"
	sleep 20
fi

if [ "$1" == "LocusLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/LocusLifetimeMaster.py > /home/admin/Logs/LogsLocusLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --LocusLifetimeMaster"
	sleep 20
fi


if [ "$1" == "Lifetime_Gen2" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/Lifetime_Gen2.py > /home/admin/Logs/LogsLifetimeGen_2.txt &
	y=`echo $!`
	x="$x\n$y --Lifetime_Gen2"
	sleep 10
fi

if [ "$1" == "System_Uptime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/System_Uptime.py > /home/admin/Logs/LogsSystem_Uptime.txt &
	y=`echo $!`
	x="$x\n$y --System_Uptime"
	sleep 10
fi

if [ "$1" == "Solar_Percentage" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Solar_Percentage.py > /home/admin/Logs/LogsSolar_Percentage.txt &
	y=`echo $!`
	x="$x\n$y --Solar_Percentage"
	sleep 10
fi

if [ "$1" == "Azure_GIS_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Azure_GIS_Live.py > /home/admin/Logs/LogsAzure_GIS_Live.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Azure_GIS_Live"
	sleep 10
fi

if [ "$1" == "Azure_Met_Stations" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Azure_Met_Stations.py "L" > /home/admin/Logs/LogsAzure_Met_Stations.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Azure_Met_Stations"
	sleep 10
fi

if [ "$1" == "MasterMail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MasterMail/MasterMail.R &
	y=`echo $!`
	x="$x\n$y --MasterMail"
	sleep 10
fi

if [ "$1" == "KH008_Curtailment_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/KH008_Curtailment_Alarm.py > /home/admin/Logs/LogsKH008CurtailmentAlarm.txt &
	y=`echo $!`
	x="$x\n$y --KH008_Curtailment_Alarm"
	sleep 10
fi

#SG-005 SERIS AND SERIS LIVE

if [ "$1" == "SG-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/716Live/Live.py > /home/admin/Logs/Logs716History.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live"
	sleep 10
fi

if [ "$1" == "SG-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/716Live/Live_Azure_4g.py > /home/admin/Logs/Logs716AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/717Live/Live.py > /home/admin/Logs/Logs717History.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live"
	sleep 10
fi

if [ "$1" == "SG-004S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/717Live/Live_Azure_4g.py > /home/admin/Logs/Logs717AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-005S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/725Live/Live.py > /home/admin/Logs/Logs725History.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/725Live/Live_Azure_4g.py > /home/admin/Logs/Logs725AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/714Live/Live.py > /home/admin/Logs/Logs714History.txt &
	y=`echo $!`
	x="$x\n$y --KH-001S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/728Live/Live.py > /home/admin/Logs/Logs728History.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/728Live/Live_Azure_4g.py > /home/admin/Logs/Logs728AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/715Live/Live.py > /home/admin/Logs/Logs715History.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live"
	sleep 10
fi

if [ "$1" == "KH-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/715Live/Live_Azure_4g.py > /home/admin/Logs/Logs715AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "MY-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/720Live/Live.py > /home/admin/Logs/Logs720History.txt &
	y=`echo $!`
	x="$x\n$y --MY-004S_Live"
	sleep 10
fi
if [ "$1" == "MY-006S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/729Live/Live.py > /home/admin/Logs/Logs729History.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live"
	sleep 10
fi

if [ "$1" == "MY-006S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/729Live/Live_Azure_4g.py > /home/admin/Logs/Logs729AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-015S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/712Live/Live.py > /home/admin/Logs/Logs712History.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live"
	sleep 10
fi

if [ "$1" == "IN-015S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/712Live/Live_Azure_4g.py > /home/admin/Logs/Logs712AzureHistory.txt &	
	y=`echo $!`
	x="$x\n$y --IN-015S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-036S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/718Live/Live.py > /home/admin/Logs/Logs718History.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live"
	sleep 10
fi

if [ "$1" == "IN-036S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/718Live/Live_Azure_4g.py > /home/admin/Logs/Logs718AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "VN-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/722Live/Live.py > /home/admin/Logs/Logs722History.txt &
	y=`echo $!`
	x="$x\n$y --VN-001S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG005SDigest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-005S_Mail"
	sleep 10
fi

if [ "$1" == "SG-999S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/Customer/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-999S_Mail"
	sleep 10
fi

if [ "$1" == "Seris_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Seris_Alarm.py > /home/admin/Logs/LogsSeris_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Alarm"
	sleep 10
fi

#New Arch

if [ "$1" == "Lotus_Client_Extracts" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Misc/lotus_client_extracts > /home/admin/Logs/LogsLotusClientExtracts.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Lotus_Client_Extracts"
	sleep 10
fi

if [ "$1" == "IN_Live" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Live/locus_live.py 'IN' > /home/admin/Logs/LogsLocusLiveIN.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --IN_Live"
	sleep 10
fi

if [ "$1" == "MY_Live" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Live/locus_live.py 'MY' > /home/admin/Logs/LogsLocusLiveMY.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --MY_Live"
	sleep 10
fi

if [ "$1" == "SG_Live" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Live/locus_live.py 'SG' > /home/admin/Logs/LogsLocusLiveSG.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --SG_Live"
	sleep 10
fi

if [ "$1" == "TH_Live" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Live/locus_live.py 'TH' > /home/admin/Logs/LogsLocusLiveTH.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --TH_Live"
	sleep 10
fi

if [ "$1" == "VN_Live" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Live/locus_live.py 'VN' > /home/admin/Logs/LogsLocusLiveVN.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --VN_Live"
	sleep 10
fi

if [ "$1" == "Data_Tunnel" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Misc/data_tunnel.py > /home/admin/Logs/LogsDataTunnel.txt 2>&1 & 
	y=`echo $!`
	x="$x\n$y --Data_Tunnel"
	sleep 10
fi

if [ "$1" == "Master_Digest" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/NewArchitecture/Digest/MasterDigest.py > /home/admin/Logs/LogsMasterDigest.txt 2>&1 & 
	y=`echo $!`
	x="$x\n$y --Master_Digest"
	sleep 10
fi

if [ "$1" == "Web_Interface" ] || [ $RUNALL == 1 ] 
then
	nohup python3  /home/admin/CODE/WebInterface/Interface_V3.py > /home/admin/Logs/LogsWebInterface.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Web_Interface"
	sleep 10
fi