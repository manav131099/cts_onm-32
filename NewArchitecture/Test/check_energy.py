import smtplib
import numpy as np
import os
import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *

def get_eac_1(site,cnxn):
    query = pd.read_sql_query('SELECT [Date],SUM([Eac-MFM]) AS sum FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM Stations WHERE Station_Name=\''+site+'\') AND Date<\'2021-06-15\'\n GROUP BY DATE\n ORDER BY DATE DESC', cnxn)
    df = pd.DataFrame(query)
    return df 

def get_eac_2(site,cnxn):
    query = pd.read_sql_query('SELECT  [Date],SUM([Value]) AS sum FROM [Portfolio].[ProcessedData] WHERE SiteId = (SELECT SiteId from Portfolio.Site WHERE SiteCode=\''+site+'\') AND ParameterId=5 AND Date<\'2021-06-15\'\nGROUP BY DATE\nORDER BY DATE DESC', cnxn)
    df = pd.DataFrame(query)
    return df   

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
df_site = get_sites('All',cnxn)

df = pd.DataFrame()
for i in df_site['SiteCode'].tolist():
    df1 = get_eac_1(i,cnxn)
    df2 = get_eac_2(i,cnxn)
    df_temp = pd.DataFrame({'Site':i,'Difference':df1['sum'].sum()-df2['sum'].sum()},index=[0])
    df = df.append(df_temp)
    print(df1['sum'].sum()-df2['sum'].sum())

cnxn.close()
df.to_csv("Differences.csv")