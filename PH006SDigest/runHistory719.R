
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
prepareSumm = function(dataread)
{
	da = nrow(dataread)
	daPerc = round(da/14.4,1)
  thresh = 5/1000
	gsi1 = tamb=tambst=hamb=hambst=tambmx=tambstmx=hambmx=hambstmx=tambmn=tambstmn=hambmn=hambstmn=tsi1=NA
  
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
		gsi1 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsismp = sum(dataread[complete.cases(dataread[,3]),3])/60000
  subdata = dataread[complete.cases(dataread[,4]),]
  subdata = subdata[as.numeric(subdata[,4]) > thresh,]
	
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
	tamb = mean(dataread[complete.cases(dataread[,6]),6])
	
	dataread2 = subdata[complete.cases(subdata[,6]),6]
	if(length(dataread2))
  tambst = mean(subdata[complete.cases(subdata[,6]),6])
  
	dataread2 = dataread[complete.cases(dataread[,7]),7]
	if(length(dataread2))
  hamb = mean(dataread[complete.cases(dataread[,7]),7])
  
	dataread2 = subdata[complete.cases(subdata[,7]),7]
	if(length(dataread2))
	hambst = mean(subdata[complete.cases(subdata[,7]),7])
  
	dataread2 = dataread[complete.cases(dataread[,6]),6]
	if(length(dataread2))
  tambmx = max(dataread[complete.cases(dataread[,6]),6])
  
	dataread2 = subdata[complete.cases(subdata[,6]),6]
	if(length(dataread2))
	tambstmx = max(subdata[complete.cases(subdata[,6]),6])
  
	dataread2 = dataread[complete.cases(dataread[,7]),7]
	if(length(dataread2))
  hambmx = max(dataread[complete.cases(dataread[,7]),7])
  
	
	dataread2 = subdata[complete.cases(subdata[,7]),7]
	if(length(dataread2))
	hambstmx = max(subdata[complete.cases(subdata[,7]),7])
  
	dataread2 = dataread[complete.cases(dataread[,6]),6]
	if(length(dataread2))
  tambmn = min(dataread[complete.cases(dataread[,6]),6])
  
	dataread2 = subdata[complete.cases(subdata[,6]),6]
	if(length(dataread2))
	tambstmn = min(subdata[complete.cases(subdata[,6]),6])
  
	dataread2 = dataread[complete.cases(dataread[,7]),7]
	if(length(dataread2))
  hambmn = min(dataread[complete.cases(dataread[,7]),7])
  
	dataread2 = subdata[complete.cases(subdata[,7]),7]
	if(length(dataread2))
	hambstmn = min(subdata[complete.cases(subdata[,7]),7])
  
	dataread2 = dataread[complete.cases(dataread[,8]),8]
	if(length(dataread2))
  tsi1 = mean(dataread[complete.cases(dataread[,8]),8])

	Eac11 = Eac12 = Eac13 = Eac21 = Eac22 = Eac23 = PR11 = PR12 = PR13 = PR21 = PR22 = PR23 = LastR1 = LastT1 = NA
	
	dataread2 = dataread[complete.cases(dataread[,24]),24]
	if(length(dataread2))
	Eac11 = sum(dataread[complete.cases(dataread[,24]),24])/60
	if(is.finite(Eac11) && Eac11 < 0)
		Eac11 = Eac11 * -1
	LastR1=LastR2=LastR3=NA
	dataread2 = dataread[complete.cases(dataread[,39]),39]
	dataread3 = dataread[complete.cases(dataread[,39]),1]
	
	if(length(dataread2))
	{
  Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	if(is.finite(Eac21) && Eac21 < 0)
		Eac21 = -1 * Eac21
	if(length(dataread2))
		LastR1 = as.numeric(dataread2[length(dataread2)])
	
	if(length(dataread3))
		LastT1 = as.character(dataread3[length(dataread3)])
	}
	PR11 = (Eac11 * 100) / (gsi1*300.8)
	YLD11 = 0.01*PR11 * gsi1
	PR21 = (Eac21 * 100) / (gsi1*300.8)
	YLD21 = 0.01*PR21 * gsi1
	PR12 = (Eac11 * 100) / (gsismp*300.8)
	PR22 = (Eac21 * 100) / (gsismp*300.8)
#	FullSiteProd = Eac21+Eac22
#	CarParkProd=Eac23
#	MainRoofProd = FullSiteProd-CarParkProd
	dateAc = NA
	if(nrow(dataread))
		dateAc = substr(dataread[1,1],1,10)
  datawrite = data.frame(Date = dateAc,PtsRec = rf(da),Gsi00 = rf(gsi1),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         TMod = rf1(tsi1),
												 Eac1 = rf(Eac11),
												 Eac2 = rf(Eac21),
												 PR1Si = rf1(PR11),
												 PR2Si = rf1(PR21),
												 YLD1 = rf(YLD11),
												 YLD2 = rf(YLD21),
												 LastR1=LastR1,
												 LastT1=LastT1,
												 PR1Py = rf1(PR11),
												 PR2Py = rf1(PR21),
												 DA=rf1(daPerc),
												 stringsAsFactors=F
												)
  datawrite
}

rewriteSumm = function(datawrite)
{
  
  df = datawrite
  df
}

path = "/home/admin/Dropbox/SerisData/1min/[719]"
pathwrite = "/home/admin/Dropbox/Second Gen/[PH-006S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[PH-006S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("719","PH-006S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
