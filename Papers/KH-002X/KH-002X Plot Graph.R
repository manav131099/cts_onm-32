rm(list=ls(all =TRUE))

library(ggplot2)
library(reshape)
library(lubridate)
library(devtools)

pathWrite <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Plot Wall Graph/"
result <- read.csv("C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/KH-002X_PR_summary.csv",stringsAsFactors = F)

rownames(result) <- NULL
result <- data.frame(result)
result$Date <- as.POSIXct(result$Date, format = "%Y-%m-%d")

avg = mean(result$PR,na.rm=TRUE)
std = sd(result$PR,na.rm=TRUE)
sigma2POS = avg + (2*std)
sigma2NEG = avg - (2*std)

No_of_points <- nrow(result)

m <- lm(result$PR ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")


PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR)) + geom_point() +
  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  scale_y_continuous(limits = c(60, 100), expand= c(0,0), breaks=seq(60,100,10)) +
  scale_x_continuous(limits = c(0, 900), expand= c(0,0),breaks=seq(0,900,100)) +
  ggtitle(expression("[KH-002X] Lifetime Performance Ratio (without 2"*sigma~"filter)"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
  ylab("Performance Ratio AC [%]") + 
  xlab("# of Days") +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  geom_hline(yintercept=sigma2POS, linetype="dashed") +
  geom_hline(yintercept=sigma2NEG, linetype="dashed") + 
  annotate("text",label = expression("+2"*sigma~"[91.2%]"),size = 3,  #update the value(%) according to Sigma2POS
           x = 100, y= sigma2POS+1.5,fontface =1,hjust = 0) +
  annotate("text",label = expression("-2"*sigma~"[71.8%]"),size = 3,   #update the value(%) according to Sigma2NEG
           x = 100, y= sigma2NEG-3.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", avg)," %"),size = 3,
           x = 600, y= 64,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
           x = 600, y= 62.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0(equation),size = 3,
           x = 600, y= 97.1,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
           x = 600, y= 95.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
           x = 600, y= 94,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left

ggsave(PRGraph,filename = paste0(pathWrite,"KH-002X_lifetime_PR_Without_Filter.pdf"), width = 7.92, height = 5) 

count <- 0
exclude_index <- c()
for(i in 1:nrow(result)){
  if(isTRUE (result[i,3] < sigma2NEG)){  # eliminate PR below -2Sigma
    exclude_index <- c(exclude_index,i)
    count <- count + 1
  }
  
  if(isTRUE (result[i,3] > sigma2POS)){   # eliminate PR above +2Sigma
    exclude_index <- c(exclude_index,i)
    count <- count + 1
  }
}
result[exclude_index,3] <- NA

m <- lm(result$PR ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")

PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR)) + geom_point() +
  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  scale_y_continuous(limits = c(65, 96), expand= c(0,0), breaks=seq(70,90,10)) +
  scale_x_continuous(limits = c(0, 900), expand= c(0,0),breaks=seq(0,900,100)) +
  ggtitle(paste("[KH-002X] Lifetime Performance Ratio"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
  ylab("Performance Ratio AC [%]") + 
  xlab("# of Days") +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  geom_hline(yintercept=sigma2POS, linetype="dashed") +
  geom_hline(yintercept=sigma2NEG, linetype="dashed") + 
  annotate("text",label = expression("+2"*sigma~"[91.2%]"),size = 3,  #update the value(%) according to Sigma2POS
           x = 100, y= sigma2POS+1.5,fontface =1,hjust = 0) +
  annotate("text",label = expression("-2"*sigma~"[71.8%]"),size = 3,   #update the value(%) according to Sigma2NEG
           x = 100, y= sigma2NEG-1.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", avg)," %"),size = 3,
           x = 500, y= 70,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
           x = 500, y= 69,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Number of Point availble = ", nrow(result)-count),size = 3,
           x = 500, y= 68,fontface =1,hjust = 0) +    
  annotate("text",label = expression("Points eliminated (2"*sigma~"filter) = 41"),size = 3, #update the value(%) according to count
           x = 500, y= 67,fontface =1,hjust = 0) +
  annotate("text",label = paste0("% of points eliminated = ",round(count/No_of_points*100,1)," %"),size = 3,
           x = 500, y= 66,fontface =1,hjust = 0) +
  annotate("text",label = paste0(equation),size = 3,
           x = 500, y= 94,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
           x = 500, y= 93,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
           x = 500, y= 92,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left


ggsave(PRGraph,filename = paste0(pathWrite,"KH-002X_lifetime_PR.pdf"), width = 7.92, height = 5) 



