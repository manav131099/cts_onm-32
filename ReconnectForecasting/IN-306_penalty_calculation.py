import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import math
import numpy as np
import logging
import pyodbc
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

user = 'admin'
date = sys.argv[1]
sites = ['IN-306', 'IN-307', 'IN-308', 'IN-309']
PA = sys.argv[2]
PPA = sys.argv[3]

path = '/home/admin/Dropbox/ReconnectForecasting/' + sites[0] 
path2= '/home/admin/Dropbox/Gen 1 Data/'

def datetime(x):
    return(day+' '+x[:5])

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)

df1 = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
df4 = pd.DataFrame()
df5 = pd.DataFrame()

mapping = {}
for site in sites:
  station_columns = pd.read_sql_query("SELECT Station_Columns FROM [dbo].[Stations] WHERE Station_Name='" + str(site) + "'", connStr)
  components = station_columns.values[0][0].strip().split(',')
  components = components[2:-2]
  components = components[:len(components)//2]
  for i in range(len(components)):
    if 'Import' not in components[i]:
      components[i] = components[i].split('.')[0]
      components[i] = components[i].replace('"', '')
    else:
      components.remove(components[i])
  
  mapping[site] = components

print(mapping)

for year in os.listdir(path):
  for monthyear in os.listdir(path+'/'+year):
    for day in os.listdir(path+'/'+year+'/'+monthyear):
      filepath = path+'/'+year+'/'+monthyear+'/'+day+'/'+sorted(os.listdir(path+'/'+year+'/'+monthyear+'/'+day))[-1]
      rev_number = filepath[-8:-5]
      temp1 = pd.read_excel(filepath, engine='openpyxl')
      temp1 = temp1.drop([0,1,2,3,4])
      temp1 = temp1[[temp1.columns[2], temp1.columns[4]]]
      temp1[temp1.columns[0]] = temp1[temp1.columns[0]].apply(datetime)
      temp1['File Revision Number'] = rev_number
      df1 = pd.concat([df1, temp1])      
      
      temp = pd.read_csv(path2+'['+sites[0]+'L]'+'/'+year+'/'+monthyear+'/'+mapping[sites[0]][0]+'/['+sites[0]+'L]-MFM'+mapping[sites[0]][0].split('_')[1]+'-'+day+'.txt', delimiter='\t')[['ts','W_avg']]
      df2 = pd.concat([df2, temp])
      
      temp = pd.read_csv(path2+'['+sites[1]+'L]'+'/'+year+'/'+monthyear+'/'+mapping[sites[1]][0]+'/['+sites[1]+'L]-MFM'+mapping[sites[1]][0].split('_')[1]+'-'+day+'.txt', delimiter='\t')[['ts','W_avg']]
      df3 = pd.concat([df3, temp])
      
      temp = pd.read_csv(path2+'['+sites[2]+'L]'+'/'+year+'/'+monthyear+'/'+mapping[sites[2]][0]+'/['+sites[2]+'L]-MFM'+mapping[sites[2]][0].split('_')[1]+'-'+day+'.txt', delimiter='\t')[['ts','W_avg']]
      df4 = pd.concat([df4, temp])
      
      temp = pd.read_csv(path2+'['+sites[3]+'L]'+'/'+year+'/'+monthyear+'/'+mapping[sites[3]][0]+'/['+sites[3]+'L]-MFM'+mapping[sites[3]][0].split('_')[1]+'-'+day+'.txt', delimiter='\t')[['ts','W_avg']]
      df5 = pd.concat([df5, temp])

df1.reset_index(drop=True, inplace=True)
df1.columns = ['Time Stamp','Forecasted Generation (MW)','File Revision Number']
df1['Time Stamp'] = pd.to_datetime(df1['Time Stamp'])

df2 = pd.merge(df2, df3, how='outer', on='ts')
df2 = pd.merge(df2, df4, how='outer', on='ts')
df2 = pd.merge(df2, df5, how='left', on='ts', suffixes=('_p', '_q'))
df2.reset_index(drop=True, inplace=True)

cols = list(df2.columns)[1:]
df2['W_Tot'] = 0
for i in cols:
  df2['W_Tot'] += df2[i]

df2.reset_index(drop=True, inplace=True)
df2 = df2[['ts','W_Tot']]
df2['ts']= pd.to_datetime(df2['ts'])
df2.loc[df2.W_Tot < 0, 'W_Tot'] = 0
df2 = df2.resample('15Min', on='ts').mean()
df2.reset_index(level=0, inplace=True)
df2['W_Tot'] = df2['W_Tot']/1000000
df2.columns = ['Time Stamp','Actual Generation (MW)']

df = pd.merge(df1, df2, how='left')

df['Deviation (MW)'] = df['Actual Generation (MW)'] - df['Forecasted Generation (MW)']
df['Absolute Deviation'] = df['Deviation (MW)'].abs()
df['Plant Availability (MW)'] = int(PA)
df['Error %'] = df['Absolute Deviation']*100/df['Plant Availability (MW)']
df['Total Deviation'] = df['Absolute Deviation']*250

df['Deviation in Units (within +/-15%)'] = 0
df.loc[df['Error %'] > 15, 'Deviation in Units (within +/-15%)'] = df['Plant Availability (MW)']*0.15*250
df.loc[df['Error %'] <= 15, 'Deviation in Units (within +/-15%)'] = df['Absolute Deviation']*250
df['Deviation in Units (within +/-15%)'] = df['Deviation in Units (within +/-15%)']

df['Deviation in Units (+/-15% to +/-25%)'] = 0
df.loc[(df['Error %'] > 15) & (df['Error %'] <= 25), 'Deviation in Units (+/-15% to +/-25%)'] = df['Plant Availability (MW)']*(df['Error %']-15)*250/100
df.loc[df['Error %'] > 25, 'Deviation in Units (+/-15% to +/-25%)'] = df['Plant Availability (MW)']*0.1*250
df['Deviation in Units (+/-15% to +/-25%)'] = df['Deviation in Units (+/-15% to +/-25%)']

df['Deviation in Units (+/-25% to +/-35%)'] = 0
df.loc[(df['Error %'] > 25) & (df['Error %'] <= 35), 'Deviation in Units (+/-25% to +/-35%)'] = df['Plant Availability (MW)']*(df['Error %']-25)*250/100
df.loc[df['Error %'] > 35, 'Deviation in Units (+/-25% to +/-35%)'] = df['Plant Availability (MW)']*0.1*250
df['Deviation in Units (+/-25% to +/-35%)'] = df['Deviation in Units (+/-25% to +/-35%)']

df['Deviation in Units (> +/-35%)'] = 0
df.loc[df['Error %'] > 35, 'Deviation in Units (> +/-35%)'] = df['Plant Availability (MW)']*(df['Error %']-35)*250/100
df['Deviation in Units (> +/-35%)'] = df['Deviation in Units (> +/-35%)']

df['PPA Rate (Rs.)'] = int(PPA)
df['Net Penalty payable to State DSM Pool'] = (df['Deviation in Units (+/-15% to +/-25%)']*0.5 + df['Deviation in Units (+/-25% to +/-35%)'] + df['Deviation in Units (> +/-35%)']*1.5) 
df['Gross Revenue as per Actual Generation (Rs.)'] = (df['Actual Generation (MW)']*df['PPA Rate (Rs.)']*250)
df['Net Revenue Realized from Actual Generation (Rs.)'] = df['Gross Revenue as per Actual Generation (Rs.)'] - df['Net Penalty payable to State DSM Pool']
df['Month No.'] = df['Time Stamp'].astype(str).str[5:7].astype(int)

df['File Revision Number'] = df.pop('File Revision Number')

if os.path.isdir("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+sites[0]+"/") == False:
  os.makedirs("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+sites[0]+"/")

df.to_csv("/home/"+user+"/Dropbox/ReconnectForecasting/DSM_Penalty/"+sites[0]+"/["+sites[0]+"L] "+date+" - DSM Penalty.csv", index=False)