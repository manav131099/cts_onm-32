errHandle = file('/home/admin/Logs/LogsSG004Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/SGDigest/SG004XDigest/HistoricalAnalysis2G3GSG004.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/SGDigest/SG004XDigest/aggregateInfo.R')
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Meter-A'
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Meter-B'
	}
	}
	ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n________________________________________________\n"
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n________________________________________________\n\n")
  body = paste(body,"Eac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nYield",no,"-1 [kWh/kWp]: ",as.character(df[,9]),sep="")
  body = paste(body,"\n\nYield",no,"-2 [kWh/kWp]: ",as.character(df[,10]),sep="")
  body = paste(body,"\n\nTotal Energy (from SG-004S) [kWh]: ",as.character(df[,14]),sep="")
  body = paste(body,"\n\nIrradiance (from SG-004S) [kWh]: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nPR",no,"-1 [kWh/kWp]: ",as.character(df[,12]),sep="")
  body = paste(body,"\n\nPR",no,"-2 [kWh/kWp]: ",as.character(df[,13]),sep="")
	body = paste(body,"\n\nRatio [%]:",as.character(df[,4]))
  acpts = round(as.numeric(df[,5]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,5]),"%)",sep="")
 # if(as.numeric(no)!=3)
		body = paste(body,"\n\nDowntime (%):",as.character(df[,6]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,8]))
  return(body)
}
printtsfaults = function(TS,num,body)
{
  {
	if(as.numeric(num) == 1)
	{
	  num = 'Meter-A'
	}
	else if(as.numeric(num)==2)
	{
	  num = 'Meter-B'
	}
	}
	if(length(TS) > 1)
	{
		body = paste(body,"\n________________________________________________\n")
		body = paste(body,paste("\nTimestamps for",num,"where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		body = paste(body,"\n________________________________________________\n")
	}
	return(body)
}
sendMail = function(df1,df2,pth1,pth2,pth3)
{
  filetosendpath = c(pth1,pth2)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth2,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	data3G = read.table(pth3,header =T,sep = "\t")
	dateineed = unlist(strsplit(filenams[1]," "))
	dateineed = unlist(strsplit(dateineed[2],"\\."))
	dateineed = dateineed[1] 
	idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: Kuehne & Nagel\n",sep="")
	body = paste(body,"\nLocation: 10 Pioneer Crescent, Singapore - 628566\n")
	body = paste(body,"\nO&M Code: SG-004\n")
	body = paste(body,"\nSystem Size: 844.8 kWp\n")
	body = paste(body,"\nNumber of Energy Meters: 2\n")
	body = paste(body,"\nModule Brand / Model / Nos: REC / 320 / 2640\n")
	body = paste(body,"\nInverter Brand / Model / Nos: SMA / STP60 / 11\n")
	body = paste(body,"\nSite COD: 2017-05-12\n")
	body = paste(body,"\nSystem age [days]:",DAYSALIVE,"\n")
	body = paste(body,"\nSystem age [years]:",round(DAYSALIVE/365,2))
  body = paste(body,initDigest(df1,1))  #its correct, dont change
	body = paste(body,initDigest(df2,2))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
	print('2G data processed')
	body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	#if(is.finite(idxineed))
	#{
  #body = paste(body,"\n\n# Artificial Load:",as.character(data3G[idxineed,20]))
  #body = paste(body,"\n\n# Percentage Solar:",as.character(data3G[idxineed,21]))
  #body = paste(body,"\n\n# Daily Consumption (kWh):",as.character(data3G[idxineed,22]))
  #}
	print('3G data processed')
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [SG-004X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls= TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
recordTimeMaster("SG-004X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
#recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com', 'lucas.ferrand@cleantechsolar.com',
#'Thareth.Song@comin.com.kh','Sothea.Hin@comin.com.kh')
# recipients = getRecipients("SG-004X","m")
recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com')


pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("SG-004X","m")
  recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com')
	recordTimeMaster("SG-004X","Bot")
  sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[SG-004X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t],days2[t])))
				 {
				 	daycop = c(days[t],days2[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")

         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")

				 dataprev = read.table(readpath2,header = T,sep = "\t")
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 datemtch = unlist(strsplit(days[t]," "))
		 		 thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)

  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
