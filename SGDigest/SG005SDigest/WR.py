# Introducing dependencies
from windrose import WindroseAxes
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd
import datetime
import os
import glob
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.ticker as ticker
from datetime import datetime
import matplotlib.dates as mdates
from datetime import datetime as dt
from datetime import timedelta 
import numpy as np
import csv


#DATA FRAME
yesterday = datetime.now() - timedelta(days=1)
yesterday = str(yesterday)
yesterday = yesterday[0:10]
print(yesterday)
interesting_files = []
path = os.listdir('/home/admin/Dropbox/SerisData/1min/[724]/')
for j in path:
    path1 = os.listdir('/home/admin/Dropbox/SerisData/1min/[724]/'+str(j)+'/')
    for k in path1: 
        path2 = os.listdir('/home/admin/Dropbox/SerisData/1min/[724]/'+str(j)+'/'+str(k)+'/')
        path_final = '/home/admin/Dropbox/SerisData/1min/[724]/'+str(j)+'/'+str(k)+'/'
        for l in path2:
            fullpath = os.path.join(path_final, l)
            interesting_files.append(fullpath)
            
interesting_files = sorted(interesting_files)
df = pd.concat((pd.read_csv(f, header = 0, sep = '\t') for f in interesting_files))
df.to_csv("/home/admin/Graphs/Graph_Extract/SG-005/WR_extract.csv", index = False)



# Randomly generate an array of wind speed and direction using nmupy
col_names=['Timestamp', 'GHI', 'GTI', 'WS', 'WD'] 
data = pd.read_csv('/home/admin/Graphs/Graph_Extract/SG-005/WR_extract.csv')
data = data[(data['Tm'] > str(yesterday))]
data.fillna(0)
print(data)
ws = data['AvgWindS']
wd = data['AvgWindD']
ws_avg = round(data['AvgWindS'].mean(skipna = True),1)
ws_max = round(data['AvgWindS'].max(skipna = True),1)


ax = WindroseAxes.from_ax()
ax.bar(wd, ws, normed = True, edgecolor='white')
ax.set_legend()
ax.set_facecolor('lightcyan')
ax.text(0.6, 0.000, 'Avg Wind Speed [m/s]: '+str(ws_avg), va="top",transform=ax.transAxes,weight='bold', size = 12)
#ax.text(0.01, 0.000, 'Max Wind Speed [m/s]: '+str(ws_max), va="top",transform=ax.transAxes,weight='bold', size = 12)

plt.suptitle('Wind Rose - SG-005S - '+str(yesterday), x=0.51, y=.98, horizontalalignment='center', verticalalignment='top', fontsize = 15)
#plt.savefig('/home/admin/Graphs/Graph_Extract/SG-005/WR_extract.csv')
plt.savefig('/home/admin/Graphs/Graph_Output/SG-724/Graph [SG-005S] Wind Rose '+str(yesterday)+'.pdf' ,bbox_inches='tight')
plt.show()