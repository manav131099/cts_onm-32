import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import shutil
import time
import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf

asd= ['TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028']

#GA
#for i in asd:
#    os.system(" ".join(['python3 /home/admin/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py' ,i,'2021-08-04','2020-03-01']))

#PR
""" asd= ['TH-010','TH-011','TH-012','TH-013','TH-014','TH-015','TH-016','TH-017','TH-018','TH-019','TH-020','TH-021','TH-022','TH-023','TH-024','TH-025','TH-026','TH-027','TH-028']
for s in asd:
    stn=s
    path = '/home/admin/Graphs/Graph_Extract/'+stn+'/['+stn+'] Graph 2021-07-31 - PR Evolution.txt'
    df =  pd.read_csv(path,sep='\t')
    df = df[(df['Date']>='2019-01-01') & (df['Date']<='2021-07-31')]
    df_2 = pd.read_csv('/home/admin/Graphs/Graph_Extract/'+stn+'/['+stn+'] - Master Extract Monthly.txt',sep='\t')
    df.index=pd.to_datetime(df['Date'])
    df=df.resample("1m").mean()
    df['Date'] = df.index.to_series().apply(lambda x: dt.datetime.strftime(x, '%b-%Y'))
    df = df.reset_index(level=0, drop=True).reset_index()
    df = pd.merge(df,df_2,on='Date',how='left') 
    df=df.round(2)
    cols = list(df.columns)
    cols = [cols[-1]] + cols[:-1]
    df = df[cols]
    df = df[['Date',"PR","GA","PA","DA"]]
    print(df)
    df.to_csv(stn+' Master Extract Monthly.txt',sep='\t',index=False) """
    
#PR
asd = []
for i in range(1,26):
    if(i<10):
        asd.append('MY-40'+str(i))
    else:
        asd.append('MY-4'+str(i))
print(asd)
""" for i in asd:
    os.system(" ".join(['python3 /home/pranav/ctsfork/NewArchitecture/Graphs/grid_availability_graph.py' ,i,'2021-07-31'])) """
""" params = ['GA','PA',"DA","SA"]
for p in params:
    final = pd.DataFrame()
    for s in asd:
        print(s)
        df = pd.read_csv('/home/admin/Graphs/Graph_Extract/'+s+'/['+s+'] - Master Extract Monthly.txt',sep='\t')
        if(s=='MY-401'):
            cols = df['Date'].tolist()
        df['O&M Code'] = s
        print(df.pivot(index='O&M Code', columns='Date', values=p))
        final = final.append(df.pivot(index='O&M Code', columns='Date', values=p))

    final = final[ cols]
    final.to_csv('MY-401-425 '+p+'.txt',sep='\t') """

path = '/home/admin/public/Monthly Values/'
params = ['Grid Availability','Plant Availability','System Availability']

country = 'VN'
ref = 'VN-001'
for p in params:
    final = pd.DataFrame()
    for i in os.listdir(path):
        if country in i:
            df = pd.read_csv(path+i)
            df['O&M Code'] = i[:-7]
            print(i)
            if(i[:-7]==ref):
                cols = df['Date'].tolist() 
            final = final.append(df.pivot(index='O&M Code', columns='Date', values=p))
    final = final[cols]
    final = final.sort_values(by=['O&M Code'])
    final.to_csv(country+'_Portfolio_'+p+'.txt',sep='\t') 

print(final)




""" cap = [70.875,
70.875,
58.32,
70.875,
64.8,
70.875,
34.425,
29.565,
44.55,
50.85,
41.31,
53.865,
33.21,
71.76,
71.1,
40.95,
33.3,
35.55,
71.1,
40.5,
29.7,
27.45,
38.25,
36,
60.3]



df = pd.read_csv('MY-401-425 PA.txt',sep='\t')
df['Size'] = cap
wa = []
for i in df.columns[1:-1]:
    temp_df = df[[i,'Size']].dropna()
    print(temp_df)
    wav = (temp_df['Size']*temp_df[i]).sum()/temp_df['Size'].sum()
    wa.append(wav)
print(wa)
df.loc[len(df)] = ['Weighted Average']+wa+['']
df = df.iloc[:,:-1]
print(df)
df.to_csv('MY-401-425 PA.txt',sep='\t') """