import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *
import process_functions
from pvlib import location
from pvlib import irradiance
from pvlib import clearsky, atmosphere, solarposition
from pvlib.location import Location

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

#DA Extract
stn='SG-003'
site=716
frqcol=['Freq_A','Freq_B'] 
powercol=['Act_Pwr-Tot_A','Act_Pwr-Tot_B'] 
energycol = ['Act_E-Del_A','Act_E-Del_B']
irrcol='AvgGsi00'
metername=[3574,3575]


path='/home/admin/Dropbox/SerisData/1min/['+str(site)+']/'
tz = pytz.timezone('Asia/Calcutta')

df_final_2 = pd.DataFrame()
for n in range(len(metername)):
    df_final=pd.DataFrame()
    for i in sorted(os.listdir(path)):
        for j in sorted(os.listdir(path+i)):
            if('2019' in j or '2020' in j or '2021' in j):
                print(metername[n])
                try:   
                    for m in sorted(os.listdir(path+i+'/'+j)): 
                        mfm_df=pd.read_csv(path+i+'/'+j+'/'+m,sep='\t')
                        #mfm_df_irr=pd.read_csv('/home/admin/Dropbox/SerisData/1min/[724]/'+i+'/'+j+'/'+'[724] '+m.split(' ')[1][:-4]+'.txt',sep='\t')
                        #mfm_df_irr = mfm_df_irr[['Tm','AvgSMP10']]
                        #mfm_df = pd.merge(mfm_df_irr,mfm_df,how='left',on='Tm')
                        mfm_df=mfm_df[['Tm',frqcol[n],powercol[n],irrcol,energycol[n]]]
                        mfm_df[powercol[n]] = mfm_df[powercol[n]]*-1
                        mfm_df_drop = mfm_df.dropna()  
                        print(len(mfm_df_drop))   
                        mfm_df_drop_2 = mfm_df[(mfm_df[energycol[n]]<1000000000000)]
                        mfm_df_drop_2 = mfm_df_drop[(mfm_df_drop[energycol[n]]>0)]
                        #mfm_df_drop_2 = mfm_df_drop_2[(mfm_df_drop[energycol[n]]!=0)]
                        mfm_df_drop = mfm_df_drop[(mfm_df_drop[energycol[n]]<1000000000000)]
                        mfm_df_drop = mfm_df_drop[(mfm_df_drop[energycol[n]]>0)]
                        #mfm_df_drop = mfm_df_drop[(mfm_df_drop[energycol[n]]!=0)]
                        irr_timestamps=mfm_df_drop_2.loc[mfm_df_drop_2[irrcol]>20,'Tm']
                        freq_timestamps=mfm_df_drop.loc[mfm_df_drop[frqcol[n]]>40,'Tm']
                        power_timestamps=mfm_df_drop.loc[mfm_df_drop[powercol[n]]>0,'Tm']
                        ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                        pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                        mfm_df_sh = mfm_df.copy()
                        mfm_df_sh['Tm']=pd.to_datetime(mfm_df_sh['Tm'])
                        mask = (mfm_df_sh['Tm'].dt.hour > 7) & (mfm_df_sh['Tm'].dt.hour < 18)
                        mfm_df_sh = mfm_df_sh[mask]
                        DA2=(float(len(mfm_df_sh[mfm_df_sh[powercol[n]].notnull()]))/600)*100
                        irr_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[irrcol]>20,'Tm']
                        freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[frqcol[n]]>40,'Tm']
                        if(DA2<50):
                            DA=float((len(mfm_df)/1440)*100)
                            if(len(irr_timestamps)==0):
                                PA=None
                            else:
                                PA=(float(len(pa_common))/float(len(irr_timestamps)))*100
                            df_ga=pd.DataFrame({"Date":[m[6:-4]],'GA':[None],'PA':[PA],'DA':[DA],'ComponentId':[metername[n]]},columns=['Date', 'GA', 'PA', 'DA','ComponentId'])
                            print((df_ga))
                            df_final=df_final.append(df_ga,sort=False)
                        else:
                            if(len(irr_timestamps)>0 and len(freq_timestamps)>0 ):
                                GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                                PA=(float(len(pa_common))/float(len(irr_timestamps)))*100
                                DA=float((len(mfm_df)/1440)*100)
                                print((len(ga_common)),float(len(irr_timestamps)))
                                df_ga=pd.DataFrame({"Date":[m[6:-4]],'GA':[GA],'PA':[PA],'DA':[DA],'ComponentId':[metername[n]]},columns=['Date', 'GA', 'PA', 'DA','ComponentId'])
                                print((df_ga))
                                df_final=df_final.append(df_ga,sort=False)
                except:
                    pass 
            
    df_final_2 = df_final_2.append(df_final)
    df_final.to_csv(str(stn)+str(metername[n])+'Daily .txt',sep='\t',index=False)
print(df_final_2)
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
site_id = get_site_id(stn,cnxn)
df_final = pd.read_csv(str(stn)+str(metername[0])+'Daily .txt',sep='\t')
ids = {"DA":1,"GA":13,"PA":14}
df = pd.DataFrame()
for i in df_final.columns.tolist()[1:-1]:
    for index,row in df_final.iterrows():
        df_temp = pd.DataFrame({"Date":[row['Date']],"Value":[row[i]],"ParameterId":[ids[i]],"ComponentId":[row['ComponentId']],"SiteId":[site_id]})
        df = df.append(df_temp)
df = df.replace({np.nan: None})
print(df)
fast_upsert_processed(stn,df)
cnxn.close() 
"""     df=pd.read_csv(str(stn)+str(metername[n])+'Daily .txt',sep='\t')
    df = df[df['Date']<'2021-07-31']
    df.index=pd.to_datetime(df['Date'])
    df=df.resample("1m").mean()
    df['Date'] = df.index.to_series().apply(lambda x: datetime.datetime.strftime(x, '%b-%Y'))
    df=df.round(2)
    cols = list(df.columns)
    cols = [cols[-1]] + cols[:-1]
    df = df[cols]
    print(df)
    df.to_csv(stn+' ('+str(site)+') '+str(metername[n])+' - Master Extract Monthly.txt',sep='\t',index=False) """


 