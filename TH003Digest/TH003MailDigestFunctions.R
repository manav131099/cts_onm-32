rm(list = ls())
source('/home/admin/CODE/common/math.R')
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(533,533,461.5,461.5)
TOTSOLAR = 0
source('/home/admin/CODE/TH003Digest/Invoicing.R')

timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

getGTIData = function(day)
{
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	path = paste("/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[TH-001C]/",yr,"/",yrmon,"/WMS/[TH-001C]-WMS-",day,".txt",sep="")
	path2 = paste("/home/admin/Dropbox/Gen 1 Data/[TH-003X]/",yr,"/",yrmon,"/Sensors/[TH-003X-M5] ",day,".txt",sep="")
	GTI = Tamb=Tmod=IrrSrc=NA
	if(file.exists(path))
	{
		dataread = read.table(path,header = T,sep="\t")
		if(nrow(dataread))
		{
			GTI = as.numeric(dataread[1,3])
			Tamb = as.numeric(dataread[1,4])
			Tmod = as.numeric(dataread[1,5])
			IrrSrc = 0
		}
	}
	if(file.exists(path2))
	{
		dataread = read.table(path2,header=T,sep="\t",stringsAsFactors=F)
		dataread = as.numeric(dataread[,2])
		dataread = dataread[complete.cases(dataread)]
		if(length(dataread))
		{
			GTI = round(sum(dataread)/60000,2)
			IrrSrc = 1
		}
	}
	return(c(GTI,Tamb,Tmod,IrrSrc))
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1)
	 		TIMESTAMPSALARM <<- NULL
		else if(METERCALLED == 2)
			TIMESTAMPSALARM2 <<- NULL
		else if(METERCALLED == 3)
			TIMESTAMPALARM3 <<- NULL
		else if(METERCALLED == 4)
			TIMESTAMPALARM4 <<- NULL
	}
	print(paste('IN 2G',filepath))
	date = Eac1 = DA = totrowsmissing = Yld1  = Eac2 = RATIO = NA 
	lasttime = lastread = Yld2 = Gmod = Tamb =GPy=PRPy= ExpAmt=ExpPerc=PR1=PR2=NA
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread) == "try-error")
	{
		
		df = data.frame(Date = date, Eac1 = as.numeric(Eac1),Eac2= as.numeric(Eac2),
              DA = DA,Downtime = totrowsmissing,Yld1=Yld1,Yld2=Yld2,PR1=PR1,PR2=PR2,GSi=GSi,RatioEac=RATIO,
							lasttime=lasttime,lastread=lastread,
							stringsAsFactors = F)
		return()
	}

	dataread2 = dataread
	colnoUse = 39
	{
	idxpac = 15
	dataread = dataread2[complete.cases(as.numeric(dataread2[,colnoUse])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),colnoUse])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),colnoUse]) - as.numeric(dataread[1,colnoUse]))/1000,2),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
		RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	}
	dataread = dataread2
	datareadcp = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[abs(as.numeric(dataread2[,idxpac])) < ltcutoff,]
	indextouse = METERCALLED
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1)
			{
				TIMESTAMPSALARM <<- as.character(dataread2[,1])
				indextouse = 1
			}
			else if(METERCALLED == 2)
			{
			 indextouse = 2
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])
			}
			else if(METERCALLED == 3)
			{
			 indextouse = 3
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])
			}
			else if(METERCALLED == 4)
			{
			 indextouse = 4
		 	 TIMESTAMPSALARM4 <<- as.character(dataread2[,1])
			}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
	Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
	Yld2 = format(round(as.numeric(Eac2)/INSTCAP[indextouse],2),nsmall=2)
	Gmod = getGTIData(date)
	Tamb = Gmod[2]
	IrrSrc = Gmod[4]
	if(is.finite(IrrSrc))
	{
		{
			if(IrrSrc == 0)
				IrrSrc = "TH-001C"
			else
				IrrSrc = "TH-003X"
		}
	}
	Gmod = Gmod[1]
	PR1 = round((as.numeric(Yld1) * 100 / Gmod),1)
	PR2 = round((as.numeric(Yld2) * 100 / Gmod),1)
	TOTSOLAR <<- as.numeric(Eac2)
	df = data.frame(Date = date, Eac1 = as.numeric(Eac1),Eac2= as.numeric(Eac2),
            DA = DA,Downtime = totrowsmissing,Yld1=Yld1,Yld2=Yld2,PR1=PR1,PR2=PR2,GSi=Gmod,RatioEac=RATIO,
						lasttime=lasttime,lastread=lastread, IrrSrc=IrrSrc,
						stringsAsFactors = F)
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,filepathm4,writefilepath,invoicingData)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread4 = read.table(filepathm4,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	
	dt = as.character(dataread2[,5])
	da = as.character(dataread2[,4])
	GSiTot = as.character(dataread2[,10])
	IrrSrc = as.character(dataread2[,14])

	Eac1M1 = as.character(dataread1[,2])
	Eac2M1 = as.character(dataread1[,3])
	Yld1M1 = as.character(dataread1[,6])
	Yld2M1 = as.character(dataread1[,7])
	PR1M1 = as.character(dataread1[,8])
	PR2M1 = as.character(dataread1[,9])
	LastTimeM1 = as.character(dataread1[,12])
	LastReadM1 = as.character(dataread1[,13])
	
	Eac1M2 = as.character(dataread2[,2])
	Eac2M2 = as.character(dataread2[,3])
	Yld1M2 = as.character(dataread2[,6])
	Yld2M2 = as.character(dataread2[,7])
	PR1M2 = as.character(dataread2[,8])
	PR2M2 = as.character(dataread2[,9])
	LastTimeM2 = as.character(dataread2[,12])
	LastReadM2 = as.character(dataread2[,13])
	
	Eac1M3 = as.character(dataread3[,2])
	Eac2M3 = as.character(dataread3[,3])
	Yld1M3 = as.character(dataread3[,6])
	Yld2M3 = as.character(dataread3[,7])
	PR1M3 = as.character(dataread3[,8])
	PR2M3 = as.character(dataread3[,9])
	LastTimeM3 = as.character(dataread3[,12])
	LastReadM3 = as.character(dataread3[,13])
	
	Eac1M4 = as.character(dataread4[,2])
	Eac2M4 = as.character(dataread4[,3])
	Yld1M4 = as.character(dataread4[,6])
	Yld2M4 = as.character(dataread4[,7])
	PR1M4 = as.character(dataread4[,8])
	PR2M4 = as.character(dataread4[,9])
	LastTimeM4 = as.character(dataread4[,12])
	LastReadM4 = as.character(dataread4[,13])

	cov1 = c(as.numeric(Yld1M1), as.numeric(Yld1M3))
	cov2 = c(as.numeric(Yld1M2), as.numeric(Yld1M4))
	sd1 = round(sdp(cov1),3)
	sd2 = round(sdp(cov2),3)
	cov1 = format(round(sd1*100/mean(cov1),1),nsmall=1)
	cov2 = format(round(sd2*100/mean(cov2),1),nsmall=1)
	sd1 = format(sd1,nsmall=3)
	sd2 = format(sd2,nsmall=3)
	
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	GSi=GSiTot,
	Eac1M1 = Eac1M1,
	Eac2M1 = Eac2M1,
	Yld1M1 = Yld1M1,
	Yld2M1 = Yld2M1,
	PR1M1 = PR1M1,
	PR2M1 = PR2M1,
	LastTimeM1 = LastTimeM1,
	LastReadM1 = LastReadM1,
	Eac1M2 = Eac1M2,
	Eac2M2 = Eac2M2,
	Yld1M2 = Yld1M2,
	Yld2M2 = Yld2M2,
	PR1M2 = PR1M2,
	PR2M2 = PR2M2,
	LastTimeM2 = LastTimeM2,
	LastReadM2 = LastReadM2,
	Eac1M3 = Eac1M3,
	Eac2M3 = Eac2M3,
	Yld1M3 = Yld1M3,
	Yld2M3 = Yld2M3,
	PR1M3 = PR1M3,
	PR2M3 = PR2M3,
	LastTimeM3 = LastTimeM3,
	LastReadM3 = LastReadM3,
	Eac1M4 = Eac1M4,
	Eac2M4 = Eac2M4,
	Yld1M4 = Yld1M4,
	Yld2M4 = Yld2M4,
	PR1M4= PR1M4,
	PR2M4 = PR2M4,
	LastTimeM4 = LastTimeM4,
	LastReadM4 = LastReadM4,
	PeakEac=as.numeric(invoicingData[,8]),
	OffPeakEac=as.numeric(invoicingData[,9]),
	PeakEacPerc = as.numeric(invoicingData[,10]),
	OffPeakEacPerc = as.numeric(invoicingData[,11]),
	PeakEac=as.numeric(invoicingData[,15]),
	OffPeakEac=as.numeric(invoicingData[,16]),
	PeakEacPerc = as.numeric(invoicingData[,17]),
	OffPeakEacPerc = as.numeric(invoicingData[,18]),
	Day = as.character(invoicingData[,20]),
	Holiday=as.character(invoicingData[,21]),
	SDInv =as.character(sd1),
	CovInv = as.character(cov1),
	SDChk = as.character(sd2),
	CovChk = as.character(cov2),
	IrrSrc = IrrSrc,
	stringsAsFactors=F)

  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

