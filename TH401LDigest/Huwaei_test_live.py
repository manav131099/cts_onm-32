
import requests
import json
import pandas as pd
from datetime import datetime, timezone, timedelta
import os
import shutil
import re
from datetime import date


today = date.today()
today = str(today)
year = today[0:4]
yrmon  = today[0:7]


flag = 0
def authenticate():
    url = 'https://sg5.fusionsolar.huawei.com/thirdData/login'
    payload = {
    "userName": "Cleantech_Solar",
    "systemCode": "Huawei@2021!"
    }
    # Adding empty header as parameters are being sent in payload
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return(r.cookies["XSRF-TOKEN"])


def get_data_grid():
    url = 'https://sg5.fusionsolar.huawei.com/thirdData/getDevRealKpi'
    payload = {
    "devIds" : "142744976198955",
    "devTypeId":"17"
    }
    token = authenticate()
    #print(token)
    headers = {'content-type': 'application/json','XSRF-TOKEN': token}
    r = requests.post(url, data=json.dumps(payload), headers = headers)
    return(r.json())
    

def get_data_inverter():
    url = 'https://sg5.fusionsolar.huawei.com/thirdData/getDevRealKpi'
    payload = {
    "devIds" : "120144984726260",
    "devTypeId":"1"
    }
    token = authenticate()
    print(token)
    headers = {'content-type': 'application/json','XSRF-TOKEN': token}
    r = requests.post(url, data=json.dumps(payload), headers = headers)
    print(r)
    return(r.json())
       

def clean_grid_data():
        result = get_data_grid()
        #print(result)
        ts = (result['params']['currentTime'])
        ts = int(ts/1000)
        tz = timezone(+timedelta(hours=7))
        ts = (datetime.fromtimestamp(ts, tz).strftime('%Y-%m-%d %H:%M:%S'))
        t = str(ts)
        ts1 = t[0:16]
        t = t[0:10]
        code = '[TH-401L]-M1-'
        final = str(code+t+'.txt')
        d = ((result['data'][0]['dataItemMap']))
        data_list = list(d.values())
        data_list.insert(0,ts1)
        path = '/home/admin/Dropbox/Huawei/[TH-401L]/'+year+'/'+yrmon+'/MFM-1'
        files = os.listdir("/home/admin/Dropbox/Huawei/[TH-401L]/"+year+"/"+yrmon+"/MFM-1")
        files = list(files)
        t1 = str(t)
        fullpath = os.path.join(path, final)

        if(t1 in str(files)):
            flag = 0
        else:
            flag = 1

        if(flag == 1):  
            with open(fullpath, 'a+') as file:
                file.write("Timestamp" + "\t" + "active_cap"+ "\t" + "power_factor" + "\t" + "a_i" + "\t" + "c_i"+"\t"+"b_i"+"\t"+"Reverse Reactive Valley" + "\t" + "Positive Reactive Peak"+ "\t" + "Reverse Reactive Peak" + "\t" + "Positive Active Peak" + "\t" + "Reverse Active Peak"+"\t"+"a_u"+"\t"+"Reactive power" + "\t" + "Total apparent power"+ "\t" + "c_u" + "\t" + "bc_u" + "\t" + "b_u"+"\t"+"Reverse reactive power"+"\t"+"Positive reactive top" + "\t" + "reverse_active_cap"+ "\t" + "active_power_b" + "\t" + "active_power_a" + "\t" + "positive_active_top"+"\t"+"reverse_reactive_cap"+"\t"+"Positive Active Power" + "\t" + "Positive Active Valley"+ "\t" + "Reverse Reactive Top" + "\t" + "Reverse Active Top" + "\t"+"Reverse Active Power"+"\t"+"reverse_power_a"+"\t"+"reverse_power_b" +"\t"+"Forward Reactive cap"+"\t" +"Reverse Active Valley"+"\t"+"Reactive_power_c"+"\t"+"Active_power" + "\t" + "ca_u"+ "\t" + "ab_u" + "\t" + "Positive Reactive Power" + "\t" + "Active_power_c"+"\t"+"Grid Frequency"+"\t"+"Positive_reactive_valley" +"\n")
                file.write('\n') 
                for i in data_list:
                    norm = str(i)
                    file.write(norm + '\t')
        if(flag == 0): 
            with open(fullpath, 'a') as file:
                file.write('\n') 
                for i in data_list:
                    norm = str(i)
                    file.write(norm + '\t')
        
        
        
def clean_inv_data():
        result = get_data_inverter()
        print(result)
        ts = (result['params']['currentTime'])
        ts = int(ts/1000)
        tz = timezone(+timedelta(hours=7))
        ts = (datetime.fromtimestamp(ts, tz).strftime('%Y-%m-%d %H:%M:%S'))
        t = str(ts)
        ts1 = t[0:16]
        t = t[0:10]
        code = '[TH-401L]-I1-'
        final = str(code+t+'.txt')
        d = ((result['data'][0]['dataItemMap']))
        data_list = list(d.values())
        data_list.insert(0,ts1)
        path = '/home/admin/Dropbox/Huawei/[TH-401L]/2021/2021-06/INVERTER-1'
        files = os.listdir("/home/admin/Dropbox/Huawei/[TH-401L]/2021/2021-06/INVERTER-1")
        files = list(files)
        t1 = str(t)
        fullpath = os.path.join(path, final)

        if(t1 in str(files)):
            flag = 0
        else:
            flag = 1

        if(flag == 1):  
            with open(fullpath, 'a+') as file:
                file.write("Timestamp"+"\t"+"pv2_u"+"\t"+"pv4_u"+"\t"+"pv22_i"+"\t"+"pv6_u"+"\t"+"power_factor"+"\t"+"mppt_total_cap"+"\t"+
                "pv24_i"+"\t"+
                "pv8_u"+"\t"+
                "open_time"+"\t"+
                "pv22_u"+"\t"+
                "a_i"+"\t"+
                "pv24_u"+"\t"+
                "mppt_9_cap"+"\t"+
                "c_i"+"\t"+
                "pv20_u"+"\t"+
                "pv19_u"+"\t"+
                "pv15_u"+"\t"+
                "pv17_u"+"\t"+
                "reactive_power"+"\t"+
                "a_u"+"\t"+
                "c_u"+"\t"+
                "mppt_8_cap"+"\t"+
                "pv20_i"+"\t"+
                "pv15_i"+"\t"+
                "pv17_i"+"\t"+
                "efficiency"+"\t"+
                "pv11_i"+"\t"+
                "pv13_i"+"\t"+
                "pv11_u"+"\t"+
                "pv13_u"+"\t"+
                "mppt_power"+"\t"+
                "run_state"+"\t"+
                "close_time"+"\t"+
                "pv19_i"+"\t"+
                "mppt_7_cap"+"\t"+
                "mppt_5_cap"+"\t"+
                "pv2_i"+"\t"+
                "pv4_i"+"\t"+
                "active_power"+"\t"+
                "pv6_i"+"\t"+
                "pv8_i"+"\t"+
                "mppt_6_cap"+"\t"+
                "pv1_u"+"\t"+
                "pv3_u"+"\t"+
                "pv23_i"+"\t"+
                "pv5_u"+"\t"+
                "pv7_u"+"\t"+
                "pv23_u"+"\t"+
                "pv9_u"+"\t"+
                "inverter_state"+"\t"+
                "total_cap"+"\t"+
                "mppt_3_cap"+"\t"+
                "b_i"+"\t"+
                "pv21_u"+"\t"+
                "mppt_10_cap"+"\t"+
                "pv16_u"+"\t"+
                "pv18_u"+"\t"+
                "temperature"+"\t"+
                "b_u"+"\t"+
                "bc_u"+"\t"+
                "pv21_i"+"\t"+
                "elec_freq"+"\t"+
                "mppt_4_cap"+"\t"+
                "pv16_i"+"\t"+
                "pv18_i"+"\t"+
                "day_cap"+"\t"+
                "pv12_i"+"\t"+
                "pv14_i"+"\t"+
                "pv12_u"+"\t"+
                "mppt_1_cap"+"\t"+
                "pv14_u"+"\t"+
                "pv10_u"+"\t"+
                "pv1_i"+"\t"+
                "pv3_i"+"\t"+
                "mppt_2_cap"+"\t"+
                "pv5_i"+"\t"+
                "ca_u"+"\t"+
                "ab_u"+"\t"+
                "pv10_i"+"\t"+
                "pv7_i"+"\t"+
                "pv9_i"+"\n")
                file.write('\n') 
                for i in data_list:
                    norm = str(i)
                    file.write(norm + '\t')
         
        if(flag == 0): 
            with open(fullpath, 'a') as file:
                file.write('\n') 
                for i in data_list:
                    norm = str(i)
                    file.write(norm + '\t')
           

        
clean_grid_data()
          