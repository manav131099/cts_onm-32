import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import logging
import datetime as DT

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')



stn = '[TH-401L]'                                             #Enter station code for digest
irr_stn = '[TH-031L]'                                                #NA if WMS is present, NotRequired if to run without WMS
wms_name = 'WMS_5_Pyranometer'                                               #NA if WMS is present, NotRequired if to run without WMS
distance = '1.4'                                                #0 if self WMS is present
components = ['MFM-1', 'PLANT-1', 'WMS-1']                                 #Enter self WMS name if present else empty
meters = ['MFM_1_PV Meter']                                     #Enter MFM Folder Name
meters_mapped = {'MFM_1_PV Meter' : ['MFM_1', 100.86]}                  #Enter MFM foldername, MFM Display name, MFM capacity 
inverters = ['INVERTER_1']                                    #Enter Inverter names
inv_mapped = {'INVERTER_1' : 100.86}                            #Enter Inverter names and Capacities
# user = 'admin'                                              #Enter your username
recipients = ['om-it-digest@cleantechsolar.com','operationsTH@cleantechsolar.com']
Site_Name = 'Shell NTI Kanchanapisek -CEoFS'
Location = 'Bangkok, Thailand'
brands = "Module Brand / Model / Nos: Trina / TSM-DEG15MC.20(II) / 410Wp / 246" + "\n\n" + "Inverter Brand / Model / Nos: HUAWEI / SUN2000-100KTL / 1" + "\n\n"
cod_date = 'TBC'                                           #Enter COD if available else TBC   
budget_pr=71
rate=0.008
inv_limit=5
NO_MFM = len(meters)
capacity = sum([i[1] for i in meters_mapped.values()])





path_read_inv_meter = '/home/admin/Dropbox/Huawei/' + stn
path_read_plant = '/home/anusha/Dropbox/Huawei/' + stn
startpath = '/home/anusha/Start/MasterMail/'

# #this is for testing
# startpath = '/home/amogh/Start/'



if(os.path.exists(startpath + stn[1:8] + "Huawei_test_live_Mail.txt")):
  print('Exists')
else:
  with open(startpath + stn[1:8] + "Huawei_test_live_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "Huawei_test_live_Mail.txt") as f:
  startdate = f.readlines()[1].strip()

print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

def calc_availability(df):
    da = round((df.shape[0]/1440)*100,2)
    da_sun = df.copy()
    da_sun.index = pd.to_datetime(da_sun.index)
    da_sun = round((len(da_sun.loc[(7<=da_sun.index.hour) & (19>da_sun.index.hour)])/(12*60))*100,2)
    return da, da_sun

def process(date, stn):
    file_path = []
    digest_body = ''
    digest_body += site_info(date, Site_Name, Location, stn[1:7], capacity, NO_MFM, cod_date)
    dig_plant = ''
    dig_meter = ''
    dig_inv = ''
    #If WMS is not present

    for i in components:
        
        da, da_sun = "NA", "NA"
            
        if('PLANT' in i):
            file_name = 'P'+i.split('-')[-1]
            try:
                df = pd.read_csv(path_read_plant+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt', sep = "\t")
                da, da_sun = calc_availability(df)
            except:
                print("could not read file at {path}".format(path =path_read_plant+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt' ))
            dig_plant += '------------------------------\n' + i + '\n------------------------------\n\nDA [%]: ' + str(da) + '\nDA (Sun Hours) [%]: ' + str(da_sun) + '\n\n'
            file_path.append(path_read_plant+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt')
        elif('MFM' in i):
            file_name = 'M'+i.split('-')[-1]
            try:
                df = pd.read_csv(path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt', sep = "\t")
                da, da_sun = calc_availability(df)
            except:
                print("could not read file at {path}".format(path =path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt' ))
            dig_meter += '------------------------------\n' + i + '\n------------------------------\n\nDA [%]: ' + str(da) + '\nDA (Sun Hours) [%]: ' + str(da_sun) + '\n\n'
            file_path.append(path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt')
        elif('WMS' in i):
            file_name = 'WMS'+i.split('-')[-1]
            try:
                df = pd.read_csv(path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt', sep = "\t")
                da, da_sun = calc_availability(df)
            except:
                print("could not read file at {path}".format(path =path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt' ))
            dig_inv += '------------------------------\n' + i + '\n------------------------------\n\nDA [%]: ' + str(da) + '\nDA (Sun Hours) [%]: ' + str(da_sun) + '\n\n'
            file_path.append(path_read_inv_meter+f'/{date[:4]}/{date[:7]}/{i}/{stn}-{file_name}-{date}.txt')
        
    digest_body += dig_plant + dig_meter + dig_inv
        
        
    

    return [digest_body, file_path]


def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "SF API Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

if __name__ == '__main__':
    print('Historical Started!')
    startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
    while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
        print('Historical Processing!')
        processed_data = process(str(startdate)[:10], stn)
        if(bot_type == 'Mail'):
            processed_data[1] = processed_data[1]
            send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
        startdate = startdate + datetime.timedelta(days = 1)
    startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
    with open(startpath + stn[1:8] + "Huawei_test_live_Mail.txt", "w") as file:
        timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
        file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
    print('Historical Done!')

    while(1):
        try:
            date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
            timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
            with open(startpath + stn[1:8] + "Huawei_test_live_Bot.txt", "w") as file:
                file.write(timenow)   
            processed_data = process(date, stn)
            print('Done Processing')
            if(datetime.datetime.now(tz).hour == 1 and (datetime.datetime.now(tz).date()-startdate.date()).days>1 ):
                print('Sending')
                date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
                processed_data = process(date_yesterday, stn)
                processed_data[1] = processed_data[1]
                send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
                with open(startpath + stn[1:8] + "Huawei_test_live_Mail.txt", "w") as file:
                    file.write(timenow + "\n" + date_yesterday)   
                startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
            print('Sleeping')
        except:
            print('error')
            logging.exception('msg')
        time.sleep(300)

