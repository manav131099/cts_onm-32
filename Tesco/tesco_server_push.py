import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pysftp

tz = pytz.timezone('Asia/Bangkok')
path='/home/admin/Dropbox/Gen 1 Data/'
path_write='/home/admin/Dropbox/Customer/'
path_write_2='/home/admin/Dropbox/Customer/TESCO_CSV/'
cols=['TotWhNet_max','TotWhImp_max']
station={'[TH-010L]': 1088, '[TH-011L]': 5034, '[TH-012L]': 1099, '[TH-013L]': 5010, '[TH-014L]': 1059, '[TH-015L]': 1012, '[TH-016L]': 5027, '[TH-017L]': 1042, '[TH-018L]': 1067, '[TH-019L]': 5518, '[TH-020L]': 1007, '[TH-021L]': 1502, '[TH-022L]': 1501, '[TH-023L]': 1096, '[TH-024L]': 5002, '[TH-025L]': 5003, '[TH-026L]': 5007, '[TH-027L]': 5026, '[TH-028L]': 5512,'[TH-029L]': 'DC6', '[TH-030L]': 1090, '[TH-031L]': 1045, '[TH-032L]': 1021, '[TH-033L]': 1052, '[TH-034L]': 1124, '[TH-035L]': 1113, '[TH-036L]': 5020, '[TH-037L]': 5031, '[TH-038L]': 1140, '[TH-039L]': 1138, '[TH-040L]': 1110, '[TH-041L]': 5006, '[TH-042L]': 5028, '[TH-043L]': 5017, '[TH-044L]': 5019, '[TH-060L]': 1017, '[TH-061L]': 1074, '[TH-062L]': 1002, '[TH-063L]': 1016, '[TH-064L]': 1030, '[TH-065L]': 1058, '[TH-066L]': 5013}
def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return a
    else:
        os.makedirs(path)
        return a
chkdir(path_write_2)
def process(date_now,station,type):
    if(type==0):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
    elif(type==1):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV Meter/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_Grid MFM/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
    elif(type==2):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
    elif(type==3):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid meter/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
    elif(type==4):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV meter/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
    elif(type==5):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-1/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-2/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
        df_e=df_e[['ts',cols[0]]]
        df_g=df_g[['ts',cols[0]]]
        df_e[cols[0]]=df_e[cols[0]]+df_g[cols[0]]
        df_e=df_e.rename(columns = {'ts':'Date'})
        return df_e
    elif(type==6):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
    elif(type==7):
        df_e=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-01/'+station+'-MFM1-'+date_now+'.txt',sep='\t')
        df_g=pd.read_csv(path+station+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-02/'+station+'-MFM2-'+date_now+'.txt',sep='\t')
        df_e=df_e[['ts',cols[0]]]
        df_g=df_g[['ts',cols[0]]]
        df_e[cols[0]]=df_e[cols[0]]+df_g[cols[0]]
        df_e=df_e.rename(columns = {'ts':'Date'})
        return df_e
    df_e=df_e[['ts',cols[0]]]
    df_g=df_g[['ts',cols[1]]]
    df_j=pd.merge(df_e, df_g, on=['ts'],sort=False,how='left')
    df_j['Solar'] = df_j[cols[0]].diff(1)
    df_j['Difference_Grid'] = df_j[cols[1]].diff(1)
    df_j=df_j.rename(columns = {'ts':'Date'})
    return df_j

def upload(station_path):
    df_upload=pd.DataFrame(columns=['Date']+cols+['Solar','Difference_Grid'])
    for i in range(30,0,-1):
        date_now=datetime.datetime.now(tz)+datetime.timedelta(days=-i)
        date_now=date_now.strftime('%Y-%m-%d')
        if(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,0)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif((os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV Meter/'+station_path+'-MFM2-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_Grid MFM/'+station_path+'-MFM1-'+date_now+'.txt'))):
            df_j=process(date_now,station_path,1)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,2)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid meter/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,3)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV meter/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,4)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-1/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-2/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,5)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,6)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000        
        elif(os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-01/'+station_path+'-MFM1-'+date_now+'.txt') and os.path.exists(path+station_path+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-02/'+station_path+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,station_path,7)
            df_j=df_j.iloc[::3, :]
            df_upload=df_upload.append(df_j)
            df_upload_new=df_upload[['Date']].copy()
            df_upload_new['Solar'] = df_upload[cols[0]].diff(1)/1000    
    date_now=date_now.replace('-', '')
    df_upload_new.to_csv(path_write_2+'SOLAR_'+str(station[station_path])+'_'+date_now+'.csv',index=False)
    return date_now


def connect_server(date_now):#To send all the files in one go.
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    with pysftp.Connection('203.154.74.118', username='pvsolar', password='go3D8e#4S', cnopts=cnopts) as s:
        print(s.listdir())
        for i in station:
            if os.path.exists(path_write_2+'SOLAR_'+str(station[i])+'_'+date_now+'.csv'):
                s.put(path_write_2+'SOLAR_'+str(station[i])+'_'+date_now+'.csv')
                print('done')
            if os.path.exists(path_write_2+'SOLAR_'+str(station[i])+'_'+date_now+'.csv'):
                os.remove(path_write_2+'SOLAR_'+str(station[i])+'_'+date_now+'.csv')
                print('Removed!')
            else:
                print("The file does not exist")

while(1):
    date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')
    print(date_now)
    for i in station:
        print(i)
        chkdir(path_write+'/'+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7])
        if(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,0)
            print(0)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV Meter/'+i+'-MFM2-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_Grid MFM/'+i+'-MFM1-'+date_now+'.txt')):
            df_j=process(date_now,i,1)
            print(1)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,2)
            print(2)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid meter/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,3)
            print(3)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV meter/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid Meter/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,4)
            print(4)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-1/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-2/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,5)
            print(5)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_Grid MFM/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,6)
            print(6)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        elif(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV MFM-01/'+i+'-MFM1-'+date_now+'.txt') and os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_2_PV MFM-02/'+i+'-MFM2-'+date_now+'.txt')):
            df_j=process(date_now,i,7)
            print(7)
            df_j.to_csv(path_write+i[0:4]+'9'+i[4:]+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+i[0:4]+'9'+i[4:]+' '+date_now+'.txt',sep='\t',index=False)
            d=upload(i)
        else:
            print('Waiting!')
    if(datetime.datetime.now(tz).hour==3):
        connect_server(d)
        print('Uploaded!')
    time.sleep(3600)