source('/home/admin/CODE/common/aggregate.R')

registerMeterList("VN-001S",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 36 #Column no for DA
aggColTemplate[3] = 30 #column for LastRead
aggColTemplate[4] = 31 #column for LastTime
aggColTemplate[5] = 28 #column for Eac-1
aggColTemplate[6] = 29 #column for Eac-2
aggColTemplate[7] = 37 #column for Yld-1
aggColTemplate[8] = 25 #column for Yld-2
aggColTemplate[9] = 38 #column for PR-1
aggColTemplate[10] = 26 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("VN-001S","",aggNameTemplate,aggColTemplate)
