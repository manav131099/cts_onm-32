from flask import Flask, render_template, request, Response, jsonify, url_for, send_file, make_response
import requests
import pandas as pd
import itertools
from numpy.random import randint
import datetime
import re
import ast
import pandas as pd
import logging
import pandas as pd
import numpy as np
import json
import werkzeug._internal
import os
import pyodbc
from io import StringIO
from io import BytesIO
import sys
sys.path.insert(1, '/home/pranav/ctsfork/NewArchitecture/')
from database_operations import *
def demi_logger(type, message,*args,**kwargs):
    pass

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Users]", cnxn)
df_users = pd.DataFrame(SQL_Query, columns=['User_Id','Username','Password'])
users=pd.Series(df_users['User_Id'].values,index=df_users['Password']).to_dict()
cnxn.close()

def check_float(n):
    try:
        float(n)
        return True
    except:
        print(n)
        return False
        
# df_tags
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('Web_Interface_V3.html')


@app.route('/get_data',methods=['POST'])
def get_data():
    info = request.get_json(force=True)
    key=info['Key']
    if(key in users):
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        site_df = get_sites('All',cnxn)
        stn=info['Id']
        date=info['Date']
        if((site_df['SiteCode']==stn).any()):
            site_id = get_site_id(stn,cnxn)
            template_df = get_processed_template(date, site_id, cnxn)
            data_df = get_processed_data(date, site_id, cnxn)
            data_df = pd.merge(template_df, data_df,  how='left', left_on=['ComponentId','ParameterId'], right_on = ['ComponentId','ParameterId'], suffixes=('', '_dup'))
            data_df = data_df[((data_df['ComponentType'] == 'MFM' )|( data_df['ComponentType'] == 'WMSIRR' ))]
            data_df = data_df[((data_df['ParameterId'] == 5 )|( data_df['ParameterId'] == 2 )| (data_df['ParameterId'] == 10))]
            data_df['ComponentParameter'] = data_df['ComponentName'] +' '+data_df['CSName'] +'_' +data_df['ComponentId'].astype(str)+'_'+data_df['ParameterId'].astype(str) 
            if data_df.empty:
                data_df = get_processed_template(date, site_id, cnxn)
                data_df['ComponentParameter'] = data_df['ComponentName'] +' '+ data_df['CSName'] +'_' +data_df['ComponentId'].astype(str)+'_'+data_df['ParameterId'].astype(str)
                data_df['Value'] = 0
                data_df = data_df[((data_df['ComponentType'] == 'MFM' )|( data_df['ComponentType'] == 'WMSIRR' ))]
                data_df = data_df[((data_df['ParameterId'] == 5 )|( data_df['ParameterId'] == 2 )| (data_df['ParameterId'] == 10))]
            for index,row in data_df.iterrows():
                line = pd.DataFrame({"Date": date, "ComponentName": row['ComponentName'],"CSName":row['CSName']+ "-Flag","Value": 0,"ComponentType": row['ComponentType'],"ComponentId": row['ComponentId'],"ParameterId": row['ParameterId'],"DigestOrder": row['DigestOrder'],'ComponentParameter':row['ComponentName'] +' '+row['CSName'] +'-Flag_' +str(row['ComponentId'])+'_'+str(row['ParameterId'])}, index=[0])
                data_df = data_df.append(line)
            data_df = data_df.sort_values(by=['ComponentType','ComponentId','ParameterId'], ascending=[False,  True, False])
            data_df=data_df.fillna(0)
            df_temp = data_df.iloc[0,:]
            df_temp['ComponentParameter'] = 'MasterFlag'
            df_temp['Value'] = users[key]
            data_df = data_df.append(df_temp, ignore_index = True)
            print(data_df)
            cnxn.close()
            print(json.dumps(pd.Series(data_df['Value'].values,index=data_df['ComponentParameter']).to_dict()))
            return Response(json.dumps(pd.Series(data_df['Value'].values,index=data_df['ComponentParameter']).to_dict()),status=200)
        else:
            cnxn.close()
            return jsonify(['Site not automated']) 
    else:
        return jsonify(['Incorrect Secret Key'])

@app.route('/update_data',methods=['POST'])
def update_data():
    data = request.form.to_dict()
    print(data)
    if data['Date']=='' or data['Code']=='':
        return jsonify(['Failed'])
    df=pd.DataFrame(data,index=[0])
    print(df)
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    date = df.loc[0,'Date']
    site_id = get_site_id(df.loc[0,'Code'],cnxn)
    master_flag = df.loc[0,'MasterFlag']
    for i in df.columns.tolist()[2:-1]:
        if('-Flag' in i):
            continue
        value = df.loc[0,i]
        component_id = i.split('_')[1]
        parameter_id = i.split('_')[2]
        bypassflag = df.loc[0,i.split('_')[0]+'-Flag'+'_'+component_id+'_'+parameter_id]
        print(date,value,parameter_id,component_id,site_id,bypassflag,master_flag)
        upsert_processed(date,value,parameter_id,component_id,site_id, cnxn,bypassflag,master_flag)
    cnxn.close()
    return jsonify(['Success'])

@app.route('/text_recipients',methods=['GET'])
def text_recipients():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Text_Recipients]", cnxn)
    df_users = pd.DataFrame(SQL_Query, columns=['Name','Site','Status','Country'])
    SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Stations]", cnxn)
    df_sites = pd.DataFrame(SQL_Query, columns=['Station_Name'])
    df_sites['Station_Name']=df_sites['Station_Name'].str.strip()
    df_users['Name']=df_users['Name'].str.strip()
    df_users['Country']=df_users['Country'].str.strip()
    names=sorted(df_users['Name'].unique())

    df_checkbox=df_users.loc[df_users['Site'].str.strip()=='All',['Name','Country','Status']]
    checkbox_values = df_checkbox.to_json(orient="records",date_format='iso')
    checkbox_values = json.loads(checkbox_values )
    sites=sorted(df_sites['Station_Name'].tolist())
    return render_template('Recipients_Interface.html',names=names,sites=sites,checkbox=checkbox_values)


@app.route('/enable_alarm',methods=['POST'])
def enable_alarm():
    data = request.form.to_dict()
    if(data['Key']=='CTS&*(789'):
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Text_Recipients]", cnxn)
        df_users = pd.DataFrame(SQL_Query, columns=['Name','Site','Status','Country','Number'])
        df_users['Name']=df_users['Name'].str.strip()
        print(data)
        cursor = cnxn.cursor()
        for i in data:
            if(i=='Key'):
                continue
            print(i)
            name=i.split("_")[0].strip()
            print(name)
            number=df_users.loc[df_users['Name']==name,'Number'].values[0]
            site=data[i]
            if(site!='All'):
                country=site[0:2]
            else:
                country=i.split("_")[1]
            with cursor.execute("UPDATE [dbo].[Text_Recipients] SET [Status] = 1 where [Name] = '{}' AND [Site]='{}' AND [Country]='{}' if @@ROWCOUNT = 0 INSERT into [dbo].[Text_Recipients](Name, Number, Site, Country, Status) values('{}', {}, '{}','{}', {}) ".format(str(name),str(site),str(country),str(name), str(number),str(site),str(country),str(1))):
                pass
        cnxn.commit()
        cnxn.close()
        return jsonify(['Enabled'])
    else:
        return jsonify(['Incorrect Key'])

@app.route('/disable_alarm',methods=['POST'])
def disable_alarm():
    data = request.form.to_dict()
    print(data)
    if(data['Key']=='CTS&*(789'):
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Text_Recipients]", cnxn)
        df_users = pd.DataFrame(SQL_Query, columns=['Name','Site','Status','Country','Number'])
        df_users['Name']=df_users['Name'].str.strip()
        cursor = cnxn.cursor()
        for i in data:
            if(i=='Key'):
                continue
            name=i.split("_")[0].strip()
            print(name)
            number=df_users.loc[df_users['Name']==name,'Number'].values[0]
            site=data[i]
            if(site!='All'):
                country=site[0:2]
            else:
                country=i.split("_")[1]
            with cursor.execute("UPDATE [dbo].[Text_Recipients] SET [Status] = 0 where [Name] = '{}' AND [Site]='{}' AND [Country]='{}' if @@ROWCOUNT = 0 INSERT into [dbo].[Text_Recipients](Name, Number, Site, Country, Status) values('{}', {}, '{}','{}', {}) ".format(str(name),str(site),str(country),str(name), str(number),str(site),str(country),str(0))):
                pass
        cnxn.commit()
        cnxn.close()
        return jsonify(['Disabled'])
    else:
        return jsonify(['Incorrect Key'])

@app.route('/extract_interface',methods=['GET'])
def extract_interface():
    return render_template('Extracts_Interface.html')

@app.route('/extract_download/<site>/<from_date>/<to_date>/',methods=['GET'])
def extract_download(site,from_date,to_date):
    server = 'cleantechsolar.database.windows.net'
    database = 'Cleantech Meter Readings'
    username = 'RohanKN'
    password = 'R@h@nKN1'
    driver= '{ODBC Driver 17 for SQL Server}'
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    site = 'TH-010'
    SQL_Query = pd.read_sql_query('''SELECT  [ComponentId],[ComponentName],[ComponentType] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\') ORDER BY [ComponentType] DESC,[ComponentName]', cnxn)
    df_components = pd.DataFrame(SQL_Query)
    comps = dict(zip(df_components['ComponentId'],df_components['ComponentName']))
    print(comps)
    SQL_Query = pd.read_sql_query("SELECT * FROM ["+site+"].[RawData] WITH (INDEX(cci_"+site.replace('-','')+")) WHERE [Timestamp] BETWEEN '"+from_date+"' AND '"+to_date+"' ", cnxn)
    df = pd.DataFrame(SQL_Query)
    uniques = df['ComponentId'].unique()
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    for comp_id in uniques:
        df[df['ComponentId'] == comp_id].to_excel(writer, index=None, sheet_name=str(comps[comp_id]))
    writer.close()
    output.seek(0)
    return send_file(output, attachment_filename=site+"_"+from_date+"_"+to_date+".xlsx", as_attachment=True)

@app.route('/download',methods=['POST'])
def download():
    info = request.get_json(force=True)
    key=info['Key']
    print(key)
    if(key=='CTS&*(789'):
        return jsonify(['Correct Secret Key'])
    else:
        return jsonify(['Incorrect Secret Key'])

if __name__ == '__main__':
    werkzeug._internal._log = demi_logger
    app.run(host = '0.0.0.0',port=5000,debug=True)