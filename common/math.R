sdp = function(x)
{
	if(is.na(x) || is.null(x))
		return (NA)
	return(sqrt(var(x)*(length(x)-1)/length(x)))
}
